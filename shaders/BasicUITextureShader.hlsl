/********************************************************************
*
*	SHADER		:: UITexture
*	DESCRIPTION	:: Copies a texture onto a 2D plane.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 09
*
********************************************************************/

/*******************************************************************/
cbuffer GlobalVars
{
	float4x4 g_WVP;
	float4 g_colour;
	float g_imageHeight;
	float g_imageWidth;
};
/*******************************************************************/

/*******************************************************************/
struct VSInput
{
	float3 pos:POSITION;
	float4 colour:COLOUR0;
	float2 tex:TEXCOORD;
};

struct PSInput
{
	float4 pos:SV_Position;
	float4 colour:COLOUR0;
	float2 tex:TEXCOORD;
};

struct PSOutput
{
	float4 colour:SV_Target;
};

Texture2D g_texture;
SamplerState g_sampler;

/*******************************************************************/
void VSMain(const VSInput input, out PSInput output)
{
	// Multiply the vertex by the world view projection matrix.
	float4 p = { input.pos, 1.0f };
	output.pos = mul( p, g_WVP);

	// Calculate the position on the texture.
	float2 tex_pos;
	tex_pos.x = ( input.pos.x + 0.5f);
	tex_pos.y = (-input.pos.y + 0.5f);
	output.tex = tex_pos;

	// Transfer the colour without modifying it.
	output.colour = input.colour;
}

void PSMain(const PSInput input, out PSOutput output)
{
	// Get the colour of the image at specified location on the texture.
	float4 Tex1;
	Tex1 = g_texture.Sample( g_sampler, input.tex);

	// Set the colour of the pixel.
	output.colour = Tex1 * g_colour;
}