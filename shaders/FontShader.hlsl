//-----------------------------------------------
//// SHADER :- FontShader
//// DESCRIPTION :-
//	Used for rendering text for the D3DFont class.
//// CREATED BY :- Edward Willoughby
//// DATE :- 2014 / 07 / 28
//-----------------------------------------------

//-----------------------------------------------
cbuffer GlobalVars
{
	float4x4 g_WVP;
	//float4 g_colour;
}
//-----------------------------------------------

//-----------------------------------------------
struct VSInput
{
	float3 pos		: POSITION;
	float4 colour	: COLOUR;
	float2 tex		: TEXCOORD;
};

struct PSInput
{
	float4 pos		: SV_POSITION;
	float4 colour	: COLOUR;
	float2 tex		: TEXCOORD;
};

struct PSOutput
{
	float4 colour	: SV_target;
};
//-----------------------------------------------

Texture2D g_texture;

SamplerState g_sampler;

//-----------------------------------------------
void VSMain( const VSInput input, out PSInput output)
{
	// transform to homogenous clip space
	float4 p = { input.pos, 1.0f };
	output.pos = mul( p, g_WVP);

	output.colour = input.colour;

	output.tex = input.tex;
}

void PSMain( const PSInput input, out PSOutput output)
{
	output.colour = input.colour;

	output.colour = g_texture.Sample( g_sampler, input.tex) * input.colour;
}