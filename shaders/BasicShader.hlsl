/********************************************************************
*
*	SHADER		:: BasicShader
*	DESCRIPTION	:: A simple shader that does no manipulation (save for converting the verticies to
*					world space). This is for simple rendering and for testing that models work
*					before applying more advanced techniques to them.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 06 / 22
*
********************************************************************/

/*******************************************************************/
cbuffer GlobalVars
{
	float4x4 g_WVP;
	float4 g_colour;

	// Lighting.
	float4x4 g_W;
	float4x4 g_InvXposeW;
	float4 g_lightDirections[MAX_NUM_LIGHTS];
	float4 g_lightPositions[MAX_NUM_LIGHTS];
	float3 g_lightColours[MAX_NUM_LIGHTS];
	float4 g_lightAttenuations[MAX_NUM_LIGHTS];
	float4 g_lightSpots[MAX_NUM_LIGHTS];
	int g_numLights;
}
/*******************************************************************/

/*******************************************************************/
struct VSInput
{
	float3 pos		: POSITION;
    float4 colour	: COLOUR0;
	float3 normal	: NORMAL;
};


struct PSInput
{
	float4 pos		: SV_Position;
	float4 colour	: COLOUR0;
};


struct PSOutput
{
    float4 colour	: SV_Target;
};


/*******************************************************************/
float4 GetLightingColour( float3 worldPos, float3 N)
{
	float4 lightingColour = float4( 0, 0, 0, 1);

	for( int i = 0; i < g_numLights; ++i) {
		float3 D = g_lightPositions[i].w * (g_lightPositions[i].xyz - worldPos);
		float dotDD = dot( D, D);

		if( dotDD > g_lightAttenuations[i].w)
			continue;

		float atten = 1.0 / (g_lightAttenuations[i].x + g_lightAttenuations[i].y * length( D) + g_lightAttenuations[i].z * dot( D, D));

		float3 L = g_lightDirections[i].xyz;
		float dotNL = g_lightDirections[i].w * saturate( dot( N, L));

		float rho = 0.0;
		if( dotDD > 0.0)
			rho = dot( L, normalize( D));

		float spot;
		if( rho > g_lightSpots[i].y)
			spot = 1.0;
		else if( rho < g_lightSpots[i].x)
			spot = 0.0;
		else
			spot = pow(( rho - g_lightSpots[i].x) * g_lightSpots[i].z, g_lightSpots[i].w);

		float3 light = atten * spot * g_lightColours[i];
		if( g_lightDirections[i].w > 0.0f)
			light *= dotNL;
		else
			light *= saturate( dot( N, normalize( D)));

		lightingColour.xyz += light;
	}

	return lightingColour;
}

/*******************************************************************/
void VSMain( const VSInput input, out PSInput output)
{
	// transform to homogeneous clip space
	float4 p = { input.pos, 1.0f };
	output.pos = mul( p, g_WVP);

	float4 n = { input.normal, 1.0f };
	float3 N = mul( n, g_InvXposeW).xyz;
	N = normalize( N);

	float3 worldPos = mul( p, g_W).xyz;

	output.colour = GetLightingColour( worldPos, N) * input.colour * g_colour;
}

void PSMain( const PSInput input, out PSOutput output)
{
	output.colour = input.colour;
}