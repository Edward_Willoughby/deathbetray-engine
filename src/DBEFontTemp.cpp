/********************************************************************
*	Function definitions for the Font class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEFont.h"

#include <d3d11.h>

#include <stdint.h>

#include "DBEApp.h"
#include "DBEGraphicsHelpers.h"
#include "DBEShader.h"
#include "DBEUITexture.h"
#include "DBEVertexTypes.h"
#include "DBEUtilities.h"
/********************************************************************
* Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DirectX;

/// Maximum number of characters to render in one go.
static const s32 NUM_CHARS( 100);
/// Number of verticies in the vertex buffer.
static const s32 NUM_VERTS( NUM_CHARS * 4);
/// Default style to use if no style is provided.
static const Style DEFAULT_STYLE;

/**
* Holds the data about a character on the texture sheet.
*/
struct Glyph {
	DirectX::XMFLOAT2 size;
	DirectX::XMFLOAT2 texMini;
	DirectX::XMFLOAT2 texMaxi;
};



/**
* Constructor.
*/
DBE::Font::Font()
	: mp_glyphs( nullptr)
	, mp_texture( nullptr)
	, mp_textureView( nullptr)
	, mp_vertBuffer( nullptr)
	, mp_indexBuffer( nullptr)
{}

/**
* Destructor.
*/
DBE::Font::~Font() {
	SafeDeleteArray( mp_glyphs);

	ReleaseCOM( mp_textureView);
	ReleaseCOM( mp_texture);

	ReleaseCOM( mp_vertBuffer);
	ReleaseCOM( mp_indexBuffer);
}

/**
* Static function to create a font with custom information.
*/
DBE::Font* DBE::Font::CreateNewFont( const char* fontName /*= ""*/, s32 height /*= 0*/, u32 flags /*= 0*/) {
	HFONT hFont = nullptr;
	HBITMAP hBitmap = nullptr;

	HDC hDC = CreateCompatibleDC( nullptr);
	SetMapMode( hDC, MM_TEXT);
	SaveDC( hDC);

	D3DFont*					p_font( nullptr);
	Glyph*						p_glyphs( nullptr);
	ID3D11Texture2D*			p_texture( nullptr);
	ID3D11ShaderResourceView*	p_textureView( nullptr);
	ID3D11Buffer*				p_VB( nullptr);
	ID3D11Buffer*				p_IB( nullptr);
	D3DShader*					p_shader( nullptr);

	// Try to create font.
	hFont = Font::CreateGDIFont( hDC, fontName, height, flags);
	if( !hFont)
		goto done;

	SelectObject( hDC, hFont);

	// Decide on a reasonable size for the texture.
	s32 texWidth( 128), texHeight( 128);
	while( !D3DFont::PaintAlphabet( hDC, texWidth, texHeight, nullptr)) {
		texWidth *= 2;
		texHeight *= 2;

		// Exit if the font is too large.
		if( texWidth == 4096 || texHeight == 4096)
			goto done;
	}

	// Create GDI bitmap for the texture.
	BITMAPINFO bmi;
	memset( &bmi, 0, sizeof( bmi));
	bmi.bmiHeader.biSize = sizeof( bmi.bmiHeader);
	bmi.bmiHeader.biWidth = texWidth;
	bmi.bmiHeader.biHeight = -texHeight;  // -ve = top down
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biBitCount = 32;

	DWORD* p_DIBits;
	hBitmap = CreateDIBSection( hDC, &bmi, DIB_RGB_COLORS, (void**)&p_DIBits, nullptr, 0);
	if( !hBitmap)
		goto done;

	// Paint font into GDI bitmap and this time store off the texture coordinates for each glyph.
	p_glyphs = new Glyph[96];

	SelectObject( hDC, hBitmap);

	SetTextColor( hDC, RGB( 255, 255, 255));
	SetBkColor( hDC, RGB( 0, 0, 0));
	SetTextAlign( hDC, TA_TOP);

	Font::PaintAlphabet( hDC, texWidth, texHeight, p_glyphs);

	// Fix up bitmap data - the font shape should be in the alpha channel, and the RGB channels
	// should be all white.
	GdiFlush();

	for( s32 i( 0); i < texWidth * texHeight; ++i)
		p_DIBits[i] = ((p_DIBits[i] & 0xFF) << 24) | 0x00FFFFFF;

	// Create D3D texture for that.
	D3D11_TEXTURE2D_DESC td;
	td.Width = texWidth;
	td.Height = texHeight;
	td.MipLevels = 1;
	td.ArraySize = 1;
	td.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	td.Usage = D3D11_USAGE_IMMUTABLE;
	td.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	td.CPUAccessFlags = 0;
	td.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA srd;
	srd.pSysMem = p_DIBits;
	srd.SysMemPitch = texWidth * 4;
	srd.SysMemSlicePitch = 0;

	if( FAILED( GET_APP()->mp_D3DDevice->CreateTexture2D( &td, &srd, &p_texture)))
		goto done;

	// Create the index buffer.
	u16 ibData[NUM_CHARS * 6];
	for( s32 i( 0); i < NUM_CHARS; ++i) {
		ibData[i * 6 + 0] = u16( i * 4 + 0);
		ibData[i * 6 + 1] = u16( i * 4 + 1);
		ibData[i * 6 + 2] = u16( i * 4 + 2);

		ibData[i * 6 + 3] = u16( i * 4 + 1);
		ibData[i * 6 + 4] = u16( i * 4 + 3);
		ibData[i * 6 + 5] = u16( i * 4 + 2);
	}

	p_IB = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( ibData), ibData);
	if( !p_IB)
		goto done;

	// Create the vertex buffer.
	p_VB = CreateBuffer( GET_APP()->mp_D3DDevice, NUM_VERTS * sizeof( VertPos3fColour4ubTex2f), D3D11_USAGE_DYNAMIC, D3D11_BIND_VERTEX_BUFFER, D3D11_CPU_ACCESS_WRITE, nullptr);
	if( !p_VB)
		goto done;

	// Create the shader.
	char shaderPath[256];
	DebuggerDependentPath( shaderPath, sizeof( shaderPath), "DBEngine_ROOT", "", "shaders\\FontShader.hlsl");
	p_shader = new Shader( shaderPath, g_vertPos3fColour4ubTex2fMacros, "VSMain", "vs_5_0", "PSMain", "ps_5_0", g_vertPos3fColour4ubTex2fDesc, g_vertPos3fColour4ubTex2fSize);
	if( p_shader == nullptr)
		goto done;

	// Everything seemed to work; collate it all and make a D3DFont.
	p_font = new Font();

	p_font->mp_glyphs = p_glyphs;
	p_glyphs = nullptr;

	p_font->mp_texture = p_texture;
	p_texture = nullptr;

	p_font->mp_textureView = p_textureView;
	p_textureView = nullptr;

	p_font->mp_indexBuffer = p_IB;
	p_IB = nullptr;

	p_font->mp_vertBuffer = p_VB;
	p_VB = nullptr;

	p_font->mp_shader = p_shader;
	p_shader = nullptr;

	// Find the shader variables.
	p_font->mp_shader->FindCBuffer( "GlobalVars", &p_font->m_VSCBufferSlot);
	p_font->mp_shader->FindShaderVar( p_font->m_VSCBufferSlot, "g_WVP", SVT_FLOAT4x4, &p_font->m_VSWvp);

	p_font->mp_shader->FindCBuffer( "GlobalVars", &p_font->m_PSCBufferSlot);
	p_font->mp_shader->FindTexture( false, "g_texture", &p_font->m_PSTexture);

	// Create the shader buffers.
	p_font->mp_shader->mp_VSCBuffer = CreateBuffer( GET_APP()->mp_D3DDevice, p_font->mp_shader->mp_CBuffers[p_font->m_VSCBufferSlot].m_sizeBytes, D3D11_USAGE_DYNAMIC, D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, nullptr);
	p_font->mp_shader->mp_PSCBuffer = CreateBuffer( GET_APP()->mp_D3DDevice, p_font->mp_shader->mp_CBuffers[p_font->m_PSCBufferSlot].m_sizeBytes, D3D11_USAGE_DYNAMIC, D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, nullptr);

done:
	ReleaseCOM( p_textureView);
	ReleaseCOM( p_texture);
	ReleaseCOM( p_VB);
	ReleaseCOM( p_IB);
	SafeDelete( p_shader);

	if( hDC) {
		RestoreDC( hDC, -1);
		DeleteDC( hDC);
		hDC = nullptr;
	}

	if( hBitmap) {
		DeleteObject( hBitmap);
		hBitmap = nullptr;
	}

	if( hFont) {
		DeleteObject( hFont);
		hFont = nullptr;
	}

	SafeDeleteArray( p_glyphs);

	return p_font;
}

/**
* Render the text on the screen.
*/
void DBE::Font::RenderText( const DirectX::XMFLOAT3& startPos, const Style* p_style, const char* str) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);
	VertPos3fColour4ubTex2f* p_verts( nullptr);
	s32 numChars( 0);

	GET_APP()->SetRasteriserState( false, false);
	GET_APP()->SetBlendState( true);

	XMFLOAT3 pos( startPos);

	// Use the default style if one wasn't specified.
	if( !p_style)
		p_style = &DEFAULT_STYLE;

	for( size_t chIndex( 0); str[chIndex] != 0; ++chIndex) {
		s32 c( str[chIndex]);

		// Skip if this character can't be printed.
		if( c < 32 || c >= 127)
			continue;

		const Glyph* p_glyph = &mp_glyphs[c - 32];

		if( numChars >= NUM_CHARS) {
			p_context->Unmap( mp_vertBuffer, 0);

			this->Render( numChars * 6);

			p_verts = nullptr;
			numChars = 0;
		}

		if( !p_verts) {
			D3D11_MAPPED_SUBRESOURCE ms;
			if( FAILED( p_context->Map( mp_vertBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &ms)))
				return;

			p_verts = static_cast<VertPos3fColour4ubTex2f*>( ms.pData);
		}

		VertPos3fColour4ubTex2f* p_vert = &p_verts[numChars * 4];
		++numChars;

		p_vert->pos.x = pos.x;
		p_vert->pos.y = pos.y;
		p_vert->pos.z = pos.z;
		p_vert->colour = p_style->m_colour;
		p_vert->tex.x = p_glyph->texMini.x;
		p_vert->tex.y = p_glyph->texMaxi.y;
		++p_vert;

		p_vert->pos.x = pos.x + p_glyph->size.x * p_style->m_scale.x;
		p_vert->pos.y = pos.y;
		p_vert->colour = p_style->m_colour;
		p_vert->tex.x = p_glyph->texMaxi.x;
		p_vert->tex.y = p_glyph->texMaxi.y;
		++p_vert;

		p_vert->pos.x = pos.x;
		p_vert->pos.y = pos.y + p_glyph->size.y * p_style->m_scale.y;
		p_vert->pos.z = pos.z;
		p_vert->colour = p_style->m_colour;
		p_vert->tex.x = p_glyph->texMini.x;
		p_vert->tex.y = p_glyph->texMini.y;
		++p_vert;

		p_vert->pos.x = pos.x + p_glyph->size.x * p_style->m_scale.x;
		p_vert->pos.y = pos.y + p_glyph->size.y * p_style->m_scale.y;
		p_vert->pos.z = pos.z;
		p_vert->colour = p_style->m_colour;
		p_vert->tex.x = p_glyph->texMaxi.x;
		p_vert->tex.y = p_glyph->texMini.y;
		++p_vert;

		pos.x += p_glyph->size.x * p_style->m_scale.x;
	}

	if( p_verts)
		p_context->Unmap( mp_vertBuffer, 0);

	if( numChars > 0)
		this->Render( numChars * 6);
}

/**
* Render the text on the screen.
*/
void DBE::Font::RenderTextf( const DirectX::XMFLOAT3& startPos, const Style* p_style, const char* format, ...) {
	char buffer[1024];

	va_list v;
	va_start( v, format);
	vsprintf_s( buffer, format, v);
	va_end( v);

	this->RenderText( startPos, p_style, buffer);
}

/**
* 
*/
void DBE::Font::RenderFont() {
	static DBE::UITexture* p_UITexture = new DBE::UITexture( mp_texture, mp_textureView, GET_APP()->GetSamplerState());

	p_UITexture->Render( 0.0f);
}

/**
* 
*/
void DBE::Font::Render( s32 vertCount) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);
	ID3D11SamplerState* p_samplerState( GET_APP()->GetSamplerState( true));

	GET_APP()->m_matWorld = XMMatrixIdentity();
	GET_APP()->m_matWorld = XMMatrixScalingFromVector( XMLoadFloat3( &XMFLOAT3( 0.01f, 0.01f, 0.01f)));

	if( mp_shader->mp_VSCBuffer || mp_shader->mp_PSCBuffer) {
		D3D11_MAPPED_SUBRESOURCE vsMap, psMap;
		if( !mp_shader->mp_VSCBuffer || FAILED( p_context->Map( mp_shader->mp_VSCBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &vsMap)))
			vsMap.pData = nullptr;
		if( !mp_shader->mp_PSCBuffer || FAILED( p_context->Map( mp_shader->mp_PSCBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &psMap)))
			psMap.pData = nullptr;

		Matrix4 wvp = GET_APP()->GetWVP();

		// Send the WVP matrix to the shader.
		mp_shader->SetCBufferMatrix( vsMap, m_VSWvp, wvp);

		// Do... something to the shader maps.
		if( vsMap.pData)
			p_context->Unmap( mp_shader->mp_VSCBuffer, 0);
		if( psMap.pData)
			p_context->Unmap( mp_shader->mp_PSCBuffer, 0);

		// Set the constant buffers for the vertex and pixel shaders.
		if( mp_shader->mp_VSCBuffer) {
			ID3D11Buffer* p_constBuffers[1] = { mp_shader->mp_VSCBuffer };
			p_context->VSSetConstantBuffers( m_VSCBufferSlot, 1, p_constBuffers);
		}
		if( mp_shader->mp_PSCBuffer) {
			ID3D11Buffer* p_constBuffers[1] = { mp_shader->mp_PSCBuffer };
			p_context->PSSetConstantBuffers( m_PSCBufferSlot, 1, p_constBuffers);
		}

		if( m_PSTexture >= 0)
			p_context->PSSetShaderResources( m_PSTexture, 1, &mp_textureView);
	}

	p_context->VSSetShader( mp_shader->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_shader->mp_PS, nullptr, 0);

	ID3D11SamplerState* apTextureSampler[1] = { p_samplerState };
	p_context->PSSetSamplers( 0, 1, apTextureSampler);

	p_context->IASetInputLayout( mp_shader->mp_IL);
	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { mp_vertBuffer };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( mp_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	p_context->DrawIndexed( vertCount, 0, 0);
}

/**
* Creates an HFONT.
*/
HFONT DBE::Font::CreateGDIFont( HDC hDC, const char* fontName, s32 height, u32 flags) {
	LOGFONT lf;

	lf.lfHeight = -MulDiv( height, GetDeviceCaps( hDC, LOGPIXELSY), 72);
	lf.lfWeight = 0;
	lf.lfEscapement = 0;
	lf.lfOrientation = 0;
	lf.lfWeight = flags & FW_BOLD;  // FW_NORMAL
	lf.lfItalic = FALSE;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = DEFAULT_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
	lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lf.lfQuality = ANTIALIASED_QUALITY;
	lf.lfPitchAndFamily = VARIABLE_PITCH;
	strncpy_s( lf.lfFaceName, sizeof( lf.lfFaceName), fontName, _TRUNCATE);

	HFONT hFont = CreateFontIndirect( &lf);
	return hFont;
}

bool DBE::Font::PaintAlphabet( HDC hDC, s32 width, s32 height, Glyph* p_glyphs) {
	SIZE size;
	char character = 'x';
	GetTextExtentPoint32( hDC, &character, 1, &size);

	s32 spacing( s32( ceilf( size.cy * 0.3f)));
	s32 x( 0), y( 0);
	LONG lineMaxHeight( 0);

	for( s32 ch( 32); ch < 127; ++ch) {
		char c = (char)ch;
		if( !GetTextExtentPoint32( hDC, &c, 1, &size)) {
			size.cx = 0;
			size.cy = 0;
		}

		if( x + size.cx + spacing > width) {
			x = 0;
			y += lineMaxHeight + 1;

			lineMaxHeight = 0;
		}

		if( y + size.cy > height)
			return false;

		if( size.cy > lineMaxHeight)
			lineMaxHeight = size.cy;

		if( p_glyphs) {
			ExtTextOut( hDC, x, y, ETO_OPAQUE, nullptr, &c, 1, nullptr);

			Glyph* p_glyph = &p_glyphs[ch - 32];

			p_glyph->texMini.x = x / float( width);
			p_glyph->texMini.y = y / float( height);

			p_glyph->texMaxi.x = (x + size.cx) / float( width);
			p_glyph->texMaxi.y = (y + size.cy) / float( height);

			p_glyph->size.x = float( size.cx);
			p_glyph->size.y = float( size.cy);
		}

		x += size.cx + spacing;
	}

	return true;
}