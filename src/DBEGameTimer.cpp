//-----------------------------------------------
// Function definitions for the GameTimer class.
//-----------------------------------------------

//-----------------------------------------------
// Include header file
#include "DBEGameTimer.h"

#include "DBEUtilities.h"
//-----------------------------------------------


//-----------------------------------------------
//// CONSTRUCTOR :- GameTimer::GameTimer
DBE::GameTimer::GameTimer()
	: m_SecondsPerCount( 0.0f)
	, m_DeltaTime( -1.0f)
	, m_BaseTime( 0)
	, m_PausedTime( 0)
	, m_PrevTime( 0)
	, m_CurrTime( 0)
	, m_Stopped( false)
{
	__int64 countsPerSec;
	QueryPerformanceFrequency( (LARGE_INTEGER*)&countsPerSec);
	m_SecondsPerCount = 1.0f / (double)countsPerSec;
}
//-----------------------------------------------

//-----------------------------------------------
//// FUNCTION :- GameTimer::TotalTime
//// DESCRIPTION :-
//	Returns the total time elapsed since GameTimer::Reset() was called, NOT counting any time when
//	the timer was paused.
//// PRE-CONDITIONS :-
//	None.
//// PARAMETERS :-
//	None.
//// RETURNS :-
//	float	- The time, in seconds, that the timer has been running (excluding paused time).
float DBE::GameTimer::TotalTime() const {
	DBE_Assert( true);

	if( m_Stopped)
		return( (float)(((m_StopTime - m_PausedTime) - m_BaseTime) * m_SecondsPerCount));
	else
		return( (float)(((m_CurrTime - m_PausedTime) - m_BaseTime) * m_SecondsPerCount));
}


//// FUNCTION :- GameTimer::DeltaTime
//// DESCRIPTION :-
//	Returns the amount of time the last frame took to run, in seconds.
//// PRE-CONDITIONS :-
//	None.
//// PARAMETERS :-
//	None.
//// RETURNS :-
//	float	- The amount of time, in seconds, that the last frame took.
float DBE::GameTimer::DeltaTime() const {
	DBE_Assert( true);

	return (float)m_DeltaTime;
}


//// FUNCTION :- GameTimer::Reset
//// DESCRIPTION :-
//	Clears all of the timer's values and begins timing again from now.
//// PRE-CONDITIONS :-
//	None.
//// PARAMETERS :-
//	None.
//// RETURNS :-
//	None.
void DBE::GameTimer::Reset() {
	DBE_Assert( true);

	__int64 currTime;
	QueryPerformanceCounter( (LARGE_INTEGER*)&currTime);

	m_BaseTime = currTime;
	m_PrevTime = currTime;
	m_StopTime = 0;
	m_Stopped = false;
}


//// FUNCTION :- GameTimer::Start
//// DESCRIPTION :-
//	Begins timing again and unpauses the timer.
//// PRE-CONDITIONS :-
//	None.
//// PARAMETERS :-
//	None.
//// RETURNS :-
//	None.
void DBE::GameTimer::Start() {
	DBE_Assert( true);

	__int64 startTime;
	QueryPerformanceCounter( (LARGE_INTEGER*)&startTime);

	if( m_Stopped) {
		m_PausedTime += (startTime - m_StopTime);

		m_PrevTime = startTime;
		m_StopTime = 0;
		m_Stopped = false;
	}
}


//// FUNCTION :- GameTimer::Stop
//// DESCRIPTION :-
//	Pauses the timer.
//// PRE-CONDITIONS :-
//	None.
//// PARAMETERS :-
//	None.
//// RETURNS :-
//	None.
void DBE::GameTimer::Stop() {
	DBE_Assert( true);

	if( !m_Stopped) {
		__int64 currTime;
		QueryPerformanceCounter( (LARGE_INTEGER*)&currTime);

		m_StopTime = currTime;
		m_Stopped = true;
	}
}


//// FUNCTION :- GameTimer::Tick
//// DESCRIPTION :-
//	Updates any required data, provided the timer isn't paused.
//// PRE-CONDITIONS :-
//	None.
//// PARAMETERS :-
//	None.
//// RETURNS :-
//	None.
void DBE::GameTimer::Tick() {
	DBE_Assert( true);

	if( m_Stopped) {
		m_DeltaTime = 0.0f;
		return;
	}

	QueryPerformanceCounter( (LARGE_INTEGER*)&m_CurrTime);

	// time difference between this frame and the previous frame
	m_DeltaTime = (m_CurrTime - m_PrevTime) * m_SecondsPerCount;

	// prepare for the next frame
	m_PrevTime = m_CurrTime;

	// Force non-negative. The DXSDK's CDXUTTimer mentions that if the processor goes into a power
	// save mode or we get shuffled to another processor, then m_DeltaTime can be negative.
	if( m_DeltaTime < 0.0f)
		m_DeltaTime = 0.0f;
}