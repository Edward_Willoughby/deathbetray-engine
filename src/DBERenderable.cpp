/********************************************************************
*	Function definitions for the D3DRenderable class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "DBERenderable.h"

#include "DBEApp.h"
#include "DBEColour.h"
#include "DBEGraphicsHelpers.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DirectX;


/**
* Constructor.
*/
DBE::Renderable::Renderable()
	: mp_vertBuffer( nullptr)
	, mp_indexBuffer( nullptr)
	, m_vertexCount( 0)
	, m_indexCount( 0)
	, m_vertexStride( 0)
	, m_colour( WHITE)
	, m_alpha( 255)
{}

/**
* Destructor.
*/
DBE::Renderable::~Renderable() {}

/**
* Updates the renderable object.
*
* @param deltaTime :: The time taken to render the last frame.
*/
bool DBE::Renderable::OnUpdate( float deltaTime) {
	// Update the bounding box.
	this->UpdateBoundingSphere();

	return Movable::OnUpdate( deltaTime);
}

void DBE::Renderable::OnRender( float deltaTime) {
	DBE_Assert( mp_vertBuffer != nullptr && m_vertexStride != 0 && (m_vertexCount != 0 || m_indexCount != 0));

	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	ID3D11Buffer* ap_vertBuffer[1]	= { mp_vertBuffer };
	UINT aVertStride[1]				= { m_vertexStride };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	if( mp_indexBuffer != nullptr) {
		p_context->IASetIndexBuffer( mp_indexBuffer, DXGI_FORMAT_R32_UINT, 0);
		
		p_context->DrawIndexed( m_indexCount, 0, 0);
	}
	else {
		p_context->Draw( m_vertexCount, 0);
	}
}

/**
* Sets the colour for the renderable.
*/
void DBE::Renderable::SetColour( u32 colour) {
	m_colour = colour;
}

/**
* Gets the colour for the renderable.
*/
u32 DBE::Renderable::GetColour() const {
	return m_colour;
}

/**
* Sets the alpha for the renderable.
*/
void DBE::Renderable::SetAlpha( u8 alpha) {
	m_alpha = alpha;
}

/**
* Gets the alpha for the renderable.
*/
u8 DBE::Renderable::GetAlpha() const {
	return m_alpha;
}

/**
* Get the bounding sphere of the object.
*
* @return A pointer to the object's bounding sphere.
*/
const DBE::BoundingSphere* DBE::Renderable::GetBoundingSphere() const {
	return &m_boundingSphere;
}

/**
* Updates the bounding sphere's radius.
*
* @param fromVerticies :: Default: True. If true, searches through the mesh's verticies to find the
*							farthest points to use as the radius.
*/
void DBE::Renderable::UpdateBoundingSphere( bool fromVerticies /*= false*/) {
	Vec3 vScale( this->GetScale());
	float scale( Max( vScale.GetX(), vScale.GetY()));
	scale = Max( scale, vScale.GetZ());

	m_boundingSphere.m_center = m_baseBoundingSphere.m_center + this->GetPosition();
	m_boundingSphere.m_radius = m_baseBoundingSphere.m_radius * scale;

	//// Early out if the verticies haven't been modified.
	//if( !fromVerticies)
	//	return;
}