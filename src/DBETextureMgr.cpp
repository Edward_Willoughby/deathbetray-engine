/********************************************************************
*	Function definitions for the TextureMgr class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBETextureMgr.h"

#include <fstream>

#include "DBEApp.h"
#include "DBEDDSTextureLoader.h"
#include "DBEGraphicsHelpers.h"
#include "DBEUtilities.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
DBE::TextureMgr::TextureMgr()
	: m_maxTextures( gsc_maxTextures)
	, m_deleteUnreferenced( true)
{}

/**
* Destructor.
*/
DBE::TextureMgr::~TextureMgr() {
	
}

/**
* Deletes any remaining textures. This should only be called by 'App'.
*/
void DBE::TextureMgr::Shutdown() {
	for( u32 i( 0); i < m_maxTextures; ++i) {
		TextureInfo* texture( &m_textures[i]);

		if( !this->TextureExists( texture))
			continue;

#ifdef _DEBUG
		if( texture->m_refCount > 0)
			DebugTrace( "TextureMgr: %s still has %d reference%s.\n", texture->m_texture.m_fileName, texture->m_refCount, texture->m_refCount == 1 ? "" : "s");
#endif	// #ifdef _DEBUG

		this->ReleaseTexture( texture);
	}
}

///**
//* Sets how many textures can be loaded in by the TextureMgr. The limit cannot be set to a lower
//* number than the amount of existing textures.
//*
//* @param limit :: The maximum number of textures that the TextureMgr can load in.
//*/
//void DBE::TextureMgr::SetTextureLimit( u32 limit) {
//	// If it's the first initialisation, then just set the data and return.
//	if( mp_textures == nullptr) {
//		mp_textures = new TextureInfo[limit];
//		m_maxTextures = limit;
//		return;
//	}
//
//	// If the array already exists, then the data needs to be copied over.
//	TextureInfo* p_textures( nullptr);
//	u32 count( 0);
//
//	for( u32 i( 0); i < m_maxTextures; ++i) {
//		if( this->TextureExists( &mp_textures[i]))
//	}
//}

/**
* Loads a texture.
*
* @param fileName	:: The path, file name, and extension of the texture (should be '.dds').
* @param sampler	:: The sampler state to be used for this texture.
*
* @return A container with all the vital data about the loaded texture.
*/
Texture* DBE::TextureMgr::LoadTexture( const char* fileName, ID3D11SamplerState* sampler) {
	TextureInfo* texture( nullptr);
	
	this->CreateTextureInfo( texture, fileName, sampler);

	return &texture->m_texture;
}

/**
* Gets a space in the texture array so a custom texture can be loaded in.
*/
void DBE::TextureMgr::ReserveTexture( Texture* &texture) {
	s64 slot( this->FindEmptySlot());

	if( slot < 0)
		texture = nullptr;

	m_textures[slot].m_exists = true;
	++m_textures[slot].m_refCount;
	texture = &m_textures[slot].m_texture;
}

/**
* Creates a 2D texture.
*/
bool DBE::TextureMgr::Create2DTexture( Texture* &texture, const char* fileName, D3D11_TEXTURE2D_DESC* td, D3D11_SUBRESOURCE_DATA* srd, D3D11_SHADER_RESOURCE_VIEW_DESC* srvd, ID3D11SamplerState* sampler) {
	if( td == nullptr || srd == nullptr || srvd == nullptr)
		return false;

	TextureInfo* t( nullptr);

	this->CreateTextureInfo( t, fileName, sampler, td, srd, srvd);

	texture = &t->m_texture;

	return true;
}

/**
* Gets the texture's dimensions.
*/
void DBE::TextureMgr::GetTextureDimensions( Texture* t, s32* height, s32* width) {
	D3D11_TEXTURE2D_DESC desc;
	t->mp_texture->GetDesc( &desc);

	*height = desc.Height;
	*width = desc.Width;
}

/**
* Deletes a texture.
*
* @param fileName :: The path, file name, and extension of the texture (should be '.dds').
*/
void DBE::TextureMgr::DeleteTexture( const char* fileName) {
	TextureInfo* texture( this->FindTexture( fileName));

	if( texture == nullptr)
		return;

	--texture->m_refCount;

	if( m_deleteUnreferenced && texture->m_refCount <= 0)
		this->ReleaseTexture( texture);
}

/**
* Deletes a texture. Using this method ensures that you don't end up with pointers to texture that
* no longer exist, as it nullifies the pointer as well as deleting the texture.
*
* @param texture :: The container for the texture that you want to delete.
*/
void DBE::TextureMgr::DeleteTexture( Texture* &texture) {
	if( texture == nullptr)
		return;

	this->DeleteTexture( texture->m_fileName);

	// The texture doesn't exist anymore, so stop pointing to it.
	texture = nullptr;
}

/**
* Set whether to release textures that no longer have any references. If this is set to true then
* textures that reach 0 references will be deleted from memory immediately. If it's false, then all
* textures that have been loaded during run-time will remain in the 'm_textures' vector until the
* program closes. This saves on loading time for textures (so you don't keep loading/deleting the
* same textures, at the cost of memory.
*
* @param del :: True if you want textures to be deleted from memory once they reach 0 references,
*				instead of being held until the program shuts down.
*/
void DBE::TextureMgr::DeleteUnreferencedTextures( bool del) {
	m_deleteUnreferenced = del;
}

/**
* Creates a texture.
*/
void DBE::TextureMgr::CreateTextureInfo( DBE::TextureMgr::TextureInfo* &ti, const char* fileName, ID3D11SamplerState* sampler, D3D11_TEXTURE2D_DESC* td /*= nullptr*/, D3D11_SUBRESOURCE_DATA* srd /*= nullptr*/, D3D11_SHADER_RESOURCE_VIEW_DESC* srvd /*= nullptr*/) {
	ti = this->FindTexture( fileName);

	TextureInfo* texture;

	if( ti != nullptr) {
		texture = ti;

		if( ti->m_exists) {
			++ti->m_refCount;
			return;
		}
	}
	else {
		// Ensure that there's space to fit the texture into the manager.
		s64 pos( this->FindEmptySlot());
		DBE_AssertWithMsg( pos >= 0, "TextureMgr: Too many textures.\n", nullptr);

		texture = &m_textures[pos];
		ti = &m_textures[pos];
	}

	// Load in the texture.
	if( td != nullptr && srd != nullptr && srvd != nullptr) {
		HR( GET_APP()->mp_D3DDevice->CreateTexture2D( td, srd, &texture->m_texture.mp_texture));

		HR( GET_APP()->mp_D3DDevice->CreateShaderResourceView( texture->m_texture.mp_texture, srvd, &texture->m_texture.mp_textureView));
	}
	else {
		this->LoadTextureFromFile( GET_APP()->mp_D3DDevice, fileName, &texture->m_texture.mp_texture, &texture->m_texture.mp_textureView);
	}

	strcpy_s( texture->m_texture.m_fileName, 256, fileName);
	texture->m_texture.mp_samplerState = sampler;

	// Get the texture's height and width.
	this->GetTextureDimensions( &texture->m_texture, &texture->m_texture.m_textureHeight, &texture->m_texture.m_textureWidth);

	// Get the size of the file.
	std::ifstream in( fileName, std::ios::binary | std::ios::ate);
	texture->m_texture.m_sizeInBytes = (s32)in.tellg();
	in.close();

	++texture->m_refCount;
	texture->m_exists = true;

	if( ti == nullptr)
		DBE_AssertAlways();
}

/**
* Finds an empty slot in the array.
*
* @return A position in the 'm_textures' array that doesn't currently have a texture in it.
*/
s64 DBE::TextureMgr::FindEmptySlot() {
	for( u32 i( 0); i < m_maxTextures; ++i)
		if( !this->TextureExists( &m_textures[i]))
			return i;

	return -1;
}

/**
* Finds a texture in the array.
*
* @param fileName :: The path, file name, and extension of the texture (should be '.dds').
*
* @return The texture's vital data. If the texture doesn't exist, then it'll return nullptr.
*/
DBE::TextureMgr::TextureInfo* DBE::TextureMgr::FindTexture( const char* fileName) {
	if( fileName == nullptr)
		return nullptr;

	TextureInfo* texture( nullptr);

	for( u32 i( 0); i < m_maxTextures; ++i) {
		if( strcmp( m_textures[i].m_texture.m_fileName, fileName) == 0) {
			texture = &m_textures[i];
			break;
		}
	}

	return texture;
}

/**
* Check whether a texture exists.
*
* @param t :: The texture.
*
* @return True if the texture exists.
*/
bool DBE::TextureMgr::TextureExists( DBE::TextureMgr::TextureInfo* t) {
	if( t->m_exists)
		return true;

	if( m_deleteUnreferenced && t->m_texture.mp_texture != nullptr && t->m_texture.mp_textureView != nullptr)
		return true;

	return false;
}

/**
* Releases the texture from memory.
*
* @param texture :: A texture in the 'm_textures' vector.
*/
void DBE::TextureMgr::ReleaseTexture( DBE::TextureMgr::TextureInfo* texture) {
#ifdef _DEBUG
	if( texture->m_refCount < 0)
		DebugTrace( "TextureMgr: %s has been deleted %d too many time%s.\n", texture->m_texture.m_fileName, texture->m_refCount, texture->m_refCount == 1 ? "" : "s");
#endif	// #ifdef _DEBUG

	Texture* t( &texture->m_texture);

	ReleaseCOM( t->mp_texture);
	ReleaseCOM( t->mp_textureView);

	//t->m_fileName[0] = '\0';
	t->m_sizeInBytes = 0;
	t->m_textureHeight = 0;
	t->m_textureWidth = 0;

	texture->m_exists = false;
}

/**
* Loads a texture from a file.
*
* @param p_device				:: The DirectX device.
* @param p_fileName				:: The file name of the texture to be loaded.
* @param pp_texture				:: The destination of where to store the loaded texture.
* @param pp_textureResourceView	:: I have no idea...
* @param pp_samplerState		:: The sampler state.
*
* @return True if the texture was loaded correctly.
*/
bool DBE::TextureMgr::LoadTextureFromFile(  ID3D11Device* p_device, const char* p_fileName, ID3D11Texture2D** pp_texture, ID3D11ShaderResourceView** pp_textureResourceView)
{
	ReleaseCOM(( *pp_texture));
	ReleaseCOM(( *pp_textureResourceView));

	// Get the size of the file and ensure that the file exists.
	std::ifstream in( p_fileName, std::ios::binary | std::ios::ate);
	DBE_AssertWithMsg( in.is_open(), "Texture %s could not be found.\n", p_fileName);
	in.close();

	// Load the texture.
	size_t strLength( strlen( p_fileName) + 1);
	std::wstring wFileName( strLength, L'#');
	mbstowcs_s( &strLength, &wFileName[0], strLength, p_fileName, strLength);
	HR( DirectX::CreateDDSTextureFromFile( p_device, wFileName.c_str(), (ID3D11Resource**)pp_texture, pp_textureResourceView));

	return true;

//bad:
//	ReleaseCOM(( *pp_texture));
//	ReleaseCOM(( *pp_textureResourceView));
//
//	return false;
}