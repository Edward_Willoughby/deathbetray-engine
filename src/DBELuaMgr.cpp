/********************************************************************
*	Function definitions for the LuaMgr class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBELuaMgr.h"

#include <fstream>
#include <iterator>

#include "DBEApp.h"
#include "DBEUtilities.h"

/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/


/**
* Constructor for the LuaMgr class.
*/
DBE::LuaMgr::LuaMgr()
	: mp_fileName( nullptr)
{
	// Create the lua_State instance.
	mp_lua = lua_open();

	// This opens ALL of the Lua libraries; it's easier than opening them one-by-one.
	luaL_openlibs( mp_lua);

	this->RegisterFunction( "DebugTrace", LuaFunction_DebugTrace);
}

/**
* Destructor for the LuaMgr class.
*/
DBE::LuaMgr::~LuaMgr() {
	// Close the Lua interpreter.
	lua_close( mp_lua);
}

/**
* Sends information to the Lua interpreter on the name of a static function (the second parameter)
* when the name of a custom made function (the first parameter) is found in a Lua script.
*
* @param functionName	:: The name of the function that'll be used in the Lua script.
* @param function		:: The function that the Lua interpreter will call when the first
*							parameter's name was found in a script.
*/
void DBE::LuaMgr::RegisterFunction( const char* functionName, LuaFunction function) {
	lua_pushstring( mp_lua, functionName);
	lua_pushcclosure( mp_lua, LuaFunctionInterpreter, 1);
	lua_setglobal( mp_lua, functionName);

	m_functions.push_back( FunctionPair( function, functionName));
	//lua_register( mp_lua, functionName, function);
}

/**
* Stores a file name of a Lua script file that'll be read in later.
*
* @param fileName :: The name of the script file that'll be saved to be read in later.
*/
void DBE::LuaMgr::SetScriptToRead( char* fileName) {
	// I was going to do some error checking here to ensure that the file exists, but I can't find
	// the right function at the moment, so sod it.
	mp_fileName = fileName;
}

/**
* Reads in a Lua script file. If no parameter is given, then the local member variable will be used.
* If neither are set the it'll hit an assert.
*
* @param fileName :: The name of the script file that'l be read in. If a value is not given, then
*						the value given by the function 'SetScriptToRead()' will be used.
*/
void DBE::LuaMgr::ReadScript( char* fileName /*= nullptr*/) {
	// If the function has a valid parameter then execute that file.
	if( fileName != nullptr)
		luaL_dofile( mp_lua, fileName);
	// Otherwise, check if the member variable has been set and use that.
	else if( mp_fileName != nullptr)
		luaL_dofile( mp_lua, mp_fileName);
	// If neither of the file names have data in them then the LuaMgr won't know what file to
	// execute and the programmer has done something wrong; thus hit an assert.
	else
		DBE_AssertAlways();
}

/**
* Executes Lua code from the given string.
*
* @param code :: The string that holds Lua code to be executed.
*/
void DBE::LuaMgr::ExecuteCode( char* code) {
	if( code == nullptr)
		DBE_AssertAlways();

	luaL_dostring( mp_lua, code);
}

/**
* Returns the name of the last Lua function that was called. This is the name of the Lua function
* and NOT the name of the C++ function that it calls.
*
* @return The name of the Lua function.
*/
const char* DBE::LuaMgr::GetFunctionName() {
	return lua_tostring( mp_lua, lua_upvalueindex( 1));
}

/**
* Returns the number of parameters of the most recent function call that the Lua interpreter has
* encountered. If the number of parameters does not agree with 'min' and 'max' then the function
* will return false.
*
* @param number :: Where the number of paramters will be returned.
* @param min	:: The minimum number of parameters that should be found.
* @param max	:: Default: -1. The maximum number of parameters that can be found. If this
*					parameter isn't specified then there won't be any upper limit to the number of
*					parameters.
*
* @return True if the number of parameters found was acceptable.
*/
bool DBE::LuaMgr::GetNumberOfParameters( s32& number, s32 min, s32 max /*= -1*/) {
	number = lua_gettop( mp_lua);

	if( number <= 0 || number < min)
		return false;
	else if( max > -1 && number > max)
		return false;
	else
		return true;
}

/**
* Takes the name of a function called in Lua to find its associated C++ function and calls it. If
* the function name wasn't found in the vector then something has either gone wrong with the 'char*'
* comparison or storing of the function in 'LuaMgr::RegisterFunction()'. I wanted to make this
* function private as it SHOULDN'T be called outside of the LuaMgr class, but the Lua interpreter
* requires a static function to be called and I can't call a private function from a static
* function.
*
* @param functionName :: The name of the function in Lua.
*
* @return The number of parameters pushed (i.e. values being sent from C++ to Lua).
*/
s32 DBE::LuaMgr::LuaFunctionNameInterpreter( const char* functionName) {
	for( FunctionVector::iterator it( m_functions.begin()); it != m_functions.end(); ++it)
		if( strcmp( it->second, functionName) == 0)
			return it->first();

	// If the function name wasn't found then something went wrong.
	DBE_AssertAlways();
	// ... To get rid of the warning.
	return 0;
}

/**
* Function exposed to Lua that allows text to be displayed in the output window.
*
* @return The number of objects pushed onto the stack (i.e. returned).
*/
s32 DBE::LuaMgr::LuaFunction_DebugTrace() {
	s32 noOfParams( 0);
	const char* text( nullptr);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	if( !MGR_LUA().GetParameterString( 1, text))
		return 0;

	DebugTrace( "Lua Message: %s\n", text);

	return 0;
}

/**
* This function is what the Lua interpreter calls. This function's prototype has been specified by
* the Lua interpreter libraries. This function, in turn, then calls a member function of the
* LuaMgr which uses the name of the function in Lua to call the specified associated function.
*
* @param p_lua :: The pointer to the lua_State.
*
* @return The number of parameters pushed (i.e. values being sent from C++ to Lua).
*/
s32 DBE::LuaMgr::LuaFunctionInterpreter( lua_State* p_lua) {
	// Get the name of the Lua function that was called.
	const char* fileName( MGR_LUA().GetFunctionName());

	// ... To get rid of the warning.
	p_lua = p_lua;
	
	// Decipher which C++ function was meant to be called using that Lua name.
	return MGR_LUA().LuaFunctionNameInterpreter( fileName);
}