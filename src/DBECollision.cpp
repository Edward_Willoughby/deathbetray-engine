/********************************************************************
*	Function definitions for the collision functionality.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBECollision.h"

#include "DBEApp.h"
#include "DBEMovable.h"
#include "DBEUtilities.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Converts an orthographic position to a 3D ray.
*
* @param pos		:: The orthographic position (e.g. cursor position).
* @param worldMat	:: World matrix of the object the ray is going to be tested against (I have no
*						idea why this is required).
* @param rayOrigin	:: The origin of the resultant ray.
* @param rayDir		:: The direction of the resultant ray.
*/
void DBE::Collision::ConvertOrthographicPosToRay( const DBE::Vec2& pos, const DBE::Matrix4& worldMat, DBE::Vec3& rayOrigin, DBE::Vec3& rayDir) {
	float windowWidth( 0.0f), windowHeight( 0.0f);
	XMFLOAT4X4 p;

	windowWidth = (float)GET_APP()->GetWindowWidth();
	windowHeight = (float)GET_APP()->GetWindowHeight();
	DirectX::XMStoreFloat4x4( &p, GET_APP()->m_matProj);

	// Compute picking ray in view space.
	float vx = ( 2.0f * pos.GetX() / windowWidth - 1.0f) / p._11;
	float vy = (-2.0f * pos.GetY() / windowHeight + 1.0f) / p._22;

	// Ray definition in view space.
	rayOrigin	= XMVectorSet( 0.0f, 0.0f, 0.0f, 1.0f);
	rayDir		= XMVectorSet( vx, vy, 1.0f, 0.0f);

	// Transform ray to local space of mesh.
	Matrix4 v = GET_APP()->m_matView;
	Matrix4 invView = XMMatrixInverse( &XMMatrixDeterminant( v), v);

	Matrix4 invWorld = XMMatrixInverse( &XMMatrixDeterminant( worldMat), worldMat);

	Matrix4 toLocal = XMMatrixMultiply( invView, invWorld);

	rayOrigin = XMVector3TransformCoord( rayOrigin, toLocal);
	rayDir = XMVector3TransformNormal( rayDir, toLocal);

	// Make the ray direction unit length for the intersection tests.
	rayDir = XMVector3Normalize( rayDir);
}

/**
* Test if a ray intersects a triangle.
* SOURCE: Frank Luna's 'Introduction to DirectX 11'.
*
* @param origin	:: The origin of the ray.
* @param dir	:: The direction of the ray.
* @param v0		:: The first vertex of the triangle.
* @param v1		:: The second vertex of the triangle.
* @param v2		:: The third vertex of the triangle.
* @param p_dist	:: The distance from the ray's origin to the triangle, if an intersection occurred.
*
* @return True if an intersection occurred.
*/
bool DBE::Collision::IntersectRayTriangle( const DBE::Vec3& origin, const DBE::Vec3& dir, const DBE::Vec3& v0, const DBE::Vec3& v1, const DBE::Vec3& v2, float* p_dist) {
	DBE_Assert( p_dist != nullptr);
	//DBE_Assert( XMVector3IsUnit( dir.GetXMVector()));

	static const XMVECTOR epsilon = {
		1e-20f, 1e-20f, 1e-20f, 1e-20f,
	};

	XMVECTOR zero = XMVectorZero();

	XMVECTOR e1 = (v1 - v0).GetXMVector();
	XMVECTOR e2 = (v2 - v0).GetXMVector();

	// p = direction ^ e2.
	XMVECTOR p = Cross( dir, e2).GetXMVector();

	// det = e1 * p.
	XMVECTOR det = XMVector3Dot( e1, p);

	XMVECTOR u, v, t;

	if( XMVector3GreaterOrEqual( det, epsilon)) {
		// Determinate is positive (front side of the triangle).
		XMVECTOR s = (origin - v0).GetXMVector();

		// u = s * p.
		u = XMVector3Dot( s, p);

		XMVECTOR noIntersection = XMVectorLess( u, zero);
		noIntersection = XMVectorOrInt( noIntersection, XMVectorGreater( u, det));

		// q = s ^ e1.
		XMVECTOR q = XMVector3Cross( s, e1);

		// v = direction * q.
		v = XMVector3Dot( dir.GetXMVectorConst(), q);

		noIntersection = XMVectorOrInt( noIntersection, XMVectorLess( v, zero));
		noIntersection = XMVectorOrInt( noIntersection, XMVectorGreater( u + v, det));

		// t = e2 * q.
		t = XMVector3Dot( e2, q);

		noIntersection = XMVectorOrInt( noIntersection, XMVectorLess( t, zero));

		if( XMVector4EqualInt( noIntersection, XMVectorTrueInt()))
			return false;
	}
	else if( XMVector3LessOrEqual( det, -epsilon)) {
		// determinate is negative (back side of the triangle).
		XMVECTOR s = (origin - v0).GetXMVector();

		// u = s * p.
		u = XMVector3Dot( s, p);

		XMVECTOR noIntersection = XMVectorGreater( u, zero);
		noIntersection = XMVectorOrInt( noIntersection, XMVectorLess( u, det));

		// q = s ^ e1.
		XMVECTOR q = XMVector3Cross( s, e1);

		// v = direction * q.
		v = XMVector3Dot( dir.GetXMVectorConst(), q);

		noIntersection = XMVectorOrInt( noIntersection, XMVectorGreater( v, zero));
		noIntersection = XMVectorOrInt( noIntersection, XMVectorLess( u + v, det));

		// t = e2 * q.
		t = XMVector3Dot( e2, q);

		noIntersection = XMVectorOrInt( noIntersection, XMVectorGreater( t, zero));

		if( XMVector4EqualInt( noIntersection, XMVectorTrueInt()))
			return false;
	}
	else {
		// Parallel ray.
		return false;
	}

	XMVECTOR invDet = XMVectorReciprocal( det);

	t *= invDet;

	// u * invDet and v * invDet are the barycentric coordinates of the intersection.

	// Store the x-component to p_dist
	XMStoreFloat( p_dist, t);

	return true;
}

/**
* Test if two spheres are overlapping.
*
* @param objA	:: The first sphere.
* @param objB	:: The second sphere.
* @param p_dist	:: The distance between the two objects.
*
* @return True if an intersection occurred.
*/
bool DBE::Collision::IntersectSphereSphere( const DBE::BoundingSphere* objA, const DBE::BoundingSphere* objB, float* p_dist) {
	DBE_Assert( objA != nullptr && objB != nullptr);

	// Calculate the combined sizes of the spheres.
	float sumOfRadii( objA->m_radius + objB->m_radius);

	// Store the actual distance between the spheres.
	*p_dist = Distance( objA->m_center, objB->m_center);

	// If the distance is smaller than the two radii then the sphere are overlapping.
	if( sumOfRadii < *p_dist)
		return false;

	return true;
}

/**
* Test if a point intersects with a square.
*
* @param point		:: The x and y coordinates of the point.
* @param squareMin	:: The minimum x and y coordinates of the square.
* @param squareMax	:: The maximum x and y coordinates of the square.
*
* @return True if an intersection occurred.
*/
bool DBE::Collision::IntersectPointSquare( const DBE::Vec2& point, const DBE::Vec2& squareMin, const DBE::Vec2& squareMax) {
	bool intersected( false);

	if( point.GetX() >= squareMin.GetX() && point.GetX() <= squareMax.GetX())
		if( point.GetY() >= squareMin.GetY() && point.GetY() <= squareMax.GetY())
			intersected = true;

	return intersected;
}