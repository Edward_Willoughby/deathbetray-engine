/********************************************************************
*	Function definitions for the Time class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBETime.h"

#include <chrono>

#include <DBEUtilities.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

static DBE::Time* gsp_time = nullptr;
static std::chrono::system_clock::time_point gs_start;
static tm gs_nowRaw;


/**
* Constructor.
*/
Time::Time() {
	gsp_time = this;

	// Gets the point at which this class was initialised... It's meant to be at the start of the
	// program but it should be close enough.
	gs_start = std::chrono::high_resolution_clock::now();
}

/**
* Updates the Time class's local variables (this should only be called by D3DApp).
*/
void Time::Update() {
	time_t t( time( 0));
	localtime_s( &gs_nowRaw, &t);
}

/**
* Deletes the Time class (this should only be called by D3DApp).
*/
void Time::Shutdown() {
	SafeDelete( gsp_time);
}

/**
* Gets the current system time.
*
* @return A struct containing the hours, minutes and seconds.
*/
CurrentTime Time::GetTime() {
	EnsureInstantiated();

	CurrentTime now;

	now.m_hours		= gs_nowRaw.tm_hour;
	now.m_minutes	= gs_nowRaw.tm_min;
	now.m_seconds	= gs_nowRaw.tm_sec;

	return now;
}

/**
* Gets the current system date.
*
* @return A struct containing the year, month and day.
*/
CurrentDate Time::GetDate() {
	EnsureInstantiated();

	CurrentDate now;

	now.m_years		= gs_nowRaw.tm_year;
	now.m_months	= gs_nowRaw.tm_mon;
	now.m_days		= gs_nowRaw.tm_mday;

	return now;
}

/**
* Gets the current system time and date.
*/
CurrentTimeAndDate Time::GetTimeAndDate() {
	EnsureInstantiated();

	CurrentTimeAndDate now;
	
	now.m_years		= gs_nowRaw.tm_year;
	now.m_months	= gs_nowRaw.tm_mon;
	now.m_days		= gs_nowRaw.tm_mday;
	now.m_hours		= gs_nowRaw.tm_hour;
	now.m_minutes	= gs_nowRaw.tm_min;
	now.m_seconds	= gs_nowRaw.tm_sec;

	return now;
}

/**
* Gets the current system time as a string.
*
* @param buffer	:: The char array to hold the string.
* @param size	:: The size of the buffer sent to the function.
*
* @return The time in the following string format: "hh:mm:ss". The return value is the same as the
*			parameter 'buffer', so the function can be used more easily.
*/
char* Time::GetTimeString( char* buffer, u32 size) {
	EnsureInstantiated();

	DBE_Assert( size < 9);

	ConvertNumberToChars( gs_nowRaw.tm_hour,	&buffer[0]);
	buffer[2] = ':';
	ConvertNumberToChars( gs_nowRaw.tm_min,		&buffer[3]);
	buffer[5] = ':';
	ConvertNumberToChars( gs_nowRaw.tm_sec,		&buffer[6]);
	buffer[8] = '\0';

	return buffer;
}

/**
* Gets the current system date as a string.
*
* @param buffer	:: The char array to hold the string.
* @param size	:: The size of the buffer sent to the function.
*
* @return The date in the following string format: "yyyy/mm/dd". The return value is the same as the
*			parameter 'buffer', so the function can be used more easily.
*/
char* Time::GetDateString( char* buffer, u32 size) {
	EnsureInstantiated();

	DBE_Assert( size < 11);

	ConvertNumberToChars( gs_nowRaw.tm_year / 100,	&buffer[0]);
	ConvertNumberToChars( gs_nowRaw.tm_year,		&buffer[2]);
	buffer[4] = '/';
	ConvertNumberToChars( gs_nowRaw.tm_mon,			&buffer[5]);
	buffer[7] = '/';
	ConvertNumberToChars( gs_nowRaw.tm_mday,		&buffer[8]);
	buffer[10] = '\0';

	return buffer;
}

/**
* Gets the amount of time, in microseconds, that's passed since the program started.
*
* @return The amont of time, in microseconds, since the program was started.
*/
u64 Time::GetTimeSinceStartUp() {
	EnsureInstantiated();

	std::chrono::microseconds temp = std::chrono::duration_cast<std::chrono::microseconds>( std::chrono::high_resolution_clock::now() - gs_start);
	u64 time( temp.count());

	return time;
}

/**
* Checks that the class exists, if it doesn't then it creates it.
*/
void Time::EnsureInstantiated() {
	if( gsp_time != nullptr)
		return;

	gsp_time = new Time();

	// Make sure that the class was created.
	DBE_Assert( gsp_time != nullptr);
}

/**
* Converts a number to a two digit string.
*
* @param number	:: The number to convert into characters.
* @param word	:: The two-slot char array to hold the converted numbers.
*/
void Time::ConvertNumberToChars( s32 number, char word[2]) {
	word[0] = (number % 10) + 48;
	word[1] = ((number % 100) / 10) + 48;
}