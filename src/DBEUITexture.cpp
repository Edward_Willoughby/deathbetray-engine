/********************************************************************
*	Function definitions for the D3DUITexture class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEUITexture.h"

#include <d3d11.h>

#include "DBEApp.h"
#include "DBEPixelShader.h"
#include "DBEVertexShader.h"
#include "DBEGraphicsHelpers.h"
#include "DBEShader.h"
#include "DBEVertexTypes.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

/// The buffers and index counts for the plane that all the U.I. textures will render onto.
static ID3D11Buffer* gsp_vertBuffer = nullptr;
static ID3D11Buffer* gsp_indexBuffer = nullptr;
static u8 gs_indexCount = 0;

static DBE::VertexShader* gsp_defaultVS = nullptr;
static DBE::PixelShader* gsp_defaultPS = nullptr;

static s32 gs_slotCBufferGlobalVars = -1;
static s32 gs_slotWVP = -1;
static s32 gs_slotColour = -1;
static s32 gs_slotTexture = -1;
static s32 gs_slotSampler = -1;


/**
* Constructor.
*/
DBE::UITexture::UITexture()
	: mp_VS( nullptr)
	, mp_PS( nullptr)
	, m_slotTextureHeight( -1)
	, m_slotTextureWidth( -1)
	, mp_texture( nullptr)
{
	// Create the plane, if it doesn't exist.
	this->CreatePlaneAndDefaultShader();
}

/**
* Constructor.
*/
DBE::UITexture::UITexture( const char* textureFileName, ID3D11SamplerState* sampler)
	: mp_VS( nullptr)
	, mp_PS( nullptr)
	, m_slotTextureHeight( -1)
	, m_slotTextureWidth( -1)
	, mp_texture( nullptr)
{
	// Create the plane, if it doesn't exist.
	this->CreatePlaneAndDefaultShader();

	this->SetTexture( textureFileName, sampler);
}

/**
* Constructor.
*/
DBE::UITexture::UITexture( Texture* texture, ID3D11SamplerState* sampler /*= nullptr*/)
	: mp_VS( nullptr)
	, mp_PS( nullptr)
	, m_slotTextureHeight( -1)
	, m_slotTextureWidth( -1)
	, mp_texture( nullptr)
{
	// Create the plane, if it doesn't exist.
	this->CreatePlaneAndDefaultShader();

	this->SetTexture( texture, sampler);
}

/**
* Destructor.
*/
DBE::UITexture::~UITexture() {
	MGR_TEXTURE().DeleteTexture( mp_texture);

	// Potential memory leak until a ShaderMgr has been implemented.
	//if( mp_shader != gsp_defaultShader)
	//	SafeDelete( mp_shader);
}

/**
* Sets the texture to be used.
*/
void DBE::UITexture::SetTexture( const char* textureFileName, ID3D11SamplerState* sampler) {
	mp_texture = MGR_TEXTURE().LoadTexture( textureFileName, sampler);
}

/**
* Sets the texture to be used.
*/
void DBE::UITexture::SetTexture( Texture* texture, ID3D11SamplerState* sampler) {
	mp_texture = texture;

	if( sampler == nullptr && mp_texture->mp_samplerState != nullptr)
		return;

	mp_texture->mp_samplerState = sampler;
}

/**
* Sets the sampler to be used.
*/
void DBE::UITexture::SetSampler( ID3D11SamplerState* sampler) {
	mp_texture->mp_samplerState = sampler;
}

/**
* Renders the U.I. texture (call 'Render').
*
* @param deltaTime :: Time taken to render the previous frame.
*/
void DBE::UITexture::OnRender( float deltaTime) {
	// Don't try to render if the vertex or index buffers are invalid, the shader isn't loaded, or
	// if there are too few polygons.
	if( gsp_vertBuffer == nullptr || gsp_indexBuffer == nullptr || gsp_defaultVS == nullptr || gsp_defaultPS == nullptr || gs_indexCount < 3)
		return;

	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	if( gsp_defaultVS->mp_CBuffers[gs_slotCBufferGlobalVars]) {
		D3D11_MAPPED_SUBRESOURCE vsMap, psMap;
		if( !gsp_defaultVS->mp_CBuffers[gs_slotCBufferGlobalVars] || FAILED( p_context->Map( gsp_defaultVS->mp_CBuffers[gs_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &vsMap)))
			vsMap.pData = nullptr;
		if( !gsp_defaultPS->mp_CBuffers[gs_slotCBufferGlobalVars] || FAILED( p_context->Map( gsp_defaultPS->mp_CBuffers[gs_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &psMap)))
			psMap.pData = nullptr;

		Matrix4 wvp = GET_APP()->GetWVP();

		// Send the WVP matrix to the shader.
		gsp_defaultVS->SetCBufferMatrix( vsMap, gs_slotWVP, wvp);

		// Send the height and width of the texture to the shader.
		gsp_defaultVS->SetCBufferVar( vsMap, m_slotTextureHeight, mp_texture->m_textureHeight);
		gsp_defaultVS->SetCBufferVar( vsMap, m_slotTextureWidth, mp_texture->m_textureWidth);

		// Send the colour of the texture.
		float c[4];
		ColourToFloats( c[0], c[1], c[2], c[3], this->GetColour());
		Vec4 colour( c[0], c[1], c[2], c[3]);
		gsp_defaultPS->SetCBufferVar( psMap, gs_slotColour, colour);

		// Do... something to the shader maps.
		if( vsMap.pData)
			p_context->Unmap( gsp_defaultVS->mp_CBuffers[gs_slotCBufferGlobalVars], 0);
		if( psMap.pData)
			p_context->Unmap( gsp_defaultPS->mp_CBuffers[gs_slotCBufferGlobalVars], 0);

		// Set the constant buffers for the vertex shader.
		ID3D11Buffer* p_vsConstBuffers[1] = { gsp_defaultVS->mp_CBuffers[gs_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( gs_slotCBufferGlobalVars, 1, p_vsConstBuffers);

		// Set the constant buffers for the pixel shader.
		ID3D11Buffer* p_psConstBuffers[1] = { gsp_defaultPS->mp_CBuffers[gs_slotCBufferGlobalVars] };
		p_context->PSSetConstantBuffers( gs_slotCBufferGlobalVars, 1, p_psConstBuffers);
	}

	// Set the texture.
	if( gs_slotTexture >= 0)
		p_context->PSSetShaderResources( gs_slotTexture, 1, &mp_texture->mp_textureView);

	// Set the sampler.
	if( gs_slotSampler >= 0)
		p_context->PSSetSamplers( gs_slotSampler, 1, &mp_texture->mp_samplerState);

	// Set up the vertex and pixel shaders.
	p_context->VSSetShader( gsp_defaultVS->mp_VS, nullptr, 0);
	p_context->IASetInputLayout( gsp_defaultVS->mp_IL);

	p_context->PSSetShader( gsp_defaultPS->mp_PS, nullptr, 0);

	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { gsp_vertBuffer };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( gsp_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	p_context->DrawIndexed( gs_indexCount, 0, 0);
}

ID3D11Buffer* DBE::UITexture::GetVertexBuffer() const {
	return gsp_vertBuffer;
}

ID3D11Buffer* DBE::UITexture::GetIndexBuffer() const {
	return gsp_indexBuffer;
}

u32 DBE::UITexture::GetIndexCount() const {
	return gs_indexCount;
}

/**
* Deletes the static plane that's used for rendering (should only be called by 'App').
*/
void DBE::UITexture::Shutdown() {
	if( gsp_vertBuffer == nullptr && gsp_indexBuffer == nullptr)
		return;

	ReleaseCOM( gsp_vertBuffer);
	ReleaseCOM( gsp_indexBuffer);
	gs_indexCount = 0;

	MGR_SHADER().RemoveShader<VertexShader>( gsp_defaultVS);
	MGR_SHADER().RemoveShader<PixelShader>( gsp_defaultPS);
}

/**
* Creates the plane used by U.I. textures.
*/
void DBE::UITexture::CreatePlaneAndDefaultShader() {
	if( gsp_vertBuffer != nullptr && gsp_indexBuffer != nullptr && gsp_defaultVS != nullptr && gsp_defaultPS != nullptr)
		return;

	// Make sure the memory is released.
	this->Shutdown();

	VertPos3fColour4ubTex2f verts[] = {
		VertPos3fColour4ubTex2f( Vec3( -0.5f, -0.5f, 0.0f), WHITE, Vec2( 0.0f, 0.0f)),	// 0
		VertPos3fColour4ubTex2f( Vec3( -0.5f,  0.5f, 0.0f), WHITE, Vec2( 0.0f, 1.0f)),	// 1
		VertPos3fColour4ubTex2f( Vec3(  0.5f,  0.5f, 0.0f), WHITE, Vec2( 1.0f, 1.0f)),	// 2
		VertPos3fColour4ubTex2f( Vec3(  0.5f, -0.5f, 0.0f), WHITE, Vec2( 1.0f, 0.0f)),	// 3
	};
	gsp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubTex2f)*4, verts);

	UINT indicies[] = {
		// front face
		0, 1, 2,
		0, 2, 3,
	};
	gs_indexCount = sizeof( indicies) / sizeof( indicies[0]);
	gsp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT)*gs_indexCount, indicies);

	char shaderPath[256];
	DebuggerDependentPath( shaderPath, sizeof( shaderPath), "DBEngine_ROOT", "", "shaders\\BasicUITextureShader.hlsl");

	// Create the vertex shader.
	if( !MGR_SHADER().GetShader<VertexShader>( gsc_UITextureVertexShaderName, gsp_defaultVS)) {
		gsp_defaultVS = new VertexShader();

		gsp_defaultVS->LoadShader( shaderPath, nullptr, g_vertPos3fColour4ubTex2fDesc, g_vertPos3fColour4ubTex2fSize);

		gsp_defaultVS->FindCBuffer( "GlobalVars", &gs_slotCBufferGlobalVars);
		gsp_defaultVS->FindShaderVar( gs_slotCBufferGlobalVars, "g_WVP",			SVT_FLOAT4x4, &gs_slotWVP);
		gsp_defaultVS->FindShaderVar( gs_slotCBufferGlobalVars, "g_imageHeight",	SVT_FLOAT, &m_slotTextureHeight);
		gsp_defaultVS->FindShaderVar( gs_slotCBufferGlobalVars, "g_imageWidth",		SVT_FLOAT, &m_slotTextureWidth);

		gsp_defaultVS->CreateCBuffer( gs_slotCBufferGlobalVars);

		MGR_SHADER().AddShader<VertexShader>( gsc_UITextureVertexShaderName, gsp_defaultVS);
	}
	
	// Create the pixel shader.
	if( !MGR_SHADER().GetShader<PixelShader>( gsc_UITexturePixelShaderName, gsp_defaultPS)) {
		gsp_defaultPS = new PixelShader();

		gsp_defaultPS->LoadShader( shaderPath, nullptr);

		gsp_defaultPS->FindCBuffer( "GlobalVars", &gs_slotCBufferGlobalVars);
		gsp_defaultPS->FindShaderVar( gs_slotCBufferGlobalVars, "g_colour", SVT_FLOAT4, &gs_slotColour);

		gsp_defaultPS->CreateCBuffer( gs_slotCBufferGlobalVars);

		gsp_defaultPS->FindTexture( "g_texture", &gs_slotTexture);
		gsp_defaultPS->FindSampler( "g_sampler", &gs_slotSampler);

		MGR_SHADER().AddShader<PixelShader>( gsc_UITexturePixelShaderName, gsp_defaultPS);
	}
}