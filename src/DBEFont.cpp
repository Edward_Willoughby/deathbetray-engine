
#include "DBEFont.h"

#include <d3d11.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "DBEApp.h"
#include "DBEFontPixelShader.h"
#include "DBEFontVertexShader.h"
#include "DBEGraphicsHelpers.h"
#include "DBEShader.h"
#include "DBETextureMgr.h"
#include "DBEUITexture.h"
#include "DBEVertexTypes.h"

using namespace DirectX;
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

static const s32 MAX_NUM_FONTS = 12;
static const char* gs_tempFonts[MAX_NUM_FONTS] = { nullptr };

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

static const int NUM_CHARS = 100;
static const int NUM_VTXS = NUM_CHARS * 4;

static DBE::FontVertexShader* gsp_VS = nullptr;
static DBE::FontPixelShader* gsp_PS = nullptr;

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

static HFONT CreateGDIFont(HDC hDC, const char *pFontName, int height, uint32_t fontCreateFlags)
{
	LOGFONT lf;

	lf.lfHeight = -MulDiv(height, GetDeviceCaps(hDC, LOGPIXELSY), 72);
	lf.lfWidth = 0;
	lf.lfEscapement = 0;
	lf.lfOrientation = 0;
	lf.lfWeight = fontCreateFlags & DBE::Font::CREATE_BOLD ? FW_BOLD : FW_NORMAL;
	lf.lfItalic = FALSE;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = DEFAULT_CHARSET;
	lf.lfOutPrecision =	OUT_DEFAULT_PRECIS;
	lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lf.lfQuality = ANTIALIASED_QUALITY;
	lf.lfPitchAndFamily = VARIABLE_PITCH;
	strncpy_s(lf.lfFaceName, sizeof lf.lfFaceName, pFontName, _TRUNCATE);

	HFONT hFont = CreateFontIndirect(&lf);
	return hFont;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

const DBE::Vec2 DBE::Font::Style::DEFAULT_SCALE(1.f, 1.f);
const VertColour DBE::Font::Style::DEFAULT_COLOUR(255, 255, 255, 255);

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

DBE::Font::Style::Style():
scale(DEFAULT_SCALE),
colour(DEFAULT_COLOUR)
{
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

DBE::Font::Style::Style(const VertColour &colourArg):
scale(DEFAULT_SCALE),
colour(colourArg)
{
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

DBE::Font::Style::Style(const VertColour &colourArg, const DBE::Vec2 &scaleArg):
scale(scaleArg),
colour(colourArg)
{
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

struct DBE::Font::Glyph
{
	DBE::Vec2 size;
	DBE::Vec2 texMini;
	DBE::Vec2 texMaxi;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

bool DBE::Font::PaintAlphabet(HDC hDC, int width, int height, DBE::Font::Glyph *pGlyphs)
{
	SIZE size;

	char ch = 'x';
	GetTextExtentPoint32(hDC, &ch, 1, &size);

	int spacing = int(ceilf(size.cy * .3f));

	int x = 0;
	int y = 0;
	LONG lineMaxHeight = 0;//height of tallest glyph on this line

	for (int ch = 32; ch < 127; ++ch)
	{
		char c = (char)ch;
		if (!GetTextExtentPoint32(hDC, &c, 1, &size))
		{
			size.cx = 0;
			size.cy = 0;
		}

		if (x + size.cx + spacing > width)
		{
			x = 0;
			y += lineMaxHeight + 1;//tm.tmHeight + 1;

			lineMaxHeight = 0;
		}

		if (y + size.cy > height)
			return false;

		if (size.cy > lineMaxHeight)
			lineMaxHeight = size.cy;

		if (pGlyphs)
		{
			ExtTextOut(hDC, x, y, ETO_OPAQUE, NULL, &c, 1, NULL);

			Glyph *pGlyph = &pGlyphs[ch - 32];

			pGlyph->texMini.SetX( x / float(width));
			pGlyph->texMini.SetY( y / float(height));

			pGlyph->texMaxi.SetX( (x + size.cx) / float(width));
			pGlyph->texMaxi.SetY( (y + size.cy) / float(height));

			pGlyph->size.SetX( float(size.cx));
			pGlyph->size.SetY( float(size.cy));
		}

		x += size.cx + spacing;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

bool DBE::Font::InstallFont( const char* pFontName)
{
	// Find an empty slot in the array.
	s32 slot( 0);
	for( slot; slot < MAX_NUM_FONTS; ++slot)
		if( gs_tempFonts[slot] == nullptr)
			break;

	// If the array is full, don't add the font.
	if( slot == MAX_NUM_FONTS) {
		DebugTrace( "Too many temporary fonts; didn't install: %s", pFontName);
		return false;
	}

	if( AddFontResource( pFontName) != 0) {
		gs_tempFonts[slot] = pFontName;
	}
	else {
		DebugTrace( "Font couldn't be installed: %s", pFontName);
		DBE_Assert( true);
		return false;
	}

	return true;
}

void DBE::Font::UninstallFonts()
{
	for( s32 i( 0); i < MAX_NUM_FONTS; ++i) {
		if( gs_tempFonts[i] == nullptr)
			break;

		if( !RemoveFontResource( gs_tempFonts[i]))
			DebugTrace( "Font didn't release correctly: %s", gs_tempFonts[i]);
	}
}

void DBE::Font::Shutdown()
{
	Font::UninstallFonts();

	MGR_SHADER().RemoveShader<FontVertexShader>( gsp_VS);
	MGR_SHADER().RemoveShader<FontPixelShader>( gsp_PS);
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

DBE::Font *DBE::Font::CreateByName(const char *pTextureName, const char *pFontName, int height, uint32_t createFlags)
{
	HFONT hFont = NULL;

	HBITMAP hBitmap = NULL;

	HDC hDC = CreateCompatibleDC(NULL);
	SetMapMode(hDC, MM_TEXT);
	SaveDC(hDC);

	Font *pFont = NULL;

	Glyph *pGlyphs = NULL;

	Texture *pTexture = new Texture();

	ID3D11Buffer *pVB = NULL;
	ID3D11Buffer *pIB = NULL;

	{
		// Try to create font.
		hFont = CreateGDIFont(hDC, pFontName, height, createFlags);
		if (!hFont)
			goto done;//font creation failed.

		SelectObject(hDC, hFont);

		// Decide on reasonable size for texture.
		int texWidth = 128, texHeight = 128;

		while (!PaintAlphabet(hDC, texWidth, texHeight, NULL))
		{
			texWidth *= 2;
			texHeight *= 2;

			if (texWidth == 4096 || texHeight == 4096)
				goto done;//font too large.
		}

		// Create GDI bitmap for texture.
		BITMAPINFO bmi;
		memset(&bmi, 0, sizeof bmi);

		bmi.bmiHeader.biSize = sizeof bmi.bmiHeader;
		bmi.bmiHeader.biWidth = texWidth;
		bmi.bmiHeader.biHeight = -texHeight;//-ve = top down
		bmi.bmiHeader.biPlanes = 1;
		bmi.bmiHeader.biCompression = BI_RGB;
		bmi.bmiHeader.biBitCount = 32;

		DWORD *pDIBits;

		hBitmap = CreateDIBSection(hDC, &bmi, DIB_RGB_COLORS, (void **)&pDIBits, NULL, 0);
		if (!hBitmap)
			goto done;

		// Paint font into GDI bitmap and this time store off the texture
		// coordinates for each glyph.
		pGlyphs = new Glyph[96];

		SelectObject(hDC, hBitmap);

		SetTextColor(hDC, RGB(255, 255, 255));
		SetBkColor(hDC, RGB(0, 0, 0));
		SetTextAlign(hDC, TA_TOP);

		PaintAlphabet(hDC, texWidth, texHeight, pGlyphs);

		// Fix up bitmap data - the font shape should be in the alpha channel,
		// and the RGB channels should be all white.
		GdiFlush();

		for (int i = 0; i < texWidth * texHeight; ++i)
			pDIBits[i] = ((pDIBits[i] & 0xFF) << 24) | 0x00FFFFFF;

		// Create D3D texture for that.
		D3D11_TEXTURE2D_DESC td;
		td.Width = texWidth;
		td.Height = texHeight;
		td.MipLevels = 1;
		td.ArraySize = 1;
		td.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		td.SampleDesc.Count = 1;
		td.SampleDesc.Quality = 0;
		td.Usage = D3D11_USAGE_IMMUTABLE;
		td.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		td.CPUAccessFlags = 0;
		td.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA srd;
		srd.pSysMem = pDIBits;
		srd.SysMemPitch = texWidth * 4;
		srd.SysMemSlicePitch = 0;

		D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
		srvd.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		srvd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvd.Texture2D.MostDetailedMip = 0;
		srvd.Texture2D.MipLevels = 1;

		if (!MGR_TEXTURE().Create2DTexture(pTexture, pTextureName, &td, &srd, &srvd, GET_APP()->GetSamplerState( true)))
			goto done;
	}

	// Create IB
	{
		uint16_t ibData[NUM_CHARS * 6];

		for (int i = 0; i < NUM_CHARS; ++i)
		{
			ibData[i * 6 + 0] = uint16_t(i * 4 + 0);
			ibData[i * 6 + 1] = uint16_t(i * 4 + 1);
			ibData[i * 6 + 2] = uint16_t(i * 4 + 2);

			ibData[i * 6 + 3] = uint16_t(i * 4 + 1);
			ibData[i * 6 + 4] = uint16_t(i * 4 + 3);
			ibData[i * 6 + 5] = uint16_t(i * 4 + 2);
		}

		pIB = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof ibData, ibData);
		if (!pIB)
			goto done;
	}

	// Create VB
	{
		pVB = CreateBuffer( GET_APP()->mp_D3DDevice, NUM_VTXS * sizeof( VertPos3fColour4ubTex2f), D3D11_USAGE_DYNAMIC, D3D11_BIND_VERTEX_BUFFER, D3D11_CPU_ACCESS_WRITE, NULL);
		if (!pVB)
			goto done;
	}

	// Only load the shaders in once as all fonts will use the same shaders.
	if( gsp_VS == nullptr && gsp_PS == nullptr) {
		if( !MGR_SHADER().GetShader<FontVertexShader>( gsc_fontVertexShaderName, gsp_VS)) {
			gsp_VS = new FontVertexShader();
			gsp_VS->Init();
			MGR_SHADER().AddShader<FontVertexShader>( gsc_fontVertexShaderName, gsp_VS);
		}

		if( !MGR_SHADER().GetShader<FontPixelShader>( gsc_fontPixelShaderName, gsp_PS)) {
			gsp_PS = new FontPixelShader();
			gsp_PS->Init();
			MGR_SHADER().AddShader<FontPixelShader>( gsc_fontPixelShaderName, gsp_PS);
		}
	}

	// That seemed to work; collate it all and make a CommonFont.

	pFont = new DBE::Font;

	pFont->m_pGlyphs = pGlyphs;
	pGlyphs = NULL;

	pFont->m_pTexture = pTexture;

	pFont->m_pIB = pIB;
	pIB = NULL;

	pFont->m_pVB = pVB;
	pVB = NULL;

done:
	if( pFont->m_pTexture == nullptr)
		MGR_TEXTURE().DeleteTexture(pTexture);

	ReleaseCOM(pVB);
	ReleaseCOM(pIB);

	if (hDC)
	{
		RestoreDC(hDC, -1);

		DeleteDC(hDC);
		hDC = NULL;
	}

	if (hBitmap)
	{
		DeleteObject(hBitmap);
		hBitmap = NULL;
	}

	if (hFont)
	{
		DeleteObject(hFont);
		hFont = NULL;
	}

	delete[] pGlyphs;
	pGlyphs = NULL;

	return pFont;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

DBE::Font::Font():
m_pGlyphs(NULL),
m_pTexture(NULL),
m_pIB(NULL),
m_pVB(NULL)
{
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

DBE::Font::~Font()
{
	ReleaseCOM( m_pVB);
	ReleaseCOM( m_pIB);

	MGR_TEXTURE().DeleteTexture(m_pTexture);

	delete[] m_pGlyphs;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

static const DBE::Font::Style DEFAULT_STYLE;

void DBE::Font::DrawString2D(const DBE::Vec3 &pos, const DBE::Vec3& rot, float scale, const DBE::Font::Style *pStyle, const char *pStr)
{
	// Divide the position by the screen width and height to make it screen coordinates.
	DBE::Vec3 coords( pos.GetX() * SCREEN_WIDTH, pos.GetY() * SCREEN_HEIGHT, pos.GetZ());

	// The text needs to be scaled because it's a bit weird... can't be bothered to figure out why
	// at the moment; this is a 'temporary' fix.
	float x( scale / SCREEN_WIDTH);
	float y( scale / SCREEN_HEIGHT);

	GET_APP()->m_matWorld = this->CalculateWorldMatrix( rot, Vec3( x, y, 1.0f));

	this->PreRender( coords, pStyle, pStr);
}

void DBE::Font::DrawStringf2D(const DBE::Vec3 &pos, const DBE::Vec3& rot, float scale, const DBE::Font::Style *pStyle, const char *pFmt, ...)
{
	char aStr[1000];

	va_list v;
	va_start(v, pFmt);
	_vsnprintf_s(aStr, sizeof aStr, _TRUNCATE, pFmt, v);
	va_end(v);

	this->DrawString2D( pos, rot, scale, pStyle, aStr);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void DBE::Font::DrawString3D( const DBE::Vec3 &pos, const DBE::Vec3& rot, float scale, const DBE::Font::Style* pStyle, const char* pStr) {
	// The text needs to be scaled because it's a bit weird... can't be bothered to figure out why
	// at the moment; this is a 'temporary' fix.
	//static float shrink = 0.1f;
	//float s( scale * shrink);

	GET_APP()->m_matWorld = this->CalculateWorldMatrix( rot, Vec3( scale));

	this->PreRender( pos, pStyle, pStr);
}

void DBE::Font::DrawStringf3D( const DBE::Vec3 &pos, const DBE::Vec3& rot, float scale, const DBE::Font::Style* pStyle, const char* pFmt, ...) {
	char aStr[1000];

	va_list v;
	va_start( v, pFmt);
	_vsnprintf_s(aStr, sizeof aStr, _TRUNCATE, pFmt, v);
	va_end(v);

	this->DrawString3D( pos, rot, scale, pStyle, aStr);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

float DBE::Font::GetFontHeight() const {
	float height( 0.0f);

	// Get the tallest glyph in this font.
	for( s32 i( 0); i < 96; ++i)
		if( m_pGlyphs[i].size.GetY() > height)
			height = m_pGlyphs[i].size.GetY();

	return height;
}

void DBE::Font::CalculateTextDimensions( const char* text, const DBE::Vec3& pos, DBE::Vec2& sMin, DBE::Vec2& sMax) const {
	float rowHeight( 0);
	float rowWidest( 0);
	float glyphWidest( 0);
	s32 rowCount( 0);

	rowHeight = this->GetFontHeight();

	s32 textLength( strlen( text));
	float thisRowsWidth( 0);
	for( s32 i( 0); i < textLength; ++i) {
		s32 j( text[i]);

		// If it's a character that can't be output...
		if( j < 32 || j >= 127) {
			// If it's a new line character...
			if( j == 10) {
				if( thisRowsWidth > rowWidest)
					rowWidest = thisRowsWidth;

				++rowCount;
			}

			continue;
		}

		thisRowsWidth += m_pGlyphs[j - 32].size.GetX();
	}

	if( thisRowsWidth > 0.0f) {
		if( thisRowsWidth > rowWidest)
			rowWidest = thisRowsWidth;

		++rowCount;
	}

	// If there are no rows of text (i.e. the text length is 0, or it's full of un-writable
	// characters) then return dimensions that can't collide with the cursor.
	if( rowCount == 0) {
		sMin = Vec2( -1.0f, -1.0f);
		sMax = Vec2( -1.0f, -1.0f);
		return;
	}

	// Convert the dimensions into screen coordinates distances (because the position should
	// already be in screen coordinates.
	rowHeight = rowHeight / SCREEN_HEIGHT;
	rowWidest = rowWidest / SCREEN_WIDTH;

	sMin = pos.GetXY() + Vec2( 0.0f, 0.0f);
	sMax = pos.GetXY() + Vec2( rowWidest, rowHeight * float( rowCount));
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void DBE::Font::RenderFont() {
	static DBE::UITexture* p_UITexture = new DBE::UITexture( m_pTexture);

	p_UITexture->Render( 0.0f);
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

DBE::Matrix4 DBE::Font::CalculateWorldMatrix( const DBE::Vec3& rot, const DBE::Vec3& scale) {
	Matrix4 matRot = XMMatrixRotationRollPitchYaw( rot.GetX(), rot.GetY(), rot.GetZ());
	Matrix4 matScale = XMMatrixScaling( scale.GetX(), scale.GetY(), scale.GetZ());

	return XMMatrixMultiply( matRot, matScale);
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

void DBE::Font::PreRender( const DBE::Vec3& startPos, const DBE::Font::Style* pStyle, const char* pStr) {
	HRESULT hr;

	ID3D11DeviceContext *pContext = GET_APP()->mp_D3DDeviceContext;
	VertPos3fColour4ubTex2f *pVtxs = NULL;
	int numChars = 0;

	//GET_APP()->SetRasteriserState( false, false);
	//GET_APP()->SetBlendState( true);

	Vec3 pos = startPos.GetVector();

	// Use the default style if one wasn't specified.
	if (!pStyle)
		pStyle = &DEFAULT_STYLE;

	for (size_t chIdx = 0; pStr[chIdx] != 0; ++chIdx)
	{
		int c = pStr[chIdx];

		if (c < 32 || c >= 127) {
			// If it's a '\n' then move the position of the next characters down and reset the x
			// coordinate.
			if( c == 10) {
				pos.SetX( startPos.GetX());
				pos.SetY( pos.GetY() - m_pGlyphs[0].size.GetY());
			}

			continue;  // can't print this char
		}

		const Glyph *pGlyph = &m_pGlyphs[c - 32];

		if (numChars >= NUM_CHARS)
		{
			assert(numChars == NUM_CHARS);

			pContext->Unmap(m_pVB, 0);

			this->Render( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, m_pVB, m_pIB, numChars * 6, m_pTexture);

			pVtxs = NULL;
			numChars = 0;
		}

		if (!pVtxs)
		{
			D3D11_MAPPED_SUBRESOURCE ms;
			hr = pContext->Map(m_pVB, 0, D3D11_MAP_WRITE_DISCARD, 0, &ms);
			if (FAILED(hr))
			{
				// erm...
				return;
			}

			pVtxs = static_cast<VertPos3fColour4ubTex2f *>(ms.pData);
			numChars = 0;
		}

		VertPos3fColour4ubTex2f *pVtx = &pVtxs[numChars * 4];
		++numChars;

		pVtx->pos.SetX( pos.GetX());
		pVtx->pos.SetY( pos.GetY());
		pVtx->pos.SetZ( pos.GetZ());
		pVtx->colour = pStyle->colour;
		pVtx->tex.SetX( pGlyph->texMini.GetX());
		pVtx->tex.SetY( pGlyph->texMaxi.GetY());
		++pVtx;

		pVtx->pos.SetX( pos.GetX() + pGlyph->size.GetX() * pStyle->scale.GetX());
		pVtx->pos.SetY( pos.GetY());
		pVtx->pos.SetZ( pos.GetZ());
		pVtx->colour = pStyle->colour;
		pVtx->tex.SetX( pGlyph->texMaxi.GetX());
		pVtx->tex.SetY( pGlyph->texMaxi.GetY());
		++pVtx;

		pVtx->pos.SetX( pos.GetX());
		pVtx->pos.SetY( pos.GetY() + pGlyph->size.GetY() * pStyle->scale.GetY());
		pVtx->pos.SetZ( pos.GetZ());
		pVtx->colour = pStyle->colour;
		pVtx->tex.SetX( pGlyph->texMini.GetX());
		pVtx->tex.SetY( pGlyph->texMini.GetY());
		++pVtx;

		pVtx->pos.SetX( pos.GetX() + pGlyph->size.GetX() * pStyle->scale.GetX());
		pVtx->pos.SetY( pos.GetY() + pGlyph->size.GetY() * pStyle->scale.GetY());
		pVtx->pos.SetZ( pos.GetZ());
		pVtx->colour = pStyle->colour;
		pVtx->tex.SetX( pGlyph->texMaxi.GetX());
		pVtx->tex.SetY( pGlyph->texMini.GetY());
		++pVtx;

		pos.SetX( pos.GetX() + pGlyph->size.GetX() * pStyle->scale.GetX());
	}

	if (pVtxs)
		pContext->Unmap(m_pVB, 0);

	if (numChars > 0)
		this->Render( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, m_pVB, m_pIB, numChars * 6, m_pTexture);
}

void DBE::Font::Render( D3D11_PRIMITIVE_TOPOLOGY topology, ID3D11Buffer *pVertexBuffer, ID3D11Buffer *pIndexBuffer, unsigned numItems, Texture *pTexture) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	if( gsp_VS->mp_CBuffers[gsp_VS->m_slotCBufferGlobalVars]) {
		D3D11_MAPPED_SUBRESOURCE vsMap;
		if( !gsp_VS->mp_CBuffers[gsp_VS->m_slotCBufferGlobalVars] || FAILED( p_context->Map( gsp_VS->mp_CBuffers[gsp_VS->m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &vsMap)))
			vsMap.pData = nullptr;

		DBE::Matrix4 wvp = GET_APP()->GetWVP();

		// Send the WVP matrix to the shader.
		gsp_VS->SetCBufferMatrix( vsMap, gsp_VS->m_slotWVP, wvp);

		// Do... something to the shader maps.
		if( vsMap.pData)
			p_context->Unmap( gsp_VS->mp_CBuffers[gsp_VS->m_slotCBufferGlobalVars], 0);

		// Set the constant buffers for the vertex and pixel shaders.
		if( gsp_VS->mp_CBuffers[gsp_VS->m_slotCBufferGlobalVars]) {
			ID3D11Buffer* p_constBuffers[1] = { gsp_VS->mp_CBuffers[gsp_VS->m_slotCBufferGlobalVars] };
			p_context->VSSetConstantBuffers( gsp_VS->m_slotCBufferGlobalVars, 1, p_constBuffers);
		}

		p_context->PSSetShaderResources( gsp_PS->m_slotTexture, 1, &pTexture->mp_textureView);
	}

	ID3D11SamplerState* apTextureSampler[1] = { pTexture->mp_samplerState };
	p_context->PSSetSamplers( gsp_PS->m_slotSampler, 1, apTextureSampler);

	p_context->VSSetShader( gsp_VS->mp_VS, nullptr, 0);
	p_context->IASetInputLayout( gsp_VS->mp_IL);

	p_context->PSSetShader( gsp_PS->mp_PS, nullptr, 0);

	p_context->IASetPrimitiveTopology( topology);

	ID3D11Buffer* ap_vertBuffer[1]	= { pVertexBuffer };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	p_context->DrawIndexed( numItems, 0, 0);
}