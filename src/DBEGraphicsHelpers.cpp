/********************************************************************
*	Function definitions for the utility functions in the GraphicsHelpers header.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEGraphicsHelpers.h"

#include <d3d11.h>

#include <fstream>

#include "DBEUtilities.h"
/********************************************************************
*	Defines, constants, namespaces and local variables.
********************************************************************/
using namespace DirectX;


/*******************************************************************/
/**
* Creates a vertex buffer.
*
* @param p_device		:: Pointer to the DirectX 11 Device.
* @param sizeBytes		:: Size of the contents.
* @param p_initialData	:: Void pointer to the contents that will be converted into a buffer.
*
* @return A pointer to the vertex buffer.
*/
ID3D11Buffer* CreateImmutableVertexBuffer( ID3D11Device* p_device, UINT sizeBytes, const void* p_initialData)
{
	return CreateBuffer( p_device, sizeBytes, D3D11_USAGE_IMMUTABLE, D3D11_BIND_VERTEX_BUFFER, 0, p_initialData);
}

/**
* Creates an index buffer.
*
* @param p_device		:: Pointer to the DirectX 11 Device.
* @param sizeBytes		:: Size of the contents.
* @param p_initialData	:: Void pointer to the contents that will be converted into a buffer.
*
* @return A pointer to the index buffer.
*/
ID3D11Buffer* CreateImmutableIndexBuffer( ID3D11Device* p_device, UINT sizeBytes, const void* p_initialData)
{
	return CreateBuffer( p_device, sizeBytes, D3D11_USAGE_IMMUTABLE, D3D11_BIND_INDEX_BUFFER, 0, p_initialData);
}

/**
* Creates a buffer.
*
* @param p_device		:: Pointer to the DirectX 11 Device.
* @param sizeBytes		:: Size of the contents.
* @param usage			:: Determines how the buffer can be used.
* @param bindFlags		:: Specifies what the buffer will be used for.
* @param cpuAccessFlags	:: I have no idea...
* @param p_initialData	:: Void pointer to the contents that will be converted into a buffer.
*
* @return A pointer to the index buffer.
*/
ID3D11Buffer* CreateBuffer( ID3D11Device* p_device, UINT sizeBytes, D3D11_USAGE usage, UINT bindFlags, UINT cpuAccessFlags, const void* p_initialData)
{
	// Early out if the size of the data is zero.
	if( sizeBytes == 0)
		return nullptr;

	D3D11_BUFFER_DESC desc;
	desc.ByteWidth				= sizeBytes;
	desc.Usage					= usage;
	desc.BindFlags				= bindFlags;
	desc.CPUAccessFlags			= cpuAccessFlags;
	desc.MiscFlags				= 0;
	desc.StructureByteStride	= 0;	// "structure" here is a D3D notion. The value is correctly zero.

	D3D11_SUBRESOURCE_DATA data;
	data.pSysMem			= p_initialData;
	data.SysMemPitch		= 0;
	data.SysMemSlicePitch	= 0;

	ID3D11Buffer* p_buffer( nullptr);
	HRESULT hr = p_device->CreateBuffer( &desc, p_initialData ? &data : nullptr, &p_buffer);
	if( FAILED( hr)) {
		char buffer[256];
		sprintf_s( buffer, 256, "Buffer failed to be created.\n\nError code: %X", hr);
		MessageBox( nullptr, buffer, "Buffer Creation Failed", MB_OK | MB_ICONERROR);

		return nullptr;
	}

	return p_buffer;
}