/********************************************************************
*	Function definitions for the Movable class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEMovable.h"

#include "DBEApp.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Constructor.
*/
DBE::Movable::Movable()
	: m_position( 0.0f, 0.0f, 0.0f)
	, m_rotation( 0.0f, 0.0f, 0.0f)
	, m_scale( 1.0f, 1.0f, 1.0f)
	, m_direction( 1.0f, 0.0f, 0.0f)
	, m_velocity( 0.0f, 0.0f, 0.0f)
	, m_flags( RF_Enabled | RF_Visible | RF_Cull | RF_Collidable)
{}

/**
* Destructor.
*/
DBE::Movable::~Movable() {}

/**
* Updates the movable object then calls the 'OnUpdate' function.
*/
bool DBE::Movable::Update( float deltaTime) {
	if( !this->FlagIsSet( RenderableFlags::RF_Enabled))
		return true;

	bool res = this->OnUpdate( deltaTime);

	m_prevPosition	= m_position;
	m_prevRotation	= m_rotation;
	m_prevScale		= m_scale;
	m_prevDirection	= m_direction;

	return res;
}

/**
* Generates the world matrix then calls the 'OnRender' function. This is always the function that
* should be called to render the object (not 'OnRender').
*
* @param deltaTime :: Time taken to render the previous frame.
*/
void DBE::Movable::Render( float deltaTime) {
	// Calculate the world matrix and set it if the object is enabled.
	if( this->FlagIsSet( RenderableFlags::RF_Enabled))
		m_world = this->GetWorldMatrix();

	GET_APP()->m_matWorld = m_world;

	// Only render it if it's visible.
	if( this->FlagIsSet( RenderableFlags::RF_Visible))
		this->OnRender( deltaTime);
}

/**
* Generates the world matrix then calls the 'OnRender' function. This is always the function that
* should be called to render the object (not 'OnRender').
*
* @param deltaTime		:: Time taken to render the previous frame.
* @param parentWorld	:: The world matrix of the 'Movable's parent.
*/
void DBE::Movable::Render( float deltaTime, const DBE::Matrix4& parentWorld) {
	// Calculate the world matrix and set it if the object is enabled.
	if( this->FlagIsSet( RenderableFlags::RF_Enabled)) {
		m_world = this->GetWorldMatrix();

		m_world = XMMatrixMultiply( m_world, parentWorld);
	}

	GET_APP()->m_matWorld = m_world;

	// Only render it if it's visible.
	if( this->FlagIsSet( RenderableFlags::RF_Visible))
		this->OnRender( deltaTime);
}

/**
* Gets the position of the object.
*
* @return Copy of the object's position.
*/
DBE::Vec3 DBE::Movable::GetPosition() const {
	return m_position;
}

/**
* Gets the previous position of the object.
*
* @return Copy of the object's previous position.
*/
DBE::Vec3 DBE::Movable::GetPrevPosition() const {
	return m_prevPosition;
}

/**
* Gets the rotation of the object.
*
* @return Copy of the object's rotation.
*/
DBE::Vec3 DBE::Movable::GetRotation() const {
	return m_rotation;
}

/**
* Gets the previous rotation of the object.
*
* @return Copy of the object's previous rotation.
*/
DBE::Vec3 DBE::Movable::GetPrevRotation() const {
	return m_prevRotation;
}

/**
* Gets the scale of the object.
*
* @return Copy of the object's scale.
*/
DBE::Vec3 DBE::Movable::GetScale() const {
	return m_scale;
}

/**
* Gets the previous scale of the object.
*
* @return Copy of the object's prevous scale.
*/
DBE::Vec3 DBE::Movable::GetPrevScale() const {
	return m_prevScale;
}

/**
* Gets the direction of the object.
*
* @return Copy of the object's direction.
*/
DBE::Vec3 DBE::Movable::GetDirection() const {
	return m_direction;
}

/**
* Gets the previous direction of the object.
*
* @return Copy of the object's previous direction.
*/
DBE::Vec3 DBE::Movable::GetPrevDirection() const {
	return m_prevDirection;
}

/**
* Gets the direction that the camera should be facing.
*/
DBE::Vec3 DBE::Movable::GetCamDirection() const {
	return m_camDirection;
}

/**
* Gets the velocity of the object.
*
* @return Copy of the object's velocity.
*/
DBE::Vec3 DBE::Movable::GetVelocity() const {
	return m_velocity;
}

/**
* Gets the position of the object.
*
* @return Copy of the object's position.
*/
DBE::Vec3* DBE::Movable::GetPositionPointer() {
	return &m_position;
}

/**
* Gets the previous position of the object.
*
* @return Copy of the object's previous position.
*/
DBE::Vec3* DBE::Movable::GetPrevPositionPointer() {
	return &m_prevPosition;
}

/**
* Gets the rotation of the object.
*
* @return Copy of the object's rotation.
*/
DBE::Vec3* DBE::Movable::GetRotationPointer() {
	return &m_rotation;
}

/**
* Gets the previous rotation of the object.
*
* @return Copy of the object's previous rotation.
*/
DBE::Vec3* DBE::Movable::GetPrevRotationPointer() {
	return &m_prevRotation;
}

/**
* Gets the scale of the object.
*
* @return Copy of the object's scale.
*/
DBE::Vec3* DBE::Movable::GetScalePointer() {
	return &m_scale;
}

/**
* Gets the previous scale of the object.
*
* @return Copy of the object's prevous scale.
*/
DBE::Vec3* DBE::Movable::GetPrevScalePointer() {
	return &m_prevScale;
}

/**
* Gets the direction of the object.
*
* @return Copy of the object's direction.
*/
DBE::Vec3* DBE::Movable::GetDirectionPointer() {
	return &m_direction;
}

/**
* Gets the previous direction of the object.
*
* @return Copy of the object's previous direction.
*/
DBE::Vec3* DBE::Movable::GetPrevDirectionPointer() {
	return &m_prevDirection;
}

/**
* Gets the direction that the camera should be facing.
*/
DBE::Vec3* DBE::Movable::GetCamDirectionPointer() {
	return &m_camDirection;
}

/**
* Gets the velocity of the object.
*
* @return Copy of the object's velocity.
*/
DBE::Vec3* DBE::Movable::GetVelocityPointer() {
	return &m_velocity;
}

/**
* Calculates and returns the world matrix of the object.
*
* @param calculate :: Default: True. If true, it will calculate the object's world matrix from the
*						position, rotation and scaling vectors. If false, it will simply return the
*						existing world matrix that is stored by the class.
*
* @return The object's world matrix.
*/
DBE::Matrix4 DBE::Movable::GetWorldMatrix( bool calculate /*= true*/) const {
	if( !calculate)
		return m_world;

	Matrix4 world;

	Matrix4 matTrans =	XMMatrixTranslation( m_position.GetX(), m_position.GetY(), m_position.GetZ());
	Matrix4 matRot =	XMMatrixRotationRollPitchYaw( m_rotation.GetX(), m_rotation.GetY(), m_rotation.GetZ());
	Matrix4 matScale =	XMMatrixScaling( m_scale.GetX(), m_scale.GetY(), m_scale.GetZ());

	world = XMMatrixMultiply( matRot, matScale);
	world = XMMatrixMultiply( world, matTrans);

	return world;
}

/**
* Calculates and returns the world matrix of the object.
*
* @param calculate :: Default: True. If true, it will calculate the object's world matrix from the
*						position, rotation and scaling vectors. If false, it will simply return the
*						existing world matrix that is stored by the class.
*
* @return A pointer to the object's world matrix.
*/
DBE::Matrix4* DBE::Movable::GetWorldMatrixPointer( bool calculate /*= true*/) {
	if( calculate)
		this->GetWorldMatrix( calculate);

	return &m_world;
}

/**
* Sets the world matrix of the object. This is a 'bypass' function and will likely be used in
* conjuction with the 'OnRender' function instead of the 'Render' function so that the world matrix
* is not modified by this class (and instead is controlled higher up the hierarchy).
*
* @param world :: The world matrix of the object.
*/
void DBE::Movable::SetWorldMatrix( DBE::Matrix4& world) {
	m_world = world;
}

/**
* Moves the position to the specified location.
*
* @param x :: The x position to move to.
* @param y :: The y position to move to.
* @param z :: The z position to move to.
*/
void DBE::Movable::MoveTo( float x, float y, float z) {
	this->MoveTo( Vec3( x, y, z));
}

/**
* Moves the position to the specified location.
*
* @param pos :: The x, y, and z values to move to.
*/
void DBE::Movable::MoveTo( const DBE::Vec3& pos) {
	m_position = pos;
}

/**
* Moves the position by the specified amount.
*
* @param x :: The x value to move by.
* @param y :: The y value to move by.
* @param z :: The z value to move by.
*/
void DBE::Movable::MoveBy( float x, float y, float z) {
	this->MoveBy( Vec3( x, y, z));
}

/**
* Moves the position by the specified amount.
*
* @param pos :: The x, y, and z values to move by.
*/
void DBE::Movable::MoveBy( const DBE::Vec3& pos) {
	m_position += pos;
}

/**
* Moves the player forward, using its direction.
*
* @param v :: The amount to move forward.
*/
void DBE::Movable::MoveForward( float v) {
	Matrix4 rot = XMMatrixRotationRollPitchYaw( m_rotation.GetX(), m_rotation.GetY() + DBE_ToRadians( 90.0f), m_rotation.GetZ());
	Vec3 dirToMove = XMVector3Transform( m_direction.GetNormalised().GetXMVector(), rot);

	dirToMove *= v;

	m_position -= dirToMove;
}

/**
* Moves the player backwards, using its direction.
*
* @param v :: The amount to move backwards.
*/
void DBE::Movable::MoveBack( float v) {
	// Does this work?
	//this->MoveForward( -v);

	Matrix4 rot = XMMatrixRotationRollPitchYaw( m_rotation.GetX(), m_rotation.GetY() + DBE_ToRadians( 90.0f), m_rotation.GetZ());
	Vec3 dirToMove = XMVector3Transform( m_direction.GetNormalised().GetXMVector(), rot);

	dirToMove *= v;

	m_position += dirToMove;
}

/**
* Moves the player left (strafe), using its direction.
*
* @param v :: The amount to move forward.
*/
void DBE::Movable::MoveLeft( float v) {
	Matrix4 rot = XMMatrixRotationRollPitchYaw( m_rotation.GetX(), m_rotation.GetY(), m_rotation.GetZ());
	Vec3 dirToMove = XMVector3Transform( m_direction.GetNormalised().GetXMVector(), rot);

	dirToMove *= v;

	m_position -= dirToMove;
}

/**
* Moves the player right (strafe), using its direction.
*
* @param v :: The amount to move forward.
*/
void DBE::Movable::MoveRight( float v) {
	// Does this work?
	//this->MoveLeft( -v);

	Matrix4 rot = XMMatrixRotationRollPitchYaw( m_rotation.GetX(), m_rotation.GetY(), m_rotation.GetZ());
	Vec3 dirToMove = XMVector3Transform( m_direction.GetNormalised().GetXMVector(), rot);

	dirToMove *= v;

	m_position += dirToMove;
}

/**
* Rotates to the specified amount.
*
* @param x :: The x value to rotate to.
* @param y :: The y value to rotate to.
* @param z :: The z value to rotate to.
*/
void DBE::Movable::RotateTo( float x, float y, float z) {
	this->RotateTo( Vec3( x, y, z));
}

/**
* Rotates to the specified amount.
*
* @param rot :: The x, y, and z values to rotate to.
*/
void DBE::Movable::RotateTo( const DBE::Vec3& rot) {
	m_rotation = rot;
}

/**
* Rotates by the specified amount.
*
* @param x :: The x value to rotate by.
* @param y :: The y value to rotate by.
* @param z :: The z value to rotate by.
*/
void DBE::Movable::RotateBy( float x, float y, float z) {
	this->RotateBy( Vec3( x, y, z));
}


/**
* Rotates by the specified amount.
*
* @param rot :: The x, y, and z values to rotate by.
*/
void DBE::Movable::RotateBy( const DBE::Vec3& rot) {
	m_rotation += rot;
}

/**
* Scales to the specified amount.
*
* @param x :: The x value to scale to.
* @param y :: The y value to scale to.
* @param z :: The z value to scale to.
*/
void DBE::Movable::ScaleTo( float x, float y, float z) {
	this->ScaleTo( Vec3( x, y, z));
}

/**
* Scales to the specified amount.
*
* @param scale :: The x, y, and z values to scale to.
*/
void DBE::Movable::ScaleTo( const DBE::Vec3& scale) {
	m_scale = scale;
}

/**
* Scales by the specified amount.
*
* @param x :: The x value to scale by.
* @param y :: The y value to scale by.
* @param z :: The z value to scale by.
*/
void DBE::Movable::ScaleBy( float x, float y, float z) {
	this->ScaleBy( Vec3( x, y, z));
}

/**
* Scales by the specified amount.
*
* @param scale :: The x, y, and z values to scale by.
*/
void DBE::Movable::ScaleBy( const DBE::Vec3& scale) {
	m_scale += scale;
}

/**
* Changes direction to the specified amount.
*
* @param x :: The x value to turn to.
* @param y :: The y value to turn to.
* @param z :: The z value to turn to.
*/
void DBE::Movable::TurnTo( float x, float y, float z) {
	this->TurnTo( Vec3( x, y, z));
}

/**
* Changes direction to the specified amount.
*
* @param rot :: The x, y, and z values to turn to.
*/
void DBE::Movable::TurnTo( const DBE::Vec3& dir) {
	m_direction = dir;
	//m_direction.Normalise();
}

/**
* Changes direction by the specified amount.
*
* @param x :: The x value to turn by.
* @param y :: The y value to turn by.
* @param z :: The z value to turn by.
*/
void DBE::Movable::TurnBy( float x, float y, float z) {
	this->TurnBy( Vec3( x, y, z));
}

/**
* Changes direction by the specified amount.
*
* @param rot :: The x, y, and z values to turn by.
*/
void DBE::Movable::TurnBy( const DBE::Vec3& dir) {
	m_direction += dir;
	//m_direction.Normalise();
}

/**
* Changes the direction of the attached camera to the specified amount.
*
* @param x :: The x value to turn the camera to.
* @param y :: The y value to turn the camera to.
* @param z :: The z value to turn the camera to.
*/
void DBE::Movable::TurnCamDirectionTo( float x, float y, float z) {
	this->TurnCamDirectionTo( Vec3( x, y, z));
}

/**
* Changes the direction of the attached camera to the specified amount.
*
* @param dir :: The x, y, and z values to turn the camera to.
*/
void DBE::Movable::TurnCamDirectionTo( const DBE::Vec3& dir) {
	m_camDirection = dir;
}

/**
* Changes the direction of the attached camera by the specified amount.
*
* @param x :: The x value to turn the camera by.
* @param y :: The y value to turn the camera by.
* @param z :: The z value to turn the camera by.
*/
void DBE::Movable::TurnCamDirectionBy( float x, float y, float z) {
	this->TurnCamDirectionBy( Vec3( x, y, z));
}

/**
* Changes the direction of the attached camera by the specified amount.
*
* @param dir :: The x, y, and z values to turn the camera by.
*/
void DBE::Movable::TurnCamDirectionBy( const DBE::Vec3& dir) {
	m_camDirection += dir;
}

/**
* Set the speed of the object.
*
* @param x :: The horizontal velocity of the object.
* @param y :: The vertical velocity of the object.
* @param z :: The vertical velocity of the object.
*/
void DBE::Movable::SetVelocity( float x, float y, float z) {
	this->SetVelocity( Vec3( x, y, z));
}

/**
* Set the speed of the object.
*
* @param vel :: The new velocity of the object.
*/
void DBE::Movable::SetVelocity( const DBE::Vec3& vel) {
	m_velocity = vel;
}

/**
* Set the speed of the object along the x axis.
*
* @param x :: The x velocity of the object.
*/
void DBE::Movable::SetXVelocity( float x) {
	this->SetVelocity( Vec3( x, m_velocity.GetY(), m_velocity.GetZ()));
}

/**
* Set the speed of the object along the y axis.
*
* @param y :: The y velocity of the object.
*/
void DBE::Movable::SetYVelocity( float y) {
	this->SetVelocity( Vec3( m_velocity.GetX(), y, m_velocity.GetZ()));
}

/**
* Set the speed of the object along the z axis.
*
* @param z :: The z velocity of the object.
*/
void DBE::Movable::SetZVelocity( float z) {
	this->SetVelocity( Vec3( m_velocity.GetX(), m_velocity.GetY(), z));
}

/**
* Toggles a flag: see 'RenderableFlags' enum for information.
*/
void DBE::Movable::ToggleFlag( RenderableFlags flag) {
	m_flags ^= flag;
}

/**
* Gets whether a flag is set: see 'RenderableFlags' enum for information.
*/
bool DBE::Movable::FlagIsSet( RenderableFlags flag) const {
	return m_flags & flag;
}