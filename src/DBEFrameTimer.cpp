/********************************************************************
*	Function definitions for the FrameTimer class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "DBEFrameTimer.h"

#include "DBEApp.h"
/********************************************************************
*	Defines, namespaces, constants, and local variables.
********************************************************************/


/**
* Constructor.
*/
DBE::FrameTimer::FrameTimer()
	: m_secondsPerCount( 0.0f)
	, m_deltaTime( -1.0f)
	, m_startTime( 0)
	, m_prevTime( 0)
	, m_currTime( 0)
	, m_constrictFPS( true)
	, m_desiredFPS( 60)
	, m_slowRenderingFPS( DEFAULT_SLOW_RENDERING_SPEED)
{
	QueryPerformanceFrequency( (LARGE_INTEGER*)&m_performanceFrequency);
	m_secondsPerCount = 1.0f / (double)m_performanceFrequency;

	QueryPerformanceCounter( (LARGE_INTEGER*)&m_startTime);

	// Calculate the approximate length of one frame.
	this->SetFPS( true, 60);
}

/**
* Returns the current F.P.S. of the application.
*
* @return The F.P.S. of the application.
*/
float DBE::FrameTimer::FPS() const {
	return( (float)(1.0f / m_deltaTime));
}

/**
* Returns the time taken to render the last frame.
*
* @return The time taken to render the last frame.
*/
float DBE::FrameTimer::DeltaTime() const {
	return (float)m_deltaTime;
}

/**
* Returns the total time the application has been running for.
*
* @return The total time the application has been running for.
*/
float DBE::FrameTimer::TotalTime() const {
	return (float)((m_currTime - m_startTime) * m_secondsPerCount);
}

/**
* Indicates the start of a new frame (and the end of the previous).
*/
void DBE::FrameTimer::Tick() {
	// Keep looping here until the approximate time for one frame has passed.
	bool slowRendering( !GET_APP()->IsInFocus() && GET_APP()->GetFocusResponse() == FocusResponse::FR_SlowRendering);
	if( m_constrictFPS || slowRendering) {
		while( true) {
			s64 now;
			QueryPerformanceCounter( (LARGE_INTEGER*)&now);
			double delayTimeLeft( (m_prevTime + m_oneFrame) - now);

			if( delayTimeLeft <= 0)
				break;
			if( delayTimeLeft >= m_sleepGap)
				Sleep( 1);
		}
	}
	QueryPerformanceCounter( (LARGE_INTEGER*)&m_currTime);

	// Time difference between this frame and the previous time.
	m_deltaTime = (m_currTime - m_prevTime) * m_secondsPerCount;

	// Prepare for the next frame.
	m_prevTime = m_currTime;

	// Force non-negative. The DXSDK's CDXUTTimer mentions that if the preprocessor goes into a
	// power save mode or we get shuffled to another processor, then 'm_deltaTime' can be negative.
	if( m_deltaTime < 0.0f)
		m_deltaTime = 0.0f;
}

/**
* Restricts the fps to a certain value.
*
* @param constrict	:: Force the frame timer to wait if the frame rate is higher than the desired
*						fps.
* @param fps		:: Default: 60. The maximum frame rate. If the value is 0 then the frame rate
*						will be set to the default (60).
*/
void DBE::FrameTimer::SetFPS( bool constrict, u8 fps /*= 60*/) {
	if( fps == 0)
		fps = 60;

	m_desiredFPS = fps;
	m_constrictFPS = constrict;

	this->SetFrameTimes( m_desiredFPS);
}

/**
* Sets the fps for when the application is out of focus.
*
* @param fps :: The maximum frame rate. If the value is 0 then the frame rate will be set to
*					DEFAULT_SLOW_RENDERING_SPEED.
*/
void DBE::FrameTimer::SetSlowRenderingFPS( u8 fps) {
	if( fps == 0)
		fps = DEFAULT_SLOW_RENDERING_SPEED;

	m_slowRenderingFPS = fps;
}

/**
* Sets the FrameTimer to use slow rendering because the application is not in focus. This function
* should only be used by D3DApp.
*
* @param focus :: Whether the application is in focus or not.
*/
void DBE::FrameTimer::SlowRendering( bool focus) {
	if( focus)
		this->SetFrameTimes( m_desiredFPS);
	else
		this->SetFrameTimes( m_slowRenderingFPS);
}

/**
* Sets how long it should expect one frame and the sleep gap to be.
*
* @param fps :: The fps that should be taken into consideration when doing the calculations.
*/
void DBE::FrameTimer::SetFrameTimes( u8 fps) {
	m_oneFrame = double( m_performanceFrequency / fps);
	m_sleepGap = double( m_performanceFrequency / (fps * 3));
}