/********************************************************************
*	Function definitions for the ShaderMgr class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEShaderMgr.h"

#include "DBEShader.h"
#include "DBEUtilities.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
DBE::ShaderMgr::ShaderMgr() {}

/**
* Destructor.
*/
DBE::ShaderMgr::~ShaderMgr() {}

/**
* Deletes any remaining shaders held by the manager.
*/
void DBE::ShaderMgr::Shutdown() {
	for( s32 i( 0); i < MAX_SHADERS; ++i) {
		if( m_shaders[i].mp_shader == nullptr && m_shaders[i].m_name == '\0')
			continue;

#ifdef _DEBUG
		if( m_shaders[i].m_refCount < 0)
			DebugTrace( "ShaderMgr Warning: Shader released too many times:\n%s\n", m_shaders[i].m_name);
		else if( m_shaders[i].m_refCount > 0)
			DebugTrace( "ShaderMgr Warning: Shader %s still has %d reference%s.\n", m_shaders[i].m_name, m_shaders[i].m_refCount, m_shaders[i].m_refCount == 1 ? "" : "s");
#endif	// #ifdef _DEBUG
	}
}

/**
* Gets an existing shader from the manager.
*
* @param name		:: The unique name of the shader.
* @param p_shader	:: Pointer to the shader. If the shader wasn't found, this is set to nullptr.
*
* @return True if the manager found a shader using the specified name.
*/
//bool DBE::ShaderMgr::GetShader( const char* name, DBE::Shader** p_shader) {
//	s32 index( this->FindShader( name));
//
//	// If the shader doesn't exist...
//	if( index == -1)
//		return false;
//
//	*p_shader = m_shaders[index].mp_shader;
//	++m_shaders[index].m_refCount;
//	return true;
//}

/*
* Adds a shader to the manager.
*
* @param name		:: The unique name of the shader.
* @param p_shader	:: Pointer to the shader to be added.
*
* @return True if the shader was added to the manager.
//*/
//bool DBE::ShaderMgr::AddShader( const char* name, DBE::Shader* p_shader) {
//	s32 index( this->FindShader( name));
//
//	if( index == -1) {
//		DebugTrace( "ShaderMgr Warning: Shader already exists: %s\n", name);
//		return false;
//	}
//
//	// Find an empty slot; if there isn't one then either slim the number of shaders in the program
//	// or increase the limit.
//	index = this->FindEmptySlot();
//	DBE_AssertWithMsg( index == -1, "ShaderMgr Error: Too many shaders.\n", 0);
//
//	m_shaders[index].mp_shader = p_shader;
//	strcpy_s( m_shaders[index].m_name, 256, name);
//	m_shaders[index].m_refCount = 1;
//
//	return true;
//}

/**
* Removes a reference to a shader.
*
* @param p_shader :: Pointer to the shader.
*/
//void DBE::ShaderMgr::RemoveShader( DBE::Shader* &p_shader) {
//	s32 index( this->FindShader( p_shader));
//
//	// Early-out if the shader doesn't exist.
//	if( index == -1) {
//		DebugTrace( "ShaderMgr Warning: Attempting to delete non-existant shader.\n");
//		return;
//	}
//
//	// Currently I'm not deleting shaders when they become unreferenced as shaders are likely to
//	// 'come-and-go' regarding usage.
//
//	--m_shaders[index].m_refCount;
//	p_shader = nullptr;
//}

/**
* Finds a shader in the manager.
*
* @param name :: The unique name of the shader.
*
* @return The position of the shader in 'm_shaders' array.
*/
s32 DBE::ShaderMgr::FindShader( const char* name) {
	s32 index( -1);

	// Early-out if the name is invalid.
	if( name == nullptr)
		return index;

	for( s32 i( 0); i < MAX_SHADERS; ++i) {
		if( strcmp( name, m_shaders[i].m_name) != 0)
			continue;

		index = i;
		break;
	}

	return index;
}

/**
* Finds a shader in the manager.
*
* @param p_shader :: Pointer to the shader.
*
* @return The position of the shader in 'm_shaders' array.
*/
s32 DBE::ShaderMgr::FindShader( const DBE::Shader* p_shader) {
	s32 index( -1);

	// Early-out if the shader doesn't exist.
	if( p_shader == nullptr)
		return index;

	for( s32 i( 0); i < MAX_SHADERS; ++i) {
		if( p_shader != m_shaders[i].mp_shader)
			continue;

		index = i;
		break;
	}

	return index;
}

/**
* Finds an empty slot in the manager to add a new shader to.
*
* @return The position of an empty slot in the 'm_shaders' array.
*/
s32 DBE::ShaderMgr::FindEmptySlot() {
	s32 index( -1);

	for( s32 i( 0); i < MAX_SHADERS; ++i) {
		if( m_shaders[i].mp_shader == nullptr && m_shaders[i].m_name[0] == '\0') {
			index = i;
			break;
		}
	}

	return index;
}

/**
* Deletes the shader at the array position.
*
* @param index :: The position of a shader in the 'm_shaders' array.
*/
void DBE::ShaderMgr::DeleteShader( s32 index) {
	SafeDelete( m_shaders[index].mp_shader);
	m_shaders[index].m_name[0] = '\0';
	m_shaders[index].m_refCount = 0;
}