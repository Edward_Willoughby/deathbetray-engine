/********************************************************************
*	Function definitions for the FontVertexShader class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEFontVertexShader.h"

#include "DBEUtilities.h"
#include "DBEVertexTypes.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
DBE::FontVertexShader::FontVertexShader() {
	
}

/**
* Destructor.
*/
DBE::FontVertexShader::~FontVertexShader() {
	
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool DBE::FontVertexShader::Init() {
	char shaderPath[256];
	DebuggerDependentPath( shaderPath, sizeof( shaderPath), "DBEngine_ROOT", "", "shaders\\FontShader.hlsl");
	this->LoadShader( shaderPath, g_vertPos3fColour4ubTex2fMacros, g_vertPos3fColour4ubTex2fDesc, g_vertPos3fColour4ubTex2fSize);

	this->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_WVP", SVT_FLOAT4x4, &m_slotWVP);

	if( !this->CreateCBuffer( m_slotCBufferGlobalVars))
		return false;

	return true;
}