/********************************************************************
*	Function definitions for the VertexShader class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEVertexShader.h"

#include "DBEUtilities.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
DBE::VertexShader::VertexShader() {
	
}

/**
* Destructor.
*/
DBE::VertexShader::~VertexShader() {
	this->DeleteShader();
}

void DBE::VertexShader::LoadShader( LPCSTR p_fileName, const D3D_SHADER_MACRO* p_macros, const D3D11_INPUT_ELEMENT_DESC* p_vertDesc, UINT vertDescSize, LPCSTR p_entryPoint /*= "VSMain"*/, LPCSTR p_profile /*= "vs_5_0"*/) {
	ShaderParams p;
	p.fileName = p_fileName;
	p.p_macros = p_macros;
	p.entryPoint = p_entryPoint;
	p.profile = p_profile;
	p.p_vertDesc = p_vertDesc;
	p.vertDescSize = vertDescSize;

	this->LoadShader( &p);
}

void DBE::VertexShader::LoadShader( const DBE::ShaderParams* p_params) {
	this->CompileShader( p_params, &mp_VS, &mp_IL, nullptr);
}

/**
* 
*
* @param :: 
*
* @return 
*/
void DBE::VertexShader::DeleteShader() {
	Shader::DeleteShader();

	ReleaseCOM( mp_VS);
	ReleaseCOM( mp_IL);
}