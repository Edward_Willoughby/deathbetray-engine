/********************************************************************
*	Function definitions for the PixelShader class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEPixelShader.h"

#include "DBEUtilities.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
DBE::PixelShader::PixelShader() {
	
}

/**
* Destructor.
*/
DBE::PixelShader::~PixelShader() {
	this->DeleteShader();
}

/**
* 
*
* @param :: 
*
* @return 
*/
void DBE::PixelShader::LoadShader( LPCSTR p_fileName, const D3D_SHADER_MACRO* p_macros, LPCSTR p_entryPoint /*= "PSMain"*/, LPCSTR p_profile /*= "ps_5_0"*/) {
	ShaderParams p;
	p.fileName = p_fileName;
	p.p_macros = p_macros;
	p.entryPoint = p_entryPoint;
	p.profile = p_profile;

	this->LoadShader( &p);
}

void DBE::PixelShader::LoadShader( const DBE::ShaderParams* p_params) {
	this->CompileShader( p_params, nullptr, nullptr, &mp_PS);
}

/**
* 
*
* @param :: 
*
* @return 
*/
void DBE::PixelShader::DeleteShader() {
	Shader::DeleteShader();

	ReleaseCOM( mp_PS);
}