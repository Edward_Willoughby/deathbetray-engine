/********************************************************************
*	Function definitions for the Win7TaskBarButtonMgr class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "DBEWin7TaskBarButtonMgr.h"

#include "DBEUtilities.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
DBE::Win7TaskBarButtonMgr::Win7TaskBarButtonMgr()
	: m_hWnd( 0)
	, m_initialised( false)
	, mp_taskbar( nullptr)
	, m_currentState( TBPF_NOPROGRESS)
	, m_completed( 0)
	, m_total( 0)
{
	//CoInitialize( nullptr);
}

/**
* Destructor
*/
DBE::Win7TaskBarButtonMgr::~Win7TaskBarButtonMgr() {
	if( mp_taskbar != nullptr)
		mp_taskbar->Release();

	//CoUninitialize();
}

/**
* Initialises the manager; MUST be called before any other functions.
*
* @param hWnd :: The handle of the window who's taskbar button you want access to.
*/
void DBE::Win7TaskBarButtonMgr::Init( HWND hWnd) {
	if( !SUCCEEDED( CoCreateInstance( CLSID_TaskbarList, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&mp_taskbar))))
		return;

	if( mp_taskbar == nullptr)
		return;

	m_hWnd = hWnd;
	m_initialised = true;
}

/**
* Returns whether or not the manager has been initialised with an HWND correctly.
*/
bool DBE::Win7TaskBarButtonMgr::IsInitialised() {
	return m_initialised;
}

/**
* Get the HWND that the manager is linked to.
*/
HWND DBE::Win7TaskBarButtonMgr::GetHWND() const {
	return m_hWnd;
}

/**
* Gets the current state of the progress bar.
*/
TBPFLAG DBE::Win7TaskBarButtonMgr::GetCurrentState() const {
	return m_currentState;
}

/**
* Gets the last 'completed' value sent to the manager.
*/
u64 DBE::Win7TaskBarButtonMgr::GetCompleted() const {
	return m_completed;
}

/**
* Gets the last 'total' value sent to the manager.
*/
u64 DBE::Win7TaskBarButtonMgr::GetTotal() const {
	return m_total;
}

/**
* Sets the progress bar as a percentage of completed/total.
*
* @param completed	:: The amount done.
* @param total		:: The amount left to do.
*/
void DBE::Win7TaskBarButtonMgr::ProgressStart( u64 completed, u64 total) {
	DBE_Assert( this->IsInitialised());

	m_currentState = TBPF_NORMAL;
	m_completed = completed;
	m_total = total;

	mp_taskbar->SetProgressState( m_hWnd, m_currentState);
	mp_taskbar->SetProgressValue( m_hWnd, m_completed, m_total);
}

/**
* Sets the progress bar to be paused. If no values are passed through, it will pause using the
* previously stored values.
*
* @param completed	:: Default: 0. The amount done.
* @param total		:: Default: 0. The amount left to do.
*/
void DBE::Win7TaskBarButtonMgr::ProgressPause( u64 completed /*= 0*/, u64 total /*= 0*/) {
	DBE_Assert( this->IsInitialised());

	m_currentState = TBPF_PAUSED;
	if( completed != 0 && total != 0) {
		m_completed = completed;
		m_total = total;
	}

	mp_taskbar->SetProgressState( m_hWnd, m_currentState);
	mp_taskbar->SetProgressValue( m_hWnd, m_completed, m_total);
}

/**
* Sets the progress bar to be unknown.
*/
void DBE::Win7TaskBarButtonMgr::ProgressIndeterminant() {
	DBE_Assert( this->IsInitialised());

	m_currentState = TBPF_INDETERMINATE;

	mp_taskbar->SetProgressState( m_hWnd, m_currentState);
}

/**
* Sets the progress bar to indicate an error occurred. If no values are passed through, it will
* pause using the previously stored values.
*
* @param completed	:: Default: 0. The amount done.
* @param total		:: Default: 0. The amount left to do.
*/
void DBE::Win7TaskBarButtonMgr::ProgressError( u64 completed /*= 0*/, u64 total /*= 0*/) {
	DBE_Assert( this->IsInitialised());

	m_currentState = TBPF_ERROR;
	if( completed != 0 && total != 0) {
		m_completed = completed;
		m_total = total;
	}

	mp_taskbar->SetProgressState( m_hWnd, m_currentState);
	mp_taskbar->SetProgressValue( m_hWnd, m_completed, m_total);
}

/**
* Removes the progress bar.
*/
void DBE::Win7TaskBarButtonMgr::ProgressDone() {
	DBE_Assert( this->IsInitialised());

	m_currentState = TBPF_NOPROGRESS;

	mp_taskbar->SetProgressState( m_hWnd, m_currentState);
}

/** 
* Make the window's taskbar button flash until the user focusses the window.
*/
void DBE::Win7TaskBarButtonMgr::Flash() {
	DBE_Assert( this->IsInitialised());

	FlashWindow( m_hWnd, true);
}

/**
* Make the window's taskbar button flash a specific number of times. Take note that it can't keep
* track of when the flashing ends when the count is specified.
*
* @param count :: The number of times the task bar button should flash. If a negative number is
*					specified then it will continue to flash indefinitely.
*/
void DBE::Win7TaskBarButtonMgr::FlashCount( u8 count) {
	DBE_Assert( this->IsInitialised());

	FLASHWINFO info;
	info.hwnd = m_hWnd;
	info.uCount = count - 1;
	info.dwFlags = FLASHW_TRAY;
	info.dwTimeout = 0;
	info.cbSize = sizeof( info);

	FlashWindowEx( &info);
}