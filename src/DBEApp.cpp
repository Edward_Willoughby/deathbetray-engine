/********************************************************************
*	Function definitions for the App class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEApp.h"

#include <mmsystem.h>

#include "DBEFont.h"
#include "DBERandom.h"
#include "DBEShader.h"
#include "DBETime.h"
#include "DBEUITexture.h"
/********************************************************************
*	Defines, constants, namespaces and local variabels.
********************************************************************/
using namespace DirectX;


/**
* Constructor for the App class.
*/
DBE::App::App()
	: mp_D3DDebug( nullptr)
	, mp_D3DDevice( nullptr)
	, mp_D3DDeviceContext( nullptr)
	, mp_D3DRenderTargetView( nullptr)
	, mp_D3DDepthStencilView( nullptr)
	, m_hWnd( nullptr)
	, mp_DXGISwapChain( nullptr)
	, mp_D3DDepthStencilBuffer( nullptr)
	, m_clearColour( 0)
	, m_graphicsIsInitialised( false)
	, m_renderTargetsInitialised( false)
	, m_playing( true)
	, m_endScene( false)
	, m_isFullScreen( false)
	, m_screenWidth( 0)
	, m_screenHeight( 0)
	, m_focusResponse( FocusResponse::FR_Pause)
	, m_isInFocus( false)
	, m_cursorIsLocked( false)
	, m_cursorIsDisplayed( true)
	, m_cursorControlIsEnabled( true)
	, m_mouseWheelInfo( 0)
	, mp_defaultFont( 0)
{
	// Initialise the states arrays.
	for( u8 i( 0); i < NUM_BLEND_STATES; ++i)
		mp_blendStates[i] = nullptr;

	for( u8 i( 0); i < NUM_DEPTH_STENCIL_STATES; ++i)
		mp_depthStencilStates[i] = nullptr;

	for( u8 i( 0); i < NUM_RASTERISER_STATES; ++i)
		mp_rasteriserStates[i] = nullptr;

	for( u8 i( 0); i < NUM_SAMPLER_STATES; ++i)
		mp_samplerStates[i] = nullptr;

	// Set a default window size.
	this->SetWindowDimensions( 800, 600);
}

/**
* Destructor for the D3DApp class.
*/
DBE::App::~App() {
	// Everything should be done in the 'HandleShutdownInternal' function.
}

/**
* Initialises the application.
*
* @param clearColour :: The colour to set the back buffer to.
*
* @return True if the application was initialised correctly.
*/
bool DBE::App::Init( u32 clearColour /*= DEFAULT_CLEAR_COLOUR*/, HWND hWnd /*= NULL*/) {
	m_clearColour = clearColour;

	// Load in the default cursor icon.
	m_defaultCursor = LoadCursor( nullptr, IDC_ARROW);

	// Output information about the current build to the output window.
	this->DumpBuildInfo();

	// Perform any pre-intialisation operations (such as setting the window dimensions).
	if( !this->HandlePreInit()) {
		MessageBox( nullptr, "Pre-Initialisation failed.", "Pre-Initialisation Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Get the refresh rate of the computer's monitor.
	if( !this->GetDisplayInfo()) {
		MessageBox( nullptr, "Failed to obtain display settings.", "Display Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Initialise the window.
	if( !this->CreateDisplayWindow( hWnd)) {
		MessageBox( nullptr, "Failed to create display window.", "Win32 Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Exit immediately if DirectX failed to initialise.
	if( !this->StartD3D()) {
		MessageBox( nullptr, "Failed to start D3D.", "D3D Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Initialise the audio manager.
	if( !m_mgrAudio.Init()) {
		MessageBox( nullptr, "Failed to initialise the Audio manager.", "FMOD Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Initialise the network manager.
	if( !m_mgrNetwork.Init()) {
		MessageBox( nullptr, "Failed to initialise the Network manager.", "WinSock Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Initialise the thread pool.
	if( !m_mgrThreadPool.Init()) {
		MessageBox( nullptr, "Failed to initialise the Thread Pool manager.", "Threading Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Create the default font.
	mp_defaultFont = Font::CreateByName( "DBEDefaultFont", "", 10, 0);

	// Set up a 'default' perspective camera... Martyn didn't turn the light off this time :D.
	float fov( 0.25f * (float)XM_PI);
	float aspectRatio( this->GetAspectRatio());
	m_matProj = DirectX::XMMatrixPerspectiveFovLH( fov, aspectRatio, 1.5f, 5000.0f);

	Vec3 pos(	50.0f, 50.0f, -50.0f);
	Vec3 target( 0.0f,  0.0f,   0.0f);
	Vec3 up(	 0.0f,  1.0f,   0.0f);
	m_matView = DirectX::XMMatrixLookAtLH( pos, target, up);
	m_matWorld = DirectX::XMMatrixIdentity();

	// Allow the derived application to initialise before continuing...
	bool res = this->HandleInit();

	// Display the window.
	ShowWindow( m_hWnd, SW_SHOW);
	
	// Initialise the taskbar button manager after the window has been shown (so it can be used
	// straight away).
	m_mgrTaskBarButton.Init( m_hWnd);

	// Calculate the approximate length of a frame.
	QueryPerformanceFrequency( &m_oneFrame);
	m_oneFrame.QuadPart /= 60;

	// If there is more than 'm_sleepGap' time left, sleep for 1ms ('m_sleepGap' itself is rather
	// more than 1ms, because sleeping isn't always very accurate.
	// This cuts down on C.P.U. usage, so that other apps run better, laptop stays cool etc.
	QueryPerformanceCounter( &m_sleepGap);
	m_sleepGap.QuadPart /= 250;

	// This makes 'Sleep()' more accurate.
	timeBeginPeriod( 1);

	// Time for next update.
	QueryPerformanceCounter( &m_nextUpdate);
	m_nextUpdate.QuadPart += m_oneFrame.QuadPart;

	m_graphicsIsInitialised = res;
	return res;
}

/**
* Runs the application loop.
*/
void DBE::App::Run() {
	// Early out if the graphics aren't initialised but it somehow managed to call this function.
	if( !m_graphicsIsInitialised)
		return;

	bool noError( true);

	// This is here so that the first frame's 'deltaTime' isn't incredibly high. It does mean that
	// it's incredibly low, but it should prevent a bug I'm having at the moment which stops a cube
	// from rotating... I'll have to keep an eye on it.
	FRAME_TIMER().Tick();

	// Keep running the program until a quit message is sent or the 'Terminate()' function is
	// called.
	while( m_playing && this->DoMessages()) {
		// Don't do anything if the window is not in focus.
		if( !this->IsInFocus() && this->GetFocusResponse() == FocusResponse::FR_Pause)
			continue;

		noError = this->RunOnce();

		if( !noError) {
			MessageBox( nullptr, "Error occurred with either the input, updating or rendering of this frame.", "Error", MB_OK | MB_ICONERROR);
			m_playing = false;
		}
	}
}

/**
* Runs the application loop once.
* This code has been extracted from the 'Run' function primarily for use when the engine is used
* inside a C# application. This allows one iteration of the game loop to be executed without C++
* retaining control.
*
* @return True if everything executed correctly.
*/
bool DBE::App::RunOnce() {
	bool noError( true);

	FRAME_TIMER().Tick();

	MGR_AUDIO().OnUpdate( FRAME_TIMER().DeltaTime());
	MGR_INPUT().OnUpdate( FRAME_TIMER().DeltaTime());
	MGR_THREADPOOL().OnUpdate( FRAME_TIMER().DeltaTime());
	Time::Update();

	noError |= this->HandleInputInternal();
	noError |= this->HandleUpdateInternal();
	noError |= this->HandleRenderInternal();

	return noError;
}

/**
* De-initalises the application.
* This should only really be used when 'APP_MAIN' isn't used. To close the program when using
* 'APP_MAIN', use 'Terminate()'.
*/
void DBE::App::Shutdown() {
	// Shutdown auxillary parts of the application (and anything in the derived application).
	this->HandleShutdownInternal();

	// Close DirectX and safely release all memory and variables.
	this->StopD3D();

	// Output to signal the end of the engine's output (to distinguish it from Windows and DirectX
	// output that occurs afterwards).
	DebugTrace( "**************************************************\n");
	DebugTrace( " Deathbetray Engine Shutdown\n");
	DebugTrace( "**************************************************\n");
}

/**
* Signals to the application to close.
*/
void DBE::App::Terminate() {
	m_playing = false;
}

/**
* Sets the back buffer colour.
*
* @param clearColour :: Default: DEFAULT_CLEAR_COLOUR. The colour to set the back buffer.
*/
void DBE::App::SetClearColour( u32 clearColour /*= DEFAULT_CLEAR_COLOUR*/) {
	m_clearColour = clearColour;
}

/**
* Gets the back buffer colour.
*
* @return The colour of the back buffer.
*/
u32 DBE::App::GetClearColour() const {
	return m_clearColour;
}

/**
* Sets the title of the application's window.
*
* @param title :: The string to be set as the title's text.
*/
void DBE::App::SetWindowTitle( const char* title) {
	SetWindowText( m_hWnd, title);
}

/**
* Sets the width and height of the window. This function can only be called in the game's
* constructor because the width and height of the window SHOULDN'T change after initialisation.
*
* @param width	:: The width of the window.
* @param height	:: The height of the window.
*/
void DBE::App::SetWindowDimensions( u32 width, u32 height) {
	// If the graphics have already been initialised, then don't change the values...
	if( m_graphicsIsInitialised)
		return;

	// If the dimensions are passed through as 0 then set it to full screen.
	if( width == 0 && height == 0) {
		RECT rect;
		GetWindowRect( GetDesktopWindow(), &rect);

		width = rect.right;
		height = rect.bottom;

		m_isFullScreen = true;
	}
	else if( width < 100 || height < 100) {
		// Do nothing if the dimensions are rediculously small.
		return;
	}

	m_screenWidth = width;
	m_screenHeight = height;
}

/**
* Gets the width and height of the window.
*
* @return The width (x) and height (y) of the window.
*/
DBE::Vec2 DBE::App::GetWindowDimensions() const {
	return Vec2( (float)SCREEN_WIDTH, (float)SCREEN_HEIGHT);
}

/**
* Gets the width and height of the window.
*
* @param width	:: The width of the window.
* @param height	:: The height of the window.
*/
void DBE::App::GetWindowDimensions( u32& width, u32& height) const {
	width = SCREEN_WIDTH;
	height = SCREEN_HEIGHT;
}

/**
* Gets the width of the window.
*
* @return The width of the window.
*/
u32 DBE::App::GetWindowWidth() const {
	return m_screenWidth;
}

/**
* Gets the height of the window.
*
* @return The height of the window.
*/
u32 DBE::App::GetWindowHeight() const {
	return m_screenHeight;
}

/**
* Checks to see if the graphics have been initialised.
*
* @return True if the graphics have been initialised.
*/
bool DBE::App::IsAppInitialised() const {
	return m_graphicsIsInitialised;
}

/**
* Checks if the scene has already finished being rendered this frame.
*
* @return True if the scene has been, or not yet started to be, rendered this frame.
*/
bool DBE::App::IsSceneRendered() const {
	return m_endScene;
}

/**
* Checks to see if the application is closing down.
*
* @return True if the application is closing down.
*/
bool DBE::App::IsTerminating() const {
	return !m_playing;
}

/**
* Sets the application's response for when it's not in focus.
*
* @param response	:: How the application should respond.
* @param frameRate	:: Default DEFAULT_SLOW_RENDERING_SPEED. What frameRate the application should
*						run at for slow rendering.
*/
void DBE::App::SetFocusResponse( FocusResponse response, s32 frameRate /*= DEFAULT_SLOW_RENDERING_SPEED*/) {
	m_focusResponse = response;

	if( response == FocusResponse::FR_SlowRendering)
		FRAME_TIMER().SetSlowRenderingFPS( frameRate);
}

/**
* Gets the application's reponse state for when it's not in focus.
*
* @return The currently set reponse to when the application goes out off focus.
*/
FocusResponse DBE::App::GetFocusResponse() const {
	return m_focusResponse;
}

/**
* Checks to see if the application is in focus.
*
* @return True if the application is in focus.
*/
bool DBE::App::IsInFocus() const {
	return m_isInFocus;
}

/**
* Locks the mouse to the boundries of the window.
*
* @param lock :: Default: True. Set to true to keep the cursor inside the window.
*/
void DBE::App::LockCursor( bool lock /*= true*/) {
	m_cursorIsLocked = lock;
}

/**
* Displays the mouse cursor.
*
* @param visible :: Default: True. Set to true to make the cursor visible.
*/
void DBE::App::ShowCursor( bool visible /*= true*/) {
	m_cursorIsDisplayed = visible;
}

/**
* Prevents any manipulation of the mouse cursor.
*
* @param defer :: Default: True. Set to true to prevent this application from modifying to cursor
*					in ANY way (i.e. clipping, icon, etc).
*/
void DBE::App::DeferCursorControl( bool defer /*= true*/) {
	m_cursorControlIsEnabled = !defer;
}

/**
* Disable a light.
*
* @param light	:: The index of the light in the 'm_lights' array.
*/
void DBE::App::DisableLight( u8 light) {
	DBE_Assert( light >= 0 && light < MAX_NUM_LIGHTS);

	m_lights[light].m_type = Light::LightType::LT_None;
}

/**
* Enable a directional light.
*
* @param light		:: The index of the light in the 'm_lights' array.
* @param worldDir	:: The direction the light is facing.
* @param colour		:: The colour of the light.
*/
void DBE::App::EnableLightDirectional( u8 light, const DBE::Vec3& worldDir, const DBE::Vec3& diffuseColour) {
	DBE_Assert( light >= 0 && light < MAX_NUM_LIGHTS);

	m_lights[light].m_type = Light::LightType::LT_Directional;

	m_lights[light].m_direction = worldDir.GetNormalised();
	m_lights[light].m_direction *= -1;

	m_lights[light].m_a0 = 1.0f;
	m_lights[light].m_a1 = 0.0f;
	m_lights[light].m_a2 = 0.0f;
	m_lights[light].m_rangeSquared = FLT_MAX;

	m_lights[light].m_colour = diffuseColour;
}

/**
* Enable a point light.
*
* @param light		:: The index of the light in the 'm_lights' array.
* @param worldPos	:: The position of the light.
* @param colour		:: The colour of the light.
*/
void DBE::App::EnableLightPoint( u8 light, const DBE::Vec3& worldPos, const DBE::Vec3& diffuseColour) {
	DBE_Assert( light >= 0 && light < MAX_NUM_LIGHTS);

	m_lights[light].m_type = Light::LightType::LT_Point;

	m_lights[light].m_pos = worldPos;

	m_lights[light].m_a0 = 1.0f;
	m_lights[light].m_a1 = 0.0f;
	m_lights[light].m_a2 = 0.0f;
	m_lights[light].m_rangeSquared = FLT_MAX;

	m_lights[light].m_colour = diffuseColour;
}

/**
* Enable a spot light.
*
* @param light		:: The index of the light in the 'm_lights' array.
* @param worldPos	:: The position of the light.
* @param worldDir	:: The direction the light is facing.
* @param theta		:: 
* @param phi		:: 
* @param fallOff	:: 
* @param colour		:: The colour of the light.
*/
void DBE::App::EnableLightSpot( u8 light, const DBE::Vec3& worldPos, const DBE::Vec3& worldDir, float theta, float phi, float fallOff, const DBE::Vec3& diffuseColour) {
	DBE_Assert( light >= 0 && light < MAX_NUM_LIGHTS);

	m_lights[light].m_type = Light::LightType::LT_Spot;

	m_lights[light].m_pos = worldPos;

	m_lights[light].m_direction = worldDir.GetNormalised();
	m_lights[light].m_direction *= -1;

	m_lights[light].m_a0 = 1.0f;
	m_lights[light].m_a1 = 0.0f;
	m_lights[light].m_a2 = 0.0f;
	m_lights[light].m_rangeSquared = FLT_MAX;

	m_lights[light].m_cosHalfTheta = DBE_Cos( theta * 0.5f);
	m_lights[light].m_cosHalfPhi = DBE_Cos( phi * 0.5f);
	m_lights[light].m_fallOff = fallOff;

	m_lights[light].m_colour = diffuseColour;
}

/**
* Set the attenuation of an existing light.
*
* @param light	:: The index of the light in the 'm_lights' array.
* @param range	:: 
* @param a0		:: 
* @param a1		:: 
* @param a2		:: 
*/
void DBE::App::SetLightAttenuation( u8 light, float range, float a0, float a1, float a2) {
	DBE_Assert( light >= 0 && light < MAX_NUM_LIGHTS);

	m_lights[light].m_a0 = a0;
	m_lights[light].m_a1 = a1;
	m_lights[light].m_a2 = a2;
	m_lights[light].m_rangeSquared = range * range;
}

/**
* Passes the lights to the shader.
*/
void DBE::App::PassLightsToShader( DBE::Shader* p_shader, D3D11_MAPPED_SUBRESOURCE& map, const ShaderLightParams& slots, const DBE::Matrix4& world) {
	DBE_Assert( p_shader != nullptr);

	Matrix4 invXposeW = XMMatrixInverse( nullptr, world);
	invXposeW = XMMatrixTranspose( invXposeW);

	p_shader->SetCBufferMatrix( map, slots.inverse, invXposeW);
	p_shader->SetCBufferMatrix( map, slots.world, world);

	// Iterate through the lights.
	s32 numLights( 0);
	for( s32 i( 0); i < MAX_NUM_LIGHTS; ++i) {
		// Early out.
		if( m_lights[i].m_type == Light::LightType::LT_None)
			continue;

		const Light* p_light = &m_lights[i];
		Vec4 dir, pos, atten, spots;
		bool set( true);

		switch( p_light->m_type) {
			case Light::LightType::LT_Directional:
				dir = Vec4( p_light->m_direction, 1.0f);
				pos = Vec4( 0.0f);
				atten = Vec4( p_light->m_a0, p_light->m_a1, p_light->m_a2, p_light->m_rangeSquared);
				spots = Vec4( 0.0f, -1.0f, 0.0f, 0.0f);
				break;

			case Light::LightType::LT_Point:
				dir = Vec4( 0.0f);
				pos = Vec4( p_light->m_pos, 1.0f);
				atten = Vec4( p_light->m_a0, p_light->m_a1, p_light->m_a2, p_light->m_rangeSquared);
				spots = Vec4( 0.0f, -1.0f, 0.0f, 0.0f);
				break;

			case Light::LightType::LT_Spot:
				dir = Vec4( p_light->m_direction, 1.0f);
				pos = Vec4( p_light->m_pos, 1.0f);
				atten = Vec4( p_light->m_a0, p_light->m_a1, p_light->m_a2, p_light->m_rangeSquared);

				spots = Vec4( p_light->m_cosHalfPhi, p_light->m_cosHalfTheta, 0.0f, p_light->m_fallOff);

				if( p_light->m_cosHalfPhi != p_light->m_cosHalfTheta)
					spots.SetZ( 1.0f / (p_light->m_cosHalfTheta - p_light->m_cosHalfPhi));
				break;

			default:
				set = false;
				break;
		}

		if( set) {
			p_shader->SetCBufferArrayVar<Vec4>( map, slots.lightDir, numLights, dir);
			p_shader->SetCBufferArrayVar<Vec4>( map, slots.lightPos, numLights, pos);
			p_shader->SetCBufferArrayVar<Vec3>( map, slots.lightColour, numLights, p_light->m_colour);
			p_shader->SetCBufferArrayVar<Vec4>( map, slots.lightAtten, numLights, atten);
			p_shader->SetCBufferArrayVar<Vec4>( map, slots.lightSpots, numLights, spots);

			++numLights;
		}
	}

	p_shader->SetCBufferVar<s32>( map, slots.lightCount, numLights);
}

/**
* Passes the lights to the shader.
*/
void DBE::App::PassLightsToShader( Shader* p_shader, D3D11_MAPPED_SUBRESOURCE& map, const ShaderLightingSlots* slots, const DBE::Matrix4& world) {
	ShaderLightParams pSlots;

	if( slots != nullptr)
		pSlots = ShaderLightParams( slots->m_slotWorld, slots->m_slotInverse, slots->m_slotLightDir, slots->m_slotLightPos, slots->m_slotLightColour, slots->m_slotLightAtten, slots->m_slotLightSpots, slots->m_slotNumLights);

	this->PassLightsToShader( p_shader, map, pSlots, world);
}

/**
* Sets the blend state.
*
* @param blendEnable :: Set to true to enable blending.
*/
void DBE::App::SetBlendState( bool blendEnable) {
	u8 i( 0);

	if( blendEnable)
		i |= BLEND_STATE_BLEND_ENABLE;

	mp_D3DDeviceContext->OMSetBlendState( mp_blendStates[i], nullptr, 0xFFFFFFFF);
}

/**
* Sets the depth stencil state.
*
* @param depthTest	:: Set to true to enable depth testing.
* @param depthWrite	:: Set to true to enable depth writing.
*/
void DBE::App::SetDepthStencilState( bool depthTest, bool depthWrite) {
	u8 i( 0);

	if( depthTest)
		i |= DEPTH_STENCIL_STATE_DEPTH_ENABLE;

	if( depthWrite)
		i |= DEPTH_STENCIL_STATE_DEPTH_WRITE_ENABLE;

	mp_D3DDeviceContext->OMSetDepthStencilState( mp_depthStencilStates[i], 0);
}

/**
* Sets the rasteriser state.
*
* @param backFaceCull	:: Set to true to enable backface culling.
* @param wireframe		:: Set to true to enable wireframe mode.
*/
void DBE::App::SetRasteriserState( bool backFaceCull, bool wireframe) {
	u8 i( 0);
	
	if( backFaceCull)
		i |= RASTERISER_STATE_BACK_FACE_CULL;

	if( wireframe)
		i |= RASTERISER_STATE_WIREFRAME;

	mp_D3DDeviceContext->RSSetState( mp_rasteriserStates[i]);
}

/**
* Gets the sampler state for the program.
*
* @param bilinear	:: Default: False. Set to true to enable bilinear state.
* @param mipmap		:: Default: False. Set to true to enable mipmap state.
* @param wrap		:: Default: False. Set to true to enable wrap state.
*
* @return The sampler state setup based on the parameters.
*/
ID3D11SamplerState* DBE::App::GetSamplerState( bool bilinear /*= false*/, bool mipmap /*= false*/, bool wrap /*= false*/) const {
	u8 i( 0);

	if( bilinear)
		i |= SAMPLER_STATE_BILINEAR;

	if( mipmap)
		i |= SAMPLER_STATE_MIPMAP;

	if( wrap)
		i |= SAMPLER_STATE_WRAP;

	return mp_samplerStates[i];
}

/**
* Sets the render target to the default.
*/
void DBE::App::SetDefaultRenderTarget() {
	RECT client;
	GetClientRect( m_hWnd, &client);

	// Set the render target.
	mp_D3DDeviceContext->OMSetRenderTargets( 1, &mp_D3DRenderTargetView, mp_D3DDepthStencilView);

	// Set the viewport for the window.
	D3D11_VIEWPORT vp;
	vp.TopLeftX	= FLOAT( client.left);
	vp.TopLeftY	= FLOAT( client.top);
	vp.Width	= FLOAT( client.right);
	vp.Height	= FLOAT( client.bottom);

	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	mp_D3DDeviceContext->RSSetViewports( 1, &vp);
}

/**
* Clears the back buffer.
*/
void DBE::App::Clear() {
	float r( 0.0f), g( 0.0f), b( 0.0f), a( 0.0f);
	ColourToFloats( r, g, b, a, m_clearColour);

	float colour[4] = { r, g, b, a };

	mp_D3DDeviceContext->ClearRenderTargetView( mp_D3DRenderTargetView, colour);
	mp_D3DDeviceContext->ClearDepthStencilView( mp_D3DDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

/**
* Setup the camera for orthographic projection.
*/
void DBE::App::SetOrthographicCamera() {
	MessageBox( nullptr, "Don't set the orthographic camera using this function.", "Incomplete Function", MB_OK);
}

/**
* Setup the camera for perspective projection.
*/
void DBE::App::SetPerspectiveCamera() {
	MessageBox( nullptr, "Don't set the perspective camera using this function.", "Incomplete Function", MB_OK);
}

/**
* Get the application's aspect ratio based on the window's current width and height.
*
* @return The aspect ratio of the currently sized window.
*/
float DBE::App::GetAspectRatio() const {
	return( static_cast<float>( SCREEN_WIDTH) / SCREEN_HEIGHT);
}

/**
* Gets the WVP matrix based on the currently set world, view and projection matricies.
*
* @return The WVP matrix.
*/
DBE::Matrix4 DBE::App::GetWVP() const {
	Matrix4 wvp;
	wvp = XMMatrixMultiply( m_matView, m_matProj);
	wvp = XMMatrixMultiply( m_matWorld, wvp);

	return wvp;
}

/**
* Outputs all the build information to the output window.
*/
void DBE::App::DumpBuildInfo() const {
#ifdef _DEBUG
	const char* config = "D3D11 Debug";
#else
	const char* config = "D3D11 Release";
#endif

	DebugTrace( "**************************************************\n");
	DebugTrace( " Deathbetray Engine Build Info\n");
	DebugTrace( "**************************************************\n");
	DebugTrace( " Build Config: %s\n", config);
	DebugTrace( " Date: %s - %s\n", __DATE__, __TIME__);
	DebugTrace( "**************************************************\n");
}

/**
* Performs any internal input. This function is mainly to separate the 'HandleInput' function from
* the 'HandleUpdateInternal' function, but there may be input commands that I want to hand-code
* into the framework at a later date.
*
* @return True if nothing went wrong.
*/
bool DBE::App::HandleInputInternal() {

	return this->HandleInput();
}

/**
* Performs any internal updating.
*
* @return True if every was updated correctly.
*/
bool DBE::App::HandleUpdateInternal() {

	return this->HandleUpdate( FRAME_TIMER().DeltaTime());
}

/**
* Performs any internal rendering.
*
* @return True if everything rendered correctly.
*/
bool DBE::App::HandleRenderInternal() {
	bool res( true);

	m_endScene = false;
	res &= this->BeginScene();

	// Render anything from the derived application.
	res &= this->HandleRender( FRAME_TIMER().DeltaTime());

	res &= this->EndScene();
	m_endScene = true;

	return res;
}

/**
* Performs any internal shutdowns.
*
* @return True if everything shutdown correctly.
*/
bool DBE::App::HandleShutdownInternal() {
	// Shutdown anything from the derived application.
	bool res = this->HandleShutdown();

	// Wait for any unfinished threads.
	MGR_THREADPOOL().Shutdown();

	// Delete the default font.
	SafeDelete( mp_defaultFont);

	// Delete the static buffers containing the plane that's used for all U.I. textures.
	UITexture::Shutdown();

	// Delete the random number generator, if it was used.
	Random::Shutdown();

	// Delete the time singleton.
	Time::Shutdown();

	// Delete any temporary fonts that were installed by this program. This needs to be called
	// after 'HandleShutdown' because the derived class will hold references to the fonts and they
	// can't be released until those references are gone.
	Font::Shutdown();

	// Delete any textures that may still exist.
	MGR_TEXTURE().Shutdown();

	// Delete any remaining shaders.
	MGR_SHADER().Shutdown();

	return res;
}

/**
* Prepares the application for rendering this frame.
*
* @return True if the render targets were set correctly.
*/
bool DBE::App::BeginScene() {
	if( !m_renderTargetsInitialised)
		return false;

	// Set the default target that'll be rendered to.
	this->SetDefaultRenderTarget();

	// Clear the screen.
	this->Clear();

	return true;
}

/**
* Signals the end of rendering for this frame.
*
* @return True if the scene was completed successfully.
*/
bool DBE::App::EndScene() {
	// Present the frame.
	HRESULT hr;
	hr = mp_DXGISwapChain->Present( 0, 0);

	return SUCCEEDED( hr);
}

/**
* Gets and performs any message sent to the program from the operating system.
*
* @return True if nothing went wrong.
*/
bool DBE::App::DoMessages() {
	MSG msg;

	while( PeekMessage( &msg, nullptr, 0, 0, PM_NOREMOVE)) {
		BOOL good( GetMessage( &msg, nullptr, 0, 0));
		if( good == 0 || good == -1)
			return false;

		TranslateMessage( &msg);
		DispatchMessage( &msg);
	}

	return true;
}

/**
* Processes the messages send to the window.
*
* @param hWnd	:: The handle of the window.
* @param uMsg	:: Messages sent to the window.
* @param wParam	:: Additional message information; value depends on uMsg.
* @param lParam	:: Additional message information; value depends on uMsg.
*
* @return I have no idea.
*/
LPARAM CALLBACK DBE::App::WndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if( uMsg == WM_CREATE) {
		CREATESTRUCT* cs = (CREATESTRUCT*)lParam;
		SetWindowLongPtr( hWnd, GWLP_USERDATA, (LONG_PTR)cs->lpCreateParams);
	}

	App* p_app = (App*)GetWindowLongPtr( hWnd, GWLP_USERDATA);

	if( uMsg == WM_DESTROY)
		SetWindowLongPtr( hWnd, GWLP_USERDATA, 0);

	switch( uMsg) {
		// Check for mouse movement.
		case WM_SETCURSOR:
			// Clip/Set the cursor.
			if( GET_APP()->m_cursorControlIsEnabled) {
				if( GET_APP()->m_cursorIsLocked && GET_APP()->IsInFocus() && !GET_APP()->m_isFullScreen) {
					RECT windowRect;
					GetWindowRect( GET_APP()->m_hWnd, &windowRect);

					// Adjust it for the window's border... I think.
					RECT actualRect = {
						windowRect.left + 8,
						windowRect.top + 30,
						windowRect.left + 8 + SCREEN_WIDTH,
						windowRect.top + 30 + SCREEN_HEIGHT};

					ClipCursor( &actualRect);
				}
				else {
					ClipCursor( nullptr);
				}

				// Hide/Show the mouse cursor.
				if( GET_APP()->m_cursorIsDisplayed)
					SetCursor( GET_APP()->m_defaultCursor);
				else
					SetCursor( nullptr);
			}
			return 0;

		// Check for mouse button clicks.
		case WM_LBUTTONDOWN:	p_app->m_mouseButtonInfo.m_posDown[0] = lParam;		p_app->m_mouseButtonInfo.m_mouseUp[0] = false;	return 0;
		case WM_RBUTTONDOWN:	p_app->m_mouseButtonInfo.m_posDown[1] = lParam;		p_app->m_mouseButtonInfo.m_mouseUp[1] = false;	return 0;
		case WM_MBUTTONDOWN:	p_app->m_mouseButtonInfo.m_posDown[2] = lParam;		p_app->m_mouseButtonInfo.m_mouseUp[2] = false;	return 0;
		case WM_LBUTTONUP:		p_app->m_mouseButtonInfo.m_posUp[0] = lParam;		p_app->m_mouseButtonInfo.m_mouseUp[0] = true;	return 0;
		case WM_RBUTTONUP:		p_app->m_mouseButtonInfo.m_posUp[1] = lParam;		p_app->m_mouseButtonInfo.m_mouseUp[1] = true;	return 0;
		case WM_MBUTTONUP:		p_app->m_mouseButtonInfo.m_posUp[2] = lParam;		p_app->m_mouseButtonInfo.m_mouseUp[2] = true;	return 0;

		// Check for mouse wheel scrolling.
		case WM_MOUSEWHEEL:
			p_app->m_mouseWheelInfo = wParam;
			return 0;

		// Toggle focus of the application.
		case WM_ACTIVATEAPP:
			p_app->m_isInFocus = !!wParam;
			DebugTrace( "WM_ACTIVATE: in focus = %d\n", p_app->m_isInFocus);

			if( p_app->GetFocusResponse() == FocusResponse::FR_SlowRendering)
				FRAME_TIMER().SlowRendering( !!wParam);

			return 0;

		// The user has closed the window, so close the application.
		case WM_CLOSE:
			p_app->Terminate();
			//PostQuitMessage( 0);
			return 0;
	}

	switch( wParam) {
		// Prevent F10 from pausing the program.
		case SC_KEYMENU:
			return 0;
	}

	return DefWindowProc( hWnd, uMsg, wParam, lParam);
}

/**
* Gets information about the currently active display.
*
* @return True if the information was obtained correctly.
*/
bool DBE::App::GetDisplayInfo() {
	DEVMODE devMode;
	devMode.dmSize = sizeof( DEVMODE);
	
	bool res = (EnumDisplaySettings( nullptr, 0, &devMode) == TRUE);
	
	if( res) {
		m_monitorRefreshRate = devMode.dmDisplayFrequency;
		DebugTrace( "Monitor resolution & refresh rate: %ix%i, %iHrz\n", devMode.dmPelsWidth, devMode.dmPelsHeight, devMode.dmDisplayFrequency);
	}

	return res;
}

/**
* Create the window that will be rendered to.
*
* @return True if everything initialised correctly.
*/
bool DBE::App::CreateDisplayWindow( HWND hWnd) {
	if( hWnd != NULL) {
		m_hWnd = hWnd;
		return true;
	}

	WNDCLASSEX wnd;
	const LPSTR WINDOW_CLASS_NAME = "WindowClass";

	wnd.cbSize			= sizeof( wnd);
	wnd.style			= CS_VREDRAW | CS_HREDRAW;
	wnd.lpfnWndProc		= &WndProc;
	wnd.cbClsExtra		= 0;
	wnd.cbWndExtra		= 0;
	wnd.hInstance		= GetModuleHandle( nullptr);  // You can always get the HINSTANCE this way.
	wnd.hIcon			= nullptr;  // If this member is NULL, the system provides a default icon.
	wnd.hCursor			= LoadCursor( nullptr, IDC_ARROW);
	wnd.hbrBackground	= nullptr;
	wnd.lpszMenuName	= nullptr;
	wnd.lpszClassName	= WINDOW_CLASS_NAME;
	wnd.hIconSm			= nullptr;

	RegisterClassEx( &wnd);

	RECT rect = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };
	DWORD style;
	if( m_isFullScreen)
		style = WS_OVERLAPPED | WS_POPUP;
	else
		style = WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX;
	AdjustWindowRect( &rect, style, false);

	m_hWnd = CreateWindow( WINDOW_CLASS_NAME, "Deathbetray Engine - DirectX 11", style,
		CW_USEDEFAULT, CW_USEDEFAULT, rect.right - rect.left, rect.bottom - rect.top, nullptr, nullptr,
		GetModuleHandle( nullptr), this);

	if( m_hWnd == NULL)
		return false;

	return true;
}

/**
* Initialise the DirectX functionality.
*
* @return True if everything initialised correctly.
*/
bool DBE::App::StartD3D() {
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	swapChainDesc.BufferDesc.Width						= 0;
	swapChainDesc.BufferDesc.Height						= 0;
	swapChainDesc.BufferDesc.RefreshRate.Numerator		= 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator	= 1;
	swapChainDesc.BufferDesc.Format						= DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.ScanlineOrdering			= DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling					= DXGI_MODE_SCALING_UNSPECIFIED;

	swapChainDesc.SampleDesc.Count		= 1;
	swapChainDesc.SampleDesc.Quality	= 0;

	swapChainDesc.BufferUsage	= DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount	= 1;

	swapChainDesc.OutputWindow	= m_hWnd;
	swapChainDesc.Windowed		= TRUE;
	swapChainDesc.SwapEffect	= DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags			= 0;

	D3D_FEATURE_LEVEL D3DFeatureLevel;
	D3D_DRIVER_TYPE driverType;
	driverType = D3D_DRIVER_TYPE_HARDWARE;

	u32 createDeviceFlags( 0);
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif	// #ifndef _DEBUG

	HRESULT hr = D3D11CreateDeviceAndSwapChain( nullptr, driverType, nullptr, createDeviceFlags, nullptr, 0, D3D11_SDK_VERSION, &swapChainDesc, &mp_DXGISwapChain, &mp_D3DDevice, &D3DFeatureLevel, &mp_D3DDeviceContext);
	if( FAILED( hr)) {
		this->StopD3D();

		char buffer[256];
		sprintf_s( buffer, 256, "Device and Swap Chain failed to be created.\n\nError code: %X", hr);
		MessageBox( nullptr, buffer, "Device and Swap Chain", MB_OK | MB_ICONERROR);

		return false;
	}

	mp_D3DDevice->QueryInterface( IID_ID3D11Debug, (void**)&mp_D3DDebug);

	// If the computer can't run DirectX 11, then exit.
	if( D3DFeatureLevel < D3D_FEATURE_LEVEL_11_0) {
		char buffer[256];
		sprintf_s( buffer, 256, "DirectX version insufficient.\n\nVersion Required: 11.0\nVersion Installed: %s", GetD3DFeatureLevelName( D3DFeatureLevel));
		MessageBox( nullptr, buffer, "DirectX Version", MB_OK | MB_ICONERROR);

		return false;
	}

	DebugTrace( "Feature Level: %s\n", GetD3DFeatureLevelName( D3DFeatureLevel));
	DebugTrace( "Got D3D11Debug: %s\n", mp_D3DDebug != nullptr ? "Yes" : "No");
	
	this->RecreateRenderTargetsAndViews();

	// Do the blend, depth stencil, rasteriser, and sampler state stuff... which I, of course,
	// fully understand and in no way copied word-for-word from Tom Seddon's code... *cough*.
	// Blend states.
	for( u8 i( 0); i < NUM_BLEND_STATES; ++i) {
		D3D11_BLEND_DESC desc;
		desc.AlphaToCoverageEnable	= FALSE;
		desc.IndependentBlendEnable	= FALSE;

		desc.RenderTarget[0].BlendEnable			= 1 & BLEND_STATE_BLEND_ENABLE ? TRUE : FALSE;
		desc.RenderTarget[0].SrcBlend				= D3D11_BLEND_SRC_ALPHA;
		desc.RenderTarget[0].DestBlend				= D3D11_BLEND_INV_SRC_ALPHA;
		desc.RenderTarget[0].BlendOp				= D3D11_BLEND_OP_ADD;
		desc.RenderTarget[0].SrcBlendAlpha			= D3D11_BLEND_ONE;
		desc.RenderTarget[0].DestBlendAlpha			= D3D11_BLEND_ZERO;
		desc.RenderTarget[0].BlendOpAlpha			= D3D11_BLEND_OP_ADD;
		desc.RenderTarget[0].RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;

		if( FAILED( mp_D3DDevice->CreateBlendState( &desc, &mp_blendStates[i]))) {
			char buffer[256];
			sprintf_s( buffer, 256, "Blend State failed to be created.\n\nError code: %X", hr);

			MessageBox( nullptr, buffer, "Blend State Error", MB_OK | MB_ICONERROR);
			return false;
		}
	}

	// Depth/Stencil states.
	for( int i( 0); i < NUM_DEPTH_STENCIL_STATES; ++i) {
		D3D11_DEPTH_STENCIL_DESC desc;
		desc.DepthEnable		= i & DEPTH_STENCIL_STATE_DEPTH_ENABLE ? TRUE : FALSE;
		desc.DepthWriteMask		= i & DEPTH_STENCIL_STATE_DEPTH_WRITE_ENABLE ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
		desc.DepthFunc			= D3D11_COMPARISON_LESS;
		desc.StencilEnable		= FALSE;
		desc.StencilReadMask	= 0xFF;
		desc.StencilWriteMask	= 0xFF;

		desc.FrontFace.StencilDepthFailOp	= D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
		desc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;

		desc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilFailOp			= D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
		desc.BackFace.StencilPassOp			= D3D11_STENCIL_OP_KEEP;

		if( FAILED( mp_D3DDevice->CreateDepthStencilState( &desc, &mp_depthStencilStates[i]))) {
			char buffer[256];
			sprintf_s( buffer, 256, "Depth Stencil State failed to be created.\n\nError code: %X", hr);

			MessageBox( nullptr, buffer, "Depth Stencil State Error", MB_OK | MB_ICONERROR);
			return false;
		}
	}

	// Rasteriser states.
	for( int i( 0); i < NUM_RASTERISER_STATES; ++i) {
		D3D11_RASTERIZER_DESC desc;
		desc.FillMode				= i & RASTERISER_STATE_WIREFRAME ? D3D11_FILL_WIREFRAME : D3D11_FILL_SOLID;
		desc.CullMode				= i & RASTERISER_STATE_BACK_FACE_CULL ? D3D11_CULL_BACK : D3D11_CULL_NONE;
		desc.FrontCounterClockwise	= FALSE;
		desc.DepthBias				= 0;
		desc.SlopeScaledDepthBias	= 0.0f;
		desc.DepthBiasClamp			= 0.0f;
		desc.DepthClipEnable		= TRUE;
		desc.ScissorEnable			= FALSE;
		desc.MultisampleEnable		= FALSE;
		desc.AntialiasedLineEnable	= FALSE;

		if( FAILED( mp_D3DDevice->CreateRasterizerState( &desc, &mp_rasteriserStates[i]))) {
			char buffer[256];
			sprintf_s( buffer, 256, "Rasteriser State failed to be created.\n\nError code: %X", hr);

			MessageBox( nullptr, buffer, "Rasteriser State Error", MB_OK | MB_ICONERROR);
			return false;
		}
	}

	// Sampler states.
	for( int i( 0); i < NUM_SAMPLER_STATES; ++i) {
		D3D11_SAMPLER_DESC desc;
		desc.Filter			= i & SAMPLER_STATE_BILINEAR ? D3D11_FILTER_MIN_MAG_MIP_LINEAR : D3D11_FILTER_MIN_MAG_MIP_POINT;
		desc.AddressU		= i & SAMPLER_STATE_WRAP ? D3D11_TEXTURE_ADDRESS_WRAP : D3D11_TEXTURE_ADDRESS_CLAMP;
		desc.AddressV		= i & SAMPLER_STATE_WRAP ? D3D11_TEXTURE_ADDRESS_WRAP : D3D11_TEXTURE_ADDRESS_CLAMP;
		desc.AddressW		= i & SAMPLER_STATE_WRAP ? D3D11_TEXTURE_ADDRESS_WRAP : D3D11_TEXTURE_ADDRESS_CLAMP;
		desc.MipLODBias		= 0.0f;
		desc.MaxAnisotropy	= 1;
		desc.ComparisonFunc	= D3D11_COMPARISON_ALWAYS;
		memset( desc.BorderColor, 0, sizeof( desc.BorderColor));
		desc.MinLOD			= 0.0f;
		desc.MaxLOD			= i & SAMPLER_STATE_MIPMAP ? D3D11_FLOAT32_MAX : 0.0f;

		hr = mp_D3DDevice->CreateSamplerState( &desc, &mp_samplerStates[i]);
		if( FAILED( hr)) {
			char buffer[256];
			sprintf_s( buffer, 256, "Sampler State failed to be created.\n\nError code: %X", hr);

			MessageBox( nullptr, buffer, "Sampler State Error", MB_OK | MB_ICONERROR);
			return false;
		}
	}

	// Set some default states.
	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	return true;
}

/**
* Destroy the DirectX variables.
*/
void DBE::App::StopD3D() {
	this->ReleaseRenderTargetsAndViews();

	// Release the states arrays.
	for( u8 i( 0); i < NUM_BLEND_STATES; ++i)
		ReleaseCOM( mp_blendStates[i]);

	for( u8 i( 0); i < NUM_DEPTH_STENCIL_STATES; ++i)
		ReleaseCOM( mp_depthStencilStates[i]);

	for( u8 i( 0); i < NUM_RASTERISER_STATES; ++i)
		ReleaseCOM( mp_rasteriserStates[i]);

	for( u8 i( 0); i < NUM_SAMPLER_STATES; ++i)
		ReleaseCOM( mp_samplerStates[i]);

	//HR( mp_D3DDebug->ReportLiveDeviceObjects( D3D11_RLDO_DETAIL));

	ReleaseCOM( mp_DXGISwapChain);
	ReleaseCOM( mp_D3DDebug);
	ReleaseCOM( mp_D3DDevice);
	ReleaseCOM( mp_D3DDeviceContext);
}

/**
* Creates the render targets and views that'll be rendered to.
*/
void DBE::App::RecreateRenderTargetsAndViews() {
	this->ReleaseRenderTargetsAndViews();

	// Get the swap chain description.
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	if( FAILED( mp_DXGISwapChain->GetDesc( &swapChainDesc)))
		return;

	// Get the size of the swap chain output window.
	RECT client;
	GetClientRect( swapChainDesc.OutputWindow, &client);

	// Resize the swap chain buffers.
	if( FAILED( mp_DXGISwapChain->ResizeBuffers( swapChainDesc.BufferCount, client.right, client.bottom, swapChainDesc.BufferDesc.Format, swapChainDesc.Flags)))
		return;

	// Recreate the render target view.
	ID3D11Texture2D* p_D3DBackBuffer;
	if( FAILED( mp_DXGISwapChain->GetBuffer( 0, IID_ID3D11Texture2D, (void**)&p_D3DBackBuffer)))
		return;

	bool goodRenderTargetView = SUCCEEDED( mp_D3DDevice->CreateRenderTargetView( p_D3DBackBuffer, nullptr, &mp_D3DRenderTargetView));

	// Don't need the back buffer any more. If CreateRenderTargetView() succeeded, it took a
	// reference to it.
	ReleaseCOM( p_D3DBackBuffer);

	if( !goodRenderTargetView)
		return;

	// Recreate depth/stencil view.
	D3D11_TEXTURE2D_DESC depthStencilDesc;
	depthStencilDesc.Width				= client.right;
	depthStencilDesc.Height				= client.bottom;
	depthStencilDesc.MipLevels			= 1;
	depthStencilDesc.ArraySize			= 1;
	depthStencilDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count	= 1;
	depthStencilDesc.SampleDesc.Quality	= 0;
	depthStencilDesc.Usage				= D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags		= 0;
	depthStencilDesc.MiscFlags			= 0;

	if( FAILED( mp_D3DDevice->CreateTexture2D( &depthStencilDesc, nullptr, &mp_D3DDepthStencilBuffer)))
		return;

	if( FAILED( mp_D3DDevice->CreateDepthStencilView( mp_D3DDepthStencilBuffer, nullptr, &mp_D3DDepthStencilView)))
		return;

	m_renderTargetsInitialised = true;
}

/**
* Deletes the render targets and views.
*/
void DBE::App::ReleaseRenderTargetsAndViews() {
	this->ClearStateAndFlushDeviceContext();

	ReleaseCOM( mp_D3DRenderTargetView);
	ReleaseCOM( mp_D3DDepthStencilView);
	ReleaseCOM( mp_D3DDepthStencilBuffer);

	m_renderTargetsInitialised = false;
}

/**
* Clears the device context's state and flushes it.
*/
void DBE::App::ClearStateAndFlushDeviceContext() {
	if( !mp_D3DDeviceContext)
		return;

	mp_D3DDeviceContext->ClearState();
	mp_D3DDeviceContext->Flush();
}

/**
* Converts the DirectX version enum into a human-readable string.
*
* @param level :: A DirectX version.
*
* @return String version of the DirectX version.
*/
const char* DBE::App::GetD3DFeatureLevelName( const D3D_FEATURE_LEVEL& level) const {
	switch( level) {
		case( D3D_FEATURE_LEVEL_9_1):
			return "9.1";
		case( D3D_FEATURE_LEVEL_9_2):
			return "9.2";
		case( D3D_FEATURE_LEVEL_9_3):
			return "9.3";
		case( D3D_FEATURE_LEVEL_10_0):
			return "10.0";
		case( D3D_FEATURE_LEVEL_10_1):
			return "10.1";
		case( D3D_FEATURE_LEVEL_11_0):
			return "11.0";
		default:
			return "Unknown Version";
	}
}