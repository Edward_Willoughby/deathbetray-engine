/********************************************************************
*	Function definitions for the FontPixelShader class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEFontPixelShader.h"

#include "DBEUtilities.h"
#include "DBEVertexTypes.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
DBE::FontPixelShader::FontPixelShader() {}

/**
* Destructor.
*/
DBE::FontPixelShader::~FontPixelShader() {}

/**
* 
*
* @param :: 
*
* @return 
*/
bool DBE::FontPixelShader::Init() {
	char shaderPath[256];
	DebuggerDependentPath( shaderPath, sizeof( shaderPath), "DBEngine_ROOT", "", "shaders\\FontShader.hlsl");
	this->LoadShader( shaderPath, g_vertPos3fColour4ubTex2fMacros);

	this->FindTexture( "g_texture", &m_slotTexture);
	this->FindSampler( "g_sampler", &m_slotSampler);

	// Doesn't need a CBuffer.

	return true;
}