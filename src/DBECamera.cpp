/********************************************************************
*	Function definitions for the Camera class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBECamera.h"

#include "DBEApp.h"
#include "DBEMovable.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Constructor.
*/
DBE::Camera::Camera()
	: m_zDist( 10.0f)
	, m_position( 10.0f, 10.0f, 0.0f)
	, m_lookAt( 0.0f)
	, m_up( 0.0f, 1.0f, 0.0f)
	, mp_target( nullptr)
{}

/**
* Destructor.
*/
DBE::Camera::~Camera() {
	
}

/**
* Updates the camera. Only useful when having the camera track an movable object, otherwise it does
* nothing.
*/
void DBE::Camera::Update() {
	if( mp_target == nullptr)
		return;

	m_lookAt = mp_target->GetPosition();
	m_up = DBE::Vec3YAxis;

	// Get the overall rotation of the object.
	Vec3 vRot = mp_target->GetRotation() + mp_target->GetCamDirection();
	Matrix4 rot = XMMatrixRotationRollPitchYaw( vRot.GetX(), vRot.GetY(), vRot.GetZ());

	// Rotate the vectors.
	Vec3 posOut = XMVector3Transform( XMVectorSet( 0.0f, 0.0f, -1.0f, 0.0f), rot);
	Vec3 upOut = XMVector3Transform( m_up.GetXMVector(), rot);

	// 'Zoom' the camera's position by the distance it should be from the object.
	posOut *= m_zDist;

	m_position = m_lookAt + posOut;
	m_up = upOut;
}

/**
* Attaches the camera to an object.
*
* @param p_target :: The Movable object to attach to.
*/
void DBE::Camera::AttachToObject( DBE::Movable* p_target) {
	mp_target = p_target;
}

/**
* Creates and sets the view matrix.
*
* @return The view matrix.
*/
DBE::Matrix4 DBE::Camera::ApplyViewMatrixLH() const {
	Matrix4 mat;

	mat = DirectX::XMMatrixLookAtLH( m_position.GetXMVectorConst(), m_lookAt.GetXMVectorConst(), m_up.GetXMVectorConst());

	GET_APP()->m_matView = mat;
	return mat;
}

/**
* Creates and sets the perspective matrix.
*
* @param zNear	:: How close faces can be before they start getting clipped.
* @param zFar	:: How far faces can be before they start getting clipped.
*
* @return The perspective projection matrix.
*/
DBE::Matrix4 DBE::Camera::ApplyPerspectiveMatrixLH( float zNear, float zFar) const {
	Matrix4 mat;

	mat = DirectX::XMMatrixPerspectiveFovLH( 0.25f * (float)XM_PI, GET_APP()->GetAspectRatio(), zNear, zFar);

	GET_APP()->m_matProj = mat;
	return mat;
}

/**
* Creates and sets the orthographic matrix.
*
* @param zNear	:: How close faces can be before they start getting clipped.
* @param zFar	:: How far faces can be before they start getting clipped.
*
* @return The orthographic projection matrix.
*/
DBE::Matrix4 DBE::Camera::ApplyOrthoMatrixLH( float zNear, float zFar) const {
	Matrix4 mat;

	mat = DirectX::XMMatrixOrthographicLH( 1.0f, 1.0f, zNear, zFar);

	GET_APP()->m_matProj = mat;
	GET_APP()->m_matView = DirectX::XMMatrixTranslation( 0.0f, 0.0f, 2.0f);
	GET_APP()->m_matWorld = DirectX::XMMatrixIdentity();

	return mat;
}