/********************************************************************
*	Function definitions for the D3DInputMgr class.
********************************************************************/

/********************************************************************
*	Include the header file.
*********************************************************************/
#include "DBEInputMgr.h"

#include <Windowsx.h>

#include "DBEApp.h"
#include "DBEUtilities.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
*********************************************************************/


/**
* Constructor for the InputMgr class.
*/
DBE::InputMgr::InputMgr()
	: m_isLocked( false)
	, m_mouseWheelState( MouseWheelState::MWS_Resting)
{
	for( int i( 0); i < NUM_OF_KEYS; ++i) {
		m_checkedStates[i] = false;
		m_states[i] = false;
		m_prevStates[i] = false;
	}
}

/**
* Destructor for the D3DInputMgr class.
*/
DBE::InputMgr::~InputMgr()
{}

/**
* Updates the D3DInputMgr.
*
* @param deltaTime :: The time since the last frame.
*
* @return True if everything updated correctly.
*/
bool DBE::InputMgr::OnUpdate( float deltaTime) {
	// Update the mouse wheel state.
	m_mouseWheelState = MouseWheelState( GET_WHEEL_DELTA_WPARAM( GET_APP()->m_mouseWheelInfo) / WHEEL_DELTA);
	GET_APP()->m_mouseWheelInfo = 0;

	// For any keys that were queried last frame, their states now need to be set as 'previous
	// states'.
	for( s32 i( 0); i < NUM_OF_KEYS; ++i) {
		if( m_checkedStates[i])
			m_prevStates[i] = m_states[i];

		m_checkedStates[i] = false;
		m_states[i] = false;
	}

	// This is stuff that I'm still testing for the mouse input:
	// Store the previous states, then get the states for this frame.
	// Then, if the mouse was pressed this frame and not previous frame, store its position.
	m_prevStates[VK_LBUTTON] = m_mouseButtonStates[0];
	m_prevStates[VK_RBUTTON] = m_mouseButtonStates[1];
	m_prevStates[VK_MBUTTON] = m_mouseButtonStates[2];

	// Using 'IsDown' because I don't care if it's debounced.
	m_mouseButtonStates[0] = this->IsDown( VK_LBUTTON);
	m_mouseButtonStates[1] = this->IsDown( VK_RBUTTON);
	m_mouseButtonStates[2] = this->IsDown( VK_MBUTTON);

#define StoreMousePos( condition, dest, pos) {	\
	if( condition)								\
		dest = pos;								\
	}

	Vec2 mousePos;
	this->GetMousePos( mousePos);
	StoreMousePos( m_mouseButtonStates[0] && !m_prevStates[VK_LBUTTON], m_mouseButtonPosDown[0], mousePos);
	StoreMousePos( !m_mouseButtonStates[0] && m_prevStates[VK_LBUTTON], m_mouseButtonPosUp[0], mousePos);
	StoreMousePos( m_mouseButtonStates[1] && !m_prevStates[VK_RBUTTON], m_mouseButtonPosDown[1], mousePos);
	StoreMousePos( !m_mouseButtonStates[1] && m_prevStates[VK_RBUTTON], m_mouseButtonPosUp[1], mousePos);
	StoreMousePos( m_mouseButtonStates[2] && !m_prevStates[VK_MBUTTON], m_mouseButtonPosDown[2], mousePos);
	StoreMousePos( !m_mouseButtonStates[2] && m_prevStates[VK_MBUTTON], m_mouseButtonPosUp[2], mousePos);

	//// Update the mouse buttons if any have come up recently.
	//if( GET_APP()->m_mouseButtonInfo.m_mouseUp[0]) {
	//	m_prevStates[VK_LBUTTON] = true;
	//	GET_APP()->m_mouseButtonInfo.m_mouseUp[0] = false;
	//}
	//if( GET_APP()->m_mouseButtonInfo.m_mouseUp[2]) {
	//	m_prevStates[VK_MBUTTON] = true;
	//	GET_APP()->m_mouseButtonInfo.m_mouseUp[2] = false;
	//}
	//if( GET_APP()->m_mouseButtonInfo.m_mouseUp[1]) {
	//	m_prevStates[VK_RBUTTON] = true;
	//	GET_APP()->m_mouseButtonInfo.m_mouseUp[1] = false;
	//}

	// Check the button sequences.
	for( u16 i( 0); i < MAX_NUM_BUTTON_SEQUENCES; ++i) {
		// Ignore this sequence if it doesn't exist.
		if( m_buttonSequences[i].m_name == nullptr)
			continue;

		// Ignore this sequence if it's been disabled.
		if( !m_buttonSequences[i].m_enabled)
			continue;

		// Ignore this sequence if it's already been completed.
		if( m_buttonSequences[i].m_completed)
			continue;

		MessageBox( nullptr, "Button sequences have not been fully implemented yet.", "DBE::InputMgr Error", MB_OK);

		//if( m_buttonSequences[i].m_index > 0) {
		//	m_buttonSequences[i].m_timeLeft += SE::Time::FrameTimer::GetFrameTimeMS();

		//	if( m_buttonSequences[i].m_timeLeft >= m_buttonSequences[i].m_timeToComplete) {
		//		m_buttonSequences[i].m_timeLeft = 0;
		//		m_buttonSequences[i].m_index = 0;
		//		DebugTrace( "BUTTON SEQUENCE %s: Reset (time expired)\n", m_buttonSequences[i].m_name);
		//	}
		//}

		////bool shouldBeStopped( m_stopSequence);

		//// If the button was pressed, then increment the index, but if another button was pressed
		//// then reset the index.
		//if( this->ButtonPressed( m_buttonSequences[i].m_buttons[m_buttonSequences[i].m_index])) {
		//	++m_buttonSequences[i].m_index;
		//	DebugTrace( "BUTTON SEQUENCE %s: Incremented to %i\n", m_buttonSequences[i].m_name, m_buttonSequences[i].m_index);
		//}
		////else if( shouldBeStopped) {
		////	m_buttonSequences[i].m_index = 0;
		////	DebugTrace( "BUTTON SEQUENCE %s: Reset (wrong button)\n", m_buttonSequences[i].m_name);
		////}

		//if( m_buttonSequences[i].m_index == m_buttonSequences[i].m_buttonCount) {
		//	m_buttonSequences[i].m_completed = true;
		//	DebugTrace( "BUTTON SEQUENCE %s: Completed\n", m_buttonSequences[i].m_name);
		//}
	}

	//m_stopSequence = false;

	return true;
}

/**
* Gets the key's state. Use this function when you want to check a specific key multiple times
* during one frame.
*
* @param key :: The button to check.
*
* @return The key's state this frame.
*/
KeyState DBE::InputMgr::GetKeyState( u16 key) {
	if( !GET_APP()->IsInFocus() || m_isLocked)
		return KeyState::KS_None;

	bool isPressed( false);
	KeyState state( KeyState::KS_None);

	if( m_checkedStates[key]) {
		isPressed = m_states[key];
	}
	else {
		isPressed = this->IsDown( key);
		m_states[key] = isPressed;
	}

	// 'KeyHeld'
	if( isPressed && m_prevStates[key])
		state = KeyState::KS_Held;
	// 'KeyPressed'
	else if( isPressed && !m_prevStates[key])
		state = KeyState::KS_Pressed;
	// 'KeyReleased'
	else if( !isPressed && m_prevStates[key])
		state = KeyState::KS_Released;

	m_checkedStates[key] = true;

	return state;
}

/**
* Test to see if the button indicated by the parameter is currently held down (i.e. was pressed
* last frame AND this frame).
*
* @param key :: The button to check.
*
* @return True if the button is being held down. If the controls are locked then it returns false.
*/
bool DBE::InputMgr::KeyHeld( u16 key) {
	if( !GET_APP()->IsInFocus() || m_isLocked)
		return false;

	return( this->GetKeyState( key) == KeyState::KS_Held);
}

/**
* Tests to see if the button indicated by the parameter is currently pressed (i.e. NOT pressed last
* frame AND pressed this frame).
*
* @param key :: The button to check.
*
* @return True if the button is being pressed. If the controls are locked then it returns false.
*/
bool DBE::InputMgr::KeyPressed( u16 key) {
	if( !GET_APP()->IsInFocus() || m_isLocked)
		return false;

	return( this->GetKeyState( key) == KeyState::KS_Pressed);
}

/**
* Tests to see if the button indicated by the parameter was released this frame (i.e. was pressed
* last frame AND not this frame).
*
* @param key :: The button to check.
*
* @return True if the button was released. If the controls are locked then it returns false.
*/
bool DBE::InputMgr::KeyReleased( u16 key) {
	if( !GET_APP()->IsInFocus() || m_isLocked)
		return false;

	return( this->GetKeyState( key) == KeyState::KS_Released);
}

/**
* Gets the last position the specified mouse button's 'up' event was triggered.
*
* @param button	:: The button to check.
* @param pos	:: The position of the mouse at the time of the event.
*
* @return True if the window is in focus.
*/
bool DBE::InputMgr::GetButtonUpPos( u16 button, DBE::Vec2& pos) const {
	if( !GET_APP()->IsInFocus() || m_isLocked)
		return false;

	if( button == VK_LBUTTON)		button = 0;
	else if( button == VK_RBUTTON)	button = 1;
	else if( button == VK_MBUTTON)	button = 2;
	else							button = -1;

	if( button != -1) {
		//pos.SetX( (float)GET_X_LPARAM( GET_APP()->m_mouseButtonInfo.m_posUp[button]));
		//pos.SetY( (float)GET_Y_LPARAM( GET_APP()->m_mouseButtonInfo.m_posUp[button]));
		pos = m_mouseButtonPosUp[button];
	}
	else {
		pos = Vec2( -1);
	}

	return true;
}

/**
* Gets the last position the specified mouse button's 'down' event was triggered.
*
* @param button	:: The button to check.
* @param pos	:: The position of the mouse at the time of the event.
*
* @return True if the window is in focus.
*/
bool DBE::InputMgr::GetButtonDownPos( u16 button, DBE::Vec2& pos) const {
	if( !GET_APP()->IsInFocus() || m_isLocked)
		return false;

	if( button == VK_LBUTTON)		button = 0;
	else if( button == VK_RBUTTON)	button = 1;
	else if( button == VK_MBUTTON)	button = 2;
	else							button = -1;

	if( button != -1) {
		//pos.SetX( (float)GET_X_LPARAM( GET_APP()->m_mouseButtonInfo.m_posDown[button]));
		//pos.SetY( (float)GET_Y_LPARAM( GET_APP()->m_mouseButtonInfo.m_posDown[button]));
		pos = m_mouseButtonPosDown[button];
	}
	else {
		pos = Vec2( -1);
	}

	return true;
}

/**
* Gets the position of the cursor within the bounds of the window.
*
* @param pos :: The vector to hold the mouse's x and y positions.
*
* @return True if the window is in focus.
*/
bool DBE::InputMgr::GetMousePos( DBE::Vec2& pos) const {
	if( !GET_APP()->IsInFocus() || m_isLocked)
		return false;

	POINT mouse;
	RECT rect;
	HWND handle( GET_APP()->m_hWnd);

	GetCursorPos( &mouse);
	GetClientRect( handle, &rect);
	ScreenToClient( handle, &mouse);

	pos.SetX( (float)mouse.x);
	pos.SetY( (float)mouse.y);

	// Set an invalid position if the cursor is outside the bounds of the window.
	if( pos.GetX() < 0 || pos.GetX() > rect.right || pos.GetY() < 0 || pos.GetY() > rect.bottom)
		pos = Vec2( -1);

	return true;
}

/**
* Sets the position of the mouse within the bounds of the window.
*
* @param pos :: The x and y positions to set the mouse to.
*
* @return True if the window is in focus.
*/
bool DBE::InputMgr::SetMousePos( const DBE::Vec2& pos) {
	if( !GET_APP()->IsInFocus() || m_isLocked)
		return false;

	POINT mouse;
	mouse.x = (LONG)pos.GetX();
	mouse.y = (LONG)pos.GetY();

	// Convert the mouse position back to screen coordinates before setting them.
	ClientToScreen( GET_APP()->m_hWnd, &mouse);
	SetCursorPos( mouse.x, mouse.y);

	return true;
}

/**
* Converts the mouse position to screen coordinates.
*
* @param pos :: The x and y position to convert.
*
* @return The parameter converte to screen coordinates.
*/
DBE::Vec2 DBE::InputMgr::ConvertMousePosToScreenCoordinates( const DBE::Vec2& pos) const {
	Vec2 p;
	p.SetX( pos.GetX() / SCREEN_WIDTH - 0.5f);
	p.SetY( (pos.GetY() / SCREEN_HEIGHT - 0.5f) * -1.0f);

	return p;
}

/**
* Gets the mouse wheel's rotation.
*
* @return The state of the mouse wheel (refer to the enum definition).
*/
MouseWheelState DBE::InputMgr::GetMouseWheelState() const {
	if( !GET_APP()->IsInFocus())
		return MouseWheelState::MWS_Resting;

	return( m_mouseWheelState);
}

/**
* Check whether or not the cursor is currently inside the program's window.
*
* @return True if the cursor is inside the window.
*/
bool DBE::InputMgr::CursorIsInsideWindow() const {
	if( !GET_APP()->IsInFocus())
		return false;

	Vec2 pos;
	this->GetMousePos( pos);

	if( pos.GetX() == -1 && pos.GetY() == -1)
		return false;

	return true;
}

/**
* Stops the D3DInputMgr from updating the controls each frame and clears any currently stored data.
*
* @param isLocked :: Default: True. Set to false to update the controls every frame.
*/
void DBE::InputMgr::LockControls( bool isLocked /*= true*/) {
	m_isLocked = isLocked;
}

/**
* Returns whether or not a specified key is down.
*
* @param button :: The ASCII code of the key.
*
* @return True if the key is down this frame.
*/
bool DBE::InputMgr::IsDown( u16 button) {
	SHORT val = GetAsyncKeyState( button);
	bool down = val & 0x8000;

	return down;
}

void DBE::InputMgr::CreateButtonSequence( const char* sequenceName, const u16* buttons, u16 buttonCount, u32 time /*= 0*/)
{
	if( sequenceName == nullptr || buttons == nullptr) {
		DebugTrace( "Cannot create button sequence; invalid parameters.\n");
		return;
	}

	u16 index( 0);
	for( index; index < MAX_NUM_BUTTON_SEQUENCES; ++index)
		if( m_buttonSequences[index].m_name == nullptr)
			break;

	if( index == MAX_NUM_BUTTON_SEQUENCES) {
		DebugTrace( "Cannot create button sequence; too many exist.\n");
		return;
	}

	// If no time was specified, then give one second per button press.
	if( time == 0)
		time = buttonCount * 1000000;

	m_buttonSequences[index].m_name = sequenceName;
	m_buttonSequences[index].m_buttons = buttons;
	m_buttonSequences[index].m_buttonCount = buttonCount;
	m_buttonSequences[index].m_timeLeft = 0;
	m_buttonSequences[index].m_timeToComplete = time;
}

bool DBE::InputMgr::IsSequenceCompleted( const char* sequenceName) {
	return m_buttonSequences[this->FindSequence( sequenceName)].m_completed;
}

void DBE::InputMgr::EnableSequence( const char* sequenceName, bool enabled) {
	m_buttonSequences[this->FindSequence( sequenceName)].m_enabled = enabled;
}

void DBE::InputMgr::ResetSequence( const char* sequenceName) {
	int index( this->FindSequence( sequenceName));

	m_buttonSequences[index].m_index = 0;
	m_buttonSequences[index].m_completed = false;
}

u16 DBE::InputMgr::FindSequence( const char* sequenceName) {
	for( u16 i( 0); i < MAX_NUM_BUTTON_SEQUENCES; ++i)
		if( m_buttonSequences[i].m_name != nullptr)
			if( strcmp(m_buttonSequences[i].m_name, sequenceName) == 0)
				return i;

	DBE_AssertAlways();
	return 0;
}