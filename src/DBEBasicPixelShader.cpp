/********************************************************************
*	Function definitions for the BasicPixelShader class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEBasicPixelShader.h"

#include "DBEApp.h"
#include "DBEUtilities.h"
#include "DBEVertexTypes.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
BasicPixelShader::BasicPixelShader() {
	
}

/**
* Destructor.
*/
BasicPixelShader::~BasicPixelShader() {
	
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool DBE::BasicPixelShader::Init() {
	char shaderPath[256];
	DebuggerDependentPath( shaderPath, sizeof( shaderPath), "DBEngine_ROOT", "", "shaders\\BasicShader.hlsl");

	this->LoadShader( shaderPath, g_vertPos3fColour4ubNormal3fMacros);

	return true;
}