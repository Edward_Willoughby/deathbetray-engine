/********************************************************************
*
*	CLASS		:: DBE::UITexture
*	DESCRIPTION	:: Used for rendering orthographic textures (i.e. mini-maps, borders, text, menus
*					etc.).
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 12 / 06
*
********************************************************************/

#ifndef DBEUITextureH
#define DBEUITextureH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBERenderable.h"
#include "DBETypes.h"

namespace DBE {
	class PixelShader;
	class VertexShader;

	struct Texture;
}
struct ID3D11Buffer;
struct ID3D11Texture2D;
struct ID3D11ShaderResourceView;
struct ID3D11SamplerState;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the U.I. texture vertex shader so it can be found in the shader manager.
static const char* gsc_UITextureVertexShaderName = "UITextureVertexShader";
/// The unique name for the U.I. texture pixel shader so it can be found in the shader manager.
static const char* gsc_UITexturePixelShaderName = "UITexturePixelShader";


/*******************************************************************/
namespace DBE
{
	class UITexture : public Renderable {
		public:
			/// Constructor.
			UITexture();
			/// Constructor.
			UITexture( const char* textureFileName, ID3D11SamplerState* sampler);
			/// Constructor.
			UITexture( DBE::Texture* texture, ID3D11SamplerState* sampler = nullptr);
			/// Destructor.
			~UITexture();

			/// Sets the texture to be used.
			void SetTexture( const char* textureFileName, ID3D11SamplerState* sampler);
			/// Sets the texture to be used.
			void SetTexture( DBE::Texture* texture, ID3D11SamplerState* sampler);
			/// Sets the sampler to be used.
			void SetSampler( ID3D11SamplerState* sampler);

			/// Renders the U.I. texture (call 'Render').
			virtual void OnRender( float deltaTime);

			ID3D11Buffer* GetVertexBuffer() const;
			ID3D11Buffer* GetIndexBuffer() const;
			u32 GetIndexCount() const;

			/// Deletes the static plane that's used for rendering (should only be called by 'App').
			static void Shutdown();

			DBE::VertexShader*	mp_VS;
			DBE::PixelShader*	mp_PS;
			s32					m_slotTextureHeight;
			s32					m_slotTextureWidth;

			DBE::Texture* mp_texture;
		
		private:
			/// Creates the plane and default shader used by U.I. textures.
			void CreatePlaneAndDefaultShader();

			/// Private copy constructor to prevent accidental multiple instances.
			UITexture( const DBE::UITexture&);
			/// Private assignment operator to prevent accidental multiple instances.
			UITexture& operator=( const DBE::UITexture&);
		
	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef D3DUITextureH
