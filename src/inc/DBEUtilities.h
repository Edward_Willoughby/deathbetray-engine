//-----------------------------------------------
//// UTILITY FUNCTIONS
//// DESCRIPTION :-
//	The following macros and functions are used across the program to make life easier.
//// CREATED BY :- Edward Willoughby
//// DATE :- 2013 / 06 / 26
//-----------------------------------------------

#ifndef DBEUtilitiesH
#define DBEUtilitiesH

//-----------------------------------------------
// Include libraries and header files
#include <algorithm>
#include <cassert>
#include <stdint.h>
#include <stdarg.h>

#include "DBETypes.h"
//-----------------------------------------------


//-----------------------------------------------
//// MACRO :- HR
//// DESCRIPTION :-
//	Checks a D3D object was loaded correctly.
#define HR( x) {																																					\
	HRESULT hr( x);																																					\
	if( FAILED( hr)) {																																				\
		char buffer[256];																																			\
		if( FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, nullptr, hr, MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&buffer, sizeof( buffer), nullptr) != 0)		\
			DebugTrace( "HRESULT Error: %s\n", buffer);																												\
		else																																						\
			DebugTrace( "HRESULT Error: Couldn't find description for error # %#x.\n", hr);																			\
																																									\
		DBE_AssertAlways();																																			\
	}																																								\
}

//// MACRO :- ReleaseCOM
//// DESCRIPTION :-
//	Releases a COM object.
#define ReleaseCOM( x) {		\
	if( x) {					\
		x->Release();			\
		x = 0;					\
	}							\
}

//// MACRO :- SafeDelete
//// DESCRIPTION :-
//	Ensures a pointer is deleted properly and doesn't leave a wild pointer behind.
#define SafeDelete( x) {	\
	if( x) {				\
		delete x;			\
		x = nullptr;		\
	}						\
}

//// MACRO :- SafeDeleteArray
//// DESCRIPTION :-
//	Ensures an array is deleted properly and doesn't leave a wild pointer behind.
#define SafeDeleteArray( x) {	\
	if( x) {					\
		delete[] x;				\
		x = nullptr;			\
	}							\
}

#ifdef _DEBUG
/// Macro to output some debug information before breaking into the debugger if a condition fails,
/// including a custom message.
#define DBE_AssertWithMsg( condition, strFormat, vars) {			\
	if( !(condition)) {												\
		DebugTrace( "****************************************\n");	\
		DebugTrace( "ERROR: Assert failed!\n");						\
		DebugTrace( strFormat, vars);								\
		DebugTrace( "\tFile: %s\n", __FILE__);						\
		DebugTrace( "\tFunction: %s\n", __FUNCTION__);				\
		DebugTrace( "\tLine: %i\n", __LINE__);						\
		DebugTrace( "****************************************\n");	\
		__debugbreak();												\
	}																\
}


/// Macro to output some debug information before breaking into the debugger if a condition fails.
#define DBE_Assert( condition) {			\
	DBE_AssertWithMsg( condition, "", "");	\
}

/// Macro to always force the program to break into the debugger.
#define DBE_AssertAlways() {	\
	__debugbreak();				\
}

/**
* Outputs a formatted string to the output window. This uses the same formatting as 'printf()'.
*/
inline void DebugTrace( const char* strFormat, ...)
{
	va_list argList;
	va_start( argList, strFormat);
	TCHAR buffer[1024];

	vsprintf_s( buffer, strFormat, argList);
	OutputDebugString( buffer);

	va_end( argList);
}
#else
#define DBE_AssertWithMsg( condition, strFormat, vars)
#define DBE_Assert( condition)
#define DBE_AssertAlways()

inline void DebugTrace( const char* strFormat, ...) {}
#endif	// #ifdef _DEBUG

namespace DBE
{
	/**
	* A function to linearly interpolate between two float variables.
	*
	* @param first	:: The value to interpolate from.
	* @param second	:: The value to interpolate to.
	* @param interp	:: The amount to interpolate between the other two parameters. This value MUST
	*					be between 0 and 1.
	*
	* @return The interpolated value between the first two parameters based on the third.
	*/
	inline float Lerp( float first, float second, float interp)
	{
		// Overall equation:
		// (1 - 5)*a + t*b
		float t( interp);

		float lerp1( 1 - t);
		float lerp2( lerp1 * first);
		float lerp3( t * second);
		float lerp4( lerp2 + lerp3);

		return lerp4;
	}

	/**
	* Converts a variable to its binary form. This is meant only for numbers, but could be used for
	* classes, if desired, but the bit-shifting might not work with classes; particularly those with
	* overloaded '>>'.
	*
	* @param dest	:: The char array to hold the binary display.
	* @param size	:: The maximum size of 'dest'.
	* @param var	:: The variable to be converted.
	*/
	template<class T>
	inline void ConvertToBinary( const char* dest, size_t size, const T& var)
	{
		u8 varSize( sizeof( var) * 8);
	
		// Don't bother if the char array is too small for the variable type.
		if( size < varSize + 1)
			return;

		for( u8 i( 0); i < varSize; ++i)
			buffer[(varSize-1) - i] = ((var >> i) & 0x01) ? '1' : '0';

		// Add a null-terminator.
		buffer[varSize] = 0;
	}

	/**
	* Uses the paramters to create a string. This function is so the same code can be used for whether
	* the debugger is present or not.
	*/
	inline void DebuggerDependentPath( char* dest, size_t size, char* env, char* relative, char* append)
	{
		// Ensure the buffer WILL hold the full path.
		DBE_Assert( size >= 256);

		// Clear the destination buffer.
		dest[0] = '\0';

		if( IsDebuggerPresent()) {
			char* envPath( nullptr);
			size_t envPathSize( 0);

			// If this fails, it means the computer running the source code doesn't have the
			// environment variable.
#pragma warning( push)
#pragma warning( disable : 4390)
			if( _dupenv_s( &envPath, &envPathSize, env) != 0)
				DBE_AssertAlways();

			strcat_s( dest, size, envPath);
			strcat_s( dest, size, "\\");
			free( envPath);
#pragma warning( pop)
		}
		else {
			strcat_s( dest, size, relative);
		}

		strcat_s( dest, size, append);
	}
}	// namespace DBE

//-----------------------------------------------

//-----------------------------------------------
namespace DBE
{
	/**
	* Converts four 8-bit values into a compact 32-bit value. The parameters should be values
	* between 0 and 255 inclusively.
	*
	* @param r	:: The red value.
	* @param g	:: The green value.
	* @param b	:: The blue value.
	* @param a	:: The alpha value.
	*
	* @return The 32-bit value.
	*/
	inline u32 IntsToColour( u8 r, u8 g, u8 b, u8 a)
	{
		u8 colour[4];
		colour[0] = a;
		colour[1] = b;
		colour[2] = g;
		colour[3] = r;
		return( *reinterpret_cast<u32*>( colour));
	}

	/**
	* Converts a 32-bit value into four integer values. The parameters should be values
	* between 0 and 255 inclusively.
	*
	* @param r		:: The red value.
	* @param g		:: The green value.
	* @param b		:: The blue value.
	* @param a		:: The alpha value.
	* @param colour	:: The 32-bit value.
	*/
	inline void ColourToInts( u8& r, u8& g, u8& b, u8& a, u32 colour)
	{
		a = (u8)(((colour)			& 0xFF));
		b = (u8)(((colour >> 8)		& 0xFF));
		g = (u8)(((colour >> 16)	& 0xFF));
		r = (u8)(((colour >> 24)	& 0xFF));
	}

	/**
	* Converts a 32-bit value into four float values. The parameters should be values
	* between 0.0f and 1.0f inclusively.
	*
	* @param r		:: The red value.
	* @param g		:: The green value.
	* @param b		:: The blue value.
	* @param a		:: The alpha value.
	* @param colour	:: The 32-bit value.
	*/
	inline void ColourToFloats( float& r, float& g, float& b, float& a, u32 colour)
	{
		a = (float)(((colour)		& 0xFF) / 255.0f);
		b = (float)(((colour >> 8)	& 0xFF) / 255.0f);
		g = (float)(((colour >> 16)	& 0xFF) / 255.0f);
		r = (float)(((colour >> 24) & 0xFF) / 255.0f);
	}

	/**
	* Sets the red value in the 32-bit value.
	*
	* @param colour	:: The 32-bit value.
	* @param r		:: The red value.
	*/
	inline void SetColourRed( u32& colour, u8 r)
	{
		u8 c[4];
		ColourToInts( c[0], c[1], c[2], c[3], colour);

		colour = IntsToColour( r, c[1], c[2], c[3]);
	}
	
	/**
	* Sets the green value in the 32-bit value.
	*
	* @param colour	:: The 32-bit value.
	* @param g		:: The green value.
	*/
	inline void SetColourGreen( u32& colour, u8 g)
	{
		u8 c[4];
		ColourToInts( c[0], c[1], c[2], c[3], colour);

		colour = IntsToColour( c[0], g, c[2], c[3]);
	}
	
	/**
	* Sets the blue value in the 32-bit value.
	*
	* @param colour	:: The 32-bit value.
	* @param b		:: The blue value.
	*/
	inline void SetColourBlue( u32& colour, u8 b)
	{
		u8 c[4];
		ColourToInts( c[0], c[1], c[2], c[3], colour);

		colour = IntsToColour( c[0], c[1], b, c[3]);
	}
	
	/**
	* Sets the alpha value in the 32-bit value.
	*
	* @param colour	:: The 32-bit value.
	* @param a		:: The alpha value.
	*/
	inline void SetColourAlpha( u32& colour, u8 a)
	{
		u8 c[4];
		ColourToInts( c[0], c[1], c[2], c[3], colour);

		colour = IntsToColour( c[0], c[1], c[2], a);
	}
}	// namespace DBE

//-----------------------------------------------
#endif	// #ifndef DBEUtilitiesH