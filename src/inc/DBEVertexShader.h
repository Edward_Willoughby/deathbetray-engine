/********************************************************************
*
*	CLASS		:: DBE::VertexShader
*	DESCRIPTION	:: Holds the vital information for a vertex shader. It is designed to be as
*					transparent as possible so that it can be used as a base class.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 14
*
********************************************************************/

#ifndef DBEVertexShaderH
#define DBEVertexShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEShader.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class VertexShader : public DBE::Shader {
		public:
			/// Constructor.
			VertexShader();
			/// Destructor.
			virtual ~VertexShader();

			void LoadShader( LPCSTR p_fileName, const D3D_SHADER_MACRO* p_macros, const D3D11_INPUT_ELEMENT_DESC* p_vertDesc, UINT vertDescSize, LPCSTR p_entryPoint = "VSMain", LPCSTR p_profile = "vs_5_0");
			void LoadShader( const DBE::ShaderParams* p_params);
			void DeleteShader();
			
			ID3D11VertexShader* mp_VS;
			ID3D11InputLayout*	mp_IL;
			
		private:
			
			
			/// Private copy constructor to prevent accidental multiple instances.
			VertexShader( const DBE::VertexShader& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::VertexShader& operator=( const DBE::VertexShader& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEVertexShaderH
