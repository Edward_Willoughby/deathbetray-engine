/********************************************************************
*
*	CLASS		:: DBE::ShaderMgr
*	DESCRIPTION	:: Manages the shaders loaded in by the engine and cleans them up during shut down.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 14
*
********************************************************************/

#ifndef DBEShaderMgrH
#define DBEShaderMgrH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEUtilities.h"
#include "DBETypes.h"

namespace DBE {
	class Shader;
}
/********************************************************************
*	Defines and constants.
********************************************************************/
/// Constant for how many shaders the 'ShaderMgr' can store.
const static s32 MAX_SHADERS( 256);


/*******************************************************************/
namespace DBE
{
	class ShaderMgr {
		public:
			/// Constructor.
			ShaderMgr();
			/// Destructor.
			~ShaderMgr();
			
			/// Deletes any remaining shaders held by the manager.
			void Shutdown();

			/// Gets an existing shader from the manager.
			template<class T>
			bool GetShader( const char* name, T* &p_shader) {
				s32 index( this->FindShader( name));

				// If the shader doesn't exist...
				if( index == -1)
					return false;

				p_shader = dynamic_cast<T*>( m_shaders[index].mp_shader);
				++m_shaders[index].m_refCount;
				return true;
			}
			/// Adds a shader to the manager.
			template<class T>
			bool AddShader( const char* name, T* p_shader) {
				s32 index( this->FindShader( name));

				if( index != -1) {
					DebugTrace( "ShaderMgr Warning: Shader already exists: %s\n", name);
					return false;
				}

				// Find an empty slot; if there isn't one then either slim the number of shaders in the program
				// or increase the limit.
				index = this->FindEmptySlot();
				DBE_AssertWithMsg( index != -1, "ShaderMgr Error: Too many shaders.\n", 0);

				m_shaders[index].mp_shader = dynamic_cast<DBE::Shader*>( p_shader);
				strcpy_s( m_shaders[index].m_name, 256, name);
				m_shaders[index].m_refCount = 1;

				return true;
			}
			/// Removes a reference to a shader.
			template<class T>
			void RemoveShader( T* &p_shader) {
				// Early-out if the shader doesn't exist.
				if( p_shader == nullptr)
					return;

				s32 index( this->FindShader( dynamic_cast<DBE::Shader*>( p_shader)));

				// Early-out if the shader doesn't exist in the manager.
				if( index == -1) {
					DebugTrace( "ShaderMgr Warning: Attempting to delete non-existant shader.\n");
					return;
				}

				// Currently I'm not deleting shaders when they become unreferenced as shaders are likely to
				// 'come-and-go' regarding usage.

				--m_shaders[index].m_refCount;
				p_shader = nullptr;
			}
			
		private:
			struct ShaderInfo {
				ShaderInfo()
					: mp_shader( nullptr)
					, m_refCount( 0)
				{
					m_name[0] = '\0';
				}

				DBE::Shader*	mp_shader;
				char			m_name[256];
				s32				m_refCount;
			};

			/// Finds a shader in the manager.
			s32 FindShader( const char* name);
			/// Finds a shader in the manager.
			s32 FindShader( const DBE::Shader* p_shader);
			/// Finds an empty slot in the manager to add a new shader to.
			s32 FindEmptySlot();

			/// Deletes the shader at the array position.
			void DeleteShader( s32 index);

			ShaderInfo m_shaders[MAX_SHADERS];

			
			/// Private copy constructor to prevent accidental multiple instances.
			ShaderMgr( const DBE::ShaderMgr& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::ShaderMgr& operator=( const DBE::ShaderMgr& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEShaderMgrH
