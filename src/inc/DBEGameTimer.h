/********************************************************************
*
*	CLASS		:: DBE::GameTimer
*	DESCRIPTION :: Used to time... stuff.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2013 / 06 / 26
*	SOURCE		:: '3D Game Programming with DirectX 11' by Frank Luna.
*
********************************************************************/

#ifndef DBEGameTimerH
#define DBEGameTimerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <cassert>
#include <Windows.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class GameTimer {
		public:
			// constructor
			GameTimer();

			float TotalTime() const;	// in seconds
			float DeltaTime() const;	// in seconds

			void Reset();	// call before message loop
			void Start();	// call when unpaused
			void Stop();	// call when paused
			void Tick();	// call every frame

		private:
			double m_SecondsPerCount;
			double m_DeltaTime;

			__int64 m_BaseTime;
			__int64 m_PausedTime;
			__int64 m_StopTime;
			__int64 m_PrevTime;
			__int64 m_CurrTime;

			bool m_Stopped;
	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEGameTimerH