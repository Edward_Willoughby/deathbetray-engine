/********************************************************************
*
*	CLASS		:: DBE::App
*	DECRIPTION	:: The base class for a DirectX application. This handles all of the initialisation
*					and shutdown of the window and DirectX, and exposes functions the must be
*					redefined in the inherited class (including 'HandleStart', 'HandleUpdate',
*					'HandleRender', and 'HandleShutdown').
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 06 / 22
*
********************************************************************/

#ifndef DBEAppH
#define DBEAppH

/// This define makes the compile time shorter by excluding scarcely used windows functions.
#define WIN32_LEAN_AND_MEAN

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

#include "DBEAudioMgr.h"
#include "DBEFrameTimer.h"
#include "DBEInputMgr.h"
#include "DBELuaMgr.h"
#include "DBEMath.h"
#include "DBENetworkMgr.h"
#include "DBEShaderMgr.h"
#include "DBETextureMgr.h"
#include "DBEThreadPool.h"
#include "DBETypes.h"
#include "DBEUtilities.h"
#include "DBEVertexTypes.h"
#include "DBEWin7TaskBarButtonMgr.h"

namespace DBE {
	class Font;
	class Shader;
}

struct ShaderLightingSlots;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The default colour to clear the back buffer to.
#define DEFAULT_CLEAR_COLOUR	0x00000000

/// Enumeration for the states of what to do when the application goes out of focus.
enum FocusResponse {
	FR_Pause = 0,
	FR_SlowRendering,
	FR_Normal,
};
/// The default rendering speed of the application when not in focus.
#define DEFAULT_SLOW_RENDERING_SPEED	10

/// The maximum number of lights that can be in the scene (that can be set via 'App', anyway).
/// If you change this value, you have to manually change it in the macros that include normals in
/// the 'DBEVertexTypes' header file.
static const u8 MAX_NUM_LIGHTS( NUM_LIGHTS);

/**
* Holds the data required to set the information about the lights in the scene.
*/
struct ShaderLightParams {
	/**
	* Constructor.
	*/
	ShaderLightParams() {
		world = inverse = lightDir = lightPos = lightColour = lightAtten = lightSpots = lightCount = -1;
	}

	/**
	* Constructor.
	*/
	ShaderLightParams( s32 wor, s32 inv, s32 dir, s32 pos, s32 col, s32 att, s32 spo, s32 cou)
		: world( wor)
		, inverse( inv)
		, lightDir( dir)
		, lightPos( pos)
		, lightColour( col)
		, lightAtten( att)
		, lightSpots( spo)
		, lightCount( cou)
	{}

	s32 world;
	s32 inverse;
	s32 lightDir;
	s32 lightPos;
	s32 lightColour;
	s32 lightAtten;
	s32 lightSpots;
	s32 lightCount;
};


/*******************************************************************/
namespace DBE
{
	class App {
		public:
			/// Constructor.
			App();
			/// Destructor.
			~App();

			/// Initialises the application.
			bool Init( u32 clearColour = DEFAULT_CLEAR_COLOUR, HWND hWnd = NULL);
			/// Runs the application loop.
			void Run();
			/// Runs the application loop once.
			bool RunOnce();
			/// De-initalises the application.
			void Shutdown();
			/// Signals to the application to close.
			void Terminate();

			/**
			* Functions to be overridden by the inherited class.
			*/
			virtual bool HandlePreInit() { return true; }
			virtual bool HandleInit() = 0;
			virtual bool HandleInput() = 0;
			virtual bool HandleUpdate( float deltaTime) = 0;
			virtual bool HandleRender( float deltaTime) = 0;
			virtual bool HandleShutdown() = 0;

			/// Sets the back buffer colour.
			void SetClearColour( u32 clearColour = DEFAULT_CLEAR_COLOUR);
			/// Gets the back buffer colour.
			u32 GetClearColour() const;

			/// Sets the title of the application's window.
			void SetWindowTitle( const char* title);
			/// Sets the width and height of the window.
			void SetWindowDimensions( u32 width, u32 height);
			/// Gets the width and height of the window.
			DBE::Vec2 GetWindowDimensions() const;
			/// Gets the width and height of the window.
			void GetWindowDimensions( u32& width, u32& height) const;
			/// Gets the width of the window.
			u32 GetWindowWidth() const;
			/// Gets the height of the window.
			u32 GetWindowHeight() const;

			/// Checks to see if the graphics have been initialised.
			bool IsAppInitialised() const;
			/// Checks if the scene has already finished being rendered this frame.
			bool IsSceneRendered() const;
			/// Checks to see if the application is closing down.
			bool IsTerminating() const;
			/// Sets the application's response for when it's not in focus.
			void SetFocusResponse( FocusResponse response, s32 frameRate = DEFAULT_SLOW_RENDERING_SPEED);
			/// Gets the application's reponse state for when it's not in focus.
			FocusResponse GetFocusResponse() const;
			/// Checks to see if the application is in focus.
			bool IsInFocus() const;

			/// Locks the mouse to the boundries of the window.
			void LockCursor( bool lock = true);
			/// Displays the mouse cursor.
			void ShowCursor( bool visible = true);
			/// Prevents any manipulation of the mouse cursor.
			void DeferCursorControl( bool defer = true);

			/// Disable a light.
			void DisableLight( u8 light);
			/// Enable a directional light.
			void EnableLightDirectional( u8 light, const DBE::Vec3& worldDir, const DBE::Vec3& diffuseColour);
			/// Enable a point light.
			void EnableLightPoint( u8 light, const DBE::Vec3& worldPos, const DBE::Vec3& diffuseColour);
			/// Enable a spot light.
			void EnableLightSpot( u8 light, const DBE::Vec3& worldPos, const DBE::Vec3& worldDir, float theta, float phi, float fallOff, const DBE::Vec3& diffuseColour);
			/// Set the attenuation of an existing light.
			void SetLightAttenuation( u8 light, float range, float a0, float a1, float a2);
			/// Passes the lights to the shader.
			void PassLightsToShader( DBE::Shader* p_shader, D3D11_MAPPED_SUBRESOURCE& map, const ShaderLightParams& slots, const DBE::Matrix4& world);
			/// Passes the lights to the shader.
			void PassLightsToShader( DBE::Shader* p_shader, D3D11_MAPPED_SUBRESOURCE& map, const ShaderLightingSlots* slots, const DBE::Matrix4& world);

			/// Sets the blend state.
			void SetBlendState( bool blendEnable);
			/// Sets the depth stencil state.
			void SetDepthStencilState( bool depthTest, bool depthWrite);
			/// Sets the rasteriser state.
			void SetRasteriserState( bool backFaceCull, bool wireframe);
			/// Gets the sampler state for the application.
			ID3D11SamplerState* GetSamplerState( bool bilinear = false, bool mipmap = false, bool wrap = false) const;

			/// Sets the render target to the default.
			void SetDefaultRenderTarget();
			/// Clears the back buffer.
			void Clear();

			/// Setup the camera for orthographic projection.
			void SetOrthographicCamera();
			/// Setup the camera for perspective projection.
			void SetPerspectiveCamera();
			/// Get the application's aspect ratio based on the window's current width and height.
			float GetAspectRatio() const;

			/// Gets the WVP matrix.
			DBE::Matrix4 GetWVP() const;

			ID3D11Debug*			mp_D3DDebug;
			ID3D11Device*			mp_D3DDevice;
			ID3D11DeviceContext*	mp_D3DDeviceContext;
			ID3D11RenderTargetView*	mp_D3DRenderTargetView;
			ID3D11DepthStencilView* mp_D3DDepthStencilView;
		
			DBE::Matrix4 m_matProj;
			DBE::Matrix4 m_matView;
			DBE::Matrix4 m_matWorld;

			HWND	m_hWnd;
			HCURSOR	m_defaultCursor;

			WPARAM			m_mouseWheelInfo;
			MouseButtonInfo	m_mouseButtonInfo;

			DBE::FrameTimer m_frameTimer;

			DBE::AudioMgr				m_mgrAudio;
			DBE::InputMgr				m_mgrInput;
			DBE::LuaMgr					m_mgrLua;
			DBE::NetworkMgr				m_mgrNetwork;
			DBE::ShaderMgr				m_mgrShader;
			DBE::TextureMgr				m_mgrTexture;
			DBE::Win7TaskBarButtonMgr	m_mgrTaskBarButton;
			DBE::ThreadPool				m_mgrThreadPool;

			DBE::Font* mp_defaultFont;

		private:
			/**
			* Holds the data for a light.
			*/
			struct Light {
				enum LightType {
					LT_None = 0,
					LT_Directional,
					LT_Point,
					LT_Spot,
					LT_Count,
				};

				/**
				* Constructor.
				*/
				Light()
					: m_type( LT_None)
				{}

				LightType m_type;
				DBE::Vec3 m_pos;
				DBE::Vec3 m_direction;
				DBE::Vec3 m_colour;

				// Attenuation.
				float m_a0;
				float m_a1;
				float m_a2;
				float m_rangeSquared;

				// Spotlight-specific.
				float m_cosHalfTheta;
				float m_cosHalfPhi;
				float m_fallOff;

			};

			/// Outputs all the build information to the output window.
			void DumpBuildInfo() const;

			/// Performs any internal input.
			bool HandleInputInternal();
			/// Performs any internal updating.
			bool HandleUpdateInternal();
			/// Performs any internal rendering.
			bool HandleRenderInternal();
			/// Performs any internal shutdowns.
			bool HandleShutdownInternal();

			/// Prepares the application for rendering this frame.
			bool BeginScene();
			/// Signals the end of rendering for this frame.
			bool EndScene();

			/// Gets and performs any message sent to the program from the operating system.
			bool DoMessages();
			/// Processes the messages send to the window.
			static LPARAM CALLBACK WndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

			/// Gets information about the currently active display.
			bool GetDisplayInfo();

			/// Create the window that will be rendered to.
			bool CreateDisplayWindow( HWND hWnd);

			/// Initialise the DirectX functionality.
			bool StartD3D();
			/// Destroy the DirectX variables.
			void StopD3D();

			/// Creates the render targets and views that'll be rendered to.
			void RecreateRenderTargetsAndViews();
			/// Deletes the render targets and views.
			void ReleaseRenderTargetsAndViews();
			/// Clears the device context's state and flushes it.
			void ClearStateAndFlushDeviceContext();

			/// Converts the DirectX version enum into a human-readable string.
			const char* GetD3DFeatureLevelName( const D3D_FEATURE_LEVEL& level) const;

			IDXGISwapChain*		mp_DXGISwapChain;
			ID3D11Texture2D*	mp_D3DDepthStencilBuffer;

			Light m_lights[MAX_NUM_LIGHTS];

			// Not really sure how much of this is actually needed, but I'm going to use all of it for
			// now, just until I get stuff working.
			static const int BLEND_STATE_BLEND_ENABLE = 1 << 0;
			static const int NUM_BLEND_STATES = 2;
			ID3D11BlendState* mp_blendStates[NUM_BLEND_STATES];

			static const int DEPTH_STENCIL_STATE_DEPTH_ENABLE = 1 << 0;
			static const int DEPTH_STENCIL_STATE_DEPTH_WRITE_ENABLE = 1 << 1;
			static const int NUM_DEPTH_STENCIL_STATES = 4;
			ID3D11DepthStencilState* mp_depthStencilStates[NUM_DEPTH_STENCIL_STATES];

			static const int RASTERISER_STATE_WIREFRAME = 1 << 0;
			static const int RASTERISER_STATE_BACK_FACE_CULL = 1 << 1;
			static const int NUM_RASTERISER_STATES = 4;
			ID3D11RasterizerState* mp_rasteriserStates[NUM_RASTERISER_STATES];

			static const int SAMPLER_STATE_BILINEAR = 1 << 0;
			static const int SAMPLER_STATE_MIPMAP = 1 << 1;
			static const int SAMPLER_STATE_WRAP = 1 << 2;
			static const int NUM_SAMPLER_STATES = 8;
			ID3D11SamplerState* mp_samplerStates[NUM_SAMPLER_STATES];

			u32		m_clearColour;
			bool	m_graphicsIsInitialised;
			bool	m_renderTargetsInitialised;
			bool	m_playing;
			bool	m_endScene;

			bool	m_isFullScreen;
			u32		m_screenWidth;
			u32		m_screenHeight;
			u32		m_monitorRefreshRate;

			FocusResponse	m_focusResponse;
			bool			m_isInFocus;
			bool			m_cursorIsLocked;
			bool			m_cursorIsDisplayed;
			bool			m_cursorControlIsEnabled;

			LARGE_INTEGER m_oneFrame;
			LARGE_INTEGER m_sleepGap;
			LARGE_INTEGER m_nextUpdate;

			/// Private copy constructor to prevent multiple instances.
			App( const DBE::App&);
			/// Private assignment operator to prevent multiple instances.
			DBE::App& operator=( const DBE::App&);

	};
}	// namespace DBE

/// Static pointer to the application.
extern DBE::App* AppInstance;
/// Macro to easily access the application's static pointer.
#define GET_APP() AppInstance

/// Macro to easily create the entry point of the program.
#define APP_MAIN( AppType, BackBufferColour)				\
	AppType AppInstanceName;								\
	DBE::App* AppInstance( &AppInstanceName);				\
															\
	int WINAPI WinMain( HINSTANCE, HINSTANCE, LPSTR, int)	\
	{														\
		if( AppInstanceName.Init( BackBufferColour))		\
			AppInstanceName.Run();							\
															\
		AppInstanceName.Shutdown();							\
															\
		return 0;											\
	}

/// Macro that does declares the global pointer to the App, without running anything.
#define APP_DECLARE( AppType)					\
	AppType AppInstanceName;					\
	DBE::App* AppInstance( &AppInstanceName);

/// Macro to access the window's width.
#define SCREEN_WIDTH		GET_APP()->GetWindowWidth()
/// Macro to calculate half of the window's width.
#define HALF_SCREEN_WIDTH	SCREEN_WIDTH / 2
/// Macro to access the window's height.
#define SCREEN_HEIGHT		GET_APP()->GetWindowHeight()
/// Macro to calculate half of the window's height.
#define HALF_SCREEN_HEIGHT	SCREEN_HEIGHT / 2

/// Macro to easily access the audio manager.
#define MGR_AUDIO()			GET_APP()->m_mgrAudio
/// Macro to easily access the input manager.
#define MGR_INPUT()			GET_APP()->m_mgrInput
/// Macro to easily access the lua reader.
#define MGR_LUA()			GET_APP()->m_mgrLua
/// Macro to easily access the network manager.
#define MGR_NETWORK()		GET_APP()->m_mgrNetwork
/// Macro to easily access the shader manager.
#define MGR_SHADER()		GET_APP()->m_mgrShader
/// Macro to easily access the texture manager.
#define MGR_TEXTURE()		GET_APP()->m_mgrTexture
/// Macro to easily access the taskbar button manager.
#define MGR_TBBUTTON()		GET_APP()->m_mgrTaskBarButton
/// Macro to easily access the thread pool.
#define MGR_THREADPOOL()	GET_APP()->m_mgrThreadPool

/// Macro to easily access the frame timer.
#define FRAME_TIMER()		GET_APP()->m_frameTimer
/// Macro to easily access the default font.
#define DEFAULT_FONT()		GET_APP()->mp_defaultFont

/*******************************************************************/
#endif	// #ifndef DBEAppH