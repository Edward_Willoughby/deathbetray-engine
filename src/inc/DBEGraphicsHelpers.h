/********************************************************************
*
*	UTILITY FUNCTIONS
*	DESCRIPTION	:: These static functions assist with the creation of graphics related things, such
*					as DirectX buffers.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 06 / 22
*
********************************************************************/

#ifndef DBEGraphicsHelpersH
#define DBEGraphicsHelpersH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DirectXMath.h>

#include "DBETypes.h"

struct ID3D11Buffer;
struct ID3D11Device;
enum D3D11_USAGE;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
/// Creates a vertex buffer.
ID3D11Buffer* CreateImmutableVertexBuffer( ID3D11Device* p_device, UINT sizeBytes, const void* p_initialData);

/// Creates an index buffer.
ID3D11Buffer* CreateImmutableIndexBuffer( ID3D11Device* p_device, UINT sizeBytes, const void* p_initialData);

/// Creates a buffer.
ID3D11Buffer* CreateBuffer( ID3D11Device* p_device, UINT sizeBytes, D3D11_USAGE usage, UINT bindFlags, UINT cpuAccessFlags, const void* p_initialData);

/*******************************************************************/
#endif	// #ifndef GraphicsHelpersH