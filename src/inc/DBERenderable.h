/********************************************************************
*
*	CLASS		:: DBE::Renderable
*	DESCRIPTION	:: Base class for a renderable object in DirectX 11.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 07 / 23
*
********************************************************************/

#ifndef DBERenderableH
#define DBERenderableH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEMath.h"
#include "DBEMovable.h"

struct ID3D11Buffer;
/********************************************************************
*	Defines and constants.
********************************************************************/
//enum RenderableDebugFlags {
//	RDF_MeshLoaded		= 1 << 0,
//	RDF_TextureLoaded	= 1 << 1,
//	RDF_DrawBoundingBox	= 1 << 2,
//	RDF_Count,
//};


/*******************************************************************/
namespace DBE
{
	class Renderable : public Movable {
		public:
			/// Constructor.
			Renderable();
			/// Destructor.
			virtual ~Renderable();

			virtual bool OnUpdate( float deltaTime);
			virtual void OnRender( float deltaTime);

			/// Sets the colour for the renderable.
			void SetColour( u32 colour);
			/// Gets the colour for the renderable.
			u32 GetColour() const;

			/// Sets the alpha for the renderable.
			void SetAlpha( u8 alpha);
			/// Gets the alpha for the renderable.
			u8 GetAlpha() const;

			/**
			* Sets the object's base bounding sphere from the verticies that were used to create it.
			*
			* @param verticies :: The array of verticies that make up the mesh. The structure must
			*						have a Vec3 called 'pos' that stores the vertex's position.
			*/
			template<class T>
			void CreateBaseBoundingSphere( const T* verticies)
			{
				Vec3 vMin( FLT_MAX), vMax( FLT_MIN);

				for( u32 i( 0); i < m_vertexCount; ++i) {
					Vec3 pos( verticies[i].pos);

					// Minimum values.
					if( pos.GetX() < vMin.GetX())	vMin.SetX( pos.GetX());
					if( pos.GetY() < vMin.GetY())	vMin.SetY( pos.GetY());
					if( pos.GetZ() < vMin.GetZ())	vMin.SetZ( pos.GetZ());

					// Maximum values.
					if( pos.GetX() > vMax.GetX())	vMax.SetX( pos.GetX());
					if( pos.GetY() > vMax.GetY())	vMax.SetY( pos.GetY());
					if( pos.GetZ() > vMax.GetZ())	vMax.SetZ( pos.GetZ());
				}

				// Find the sphere's center.
				Vec3 center;
				center.SetX( vMin.GetX() + ((vMax.GetX() - vMin.GetX()) / 2));
				center.SetY( vMin.GetY() + ((vMax.GetY() - vMin.GetY()) / 2));
				center.SetZ( vMin.GetZ() + ((vMax.GetZ() - vMin.GetZ()) / 2));

				m_baseBoundingSphere.m_center = center;
				
				// Find the largest dimension and use that as the radius.
				float diameter( Max( vMax.GetX() - vMin.GetX(), vMax.GetY() - vMin.GetY()));
				diameter = Max( diameter, vMax.GetZ() - vMin.GetZ());

				m_baseBoundingSphere.m_radius = diameter / 2;
			}

			/// Get the bounding sphere of the object.
			const DBE::BoundingSphere* GetBoundingSphere() const;
			/// Updates the bounding sphere's radius.
			void UpdateBoundingSphere( bool fromVerticies = false);

			ID3D11Buffer*	mp_vertBuffer;
			ID3D11Buffer*	mp_indexBuffer;

			u32 m_vertexCount;
			u32 m_indexCount;

			u32 m_vertexStride;

		private:
			u32	m_colour;
			u8	m_alpha;

			//DBE::BoundingAABB	m_boundingAABB;
			//DBE::BoundingAABB	m_boundingFastAABB;
			DBE::BoundingSphere	m_boundingSphere;

			DBE::BoundingSphere m_baseBoundingSphere;

			//bool m_drawBoundingAABB;
			//bool m_drawBoundingFastAABB;
			//bool m_drawBoundingSphere;

			/// Private copy constructor to prevent multiple instances.
			Renderable( const DBE::Renderable&);
			/// Private assignment operator to prevent multiple instances.
			DBE::Renderable& operator=( const DBE::Renderable&);

	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBERenderableH