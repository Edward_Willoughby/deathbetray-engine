//-----------------------------------------------
//// VERTEX TYPES
//// DESCRIPTION :-
//	Provides the structures, descriptions, macros and sizes of the vertex types used.
//// CREATED BY :- Edward Willoughby
//// DATE :- 2013 / 08 / 17
//-----------------------------------------------

#ifndef DBEVertexTypesH
#define DBEVertexTypesH

//-----------------------------------------------
// Include libraries and header files
#include <stddef.h>
#include <d3d11.h>

#include "DBEColour.h"
#include "DBEMath.h"
//-----------------------------------------------

static const u8 NUM_LIGHTS( 4);
static const char* MAX_NUM_LIGHTS_STR = "4";

//-----------------------------------------------
struct VertPos3f {
	DBE::Vec3 pos;

	VertPos3f() {}

	VertPos3f( const DBE::Vec3& p)
		: pos( p)
	{}
};

const D3D11_INPUT_ELEMENT_DESC g_vertPos3fDesc[] = {
	{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,	0, offsetof( VertPos3f, pos),		D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

const D3D_SHADER_MACRO g_vertPos3fMacros[] = {
	{ nullptr },
};

const UINT g_vertPos3fSize = sizeof( g_vertPos3fDesc) / sizeof (g_vertPos3fDesc[0]);

//-----------------------------------------------
struct VertPos3fColour4ub {
	DBE::Vec3 pos;
	VertColour colour;

	VertPos3fColour4ub() {}

	VertPos3fColour4ub( const DBE::Vec3& p, VertColour c)
		: pos( p)
		, colour( c)
	{}
};

const D3D11_INPUT_ELEMENT_DESC g_vertPos3fColour4ubDesc[] = {
	{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,	0, offsetof( VertPos3fColour4ub, pos),		D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "COLOUR",		0, DXGI_FORMAT_R8G8B8A8_UNORM,	0, offsetof( VertPos3fColour4ub, colour),	D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

const D3D_SHADER_MACRO g_vertPos3fColour4ubMacros[] = {
	{ nullptr },
};

const UINT g_vertPos3fColour4ubSize = sizeof( g_vertPos3fColour4ubDesc) / sizeof (g_vertPos3fColour4ubDesc[0]);

//-----------------------------------------------
struct VertPos3fColour4ubNormal3f {
	DBE::Vec3 pos;
	VertColour colour;
	DBE::Vec3 normal;

	VertPos3fColour4ubNormal3f() {}

	VertPos3fColour4ubNormal3f( const DBE::Vec3& p, VertColour c, const DBE::Vec3& n)
		: pos( p)
		, colour( c)
		, normal( n)
	{}
};

const D3D11_INPUT_ELEMENT_DESC g_vertPos3fColour4ubNormal3fDesc[] = {
	{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,	0, offsetof( VertPos3fColour4ubNormal3f, pos),		D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "COLOUR",		0, DXGI_FORMAT_R8G8B8A8_UNORM,	0, offsetof( VertPos3fColour4ubNormal3f, colour),	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL",		0, DXGI_FORMAT_R32G32B32_FLOAT,	0, offsetof( VertPos3fColour4ubNormal3f, normal),	D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

const D3D_SHADER_MACRO g_vertPos3fColour4ubNormal3fMacros[] = {
	{ "MAX_NUM_LIGHTS", MAX_NUM_LIGHTS_STR },
	{ "LIT", nullptr },
	{ nullptr },
};

const UINT g_vertPos3fColour4ubNormal3fSize = sizeof( g_vertPos3fColour4ubNormal3fDesc) / sizeof( g_vertPos3fColour4ubNormal3fDesc[0]);

//-----------------------------------------------
struct VertPos3fColour4ubTex2f {
	DBE::Vec3 pos;
	VertColour colour;
	DBE::Vec2 tex;

	VertPos3fColour4ubTex2f( const DBE::Vec3& p, VertColour c, const DBE::Vec2& t)
		: pos( p)
		, colour( c)
		, tex( t)
	{}
};

const D3D11_INPUT_ELEMENT_DESC g_vertPos3fColour4ubTex2fDesc[] = {
	{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,	0, offsetof( VertPos3fColour4ubTex2f, pos),		D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "COLOUR",		0, DXGI_FORMAT_R8G8B8A8_UNORM,	0, offsetof( VertPos3fColour4ubTex2f, colour),	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,	0, offsetof( VertPos3fColour4ubTex2f, tex),		D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

const D3D_SHADER_MACRO g_vertPos3fColour4ubTex2fMacros[] = {
	{ "TEXTURED", nullptr },
	{ nullptr },
};

const UINT g_vertPos3fColour4ubTex2fSize = sizeof( g_vertPos3fColour4ubTex2fDesc) / sizeof( g_vertPos3fColour4ubTex2fDesc[0]);

//-----------------------------------------------
struct VertPos3fColour4ubNormal3fTex2f {
	DBE::Vec3 pos;
	VertColour colour;
	DBE::Vec3 normal;
	DBE::Vec2 tex;

	VertPos3fColour4ubNormal3fTex2f() {}

	VertPos3fColour4ubNormal3fTex2f( const DBE::Vec3& p, VertColour c, const DBE::Vec3& n, const DBE::Vec2& t)
		: pos( p)
		, colour( c)
		, normal( n)
		, tex( t)
	{}
};

const D3D11_INPUT_ELEMENT_DESC g_vertPos3fColour4ubNormal3fTex2fDesc[] = {
	{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,	0, offsetof( VertPos3fColour4ubNormal3fTex2f, pos),		D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "COLOUR",		0, DXGI_FORMAT_R8G8B8A8_UNORM,	0, offsetof( VertPos3fColour4ubNormal3fTex2f, colour),	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL",		0, DXGI_FORMAT_R32G32B32_FLOAT,	0, offsetof( VertPos3fColour4ubNormal3fTex2f, normal),	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,	0, offsetof( VertPos3fColour4ubNormal3fTex2f, tex),		D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

const D3D_SHADER_MACRO g_vertPos3fColour4ubNormal3fTex2fMacros[] = {
	{ "MAX_NUM_LIGHTS", MAX_NUM_LIGHTS_STR },
	{ "LIT",		nullptr },
	{ "TEXTURED",	nullptr },
	{ nullptr },
};

const UINT g_vertPos3fColour4ubNormal3fTex2fSize = sizeof( g_vertPos3fColour4ubNormal3fTex2fDesc) / sizeof( g_vertPos3fColour4ubNormal3fTex2fDesc[0]);

//-----------------------------------------------
#endif DBEVertexTypesH