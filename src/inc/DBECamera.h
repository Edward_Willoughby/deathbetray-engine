/********************************************************************
*
*	CLASS		:: DBE::Camera
*	DESCRIPTION	:: Controls the camera.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 12 / 29
*
********************************************************************/

#ifndef DBECameraH
#define DBECameraH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEMath.h"

namespace DBE {
	class Movable;
}
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class Camera {
		public:
			/// Constructor.
			Camera();
			/// Destructor.
			~Camera();
		
			/// Updates the camera.
			void Update();

			/// Attaches the camera to an object.
			void AttachToObject( DBE::Movable* p_target);

			/// Creates and sets the view matrix.
			DBE::Matrix4 ApplyViewMatrixLH() const;
			/// Creates and sets the perspective matrix.
			DBE::Matrix4 ApplyPerspectiveMatrixLH( float zNear, float zFar) const;
			/// Creates and sets the orthographic matrix.
			DBE::Matrix4 ApplyOrthoMatrixLH( float zNear, float zFar) const;

			float m_zDist;

			DBE::Vec3 m_position;
			DBE::Vec3 m_lookAt;
			DBE::Vec3 m_up;

			DBE::Movable* mp_target;

		private:
		
		
			/// Private copy constructor to prevent accidental multiple instances.
			Camera( const Camera&);
			/// Private assignment operator to prevent accidental multiple instances.
			Camera& operator=( const Camera&);
		
	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBECameraH
