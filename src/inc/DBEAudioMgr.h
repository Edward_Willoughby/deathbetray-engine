/********************************************************************
*
*	CLASS		:: DBE::AudioMgr
*	DESCRIPTION	:: Loads, stores and runs all the code for audio files.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 09 / 18
*
********************************************************************/

#ifndef DBEAudioMgrH
#define DBEAudioMgrH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <fmod.hpp>
#include <fmod_errors.h>

#include "DBETypes.h"

/********************************************************************
*	Defines and constants.
********************************************************************/
// The maximum number of sounds that can be played/stored at any one time. If the program requires
// more sounds to be played then either delete existing/unneeded sounds or increase the size of
// this constant.
const s32 MAX_AUDIO_CHANNELS( 100);

/// The maximum volume of an audio file.
const float VOLUME_MAX( 1.0f);
/// The minumum volume of an audio file.
const float VOLUME_MIN( 0.0f);

enum ReverbPreset {
	RP_None = 0,
	RP_Auditorium,
	RP_Cave,
	RP_Alley,
	RP_Forest,
	RP_Quarry,
	RP_Plain,
	RP_SewerPipe,
	RP_Underwater,
	RP_Count,
};


/*******************************************************************/
namespace DBE
{
	/**
	*	STRUCTURE	:: DBE::ReverbProps
	*	DESCRIPTION	:: Holds the data required to manipulate the audio based on the surroundings;
	*					e.g. make the S.F.X. seem like the player's in a pipe, or corridor, or
	*					auditorium etc.
	*					NOTE (from FMOD): Integer values that typically range from -10,000 to 1,000
	*					are represented in decibels and are of a logorithmic scale, whereas float
	*					values are always linear.
	*/
	struct ReverbProps {
		/// Constructor.
		ReverbProps( ReverbPreset r = ReverbPreset::RP_None);

		/// Set the member variables based on a preset from the 'ReverbPreset' enum.
		void UsePreset( ReverbPreset r);
		/// Set all of the member variables.
		void SetVars( s32 env, float envDiff, s32 room, s32 roomHF, s32 roomLF, float decay, float decayHF, float decayLF, s32 reflec, float reflecDelay,
					  s32 rev, float revDelay, float modTime, float modDepth, float refHF, float refLF, float diff, float density);

		/// Copies the variables into a structure for FMOD.
		void ConvertForFMOD( FMOD_REVERB_PROPERTIES& r);

									// MIN		MAX		DESCRIPTION
		s32 m_environment;			// -1		25		Sets all listener properties (-1 = off).
		float m_envDiffusion;		// 0.0		1.0		Environment diffusion.
		s32 m_room;					// -10000.0	0.0		Room effect level (at mid frequencies).	
		s32 m_roomHF;				// -10000.0	0.0		Relative room effect level at high frequencies.
		s32 m_roomLF;				// -10000.0	0.0		Relative room effect level at low frequencies.
		float m_decayTime;			// 0.1		20.0	Reverberation delay time at mid frequencies.
		float m_decayHFRatio;		// 0.1		2.0		High frequency to mid frequency decay time ratio.
		float m_decayLFRatio;		// 0.1		2.0		Low frequency to mid frequency decay time ratio.
		s32 m_reflections;			// -10000	1000	Early reflections level relative to room effect.
		float m_reflectionsDelay;	// 0.0		0.3		Initial reflection delay time.
		s32 m_reverb;				// -10000	2000	Late reverberation level relative to room effect.
		float m_reverbDelay;		// 0.0		0.1		Late reverberation delay time relative to initial reflection.
		float m_modulationTime;		// 0.04		4.0		Modulation time.
		float m_modulationDepth;	// 0.0		1.0		Modulation depth.
		float m_HFRef;				// 20.0		20000.0	Reference high frequency (hz).
		float m_LFRef;				// 20.0		1000.0	Reference low frequency (hz).
		float m_diffusion;			// 0.0		100.0	Value that controls the echo density in the late reverberation decay.
		float m_density;			// 0.0		100.0	Value that controls the modal density in the late reverberation decay.

	};

	class AudioMgr {
		public:
			/// Constructor.
			AudioMgr();
			/// Destructor.
			~AudioMgr();

			/// Initialises the audio manager.
			bool Init();

			/// Updates the FMOD system and clears out old handles in the array.
			void OnUpdate( float deltaTime);

			/// Loads an audio file into memory so it's less processor intensive to play.
			void LoadAudio( const char* fileName);

			/// Checks whether a file is currently playing or not.
			bool IsPlaying( const char* fileName) const;

			/// Plays an audio file once, regardless of whether it's already being played or not.
			void PlayAudioOneShot( const char* fileName, float volume = 1.0f);
			/// Plays an audio file, or resets it if it already exists.
			void PlayAudio( const char* fileName, float volume = 1.0f);
			/// Pauses/Unpauses an audio file.
			void PauseAudio( const char* fileName);
			/// Stops an audio file from playing.
			void StopAudio( const char* fileName);

			/// Gets the master volume.
			float GetMasterVolume();
			/// Gets the sound volume.
			float GetSoundVolume();
			/// Gets the music volume.
			float GetMusicVolume();
			/// Sets the master volume.
			void SetMasterVolume( float volume = 1.0f);
			/// Sets the sound volume.
			void SetSoundVolume( float volume = 1.0f);
			/// Sets the music volume.
			void SetMusicVolume( float volume = 1.0f);

			/// Gets the volums of an audio clip.
			float GetVolume( const char* fileName);
			/// Sets the volume of an audio clip.
			void SetVolume( const char* fileName, float volume = 1.0f);
			/// Gets the length of the audio clip, in milliseconds.
			u32 GetDuration( const char* fileName);
			/// Sets the position of the audio clip.
			void SetPlayPosition( const char* fileName, s32 pos = -1);

			/// Gets the frequency of an audio clip.
			float GetFrequency( const char* fileName);
			/// Sets the frequency of an audio clip.
			void SetFrequency( const char* fileName, float freq);

			/// Set the reverberation of a specific audio file.
			void SetReverb( const char* fileName, ReverbProps r);
			/// Get a textual representation of a reverberation preset.
			void GetReverbPresetName( ReverbPreset r, char* name) const;

			/// Stops all audio files from playing.
			void StopAllAudio();

			/// Deletes all the audio files currently stored in the Audio class.
			void DeleteAllAudio();

		private:
			/**
			* Holds the FMOD sound and channel instances that are required to modify an audio file
			* during play, as well as the name of the file to identify it.
			*/
			struct AudioContainer {
				AudioContainer()
					: mp_sound( nullptr)
					, mp_channel( nullptr)
					, m_volumeBase( 1.0f)
					, m_volume( 1.0f)
					, m_isMusic( false)
					, m_fileName( nullptr)
				{}

				//AudioContainer( FMOD::Sound* p_sound, FMOD::Channel* p_channel, const char* fileName, float volume = 1.0f)
				//	: mp_sound( p_sound)
				//	, mp_channel( p_channel)
				//	, m_volume( volume)
				//	, m_fileName( fileName)
				//{}

				~AudioContainer() {
					mp_sound->release();
				}

				void Reset() {
					if( mp_channel == nullptr || mp_sound == nullptr)
						return;

					mp_channel->stop();
					mp_sound->release();

					mp_channel = nullptr;
					mp_sound = nullptr;
				
					m_volumeBase = 1.0f;
					m_volume = 1.0f;
					m_fileName = nullptr;
				}

				FMOD::Sound*	mp_sound;
				FMOD::Channel*	mp_channel;
				float			m_volumeBase;
				float			m_volume;
				bool			m_isMusic;
				const char*		m_fileName;
			};
		
			/// Finds an empty slot in the array to insert a new Audio file into.
			s32 FindEmptyChannel() const;
			/// Finds a slot in the array with the corresponding file name.
			s32 FindChannel( const char* fileName) const;

			/// Creates a sound and inserts it into the array.
			void CreateSound( s32 index, const char* fileName, bool cache);

			/// Shuts down the Audio class and deletes the FMOD system.
			void Shutdown();

			/// Checks the result of an FMOD function and stores any error message it provides.
			bool FMODSuccess( FMOD_RESULT result);

			FMOD::System*	mp_system;
			AudioContainer	m_sounds[MAX_AUDIO_CHANNELS];

			bool m_volumeSoundChanged;
			bool m_volumeMusicChanged;

			float m_volumeMaster;
			float m_volumeSound;
			float m_volumeMusic;

			char* m_lastError;
		
			/// Private copy constructor to prevent multiple instances.
			AudioMgr( const DBE::AudioMgr&);
			/// Private assignment operator to prevent multiple instances.
			DBE::AudioMgr& operator=( const DBE::AudioMgr&);
		
	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEAudioMgrH