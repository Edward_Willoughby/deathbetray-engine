/********************************************************************
*
*	CLASS		:: DBE::Font
*	DESCRIPTION	:: Primitive font class to handle rendering of text on the screen.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 07 / 28
*	SOURCE		:: Tom Seddon's DirectX 11 Framework.
*
********************************************************************/

#ifndef DBEFontH
#define DBEFontH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DirectXMath.h>

#include "DBEColour.h"
#include "DBETypes.h"

struct Glyph;
struct ID3D11Texture2D;
struct ID3D11ShaderResourceView;
struct ID3D11Buffer;

namespace DBE {
	class Shader;
}
/********************************************************************
*	Defines and constants.
********************************************************************/
/// Default scale of fonts.
static const DirectX::XMFLOAT2 DEFAULT_SCALE( 1.0f, 1.0f);
/// Default colour of fonts.
static const VertColour DEFAULT_COLOUR( WHITE);

namespace DBE
{
	/**
	* Stores the scale and colour of the font.
	*/
	struct Style {
		/// Constructor.
		Style( const DirectX::XMFLOAT2& scale = DEFAULT_SCALE, const VertColour& colour = DEFAULT_COLOUR)
			: m_scale( scale)
			, m_colour( colour)
		{}

		DirectX::XMFLOAT2 m_scale;
		VertColour m_colour;
	};
}	// namespace DBE


/*******************************************************************/
namespace DBE
{
class Font {
	public:
		/// Destructor.
		~Font();

		/// Static function to create a font with custom information.
		static Font* CreateNewFont( const char* fontName = "", s32 height = 10, u32 flags = 0);

		/// Render the text on the screen.
		void RenderText( const DirectX::XMFLOAT3& startPos, const Style* p_style, const char* str);
		/// Render the text on the screen.
		void RenderTextf( const DirectX::XMFLOAT3& startPos, const Style* p_style, const char* format, ...);

		void RenderFont();

	private:
		/// Renders the characters sorted out by the 'RenderText' functions.
		void Render( s32 vertCount);

		/// Creates an HFONT.
		static HFONT CreateGDIFont( HDC hDC, const char* fontName, s32 height, u32 flags);
		static bool PaintAlphabet( HDC hDC, s32 width, s32 height, Glyph* p_glyphs);

		Glyph* mp_glyphs;
		
		ID3D11Texture2D*			mp_texture;
		ID3D11ShaderResourceView*	mp_textureView;

		ID3D11Buffer* mp_vertBuffer;
		ID3D11Buffer* mp_indexBuffer;

		DBE::Shader* mp_shader;
		
		s32 m_VSCBufferSlot;
		s32 m_PSCBufferSlot;

		s32 m_VSWvp;
		s32 m_PSTexture;

		/// Private constructor to force use of 'CreateNewFont'.
		Font();
		/// Private copy constructor to prevent multiple instances.
		Font( const DBE::Font&);
		/// Private assignment operator to prevent multiple instances.
		DBE::Font& operator=( const DBE::Font&);

	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEFontH