/********************************************************************
*
*	UTILITY		:: Collision
*	DESCRIPTION	:: Classes for holding bounding boxes of meshes and free functions for testing for
*					collision between those bounding boxes.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 10
*
********************************************************************/

#ifndef DBECollisionH
#define DBECollisionH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEMath.h"

namespace DBE {
	class Movable;
}
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	struct BoundingSphere {
		DBE::Vec3	m_center;
		float		m_radius;
	};

	struct BoundingAABB {
		DBE::Vec3	m_center;
		DBE::Vec3	m_min;
		DBE::Vec3	m_max;
	};

	namespace Collision {
		/// Converts an orthographic position to a 3D ray.
		extern void ConvertOrthographicPosToRay( const DBE::Vec2& pos, const DBE::Matrix4& worldMat, DBE::Vec3& rayOrigin, DBE::Vec3& rayDir);

		/// Test if a ray intersects a triangle.
		extern bool IntersectRayTriangle( const DBE::Vec3& origin, const DBE::Vec3& dir, const DBE::Vec3& v0, const DBE::Vec3& v1, const DBE::Vec3& v2, float* p_dist);

		/// Sphere - Sphere collision check.
		extern bool IntersectSphereSphere( const DBE::BoundingSphere* objA, const DBE::BoundingSphere* objB, float* p_dist);

		/// AABB - AABB collision checks.
		//extern bool IntersectAABBAABB( const DBE::BoundingAABB* objA, const DBE::BoundingAABB* objB, float* p_dist);

		/// AABB - Sphere collision checks.
		//extern bool IntersectAABBSphere( const DBE::BoundingAABB* objA, const DBE::BoundingSphere* objB, float* p_dist);
		//extern bool IntersectAABBSphere( const DBE::BoundingSphere* objA, const DBE::BoundingAABB* objB, float* p_dist);

		// (2D) Test if a point intersects a square.
		extern bool IntersectPointSquare( const DBE::Vec2& point, const DBE::Vec2& squareMin, const DBE::Vec2& squareMax);

	}	// namespace Collision
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBECollisionH
