/********************************************************************
*	Function definitions for the Emitter class.
********************************************************************/
#include <DBEApp.h>


/**
* Constructor.
*/
template<class T>
DBE::Emitter<T>::Emitter()
	: mp_particles( nullptr)
	, m_timePerSpawn( 1.0f)
	, m_accumulatedTime( 0.0f)
	, m_maxParticles( 0)
	, m_currentParticleCount( 0)
{}

/**
* Destructor.
*/
template<class T>
DBE::Emitter<T>::~Emitter() {
	this->DeleteParticleArray();
	T::ParticlesShutdown();
}

/**
* Initialise the emitter.
*
* @param maxParticles		:: The maximum number of particles this emitter can sustain.
* @param particlesPerSecond	:: The number of particles this emitter should spawn per second.
*
* @return True if everything was initialised correctly.
*/
template<class T>
bool DBE::Emitter<T>::Init( s32 maxParticles, float particlesPerSecond) {
	m_timePerSpawn = 1.0f / particlesPerSecond;

	// Just in case 'Init' is called twice, delete the existing particles.
	this->DeleteParticleArray();

	this->CreateParticleArray( maxParticles);

	if( mp_particles == nullptr)
		return false;

	return T::ParticlesInit();
}

/**
* Update the emitter (call 'Update()').
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything was updated correctly.
*/
template<class T>
bool DBE::Emitter<T>::OnUpdate( float deltaTime) {
	// Add a new particle, if enough time has passed.
	s32 particlesToAdd( 0);
	m_accumulatedTime += deltaTime;
	while( m_accumulatedTime >= m_timePerSpawn) {
		m_accumulatedTime -= m_timePerSpawn;
		++particlesToAdd;
	}

	if( particlesToAdd != 0)
		this->AddParticle( particlesToAdd);

	// Update the ones that are still alive.
	for( s32 i( 0); i < m_maxParticles; ++i)
		if( mp_particles[i]->IsActive())
			T::ParticleUpdate( deltaTime, mp_particles[i]);

	// Kill any particles that shouldn't exist any more.
	this->KillParticles();

	return true;
}

/**
* Render the particles (call 'Render()').
*
* @param deltaTime :: The time taken to render the last frame.
*/
template<class T>
void DBE::Emitter<T>::OnRender( float deltaTime) {
	DBE::Matrix4 world = GET_APP()->m_matWorld;

	T::ParticlePreRender( deltaTime);

	for( s32 i( 0); i < m_maxParticles; ++i)
		if( mp_particles[i]->IsActive())
			T::ParticleRender( deltaTime, mp_particles[i], world);
}

/**
* Add particles.
*
* @param count :: Default: 1. The number of particles to add.
*/
template<class T>
void DBE::Emitter<T>::AddParticle( s32 count /*= 1*/) {
	// Don't allow the number of particles to exceed the maximum number permitted by this emitter.
	if( m_currentParticleCount + count > m_maxParticles)
		count = m_maxParticles - m_currentParticleCount;

	for( s32 i( 0); i < count; ++i) {
		s32 index( 0);
		for( index; index < m_maxParticles; ++index)
			if( !mp_particles[index]->IsActive())
				break;

		// Something went wrong.
		if( index == m_maxParticles)
			break;

		mp_particles[index]->SetActive( true);
		T::ParticleSpawn( mp_particles[index]);

		++m_currentParticleCount;
	}
}

/**
* Clears all existing particles.
*/
template<class T>
void DBE::Emitter<T>::ClearParticles() {
	for( s32 i( 0): i < m_maxParticles; ++i) {
		// Skip the particle if it's not in use.
		if( !mp_particles[i]->IsActive())
			continue;

		mp_particles[i]->SetActive( false);

		T::ParticleDeath( mp_particles[i]);

		--m_currentParticleCount;
	}

	if( m_currentParticleCount != 0)
		DebugTrace( "Emitter::ClearParticles didn't clear correctly: %i particles unaccounted for.\n", m_currentParticleCount);

	m_currentParticleCount = 0;
}

/**
* Get the maximum number of particles this emitter can have.
*
* @return The maximum number of particles this emitter can have.
*/
template<class T>
s32 DBE::Emitter<T>::GetMaxNumberOfParticles() const {
	return m_maxParticles;
}

/**
* Get the current number of particles this emitter has.
*
* @return The current number of particles this emitter has.
*/
template<class T>
s32 DBE::Emitter<T>::GetNumberOfParticles() const {
	return m_currentParticleCount;
}

/**
* Get whether this emitter has any active particles.
*
* @return True if the emitter has active particles.
*/
template<class T>
bool DBE::Emitter<T>::HasActiveParticles() const {
	if( m_currentParticleCount > 0)
		return true;

	return false;
}

/**
* Turns each particle towards the camera.
*
* @param camPos :: The position of the camera that the particles turn to.
*/
template<class T>
void DBE::Emitter<T>::BillboardParticles( const DBE::Vec3& camPos) {
	for( s32 i( 0); i < m_maxParticles; ++i) {
		if( mp_particles[i]->IsActive()) {
			//T::ParticleBillboard( &mp_particles[i], this->GetPosition(), this->GetRotation(), camPos);
			Particle* particle( mp_particles[i]);
			Vec3 emitterPos( this->GetPosition());

			Vec3 parPos( particle->GetPosition() + emitterPos);
			Vec3 parNorm( 0.0f, 0.0f, 1.0f);

			Vec3 xVec( Vec3( 0.0f, parPos.GetY(), parPos.GetZ()) - Vec3( 0.0f, camPos.GetY(), camPos.GetZ()));
			Vec3 yVec( Vec3( parPos.GetX(), 0.0f, parPos.GetZ()) - Vec3( camPos.GetX(), 0.0f, camPos.GetZ()));

			float xCAngle( Dot( parNorm, xVec.GetNormalised()));
			float yCAngle( Dot( parNorm, yVec.GetNormalised()));

			float xAngle = DBE_ACos( xCAngle);
			float yAngle = DBE_ACos( yCAngle);

			float behindPlane( Dot( Vec3( 1.0f, 0.0f, 0.0f), (parPos - camPos).GetNormalised()));
			if( behindPlane < 0.0f)
				yAngle = DBE_ToRadians( 360.0f) - yAngle;

			behindPlane = Dot( Vec3( 0.0f, 0.0f, 1.0f), (parPos - camPos).GetNormalised());
			float abovePlane( Dot( Vec3( 0.0f, 1.0f, 0.0f), (parPos - camPos).GetNormalised()));
			const char* pos( nullptr);
			if( abovePlane < 0.0f) {
				if( behindPlane < 0.0f) {
					xAngle = DBE_ToRadians( 180.0f) - xAngle;
					//pos = "Above + Behind!\n";
				}
				else {
					// Doesn't need any modifiers.
					//pos = "Above!\n";
				}
			}
			else {
				if( behindPlane < 0.0f) {
					xAngle = DBE_ToRadians( 180.0f) + xAngle;
					//pos = "Below + Behind!\n";
				}
				else {
					xAngle = DBE_ToRadians( 360.0f) - xAngle;
					//pos = "Below!\n";
				}
			}

			//DebugTrace( "X: %f, Y: %f\n", DBE_ToDegrees( xAngle), DBE_ToDegrees( yAngle));
			//particle->RotateTo( xAngle, yAngle, 0.0f);
			particle->m_billBoardRotation = Vec3( xAngle, yAngle, 0.0f);
		}
	}
}

/**
* Remove particles that have expired.
*/
template<class T>
void DBE::Emitter<T>::KillParticles() {
	for( s32 i( 0); i < m_maxParticles; ++i) {
		if( !mp_particles[i]->IsActive())
			continue;

		if( !T::ParticleIsDead( mp_particles[i]))
			continue;

		mp_particles[i]->SetActive( false);

		T::ParticleDeath( mp_particles[i]);

		--m_currentParticleCount;
	}
}

/**
* Creates the particle array.
*
* @param maxParticles :: The maximum number of particles this emitter can sustain.
*/
template<class T>
void DBE::Emitter<T>::CreateParticleArray( s32 maxParticles) {
	m_maxParticles = maxParticles;
	mp_particles = new Particle*[maxParticles];

	T::ParticlesCreateArray( mp_particles, m_maxParticles);

	for( s32 i( 0); i < m_maxParticles; ++i)
		mp_particles[i]->SetActive( false);
}

/**
* Deletes the particle array.
*/
template<class T>
void DBE::Emitter<T>::DeleteParticleArray() {
	for( s32 i( 0); i < m_maxParticles; ++i)
		SafeDelete( mp_particles[i]);

	SafeDeleteArray( mp_particles);
}