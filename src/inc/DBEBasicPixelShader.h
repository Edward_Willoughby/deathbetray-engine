/********************************************************************
*
*	CLASS		:: DBE::BasicPixelShader
*	DESCRIPTION	:: Simple pixel shader.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 19
*
********************************************************************/

#ifndef BasicPixelShaderH
#define BasicPixelShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEPixelShader.h"

/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the basic pixel shader so it can be found in the shader manager.
static const char* gsc_basicPixelShaderName = "BasicPixelShader";


/*******************************************************************/
namespace DBE
{
	class BasicPixelShader : public DBE::PixelShader {
		public:
			/// Constructor.
			BasicPixelShader();
			/// Destructor.
			~BasicPixelShader();
			
			bool Init();

		private:
			
			
			/// Private copy constructor to prevent accidental multiple instances.
			BasicPixelShader( const BasicPixelShader& other);
			/// Private assignment operator to prevent accidental multiple instances.
			BasicPixelShader& operator=( const BasicPixelShader& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef BasicPixelShaderH
