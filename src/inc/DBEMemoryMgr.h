/********************************************************************
*
*	CLASS		:: DBE::MemoryMgr
*	DESCRIPTION	:: Keeps track of dynamically allocated objects and deletes any remaining references
*					when the program ends.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 15
*
********************************************************************/

#ifndef DBEMemoryMgrH
#define DBEMemoryMgrH

/********************************************************************
*	Include libraries and header files.
********************************************************************/


/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class MemoryMgr {
		public:
			/// Constructor.
			MemoryMgr();
			/// Destructor.
			~MemoryMgr();
			
			
		private:
			
			
			/// Private copy constructor to prevent accidental multiple instances.
			MemoryMgr( const DBE::MemoryMgr& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::MemoryMgr& operator=( const DBE::MemoryMgr& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEMemoryMgrH
