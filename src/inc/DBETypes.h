/********************************************************************
*
*	UTILITY		:: DBE::Types
*	DESCRIPTION	:: Defines for various types so that they're easier to recognise and use.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 06 / 22
*
********************************************************************/

#ifndef DBETypesH
#define DBETypesH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <Windows.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
// Unsigned variables.
typedef unsigned long long	u64;
typedef unsigned int		u32;
typedef unsigned short		u16;
typedef unsigned char		u8;
typedef u8					byte;

// Signed variables.
typedef __int64				s64;
typedef int					s32;
typedef short				s16;
typedef char				s8;


/********************************************************************/
#endif TypesH	// ifndef TypesH