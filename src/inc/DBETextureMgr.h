/********************************************************************
*
*	CLASS		:: DBE::TextureMgr
*	DESCRIPTION	:: Loads textures and keeps track of them.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 31
*
********************************************************************/

#ifndef TextureMgrH
#define TextureMgrH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBETypes.h"

struct ID3D11Texture2D;
struct ID3D11ShaderResourceView;
struct ID3D11SamplerState;
struct D3D11_TEXTURE2D_DESC;
struct D3D11_SUBRESOURCE_DATA;
struct D3D11_SHADER_RESOURCE_VIEW_DESC;
struct ID3D11Device;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The maximum number of textures that this manager can hold.
static const u32 gsc_maxTextures( 150);


/*******************************************************************/
namespace DBE
{
	/**
	* Structure to hold the data of a single texture.
	*/
	struct Texture {
		Texture()
			: mp_texture( nullptr)
			, mp_textureView( nullptr)
			, mp_samplerState( nullptr)
			, m_sizeInBytes( 0)
			, m_textureHeight( 0)
			, m_textureWidth( 0)
		{
			m_fileName[0] = '\0';
		}

		char m_fileName[256];

		ID3D11Texture2D*			mp_texture;
		ID3D11ShaderResourceView*	mp_textureView;
		ID3D11SamplerState*			mp_samplerState;

		s32 m_sizeInBytes;
		s32 m_textureHeight;
		s32 m_textureWidth;
	};

	class TextureMgr {
		public:
			/// Constructor.
			TextureMgr();
			/// Destructor.
			~TextureMgr();

			/// Deletes any remaining textures. This should only be called by 'App'.
			void Shutdown();

			/// Sets how many textures can be loaded in by the TextureMgr.
			//void SetTextureLimit( u32 limit);
			
			/// Loads a texture.
			Texture* LoadTexture( const char* fileName, ID3D11SamplerState* sampler);
			/// Gets a space in the texture array so a custom texture can be loaded in.
			void ReserveTexture( Texture* &texture);
			/// Creates a 2D texture.
			bool Create2DTexture( Texture* &texture, const char* fileName, D3D11_TEXTURE2D_DESC* td, D3D11_SUBRESOURCE_DATA* srd, D3D11_SHADER_RESOURCE_VIEW_DESC* srvd, ID3D11SamplerState* sampler);

			/// Gets the texture's dimensions.
			void GetTextureDimensions( Texture* t, s32* height, s32* width);

			/// Deletes a texture.
			void DeleteTexture( const char* fileName);
			/// Deletes a texture.
			void DeleteTexture( Texture* &texture);

			/// Set whether to release textures that no longer have any references.
			void DeleteUnreferencedTextures( bool del);
			
		private:
			struct TextureInfo {
				TextureInfo()
					: m_refCount( 0)
					, m_exists( false)
				{}

				Texture m_texture;
				s32 m_refCount;
				bool m_exists;
			};

			/// Creates a texture.
			void CreateTextureInfo( DBE::TextureMgr::TextureInfo* &ti, const char* fileName, ID3D11SamplerState* sampler, D3D11_TEXTURE2D_DESC* td = nullptr, D3D11_SUBRESOURCE_DATA* srd = nullptr, D3D11_SHADER_RESOURCE_VIEW_DESC* srvd = nullptr);

			/// Finds an empty slot in the array.
			s64 FindEmptySlot();
			/// Finds a texture in the array.
			TextureInfo* FindTexture( const char* fileName);
			/// Check whether a texture exists.
			bool TextureExists( DBE::TextureMgr::TextureInfo* t);
			/// Releases the texture from memory.
			void ReleaseTexture( DBE::TextureMgr::TextureInfo* texture);

			/// Loads a texture from a file.
			bool LoadTextureFromFile(  ID3D11Device* p_device, const char* p_fileName, ID3D11Texture2D** pp_texture, ID3D11ShaderResourceView** pp_textureResourceView);
			

			DBE::TextureMgr::TextureInfo	m_textures[gsc_maxTextures];
			u32								m_maxTextures;

			bool m_deleteUnreferenced;

			/// Private copy constructor to prevent accidental multiple instances.
			TextureMgr( const TextureMgr& other);
			/// Private assignment operator to prevent accidental multiple instances.
			TextureMgr& operator=( const TextureMgr& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef TextureMgrH
