/********************************************************************
*
*	CLASS		:: DBE::FrameTimer
*	DESCRIPTION	:: Calculates the amount of time taken between each frame, as well as how long the
*					application has been running for.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 07 / 24
*
********************************************************************/

#ifndef DBEFrameTimerH
#define DBEFrameTimerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBETypes.h"

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class FrameTimer {
		public:
			/// Constructor.
			FrameTimer();

			/// Returns the current F.P.S. of the application.
			float FPS() const;
			/// Returns the time taken to render the last frame.
			float DeltaTime() const;
			/// Returns the total time the application has been running for.
			float TotalTime() const;

			/// Indicates the start of a new frame (and the end of the previous).
			void Tick();

			/// Restricts the fps to a certain value.
			void SetFPS( bool constrict, u8 fps = 60);
			/// Sets the fps for when the application is out of focus.
			void SetSlowRenderingFPS( u8 fps);

			/// Sets the FrameTimer to use slow rendering because the application is not in focus.
			void SlowRendering( bool focus);

		private:
			/// Sets how long it should expect one frame, and the sleep gap, to be.
			void SetFrameTimes( u8 fps);

			s64 m_performanceFrequency;

			double m_secondsPerCount;
			double m_deltaTime;
			double m_oneFrame;
			double m_sleepGap;

			s64 m_startTime;
			s64 m_prevTime;
			s64 m_currTime;
		
			bool	m_constrictFPS;
			u8		m_desiredFPS;
			u8		m_slowRenderingFPS;

	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEFrameTimerH