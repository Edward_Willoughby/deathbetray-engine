/********************************************************************
*
*	CLASS		:: DBE::LuaMgr
*	DESCRIPTION	:: Handles all Lua interaction, given the file and function names that can be used.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2013 / 12 / 24
*
********************************************************************/

#ifndef DBELuaMgrH
#define DBELuaMgrH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <vector>

// This NEEDS to be included otherwise the compiler will bitch about unresolved external shite
// ... took me a good few days to discover this.
#pragma comment( lib, "lua5.1.lib")

#include <lua.hpp>

#include "DBETypes.h"

/********************************************************************
*	Defines and constants.
********************************************************************/
/**
* A list of all the possible return types that I'm willing to implement for the Lua A.P.I.
* at this time... More are possible from the Lua interpreter, but I don't understand them
* well enough to implement them at the moment.
*/
enum LuaReturnType {
	LRT_Bool = 0,
	LRT_Int,
	LRT_Float,
	LRT_Double,
};


/*******************************************************************/
namespace DBE
{
	class LuaMgr {
		public:
			/// Constructor.
			LuaMgr();
			/// Destructor.
			~LuaMgr();

			/// Typedef for the function prototype that is permitted to be registered.
			typedef s32 (*LuaFunction) (void);

			/// Opens a C++ function to be called by a Lua script.
			void RegisterFunction( const char* functionName, LuaFunction function);

			/// Stores the name of a Lua script file to be read.
			void SetScriptToRead( char* fileName);
			/// Reads a Lua script file and executes it.
			void ReadScript( char* fileName = nullptr);

			/// Executes Lua code from the given string.
			void ExecuteCode( char* code);

			/// Obtains the name of the last Lua function called.
			const char* GetFunctionName();
			/// Obtains the number of parameters of the last Lua function read in.
			bool GetNumberOfParameters( s32& number, s32 min, s32 max = -1);
		
			/**
			* Error checks and returns a parameter from the last read Lua function.
			*
			* @param index		:: The position of the parameter to be located (i.e. the first parameter
			*						is at index position '1').
			* @param type		:: The type of variable that should be found (only numbers).
			* @param variable	:: Where the value of the parameter will be stored.
			*
			* @return True if the parameter was found and valid.
			*/
			template<class T>
			bool GetParameter( s32 index, LuaReturnType type, T& variable)
			{
				bool success( false);

				switch( type) {
					// For a boolean parameter...
					case LRT_Bool:
						if( lua_isboolean( mp_lua, index)) {
							variable = lua_toboolean( mp_lua, index) == 1;
							success = true;
						}
						break;
					// For an integral parameter...
					case LRT_Int:
						if( lua_isnumber( mp_lua, index)) {
							variable = s32( lua_tointeger( mp_lua, index));
							success = true;
						}
						break;
					// For a float parameter...
					case LRT_Float:
						if( lua_isnumber( mp_lua, index)) {
							variable = float( lua_tonumber( mp_lua, index));
							success = true;
						}
						break;
					// For a double parameter...
					case LRT_Double:
						if( lua_isnumber( mp_lua, index)) {
							variable = double( lua_tonumber( mp_lua, index));
							success = true;
						}
						break;
				}

				return success;
			}

			/**
			* Error checks and returns a parameter from the last read Lua function. This function is
			* specifically for strings as it moans at me if I have it in the other function due to it
			* trying to convert a 'const char*' to a 'float'.
			*
			* @param index		:: The position of the parameter to be located (i.e. the first parameter
			*						is at index position '1').
			* @param variable	:: Where the value of the parameter will be stored.
			*
			* @return True if the parameter was found and valid.
			*/
			template<class T>
			bool GetParameterString( s32 index, T& variable)
			{
				if( lua_isstring( mp_lua, index)) {
					variable = lua_tostring( mp_lua, index);
					return true;
				}

				return false;
			}

			/**
			* Pushes a value onto the stack so that Lua can access the value.
			*
			* @param type	:: The type of variable that should be found (only numbers).
			* @param var	:: The value to be stored.
			*
			* @return True if the value was pushed.
			*/
			template<class T>
			bool ReturnValue( LuaReturnType type, const T& var)
			{
				bool success( false);

				switch( type) {
					// For a boolean parameter...
					case LRT_Bool:
						lua_pushboolean( mp_lua, var);
						success = true;
						break;
					// For an integral, float, or double parameter...
					case LRT_Int:
					case LRT_Float:
					case LRT_Double:
						lua_pushnumber( mp_lua, var);
						success = true;
						break;
				}

				return success;
			}

			/**
			* Pushes a value onto the stack so that Lua can access the value. This function is
			* specifically for strings.
			*
			* @param var	:: The value to be stored.
			* @param length	:: Default: -1. The length of the string. If this value is less than 0
			*					then it assumes that the string is null-terminated and passes the
			*					entire string.
			*
			* @return True if the value was pushed.
			*/
			template<class T>
			bool ReturnValueString( const T& var, s32 length = -1)
			{
				if( length < 0)
					lua_pushstring( mp_lua, var);
				else
					lua_pushlstring( mp_lua, var, length);

				return true;
			}

			/// Function exposed to Lua that allows text to be displayed in the output window.
			static s32 LuaFunction_DebugTrace();

		private:
			/// Function called by ALL Lua scripts that re-directs them to the member function.
			static s32 LuaFunctionInterpreter( lua_State* p_lua);
			/// Interprets the given Lua function name to call the related C++ one.
			s32 LuaFunctionNameInterpreter( const char* functionName);

			lua_State*	mp_lua;
			char*		mp_fileName;

			typedef std::pair<LuaFunction, const char*> FunctionPair;
			typedef std::vector<FunctionPair> FunctionVector;
			FunctionVector m_functions;

			/// Private copy constructor to prevent multiple instances.
			LuaMgr( const DBE::LuaMgr& other);
			/// Private assignment operator to prevent multiple instances.
			DBE::LuaMgr& operator=( const DBE::LuaMgr& other);
		
	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBELuaMgrH