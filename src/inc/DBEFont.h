/********************************************************************
*
*	CLASS		:: DBE::Font
*	DESCRIPTION	:: Renders text to the screen.
*	CREATED BY	:: Tom Seddon
*	DATE		:: -
*	SOURCE		:: Made by Tom Seddon for use by Sheffield Hallam students.
*
********************************************************************/

#ifndef HEADER_3325B9D7B7774F50B1D7AE51B29087B5
#define HEADER_3325B9D7B7774F50B1D7AE51B29087B5

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <d3d11.h>

#include "DBEColour.h"
#include "DBEMath.h"

namespace DBE {
	struct Texture;
}
//struct ID3D11Texture2D;
//struct ID3D11ShaderResourceView;
//struct ID3D11Buffer;
//enum D3D11_PRIMITIVE_TOPOLOGY;
//struct ID3D11SamplerState;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

namespace DBE
{
	class Font
	{
	public:
		enum CreateFlags
		{
			CREATE_BOLD = 1,
		};

		struct Style
		{
			static const DBE::Vec2 DEFAULT_SCALE;//(1,1).
			DBE::Vec2 scale;

			static const VertColour DEFAULT_COLOUR;//white
			VertColour colour;

			// Any members not set by a particular constructor are set to
			// their corresponding default values - hopefully in an
			// obvious way...
			Style();
			explicit Style(const VertColour &colour);
			Style(const VertColour &colour, const DBE::Vec2 &scale);
		};

		static bool InstallFont( const char* pFontName);
		static void UninstallFonts();

		static void Shutdown();
		
		static Font *CreateByName(const char *pTextureName, const char *pFontName, int height, uint32_t createFlags);

		~Font();

		// If pStyle is NULL, a default style will be used.
		//
		// Text is drawn in world space starting at `pos' then moving along
		// the positive X axis, with the positive Y axis being up:
		//
		//  Y Axis
		//    ^
		//    | TEXT
		//    +------> X Axis
		// 
		// If drawn at a scale of (1,1), 1 pixel in the font is equivalent to 1
		// world space unit.
		void DrawString2D(const DBE::Vec3 &pos, const DBE::Vec3& rot, float scale, const Style *pStyle, const char *pStr);
		void DrawStringf2D(const DBE::Vec3 &pos, const DBE::Vec3& rot, float scale, const Style *pStyle, const char *pFmt, ...);

		void DrawString3D( const DBE::Vec3 &pos, const DBE::Vec3& rot, float scale, const Style* pStyle, const char* pStr);
		void DrawStringf3D( const DBE::Vec3 &pos, const DBE::Vec3& rot, float scale, const Style* pStyle, const char* pFmt, ...);

		float GetFontHeight() const;
		void CalculateTextDimensions( const char* text, const DBE::Vec3& pos, DBE::Vec2& sMin, DBE::Vec2& sMax) const;

		void RenderFont();

	private:
		DBE::Matrix4 CalculateWorldMatrix( const DBE::Vec3& rot, const DBE::Vec3& scale);
		void PreRender( const DBE::Vec3& startPos, const Style* pStyle, const char* pStr);
		void Render( D3D11_PRIMITIVE_TOPOLOGY topology, ID3D11Buffer *pVertexBuffer, ID3D11Buffer *pIndexBuffer, unsigned numItems, Texture *pTexture);

		struct Glyph;
		Glyph *m_pGlyphs;

		Texture *m_pTexture;

		ID3D11Buffer *m_pVB, *m_pIB;

		static bool PaintAlphabet(HDC hDC, int width, int height, Glyph *pGlyphs);

		Font();

		Font(const DBE::Font&);
		DBE::Font &operator=(const DBE::Font&);
	};
}	// namespace DBE

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#endif//HEADER_3325B9D7B7774F50B1D7AE51B29087B5
