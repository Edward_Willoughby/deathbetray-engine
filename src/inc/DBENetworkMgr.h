/********************************************************************
*
*	CLASS		:: DBE::NetworkMgr
*	DESCRIPTION	:: Deals with the management of winsock and the passing of data between computers.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 09 / 18
*
********************************************************************/

#ifndef DBENetworkMgrH
#define DBENetworkMgrH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <string>
#include <winsock.h>

#include "DBETypes.h"

/********************************************************************
*	Defines and constants.
********************************************************************/
/// Maximum length of a message.
const u16 MAX_MSG_LENGTH( 256);

/// Enumeration for the status of a socket's connection to a server.
enum ServerConnection {
	SC_Connected = 0,
	SC_NotConnected,
	SC_Error,
	SC_Count,
};

/// Defines for the 'valid number' byte.
#define NETWORK_ZERO		0x0F
#define NETWORK_NON_ZERO	0xF0

namespace DBE
{
	/// Structure for a socket.
	struct NetworkSocket {
		NetworkSocket()
			//: m_socket( INVALID_SOCKET)
		{
			m_socket = INVALID_SOCKET;
		}

		~NetworkSocket() {
			this->Reset();
		}

		bool Exists() {
			bool exists( m_socket != INVALID_SOCKET);
			return exists;
		}

		void Reset() {
			if( m_socket != INVALID_SOCKET)
				closesocket( m_socket);
		
			m_socket = INVALID_SOCKET;
		}

		bool operator==( const DBE::NetworkSocket& other) {
			if( this->m_socket == other.m_socket)
				return true;
			else
				return false;
		}

		SOCKET		m_socket;
		sockaddr_in	m_sockAddr;
	};
}	// namespace DBE


/*******************************************************************/
namespace DBE
{
	class NetworkMgr {
		public:
			/// Constructor.
			NetworkMgr();
			/// Destructor.
			~NetworkMgr();
		
			/// Initialises the winsock library.
			bool Init();

			/// Forces the NetworkMgr to perform internet connection checks.
			void SetConnectionChecking( bool check);
			/// Checks that the computer is currently connected to the internet.
			bool IsConnectedToInternet() const;

			/// Fills a 'sockaddr' with all the information required to make a SOCKET.
			void FillSockAddr( DBE::NetworkSocket& socket, const char* serverName, u16 portNumber);
			/// Sets up a socket to connect to a server.
			bool CreateSocket( DBE::NetworkSocket& s, const char* serverName, u16 portNumber);
			/// Checks that a socket has successfully connected to a server.
			ServerConnection CheckSocketConnection( const DBE::NetworkSocket& s) const;
			/// Sets up a socket to listen to a port for incoming connections.
			bool CreateListeningSocket( DBE::NetworkSocket& s, u16 portNumber);
			/// Checks for an incoming connection and creates a 'NetworkSocket' for it.
			bool CheckNewConnection( const DBE::NetworkSocket& server, DBE::NetworkSocket& newConn);

			/// Sends a message through a SOCKET.
			void MessageSend( const DBE::NetworkSocket& socket, const char* msg);
			/// Returns a message sent through a SOCKET.
			bool MessageGet( const DBE::NetworkSocket& socket, std::string& msg);
			/// Check a message to see if it begins with a keyword.
			bool MsgStartsWithKeyword( const char* msg, const char* keyword);

			/// Returns the I.P. address held in a 'sockaddr'.
			char* GetSockAddrIP( const DBE::NetworkSocket& socket);
			/// Returns the port held in a 'sockaddr'.
			u16 GetSockAddrPort( const DBE::NetworkSocket& socket);

			/// Aligns an integer to correctly work in a 'sockaddr' structure.
			u16 NetworkAlignPortNumber( u16 portNumber);

			/// Gets the I.P. of a hostname (e.g. "www.google.com").
			u32 FindHostIP( const char* serverName);

			/**
			* Convert a number to chars so it can be sent in a message.
			*
			* @param buffer	:: The char array that will hold the message.
			* @param size	:: The length available in 'buffer'.
			* @param number	:: The number to convert to chars.
			*
			* @return True if it was converted successfully.
			*/
			template<class T>
			bool ConvertNumberToChars( char* buffer, s32 size, const T& number)
			{
				// Ensure there's enough space in the buffer to fit the data. The maximum space it'll require
				// amounts to:
				// +1 : One byte to indicate if the number is zero or not.
				// +1 : One byte which uses bit flags to indicate where the bytes are allocated;
				// +8 : The maximum number of bytes that the number could be;
				// +1 : One byte at the end for the null-terminator.
				// The reason for the second byte using bit flags is because if a number has a byte with
				// nothing in it then the buffer will stop early because it will appear to be a
				// null-terminating character. The first byte is used for a similar reason; a number that has
				// the value '0' will not have any bit flags; thus creating a null-terminator.
				if( size < sizeof( number) + 3)
					return false;

				// The number has too many bytes; sod off!
				if( sizeof( number) > 8)
					return false;

				char* zero( &buffer[0]);
				char* flags( &buffer[1]);
				char len( sizeof( number));
				char skipped( 0);

				// Reset all the flags.
				*flags = 0;

				for( char i( 0); i < len; ++i) {
					// Get the byte.
					char res(( number >> (i*8)) & 0xFF);

					// If there's no value in this byte, skip adding it to the message and increment the
					// counter.
					if( res == 0) {
						skipped += 1;
						continue;
					}

					// If it does contain a value, add it to the buffer at the correct position (compensating
					// for any 'skipped' bytes)...
					buffer[(i+2) - skipped] = res;

					// ... and set the flag to indicate where the byte lies.
					char val( 1);
					val = val << i;

					// Bitwise OR the existing flags with the new flag.
					// I can't bitshift the '1' directly into the correct place because it'd be a pain the ass
					// trying to work out the value something like "if i == 2 then OR with 10, but if i == 7
					// then OR with 64". So I set the flag in another char and OR them together (to maintain
					// any existing flags, while also adding the new one).
					*flags = *flags | val;
				}

				// If it skipped every byte then it means the value is 0, so set the byte to indicate that, as
				// well as setting the 'skipped' value to make sure the null-terminator position is correctly
				// calculated.
				if( skipped == len) {
					*zero = NETWORK_ZERO;
					skipped = len + 1;
				}
				else {
					*zero = NETWORK_NON_ZERO;
				}

				// Add the null-terminator.
				buffer[(len-skipped) + 2] = 0;

				return true;
			}

			/**
			* Convert a message from chars to a number.
			*
			* @param buffer	:: The char array that holds the message.
			* @param size	:: The length of 'buffer'.
			* @param number	:: The variable that will hold the result.
			*
			* @return How many bytes were read off the front of the 'buffer'.
			*/
			template<class T>
			s32 ConvertCharsToNumber( const char* buffer, s32 bufferSize, T& number)
			{
				// Early-out if the expected variable is larger than eight bytes.
				if( sizeof( number) > 8) {
					number = 0;
					return 0;
				}

				// Early-out if the value is zero.
				if( buffer[0] & NETWORK_ZERO) {
					number = 0;
					return 1;
				}

				char flags( buffer[1]);
				char noOfBytes( 0);

				// Count the number of bytes.
				for( char i( 0); i < 8; ++i) {
					char val( 1);
					val = val << i;

					if( flags & val)
						++noOfBytes;
				}

				// The buffer size in inconsistent with the number of required bytes.
				if( bufferSize < noOfBytes + 2)
					return 0;

				char bytes[8];
				char found( 0);

				for( char i( 0); i < 8; ++i) {
					char val( 1);
					val = val << i;

					// Check if this byte should have a value in it.
					if( flags & val) {
						bytes[i] = buffer[found+2];

						++found;
					}
					else {
						bytes[i] = 0;
					}
				}

				number = *reinterpret_cast<int*>( bytes);

				return noOfBytes + 2;
			}
		
		private:
			bool m_checkConn;
		
			/// Private copy constructor to prevent accidental multiple instances.
			NetworkMgr( const DBE::NetworkMgr& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::NetworkMgr& operator=( const DBE::NetworkMgr& other);
		
	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBENetworkMgrH
