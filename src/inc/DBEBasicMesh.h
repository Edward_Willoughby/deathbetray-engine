/********************************************************************
*
*	CLASS		:: DBE::BasicMesh
*	DESCRIPTION	:: Basic mesh with only a position and colour as placeholders.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 06 / 24
*
********************************************************************/

#ifndef DBEBasicMeshH
#define DBEBasicMeshH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBERenderable.h"
#include "DBETypes.h"

namespace DBE {
	class BasicPixelShader;
	class BasicVertexShader;
}

struct VertPos3fColour4ubNormal3f;
/********************************************************************
*	Defines and constants.
********************************************************************/
/**
*	ENUMRATION	:: MeshType
*	DESCRIPTION	:: 
*/
enum MeshType {
	MT_Plane = 0,
	MT_Cube,
	MT_Sphere,
	MT_Cylinder,
	MT_Cone,
	MT_Cross,
	MT_Count,
};

/// Define for the default colour of basic meshes, if a colour isn't set.
#define DEFAULT_MESH_COLOUR SILVER


/*******************************************************************/
namespace DBE
{
	class BasicMesh : public DBE::Renderable {
		public:
			/// Constructor.
			BasicMesh( MeshType type, float param1 = 1.0f, float param2 = 1.0f, float param3 = 1.0f, u32 colour = 0);
			/// Destructor.
			~BasicMesh();

			/// Renders the object.
			void OnRender( float deltaTime);

			VertPos3fColour4ubNormal3f* mp_verts;
			UINT* mp_indicies;
			
			DBE::BasicVertexShader*	mp_VS;
			DBE::BasicPixelShader*	mp_PS;

		private:
			/// Creates the mesh data for a plane.
			void CreatePlane( float width = 1.0f, float height = 1.0f, float depth = 1.0f, u32 colour = 0);
			/// Creates the mesh data for a cube.
			void CreateCube( float width = 1.0f, float height = 1.0f, float depth = 1.0f, u32 colour = 0);
			/// Creates the mesh data for a sphere.
			void CreateSphere( float radius = 1.0f, u32 vSegments = 1.0f, u32 hSegments = 1.0f, u32 colour = 0);
			/// Creates the mesh data for a cone.
			void CreateCone( float baseRadius = 1.0f, float topRadius = 1.0f, float height = 1.0f, u32 colour = 0);
			/// Creates the mesh data for a cross.
			void CreateCross( float width = 1.0f, float height = 1.0f, float depth = 1.0f, u32 colour = 0);

			/// Helper function to compute a point on a unit circle, aligned to the x/z plane.
			DBE::Vec3 GetCircleVector( s32 i, s32 tess);
			/// Helper function to compute a tangent to a circle... *shrug*.
			DBE::Vec3 GetCircleTangent( s32 i, s32 tess);
			/// Helper function to generate a flat triangle fan cap for cones and cylinders.
			void CreateCylinderCap( VertPos3fColour4ubNormal3f* verts, s32& vertCount, UINT* indicies, s32& indexCount, s32 tess, float height, float radius, u32 colour, bool isTop);


			/// Private copy constructor to prevent multiple instances.
			BasicMesh( const DBE::BasicMesh&);
			/// Private assignment operator to prevent multiple instances.
			DBE::BasicMesh& operator=( const DBE::BasicMesh&);

	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEBasicMeshH