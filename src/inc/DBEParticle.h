/********************************************************************
*
*	CLASS		:: DBE::Particle
*	DESCRIPTION	:: Holds the data for a particle.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 21
*
********************************************************************/

#ifndef DBEParticleH
#define DBEParticleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEMath.h"
#include "DBEMovable.h"

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class Particle : public DBE::Movable {
		public:
			/// Constructor.
			Particle();
			/// Destructor.
			virtual ~Particle();

			/// Gets the colour of the particle.
			u32 GetColour() const;
			/// Sets the colour of the particle.
			void SetColour( u32 colour);

			/// Gets whether the particle exists or not.
			bool IsActive() const;
			/// Sets whether the particle exists or not.
			void SetActive( bool active);

			/// Assignment operator.
			Particle& operator=( const Particle&);

			float m_age;

			DBE::Vec3 m_billBoardRotation;

		private:
			u32 m_colour;
			bool m_active;

			/// Private copy constructor to prevent accidental multiple instances.
			Particle( const Particle&);

	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEParticleH
