/********************************************************************
*
*	CLASS		:: DBE::InputMgr
*	DESCRIPTION	:: Handles input for DirectX. When getting input, ensure that you only query a key
*					ONCE per frame. Querying more than once can mess up the results, so store the
*					return value (KeyState) in a variable and use that through-out the rest of the
*					'HandleInput' function. If you're only querying a key once, then it's fine to
*					use the 'KeyPressed', 'KeyHeld', and 'KeyReleased' functions.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 02 / 08
*
********************************************************************/

#ifndef DBEInputMgrH
#define DBEInputMgrH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEMath.h"
#include "DBETypes.h"

/********************************************************************
*	Defines and constants.
*********************************************************************/
/// The maximum number of button sequences allowed.
static const int MAX_NUM_BUTTON_SEQUENCES( 16);

/// State for the key this frame.
enum KeyState {
	KS_None = 0,
	KS_Pressed,
	KS_Held,
	KS_Released,
	KS_Count,
};

///// Quick macro for testing if the KeyState is none.
//#define KeyStateNone( s)		(s == KeyState::KS_None)
///// Quick macro for testing if the KeyState is pressed.
//#define KeyStatePressed( s)		(s == KeyState::KS_Pressed)
///// Quick macro for testing if the KeyState is held.
//#define KeyStateHeld( s)		(s == KeyState::KS_Held)
///// Quick macro for testing if the KeyState is released.
//#define KeyStateReleased( s)	(s == KeyState::KS_Released)

/// States for the mouse wheel position.
enum MouseWheelState {
	MWS_Backward = -1,	// Towards the user.
	MWS_Resting,		// Not rotated enough to perform a 'click'.
	MWS_Forward,		// Away from the user.
	MWS_Count,
};

/**
* STRUCTURE		:: MouseButtonInfo
* DESCRIPTION	:: Holds the data pertaining to a mouse click (including the button and the
*					position).
*/
struct MouseButtonInfo {
	MouseButtonInfo() {
		for( u8 i( 0); i < 3; ++i) {
			m_mouseUp[i] = false;

			m_posUp[i] = 0;
			m_posDown[i] = 0;
		}
	}

	bool m_mouseUp[3];

	LPARAM m_posUp[3];
	LPARAM m_posDown[3];
};


/********************************************************************/
namespace DBE
{
	class InputMgr {
		public:
			/// Constructor.
			InputMgr();
			/// Destructor.
			~InputMgr();

			/// Updates the D3DInputMgr.
			bool OnUpdate( float deltaTime);

			/// Gets the key's state.
			KeyState GetKeyState( u16 key);
			/// Indicates if a button is being held down.
			bool KeyHeld( u16 key);
			/// Indicates if a button was pressed last frame.
			bool KeyPressed( u16 key);
			/// Indicates if a button was released last frame.
			bool KeyReleased( u16 key);

			/// Gets the last position the specified mouse button's 'up' event was triggered.
			bool GetButtonUpPos( u16 button, DBE::Vec2& pos) const;
			/// Gets the last position the specified mouse button's 'down' event was triggered.
			bool GetButtonDownPos( u16 button, DBE::Vec2& pos) const;
			/// Gets the position of the mouse.
			bool GetMousePos( DBE::Vec2& pos) const;
			/// Sets the position of the mouse.
			bool SetMousePos( const DBE::Vec2& pos);
			/// Converts the mouse position to screen coordinates.
			DBE::Vec2 ConvertMousePosToScreenCoordinates( const DBE::Vec2& pos) const;
			/// Gets the mouse wheel's rotation.
			MouseWheelState GetMouseWheelState() const;

			/// Check whether or not the cursor is currently inside the program's window.
			bool CursorIsInsideWindow() const;

			/// Locks/Unlocks all input controls.
			void LockControls( bool isLocked = true);

			void CreateButtonSequence( const char* sequenceName, const u16* buttons, u16 buttonCount, u32 time = 0);
			bool IsSequenceCompleted( const char* sequenceName);
			void EnableSequence( const char* sequenceName, bool enabled);
			void ResetSequence( const char* sequenceName);

		private:
			/**
			* Contains the information for a certain button sequence.
			*/
			struct ButtonSequence {
				ButtonSequence() {
					this->Reset();
				}

				void Reset() {
					m_name = nullptr;
					m_buttons = nullptr;
					m_index = 0;
					m_completed = false;
					m_enabled = true;
					m_timeLeft = 0;
					m_timeToComplete = 0;
				}
			
				const char* m_name;
				const u16*	m_buttons;
				u16			m_buttonCount;
				u16			m_index;
				bool		m_completed;
				bool		m_enabled;

				u32			m_timeLeft;
				u32			m_timeToComplete;
			};

			u16 FindSequence( const char* sequenceName);

			/// Returns whether or not a specified key is down.
			bool IsDown( u16 button);

			/// I'm pretty sure it only goes up to 256, but to make it easier just in case...
			static const int NUM_OF_KEYS = 256;

			bool m_checkedStates[NUM_OF_KEYS];
			bool m_states[NUM_OF_KEYS];
			bool m_prevStates[NUM_OF_KEYS];


			bool		m_mouseButtonStates[3];
			DBE::Vec2	m_mouseButtonPosDown[3];
			DBE::Vec2	m_mouseButtonPosUp[3];

			bool m_isLocked;

			MouseWheelState	m_mouseWheelState;

			ButtonSequence	m_buttonSequences[MAX_NUM_BUTTON_SEQUENCES];
			//bool			m_stopSequence;

			/// Private copy constructor to prevent multiple instances.
			InputMgr( const InputMgr&);
			/// Private assignment operator to prevent multiple instances.
			DBE::InputMgr& operator=( const InputMgr&);

	};
}	// namespace DBE

/********************************************************************/
#endif	// #ifndef DBEInputMgrH