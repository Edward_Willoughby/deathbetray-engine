/********************************************************************
*
*	CLASS		:: DBE::PixelShader
*	DESCRIPTION	:: Holds the vital information for a pixel shader. It is designed to be as
*					transparent as possible so that it can be used as a base class.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 14
*
********************************************************************/

#ifndef DBEPixelShaderH
#define DBEPixelShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEShader.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class PixelShader : public DBE::Shader {
		public:
			/// Constructor.
			PixelShader();
			/// Destructor.
			virtual ~PixelShader();
			
			void LoadShader( LPCSTR p_fileName, const D3D_SHADER_MACRO* p_macros, LPCSTR p_entryPoint = "PSMain", LPCSTR p_profile = "ps_5_0");
			void LoadShader( const DBE::ShaderParams* p_params);
			void DeleteShader();

			ID3D11PixelShader* mp_PS;

		private:
			
			
			/// Private copy constructor to prevent accidental multiple instances.
			PixelShader( const DBE::PixelShader& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::PixelShader& operator=( const DBE::PixelShader& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEPixelShaderH
