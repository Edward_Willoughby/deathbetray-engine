/********************************************************************
*
*	CLASS		:: DBE::ThreadPool
*	DESCRIPTION	:: Contains and distributes threads, when requested.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 12
*
********************************************************************/

#ifndef DBEThreadPoolH
#define DBEThreadPoolH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBETypes.h>

/********************************************************************
*	Defines and constants.
********************************************************************/
/// The maximum number of threads this pool can hold.
const s32 MAX_THREADS( 64);

/// Typedef for the prototype of the function that'll be called by the thread.
typedef void (*ThreadFunction)( void*);


/*******************************************************************/
namespace DBE
{
	class ThreadPool {
		public:
			/// Constructor.
			ThreadPool();
			/// Destructor.
			~ThreadPool();
		
			/// Initialises the thread pool.
			bool Init();
			/// Updates the threads and frees up HANDLEs that have finished.
			bool OnUpdate( float deltaTime);
			/// Waits for any remaining threads and cleans up.
			void Shutdown();

			/// Creates a new thread in suspended mode (call 'RunThread()' to start it).
			s32 CreateThread( ThreadFunction func, void* p_arguments);
			/// Starts a thread executing.
			void RunThread( s32 index);
			/// Pauses a thread (debugging purposes ONLY).
			void PauseThread( s32 index);

			/// Checks whether a thread has finished executing or not.
			bool IsThreadFinished( s32 index) const;
			/// Gets the number of threads that currently exist (active OR suspended).
			s32 GetThreadCount() const;
		
		private:
			/// Finalises the deletion of a thread and frees up the HANDLE.
			void CloseThread( s32 index);

			/// Called by all threads before calling their 'custom' function.
			static unsigned __stdcall ThreadFunc( void* p_arguments);

			HANDLE	m_threads[MAX_THREADS];
			s32		m_activeThreadCount;

		
			/// Private copy constructor to prevent accidental multiple instances.
			ThreadPool( const DBE::ThreadPool& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::ThreadPool& operator=( const DBE::ThreadPool& other);
		
	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef ThreadPoolH
