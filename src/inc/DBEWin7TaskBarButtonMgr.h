/********************************************************************
*
*	CLASS		:: DBE::Win7TaskBarButtonMgr
*	DESCRIPTION	:: Provides access to the window's taskbar button to set information. The class
*					stores all the information passed to it so the last information to be passed
*					through can be checked.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 09 / 11
*
********************************************************************/

#ifndef DBEWin7TaskBarButtonMgrH
#define DBEWin7TaskBarButtonMgrH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <Shobjidl.h>
#include <Windows.h>

#include "DBETypes.h"

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class Win7TaskBarButtonMgr {
		public:
			/// Constructor.
			Win7TaskBarButtonMgr();
			/// Destructor.
			~Win7TaskBarButtonMgr();

			/// Initialises the manager; MUST be called before any other functions.
			void Init( HWND hWnd);
			/// Returns whether or not the manager has been initialised with an HWND correctly.
			bool IsInitialised();

			/// Get the HWND that the manager is linked to.
			HWND GetHWND() const;
			/// Gets the current state of the progress bar.
			TBPFLAG GetCurrentState() const;
			/// Gets the last 'completed' value sent to the manager.
			u64 GetCompleted() const;
			/// Gets the last 'total' value sent to the manager.
			u64 GetTotal() const;

			/// Sets the progress bar as a percentage of completed/total.
			void ProgressStart( u64 completed, u64 total);
			/// Sets the progress bar to be paused.
			void ProgressPause( u64 completed = 0, u64 total = 0);
			/// Sets the progress bar to be unknown.
			void ProgressIndeterminant();
			/// Sets the progress bar to indicate an error occurred.
			void ProgressError( u64 completed = 0, u64 total = 0);
			/// Removes the progress bar.
			void ProgressDone();

			/// Make the window's taskbar button flash until the user focusses the window.
			void Flash();
			/// Make the window's taskbar button flash a specific number of times.
			void FlashCount( u8 count);

		private:
			HWND m_hWnd;
			bool m_initialised;

			ITaskbarList3* mp_taskbar;

			TBPFLAG m_currentState;
			u64		m_completed;
			u64		m_total;

			/// Private copy constructor to prevent multiple instances.
			Win7TaskBarButtonMgr( const DBE::Win7TaskBarButtonMgr&);
			/// Private assignment operator to prevent multiple instances.
			Win7TaskBarButtonMgr& operator=( const DBE::Win7TaskBarButtonMgr&);

	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEWin7TaskBarButtonMgr