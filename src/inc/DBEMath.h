/********************************************************************
*
*	UTILITY		:: Math
*	DESCRIPTION	:: Provides mathematical utility functions and containers.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 12 / 19
*
********************************************************************/

#ifndef DBEMathH
#define DBEMathH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <algorithm>
#include <DirectXMath.h>

#include "DBEUtilities.h"
/********************************************************************
*	Defines and constants.
********************************************************************/
/// Defines for alternate names for math routines for easy use.
#define DBE_Pow		pow
#define DBE_Sqrt	sqrtf

#define DBE_Ceil		ceilf
#define DBE_Floor		floorf
#define DBE_Abs			abs
#define DBE_Absf		fabsf
#define DBE_Mod( x, y)	(x % y)
#define DBE_Modf		fmod

#define DBE_Sin		sinf
#define DBE_Cos		cosf
#define DBE_Tan		tanf

#define DBE_ASin	asinf
#define DBE_ACos	acosf
#define DBE_ATan	atanf
#define DBE_ATan2	atan2f

#define DBE_Pi		DirectX::XM_PI
#define DBE_PiDiv2	DirectX::XM_PIDIV2
#define DBE_2Pi		DirectX::XM_2PI

#define DBE_Infinity	FLT_MAX
#define DBE_MAX			FLT_MAX
#define DBE_MIN			FLT_MIN

#define DBE_ToDegrees	DirectX::XMConvertToDegrees
#define DBE_ToRadians	DirectX::XMConvertToRadians


/*******************************************************************/
namespace DBE
{
	/**
	* Returns a copy of the lowest of two values.
	*
	* @param a :: The first value.
	* @param b :: The second value.
	*
	* @return The lowest of the two parameters.
	*/
	template<class T>
	inline T Min( const T& a, const T& b)
	{
		return (a < b) ? a : b;
	}

	/**
	* Returns a copy of the highest of two values.
	*
	* @param a :: The first value.
	* @param b :: The second value.
	*
	* @return The highest of the two parameters.
	*/
	template<class T>
	inline T Max( const T& a, const T& b)
	{
		return (a > b) ? a : b;
	}

	/**
	* Floating point specialisation.
	*/
	template<>
	inline float Min<float>( const float& a, const float& b)
	{
		return std::min<float>( a, b);
	}

	/**
	* Floating point specialisation.
	*/
	template<>
	inline float Max<float>( const float& a, const float& b)
	{
		return std::max<float>( a, b);
	}

	/**
	* Constricts a value to be between two specified values.
	*
	* @param value	:: The value to be clamped.
	* @param lower	:: The lowest that 'value' should be.
	* @param upper	:: The highest that 'value' should be.
	*
	* @return A value between 'lower' and 'upper' that 'value' is closest to.
	*/
	template<class T>
	inline T Clamp( const T& value, const T& lower, const T& upper)
	{
		DBE_Assert( upper > lower);

		return DBE::Max( lower, DBE::Min( upper, value));
	}

	/****************************************************************
	*
	*	CLASS		:: DBE::Vec2
	*	DESCRIPTION	:: Holds two floats.
	*	CREATED BY	:: Edward Willoughby
	*	DATE		:: 2014 / 12 / 19
	*
	****************************************************************/
	class Vec2 {
		public:
			/// Constructor.
			Vec2();
			/// Constructor.
			Vec2( float s);
			/// Constructor.
			Vec2( float x, float y);
			
			/// Constructor.
			Vec2( const DirectX::XMFLOAT2& vec);
			/// Constructor.
			Vec2( const DirectX::XMVECTOR& vec);

			/// Copy constructor.
			Vec2( const DBE::Vec2& vec);

			/// Get the x value.
			float GetX() const;
			/// Get the y value.
			float GetY() const;
			/// Get the vector.
			DirectX::XMFLOAT2 GetVector() const;
			/// Get the vector in 32-bit aligned format.
			DirectX::XMVECTOR GetXMVector();
			/// Get the vector in 32-bit aligned format.
			DirectX::XMVECTOR GetXMVectorConst() const;
			
			/// Sets the x value.
			void SetX( float x);
			/// Sets the y value.
			void SetY( float y);

			/// Get the length of the vector.
			float GetLength() const;

			/// Normalises the vector.
			void Normalise();
			/// Gets a normalised version of the vector.
			DBE::Vec2 GetNormalised() const;

			/// Sets all values to positive.
			void Abs();
			/// Gets a version of the vector that has all values set to positive.
			DBE::Vec2 GetAbs() const;

			/// Converts the vector to be in degrees.
			void ToDegrees();
			/// Gets a converted vector that's in degrees.
			DBE::Vec2 GetDegrees() const;
			/// Converts the vector to be in radians.
			void ToRadians();
			/// Gets a converted vector that's in radians.
			DBE::Vec2 GetRadians() const;

			/// Check if the vector's values are 0.0f.
			bool IsZero();

			/// Overloaded operators.
			DBE::Vec2 operator +( const DBE::Vec2& r) const;
			DBE::Vec2 operator -( const DBE::Vec2& r) const;
			DBE::Vec2 operator *( const DBE::Vec2& r) const;
			DBE::Vec2 operator *( const float r) const;
			void operator +=( const DBE::Vec2& r);
			void operator -=( const DBE::Vec2& r);
			void operator *=( const float r);
			void operator /=( const float r);

			/// Negation operator.
			Vec2 operator-() const;
			
			/// Assignment operator.
			void operator=( const DBE::Vec2& r);
			/// Assignment operator.
			void operator=( const DirectX::XMFLOAT2& r);
			/// Assignment operator.
			void operator=( const DirectX::XMVECTOR& r);
			/// Comparison operator.
			bool operator ==( const DBE::Vec2& r) const;
			/// Comparison operator.
			bool operator !=( const DBE::Vec2& r) const;

			/// Implicit conversion operator.
			operator DirectX::XMVECTOR() const;
			
		private:
			DirectX::XMFLOAT2 m_vector;
			
	};

	/****************************************************************
	*
	*	CLASS		:: DBE::Vec3
	*	DESCRIPTION	:: Holds three floats.
	*	CREATED BY	:: Edward Willoughby
	*	DATE		:: 2014 / 12 / 19
	*
	****************************************************************/
	class Vec3 {
		public:
			/// Constructor.
			Vec3();
			/// Constructor.
			Vec3( float s);
			/// Constructor.
			Vec3( float x, float y, float z);
			
			/// Constructor.
			Vec3( const DirectX::XMFLOAT3& vec);
			/// Constructor.
			Vec3( const DirectX::XMVECTOR& vec);

			/// Copy constructor.
			Vec3( const DBE::Vec3& vec);

			/// Get the x value.
			float GetX() const;
			/// Get the y value.
			float GetY() const;
			/// Get the z value.
			float GetZ() const;
			/// Gets the x and y values of the vector.
			DBE::Vec2 GetXY() const;
			/// Get the vector.
			DirectX::XMFLOAT3 GetVector() const;
			/// Get the vector in 32-bit aligned format.
			DirectX::XMVECTOR GetXMVector();
			/// Get the vector in 32-bit aligned format.
			DirectX::XMVECTOR GetXMVectorConst() const;
			
			/// Sets the x value.
			void SetX( float x);
			/// Sets the y value.
			void SetY( float y);
			/// Sets the z value.
			void SetZ( float z);

			/// Get the length of the vector.
			float GetLength() const;

			/// Normalises the vector.
			void Normalise();
			/// Gets a normalised version of the vector.
			DBE::Vec3 GetNormalised() const;

			/// Sets all values to positive.
			void Abs();
			/// Gets a version of the vector that has all values set to positive.
			DBE::Vec3 GetAbs() const;

			/// Converts the vector to be in degrees.
			void ToDegrees();
			/// Gets a converted vector that's in degrees.
			DBE::Vec3 GetDegrees() const;
			/// Converts the vector to be in radians.
			void ToRadians();
			/// Gets a converted vector that's in radians.
			DBE::Vec3 GetRadians() const;

			/// Check if the vector's values are 0.0f.
			bool IsZero();

			/// Overloaded operators.
			DBE::Vec3 operator +( const DBE::Vec3& r) const;
			DBE::Vec3 operator -( const DBE::Vec3& r) const;
			DBE::Vec3 operator *( const DBE::Vec3& r) const;
			DBE::Vec3 operator *( const float r) const;
			void operator +=( const DBE::Vec3& r);
			void operator -=( const DBE::Vec3& r);
			void operator *=( const float r);
			void operator /=( const float r);

			/// Negation operator.
			Vec3 operator-() const;

			/// Assignment operator.
			void operator=( const DBE::Vec3& r);
			/// Assignment operator.
			void operator=( const DirectX::XMFLOAT3& r);
			/// Assignment operator.
			void operator=( const DirectX::XMVECTOR& r);
			/// Comparison operator.
			bool operator ==( const DBE::Vec3& r) const;
			/// Comparison operator.
			bool operator !=( const DBE::Vec3& r) const;

			/// Implicit conversion operator.
			operator DirectX::XMVECTOR() const;
			
		private:
			DirectX::XMFLOAT3 m_vector;
			
	};

	/****************************************************************
	*
	*	CLASS		:: DBE::Vec4
	*	DESCRIPTION	:: Holds four floats.
	*	CREATED BY	:: Edward Willoughby
	*	DATE		:: 2014 / 12 / 19
	*
	****************************************************************/
	class Vec4 {
		public:
			/// Constructor.
			Vec4();
			/// Constructor.
			Vec4( float s);
			/// Constructor.
			Vec4( float x, float y, float z, float w);
			/// Constructor.
			Vec4( const DBE::Vec3& v, float w);
			
			/// Constructor.
			Vec4( const DirectX::XMFLOAT4& vec);
			/// Constructor.
			Vec4( const DirectX::XMVECTOR& vec);

			/// Copy constructor.
			Vec4( const DBE::Vec4& vec);

			/// Get the x value.
			float GetX() const;
			/// Get the y value.
			float GetY() const;
			/// Get the z value.
			float GetZ() const;
			/// Get the w value.
			float GetW() const;
			/// Gets the x and y values.
			DBE::Vec2 GetXY() const;
			/// Gets the x, y, and z values.
			DBE::Vec3 GetXYZ() const;
			/// Get the vector.
			DirectX::XMFLOAT4 GetVector() const;
			/// Get the vector in 32-bit aligned format.
			DirectX::XMVECTOR GetXMVector();
			/// Get the vector in 32-bit aligned format.
			DirectX::XMVECTOR GetXMVectorConst() const;

			/// Sets the x value.
			void SetX( float x);
			/// Sets the y value.
			void SetY( float y);
			/// Sets the z value.
			void SetZ( float z);
			/// Sets the w value.
			void SetW( float w);

			/// Get the length of the vector.
			float GetLength() const;

			/// Normalises the vector.
			void Normalise();
			/// Gets a normalised version of the vector.
			DBE::Vec4 GetNormalised() const;

			/// Sets all values to positive.
			void Abs();
			/// Gets a version of the vector that has all values set to positive.
			DBE::Vec4 GetAbs() const;

			/// Converts the vector to be in degrees.
			void ToDegrees();
			/// Gets a converted vector that's in degrees.
			DBE::Vec4 GetDegrees() const;
			/// Converts the vector to be in radians.
			void ToRadians();
			/// Gets a converted vector that's in radians.
			DBE::Vec4 GetRadians() const;

			/// Check if the vector's values are 0.0f.
			bool IsZero();

			/// Overloaded operators.
			DBE::Vec4 operator +( const DBE::Vec4& r) const;
			DBE::Vec4 operator -( const DBE::Vec4& r) const;
			DBE::Vec4 operator *( const DBE::Vec4& r) const;
			DBE::Vec4 operator *( const float r) const;
			void operator +=( const DBE::Vec4& r);
			void operator -=( const DBE::Vec4& r);
			void operator *=( const float r);
			void operator /=( const float r);

			/// Negation operator.
			Vec4 operator-() const;

			/// Assignment operator.
			void operator=( const DBE::Vec4& r);
			/// Assignment operator.
			void operator=( const DirectX::XMFLOAT4& r);
			/// Assignment operator.
			void operator=( const DirectX::XMVECTOR& r);
			/// Comparison operator.
			bool operator ==( const DBE::Vec4& r) const;
			/// Comparison operator.
			bool operator !=( const DBE::Vec4& r) const;

			/// Implicit conversion operator.
			operator DirectX::XMVECTOR() const;
			
		private:
			DirectX::XMFLOAT4 m_vector;
			
	};

	/****************************************************************
	*
	*	CLASS		:: DBE::Matrix4
	*	DESCRIPTION	:: Holds 16 floats (in a 4x4 grid).
	*	CREATED BY	:: Edward Willoughby
	*	DATE		:: 2015 / 06 / 10
	*
	****************************************************************/
	class Matrix4 {
		public:
			/// Constructor.
			Matrix4();
			/// Constructor.
			Matrix4( const DBE::Matrix4& m);
			/// Constructor.
			Matrix4( const DirectX::XMFLOAT4X4& m);
			/// Constructor.
			Matrix4( const DirectX::XMMATRIX& m);

			/// Set the matrix to the identity.
			void SetIdentity();
			
			/// Get the matrix.
			DirectX::XMFLOAT4X4 GetMatrix() const;
			/// Get the matrix in 32-bit aligned format.
			DirectX::XMMATRIX GetXMMatrix() const;

			/// Assignment operator.
			void operator=( const DBE::Matrix4& r);
			/// Assignment operator.
			void operator=( const DirectX::XMFLOAT4X4& r);
			/// Assignment operator.
			void operator=( const DirectX::XMMATRIX& r);

			/// Implicit conversion operator.
			operator DirectX::XMFLOAT4X4() const;
			/// Implicit conversion operator.
			operator DirectX::XMMATRIX() const;

		private:
			DirectX::XMFLOAT4X4 m_mat;

	};

	/// Calculates the Dot product of two 'Vec2's.
	extern float Dot( const DBE::Vec2& a, const DBE::Vec2& b);
	/// Calculates the Dot product of two 'Vec3's.
	extern float Dot( const DBE::Vec3& a, const DBE::Vec3& b);
	/// Calculates the Dot product of two 'Vec4's.
	extern float Dot( const DBE::Vec4& a, const DBE::Vec4& b);

	/// Calculates the Cross product of two 'Vec3's
	extern DBE::Vec3 Cross( const DBE::Vec3& a, const DBE::Vec3& b);

	/**
	* Calculates the distance between two vectors.
	*
	* @param a :: The first vector.
	* @param b :: The second vector.
	*
	* @return The distance between the two vectors.
	*/
	template<class T>
	inline float Distance( const T& a, const T& b)
	{
		return T( a - b).GetLength();
	}

	static const DBE::Vec3 Vec3UnitScale(	1.0f);
	static const DBE::Vec3 Vec3XAxis(		1.0f, 0.0f, 0.0f);
	static const DBE::Vec3 Vec3YAxis(		0.0f, 1.0f, 0.0f);
	static const DBE::Vec3 Vec3ZAxis(		0.0f, 0.0f, 1.0f);

	static const DBE::Vec4 Vec4UnitScale(	1.0f);
	static const DBE::Vec4 Vec4XAxis(		1.0f, 0.0f, 0.0f, 0.0f);
	static const DBE::Vec4 Vec4YAxis(		0.0f, 1.0f, 0.0f, 0.0f);
	static const DBE::Vec4 Vec4ZAxis(		0.0f, 0.0f, 1.0f, 0.0f);
	static const DBE::Vec4 Vec4WAxis(		0.0f, 0.0f, 0.0f, 1.0f);

	typedef DirectX::XMFLOAT3X3	Matrix3;

}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEMathH
