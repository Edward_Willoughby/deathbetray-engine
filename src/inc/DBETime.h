/********************************************************************
*
*	CLASS		:: DBE::Time
*	DESCRIPTION	:: Provides functionality to get the system time.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 23
*
********************************************************************/

#ifndef TimeH
#define TimeH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBETypes.h>

/********************************************************************
*	Defines and constants.
********************************************************************/
/**
* Variables for the current date.
*/
struct CurrentDate {
	u16 m_years;
	u16 m_months;
	u16 m_days;
};

/**
* Variables for the current time.
*/
struct CurrentTime {
	u16 m_hours;
	u16 m_minutes;
	u16 m_seconds;
};

/**
* Although 'CurrentTimeAndDate' doesn't have any properties, it is designed as a container to
* combine the 'CurrentDate' and 'CurrentTime' structs.
*/
struct CurrentTimeAndDate : public CurrentDate, public CurrentTime {

};


/*******************************************************************/
namespace DBE
{
	class Time {
		public:
			/// Updates the Time class's local variables (this should only be called by D3DApp).
			static void Update();
			/// Deletes the Time class (this should only be called by D3DApp).
			static void Shutdown();
			
			/// Gets the current system time.
			static CurrentTime GetTime();
			/// Gets the current system date.
			static CurrentDate GetDate();
			/// Gets the current system time and date.
			static CurrentTimeAndDate GetTimeAndDate();

			/// Gets the current system time as a string.
			static char* GetTimeString( char* buffer, u32 size);
			/// Gets the current system date as a string.
			static char* GetDateString( char* buffer, u32 size);

			/// Gets the amount of time, in microseconds, that's passed since the program started.
			static u64 GetTimeSinceStartUp();
			
		private:
			/// Checks that the class exists, if it doesn't then it creates it.
			static void EnsureInstantiated();

			/// Converts a number to a two digit string.
			static void ConvertNumberToChars( s32 number, char word[2]);

			
			/// Private constructor.
			Time();
			/// Private copy constructor to prevent accidental multiple instances.
			Time( const Time&);
			/// Private assignment operator to prevent accidental multiple instances.
			Time& operator=( const Time&);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef TimeH
