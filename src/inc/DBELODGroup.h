/********************************************************************
*
*	CLASS		:: DBE::LODGroup
*	DESCRIPTION	:: Holds multiple quality versions of the same texture.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 20
*
********************************************************************/

#ifndef DBELODGroupH
#define DBELODGroupH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBETextureMgr.h>

/********************************************************************
*	Defines and constants.
********************************************************************/
enum LevelOfDetail {
	LOD_Low = 0,
	LOD_Medium,
	LOD_High,
	LOD_Extreme,
	LOD_Count,
};

enum LODState {
	LODS_Unavailable = 0,
	LODS_NotLoaded,
	LODS_Loading,
	LODS_Loaded,
	LODS_Count,
};


/*******************************************************************/
namespace DBE
{
	class LODGroup {
		public:
			/// Constructor.
			LODGroup( const char* texturePaths[LOD_Count], LevelOfDetail lod);
			/// Destructor.
			~LODGroup();
			
			/// Sets the level of detail that the texture should use.
			void SetLevelOfDetail( LevelOfDetail lod);
			/// Get the level of detail that the texture should use.
			LevelOfDetail GetLevelOfDetail() const;

			/// Gets the correct texture based on the desired quality level.
			DBE::Texture* GetTexture( bool usable = true);

			/// Checks to make sure the LODGroup has at least one texture that is usable.
			bool HasOneUsableTexture();

		private:
			/// Gets the texture that is closest to 'm_lod' and loaded in.
			LevelOfDetail GetLoadedTexture();
			/// Gets the texture that is closest to 'm_lod'.
			LevelOfDetail GetAvailableTexture();

			/// Loads in a texture in a separate thread.
			static void Thread_LoadTexture( void* p_arguments);

			LevelOfDetail m_lod;

			char			m_texturePaths[4][256];
			DBE::Texture*	mp_textures[LOD_Count];
			LODState		m_textureStates[LOD_Count];

			
			/// Private copy constructor to prevent accidental multiple instances.
			LODGroup( const DBE::LODGroup& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::LODGroup& operator=( const DBE::LODGroup& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBELODGroupH
