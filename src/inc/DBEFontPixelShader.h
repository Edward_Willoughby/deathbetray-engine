/********************************************************************
*
*	CLASS		:: DBE::FontPixelShader
*	DESCRIPTION	:: The pixel shader for text.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 20
*
********************************************************************/

#ifndef FontPixelShaderH
#define FontPixelShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEPixelShader.h"

/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the font pixel shader so it can be found in the shader manager.
static const char* gsc_fontPixelShaderName = "FontPixelShader";


/*******************************************************************/
namespace DBE
{
	class FontPixelShader : public DBE::PixelShader {
		public:
			/// Constructor.
			FontPixelShader();
			/// Destructor.
			~FontPixelShader();

			bool Init();
			
			s32 m_slotTexture;
			s32 m_slotSampler;
			
		private:
			
			
			/// Private copy constructor to prevent accidental multiple instances.
			FontPixelShader( const DBE::FontPixelShader& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::FontPixelShader& operator=( const DBE::FontPixelShader& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef FontPixelShaderH
