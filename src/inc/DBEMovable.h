/********************************************************************
*
*	CLASS		:: DBE::Movable
*	DESCRIPTION	:: Base class for any movable object in the engine.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 12 / 30
*
********************************************************************/

#ifndef DBEMovableH
#define DBEMovableH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBECollision.h"
#include "DBEMath.h"

/********************************************************************
*	Defines and constants.
********************************************************************/
enum RenderableFlags {
	RF_Enabled		= 1 << 0,	// Update it's position, rotation, etc, and world matrix.
	RF_Visible		= 1 << 1,	// Render the object.
	RF_Cull			= 1 << 2,	// Cull unseen faces.
	RF_Collidable	= 1 << 3,	// Other objects can collide with it.
	RF_Count,
};


/*******************************************************************/
namespace DBE
{
	class Movable {
		public:
			/// Constructor.
			Movable();
			/// Destructor.
			virtual ~Movable();

			/// Updates the movable object then calls the 'OnUpdate' function.
			virtual bool Update( float deltaTime);
			/// Updates the object (this shouldn't be called outside the 'Update' function).
			virtual bool OnUpdate( float deltaTime) { return true; }
			/// Generates the world matrix then calls the 'OnRender' function.
			virtual void Render( float deltaTime);
			/// Generates the world matrix then calls the 'OnRender' function.
			virtual void Render( float deltaTime, const DBE::Matrix4& parentWorld);
			/// Renders the object (this shouldn't be called outside the 'Render' function).
			virtual void OnRender( float deltaTime) {}
			
			/// Gets the position of the object.
			DBE::Vec3 GetPosition() const;
			/// Gets the previous position of the object.
			DBE::Vec3 GetPrevPosition() const;
			/// Gets the rotation of the object.
			DBE::Vec3 GetRotation() const;
			/// Gets the previous rotation of the object.
			DBE::Vec3 GetPrevRotation() const;
			/// Gets the scale of the object.
			DBE::Vec3 GetScale() const;
			/// Gets the previous scale of the object.
			DBE::Vec3 GetPrevScale() const;
			/// Gets the direction of the object.
			DBE::Vec3 GetDirection() const;
			/// Gets the previous direction of the object.
			DBE::Vec3 GetPrevDirection() const;
			/// Gets the direction that the camera should be facing.
			DBE::Vec3 GetCamDirection() const;
			/// Gets the velocity of the object.
			DBE::Vec3 GetVelocity() const;
			
			/// Gets the position of the object.
			DBE::Vec3* GetPositionPointer();
			/// Gets the previous position of the object.
			DBE::Vec3* GetPrevPositionPointer();
			/// Gets the rotation of the object.
			DBE::Vec3* GetRotationPointer();
			/// Gets the previous rotation of the object.
			DBE::Vec3* GetPrevRotationPointer();
			/// Gets the scale of the object.
			DBE::Vec3* GetScalePointer();
			/// Gets the previous scale of the object.
			DBE::Vec3* GetPrevScalePointer();
			/// Gets the direction of the object.
			DBE::Vec3* GetDirectionPointer();
			/// Gets the previous direction of the object.
			DBE::Vec3* GetPrevDirectionPointer();
			/// Gets the direction that the camera should be facing.
			DBE::Vec3* GetCamDirectionPointer();
			/// Gets the velocity of the object.
			DBE::Vec3* GetVelocityPointer();

			/// Gets the world matrix of the object.
			virtual DBE::Matrix4 GetWorldMatrix( bool calculate = true) const;
			/// Gets the world matrix of the object.
			virtual DBE::Matrix4* GetWorldMatrixPointer( bool calculate = true);
			/// Sets the world matrix of the object.
			void SetWorldMatrix( DBE::Matrix4& world);

			/// Moves the position to the specified location.
			void MoveTo( float x, float y, float z);
			void MoveTo( const DBE::Vec3& pos);
			/// Moves the position by the specified amount.
			void MoveBy( float x, float y, float z);
			void MoveBy( const DBE::Vec3& pos);

			/// Movements based off of the object's direction.
			void MoveForward( float v);
			void MoveBack( float v);
			void MoveLeft( float v);
			void MoveRight( float v);

			/// Rotates to the specified amount.
			void RotateTo( float x, float y, float z);
			void RotateTo( const DBE::Vec3& rot);
			/// Rotates by the specified amount.
			void RotateBy( float x, float y, float z);
			void RotateBy( const DBE::Vec3& rot);

			/// Scales to the specified amount.
			void ScaleTo( float x, float y, float z);
			void ScaleTo( const DBE::Vec3& scale);
			/// Scales by the specified amount.
			void ScaleBy( float x, float y, float z);
			void ScaleBy( const DBE::Vec3& scale);

			/// Changes direction to the specified amount.
			void TurnTo( float x, float y, float z);
			void TurnTo( const DBE::Vec3& dir);
			/// Changes direction by the specified amount.
			void TurnBy( float x, float y, float z);
			void TurnBy( const DBE::Vec3& dir);

			/// Changes the direction of the attached camera to the specified amount.
			void TurnCamDirectionTo( float x, float y, float z);
			void TurnCamDirectionTo( const DBE::Vec3& dir);
			/// Changes the direction of the attached camera by the specified amount.
			void TurnCamDirectionBy( float x, float y, float z);
			void TurnCamDirectionBy( const DBE::Vec3& dir);

			/// Control the speed of the object.
			void SetVelocity( float x, float y, float z);
			void SetVelocity( const DBE::Vec3& vel);
			void SetXVelocity( float x);
			void SetYVelocity( float y);
			void SetZVelocity( float z);

			/// Toggles a flag: see 'RenderableFlags' enum for information.
			void ToggleFlag( RenderableFlags flag);
			/// Gets whether a flag is set: see 'RenderableFlags' enum for information.
			bool FlagIsSet( RenderableFlags flag) const;

		private:
			DBE::Vec3 m_prevPosition,	m_position;
			DBE::Vec3 m_prevRotation,	m_rotation;
			DBE::Vec3 m_prevScale,		m_scale;
			DBE::Vec3 m_prevDirection,	m_direction;
			
			DBE::Vec3 m_camDirection;

			DBE::Vec3 m_velocity;

			DBE::Matrix4 m_world;

			u16 m_flags;
			
			/// Private copy constructor to prevent accidental multiple instances.
			Movable( const DBE::Movable&);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::Movable& operator=( const DBE::Movable&);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEMovableH
