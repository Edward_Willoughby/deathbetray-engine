/********************************************************************
*
*	CLASS		:: DBE::FontVertexShader
*	DESCRIPTION	:: The vertex shader for text.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 20
*
********************************************************************/

#ifndef FontVertexShaderH
#define FontVertexShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEVertexShader.h"

/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the font vertex shader so it can be found in the shader manager.
static const char* gsc_fontVertexShaderName = "FontVertexShader";


/*******************************************************************/
namespace DBE
{
	class FontVertexShader : public DBE::VertexShader {
		public:
			/// Constructor.
			FontVertexShader();
			/// Destructor.
			~FontVertexShader();
			
			bool Init();

			s32	m_slotCBufferGlobalVars;

			s32	m_slotWVP;

		private:
			
			
			/// Private copy constructor to prevent accidental multiple instances.
			FontVertexShader( const DBE::FontVertexShader& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::FontVertexShader& operator=( const DBE::FontVertexShader& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef FontVertexShaderH
