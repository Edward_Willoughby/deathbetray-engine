/********************************************************************
*
*	CLASS		:: DBE::Random
*	DESCRIPTION	:: Provides static functions to generate random numbers. The class will initialise
*					itself when first used and can be seeded to generate a predictable sequence of
*					values, or set be 'truly' random in which case it'll use the time of the system
*					everytime a function is called.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 09 / 22
*
********************************************************************/

#ifndef DBERandomH
#define DBERandomH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBETypes.h"

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class Random {
		public:
			/// Constructor.
			Random() {}

			/// Deletes the Random class (this should only be called by D3DApp).
			static void Shutdown();

			/// Generates an integer between 0 and RAND_MAX inclusively.
			static s32 Int();
			/// Generates an integer between 0 and max inclusively.
			static s32 Int( s32 max);
			/// Generates an integer between min and max inclusively.
			static s32 Int( s32 min, s32 max);

			/// Generates a float between 0.0f and 1.0f inclusively.
			static float Float();
			/// Generates a float between 0.0f and max inclusively.
			static float Float( float max);
			/// Generates a float between min and max inclusively.
			static float Float( float min, float max);

			/// Generates a random boolean value.
			static bool Bool();
			/// Generates a random colour (always opaque).
			static u32 Colour();

			/// Sets whether the values that are generated have any kind of pattern.
			static void SetTrulyRandom( bool trulyRandom = true);
			/// Sets the initial value that is used to create a random number.
			static void SetSeedValue( s32 seedValue);
		
		private:
			/// Checks that the class exists, if it doesn't then it creates it.
			static void EnsureInstantiated();
			/// Seeds the random number generator.
			void Seed();

			/// Returns a number between 0 and RAND_MAX inclusively.
			s32 Rand();

			/// Private copy constructor to prevent accidental multiple instances.
			Random( const DBE::Random&);
			/// Private assignment operator to prevent accidental multiple instances.
			Random& operator=( const DBE::Random&);
		
	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBERandomH
