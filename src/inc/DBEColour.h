#ifndef DBEColourH
#define DBEColourH

#include "DBETypes.h"


#define WHITE		0xFFFFFFFF
#define LIGHT_GREY	0x717171FF
#define DARK_GREY	0xB9B9B9FF
#define BLACK		0x000000FF
#define RED			0xFF0000FF
#define GREEN		0x00FF00FF
#define BLUE		0x0000FFFF
#define YELLOW		0xFFFF00FF
#define CYAN		0x00FFFFFF
#define MAGENTA		0xFF00FFFF
#define ORANGE		0xF0BB1BFF
#define CLEAR		0x00000000

#define SILVER				0xBDBDBDFF
#define GOLD				0xF5DC32FF
#define LIGHT_STEEL_BLUE	0x0185CFFF

#define HEIGHTMAP	0xCCFF99FF


struct VertColour {
	VertColour()
		: red( 0)
		, green( 0)
		, blue( 0)
		, alpha( 0)
	{}

	VertColour( u32 rgba)
		: red(		u8( rgba>>24))
		, green(	u8( rgba>>16))
		, blue(		u8( rgba>>8))
		, alpha(	u8( rgba>>0))
	{}

	VertColour( u8 r, u8 g, u8 b, u8 a)
		: red( r)
		, green( g)
		, blue( b)
		, alpha( a)
	{}

	u8 red, green, blue, alpha;
};

/*******************************************************************/
#endif	// #ifndef DBEColourH