/********************************************************************
*
*	CLASS		:: DBE::Emitter
*	DESCRIPTION	:: I hate particle emitters...
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 22
*
********************************************************************/

#ifndef DBEEmitterH
#define DBEEmitterH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEMovable.h"
#include "DBEParticle.h"

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	template<class T>
	class Emitter : public DBE::Movable {
		public:
			/// Constructor.
			Emitter();
			/// Destructor.
			~Emitter();

			/// Initialise the emitter.
			bool Init( s32 maxParticles, float particlesPerSecond);
			/// Update the emitter (call 'Update()').
			bool OnUpdate( float deltaTime);
			/// Render the particles (call 'Render()').
			void OnRender( float deltaTime);

			/// Add particles.
			void AddParticle( s32 count = 1);
			/// Clears all existing particles.
			void ClearParticles();

			/// Get the maximum number of particles this emitter can have.
			s32 GetMaxNumberOfParticles() const;
			/// Get the current number of particles this emitter has.
			s32 GetNumberOfParticles() const;
			/// Get whether this emitter has any active particles.
			bool HasActiveParticles() const;

			/// Turns each particle towards the camera.
			void BillboardParticles( const DBE::Vec3& camPos);

		private:
			/// Remove particles that have expired.
			void KillParticles();

			/// Creates the particle array.
			void CreateParticleArray( s32 maxParticles);
			/// Deletes the particle array.
			void DeleteParticleArray();

			DBE::Particle** mp_particles;

			float m_timePerSpawn;
			float m_accumulatedTime;

			s32 m_maxParticles;
			s32 m_currentParticleCount;
		
			/// Private copy constructor to prevent accidental multiple instances.
			Emitter( const DBE::Emitter<T>& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::Emitter<T>& operator=( const DBE::Emitter<T>& other);
		
	};
}	// namespace DBE

#include "DBEEmitter.inl"

/*******************************************************************/
#endif	// #ifndef DBEEmitterH
