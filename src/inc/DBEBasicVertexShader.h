/********************************************************************
*
*	CLASS		:: DBE::BasicVertexShader
*	DESCRIPTION	:: Simple vertex shader.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 19
*
********************************************************************/

#ifndef BasicVertexShaderH
#define BasicVertexShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "DBEVertexShader.h"

namespace DBE {
	class Matrix4;
	class Vec4;
}
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the basic vertex shader so it can be found in the shader manager.
static const char* gsc_basicVertexShaderName = "BasicVertexShader";


/*******************************************************************/
namespace DBE
{
	class BasicVertexShader : public DBE::VertexShader {
		public:
			/// Constructor.
			BasicVertexShader();
			/// Destructor.
			~BasicVertexShader();

			bool Init();
			void SetVars( DBE::Matrix4* world, DBE::Vec4* colour);
			
			s32	m_slotCBufferGlobalVars;

			s32	m_slotWVP;
			s32 m_slotColour;
			ShaderLightingSlots m_slotLights;
			
		private:
			
			
			/// Private copy constructor to prevent accidental multiple instances.
			BasicVertexShader( const BasicVertexShader& other);
			/// Private assignment operator to prevent accidental multiple instances.
			BasicVertexShader& operator=( const BasicVertexShader& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef BasicVertexShaderH
