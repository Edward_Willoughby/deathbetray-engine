/********************************************************************
*
*	CLASS		:: DBE::Shader
*	DESCRIPTION	:: Holds the vital (and only the vital) information for a DirectX shader. It does
*					the bare minimum so that programmer has the most control possible over how the
*					shader works.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 06 / 23
*
********************************************************************/

#ifndef DBEShaderH
#define DBEShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <d3d11.h>
#include <d3dcompiler.h>

#include "DBEMath.h"
#include "DBETypes.h"
/********************************************************************
*	Defines and constants.
********************************************************************/
/// This value is fixed by D3D.
static const int NUM_CBUFFER_SLOTS = 16;

/**
* Provides a 'code' for various variable types so they're easily recognisable when being searched
* for within the Shader.
*/
enum ShaderVarType {
	SVT_NONE = 0,
	SVT_TEXTURE2D,
	SVT_SAMPLER_STATE,
	SVT_FLOAT,
	SVT_FLOAT2,
	SVT_FLOAT3,
	SVT_FLOAT4,
	SVT_FLOAT4x4,
	SVT_INT,
};

/**
* A container to make it simpler to create and hold the lighting offsets.
*/
struct ShaderLightingSlots {
	ShaderLightingSlots()
		: m_slotWorld( -1)
		, m_slotInverse( -1)
		, m_slotLightDir( -1)
		, m_slotLightPos( -1)
		, m_slotLightColour( -1)
		, m_slotLightAtten( -1)
		, m_slotLightSpots( -1)
		, m_slotNumLights( -1)
	{}

	s32	m_slotWorld;
	s32	m_slotInverse;
	s32	m_slotLightDir;
	s32	m_slotLightPos;
	s32	m_slotLightColour;
	s32	m_slotLightAtten;
	s32	m_slotLightSpots;
	s32	m_slotNumLights;
};

namespace DBE
{
	/**
	* Contains the vital variables required to correctly load a shader. Two of these are required to
	* create a Shader (one for the vertex shader and one for the pixel shader).
	*/
	struct ShaderParams {
		ShaderParams()
			: fileName( nullptr)
			, entryPoint( nullptr)
			, profile( nullptr)
			, p_macros( nullptr)
			, p_vertDesc( nullptr)
			, vertDescSize( 0)
		{}

		ShaderParams( LPCSTR f, LPCSTR e, LPCSTR p, const D3D_SHADER_MACRO* m, const D3D11_INPUT_ELEMENT_DESC* vd, UINT vds)
			: fileName( f)
			, entryPoint( e)
			, profile( p)
			, p_macros( m)
			, p_vertDesc( vd)
			, vertDescSize( vds)
		{}

		ShaderParams& operator=( const DBE::ShaderParams* other) {
			fileName		= other->fileName;
			entryPoint		= other->entryPoint;
			profile			= other->profile;
			p_macros		= other->p_macros;
			p_vertDesc		= other->p_vertDesc;
			vertDescSize	= other->vertDescSize;
		}

		LPCSTR							fileName;
		LPCSTR							entryPoint;
		LPCSTR							profile;
		const D3D_SHADER_MACRO*			p_macros;
		const D3D11_INPUT_ELEMENT_DESC*	p_vertDesc;
		UINT							vertDescSize;
	};

	/**
	* Stores all the information for a variable in a shader's CBuffer.
	*/
	struct VarDescription {
		VarDescription()
			: mp_name( nullptr)
			, m_sizeBytes( 0)
			, m_type( SVT_NONE)
			, m_slot( 0)
			, mp_next( nullptr)
		{}

		char*			mp_name;
		size_t			m_sizeBytes;
		ShaderVarType	m_type;
		int				m_slot;

		DBE::VarDescription*	mp_next;
	};

	/**
	* Stores all the information for a CBuffer in a shader.
	*/
	struct CBufferDescription {
		CBufferDescription()
			: mp_name( nullptr)
			, m_sizeBytes( 0)
			, mp_constants( nullptr)
		{}

		char*			mp_name;
		size_t			m_sizeBytes;

		DBE::VarDescription*	mp_constants;
	};
}	// namespace DBE


/*******************************************************************/
namespace DBE
{
	class Shader {
		public:
			/// Constructor.
			Shader();
			/// Destructor.
			virtual ~Shader();

			/// Deletes the shader's variables.
			void DeleteShader();

			/// Passes a matrix through to a shader's CBuffer.
			void SetCBufferMatrix( const D3D11_MAPPED_SUBRESOURCE& map, int packOffSet, const DBE::Matrix4& mat);

			/**
			* Passes a variable through to a shader's CBuffer.
			*
			* @param map		:: I have no idea...
			* @param packOffSet	:: The offset of the variable from the start of the CBuffer.
			* @param var		:: The variable to pass through.
			*/
			template<class T>
			void SetCBufferVar( const D3D11_MAPPED_SUBRESOURCE& map, int packOffSet, const T& var)
			{
				if( map.pData && packOffSet >= 0) {
					T* p_dest( reinterpret_cast<T*>( static_cast<char*>( map.pData) + packOffSet));
					*p_dest = var;
				}
			}

			/**
			* Passes an array variable through to a shader's CBuffer.
			*
			* @param map		:: I have no idea...
			* @param packOffset	:: The offset of the variable from the start of the CBuffer.
			* @param arrayIndex	:: The position of the variable in the array.
			* @param var		:: The variable to pass through.
			*/
			template<class T>
			void SetCBufferArrayVar( const D3D11_MAPPED_SUBRESOURCE& map, int packOffset, int arrayIndex, const T& var)
			{
				// Packing rules for H.L.S.L. constants: http://msdn.microsoft.com/en-us/library/bb509632(v=vs.85).aspx
				//
				// This is the reason for the '* 4 * sizeof( float)'; it needs to be moved into a
				// new 'slot' if it's not 16 bytes, otherwise two variables in the array could end
				// up in the same array slot.
				if( map.pData && packOffset >= 0 && arrayIndex >= 0)
				{
					T* p_dest;

					if( sizeof( var) == 4 * sizeof( float))
						p_dest = reinterpret_cast<T*>( static_cast<char*>( map.pData) + packOffset) + arrayIndex;
					else
						p_dest = reinterpret_cast<T*>( static_cast<char*>( map.pData) + packOffset + arrayIndex * 4 * sizeof( float));

					*p_dest = var;
				}
			}

			/// Finds a CBuffer within a shader.
			bool FindCBuffer( const char* p_name, int* p_slot) const;
			/// Finds a variable within a CBuffer.
			bool FindShaderVar( DBE::VarDescription* p_var, const char* p_name, ShaderVarType type, int* p_packOffSet) const;
			/// Finds a variable within a CBuffer.
			bool FindShaderVar( int cbufferSlot, const char* p_name, ShaderVarType type, int* p_packOffSet) const;
			/// Finds a texture within a CBuffer.
			bool FindTexture( const char* p_name, int* p_packOffSet) const;
			/// Finds a sampler within a CBuffer.
			bool FindSampler( const char* p_name, int* p_packOffSet) const;

			/// Creates a buffer based on a CBuffer description.
			bool CreateCBuffer( s32 cbufferSlot);

			ID3D11Buffer* mp_CBuffers[NUM_CBUFFER_SLOTS];

			DBE::CBufferDescription*	mp_CBufferDescs;
			DBE::VarDescription*		mp_textures;
			DBE::VarDescription*		mp_samplerStates;

		protected:
			/// Creates a shader based on the parameters (use 'PixelShader/VertexShader::LoadShader').
			void CompileShader( const DBE::ShaderParams* p_params, ID3D11VertexShader** p_VS, ID3D11InputLayout** p_IL, ID3D11PixelShader** p_PS);


		private:
			/// Gets a list of all of the textures or sampler states in a shader.
			VarDescription* GetResourceList( ID3D11ShaderReflection* p_rShader, int D3DShaderInputType, ShaderVarType type);
			/// Deletes a VarDescription.
			void DeleteVarDesc( DBE::VarDescription* &desc);

			/// Private copy constructor to prevent multiple instances.
			Shader( const DBE::Shader&);
			/// Private assignment operator to prevent multiple instances.
			DBE::Shader& operator=( const DBE::Shader&);

	};
}	// namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEShaderH