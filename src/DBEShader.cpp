/********************************************************************
*	Function definitions for the Shader class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "DBEShader.h"

#include <atlbase.h>

#include "DBEApp.h"
#include "DBEGraphicsHelpers.h"
#include "DBEUtilities.h"
/********************************************************************
*	Defines, constants, namespaces and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor for the D3DShader class. Use this constructor if you want to initialise the shaders
* in one line of code and the file name, macros, vertex description, and vertex description size
* are the same for both shaders.
*/
DBE::Shader::Shader()
	: mp_CBufferDescs( nullptr)
	, mp_textures( nullptr)
	, mp_samplerStates( nullptr)
{
	for( u8 i( 0); i < NUM_CBUFFER_SLOTS; ++i)
		mp_CBuffers[i] = nullptr;
}

/**
* Destructor for the D3DShader class.
*/
DBE::Shader::~Shader() {
	this->DeleteShader();
}

/**
* Deletes the shader's variables to prevent any memory leaks.
*/
void DBE::Shader::DeleteShader() {
	// Delete any CBuffers.
	if( mp_CBufferDescs) {
		for( u8 i( 0); i < NUM_CBUFFER_SLOTS; ++i)
			this->DeleteVarDesc( mp_CBufferDescs[i].mp_constants);

		SafeDeleteArray( mp_CBufferDescs);
	}

	this->DeleteVarDesc( mp_textures);
	this->DeleteVarDesc( mp_samplerStates);

	for( u8 i( 0); i < NUM_CBUFFER_SLOTS; ++i)
		ReleaseCOM( mp_CBuffers[i]);
}

/**
* Passes a matrix through to a shader's CBuffer.
*
* @param map		:: I have no idea...
* @param packOffset	:: The offset of the variable from the start of the CBuffer.
* @param mat		:: The matrix to pass through.
*/
void DBE::Shader::SetCBufferMatrix( const D3D11_MAPPED_SUBRESOURCE &map, int packOffSet, const DBE::Matrix4& mat) {
	if( map.pData && packOffSet >= 0) {
		DBE::Matrix4* p_dest( reinterpret_cast<DBE::Matrix4*>( static_cast<char*>( map.pData) + packOffSet));

		// This non-sensical local variable is required for 'XMMatrixTranspose' because XNA sucks.
		DBE::Matrix4 m = mat;
		*p_dest = DirectX::XMMatrixTranspose( m);
	}
}

/**
* Finds a CBuffer within a shader.
*
* @param p_name	:: The name of the CBuffer.
* @param p_slot	:: The variable that will store the CBuffer's number. This will be set to -1 if the
*					CBuffer wasn't found.
*
* @return True if the CBuffer was found.
*/
bool DBE::Shader::FindCBuffer( const char* p_name, int* p_slot) const {
	for( u8 i( 0); i < NUM_CBUFFER_SLOTS; ++i) {
		if( mp_CBufferDescs[i].mp_name) {
			if( strcmp( mp_CBufferDescs[i].mp_name, p_name) == 0) {
				*p_slot = i;
				return true;
			}
		}
	}

	*p_slot = -1;
	return false;
}

/**
* Finds a variable within a CBuffer.
*
* @param p_var			:: The first variable in a CBuffer.
* @param p_name			:: The name of the variable to be found.
* @param type			:: The type of the variable.
* @param p_packOffSet	:: The offset of the variable from the start of the CBuffer. If the
*							variable wasn't found then this will be set to -1.
*
* @return True if the variable was found.
*/
bool DBE::Shader::FindShaderVar( DBE::VarDescription* p_var, const char* p_name, ShaderVarType type, int* p_packOffset) const {
	for( const VarDescription* p_varIndex( p_var); p_varIndex; p_varIndex = p_varIndex->mp_next) {
		if( strcmp( p_varIndex->mp_name, p_name) == 0) {
			if( p_varIndex->m_type == type) {
				*p_packOffset = p_varIndex->m_slot;
				return true;
			}
		}
	}

	*p_packOffset = -1;
	return false;
}

/**
* Finds a variable within a CBuffer.
*
* @param cbufferSlot	:: The CBuffer to search in.
* @param p_name			:: The name of the variable to be found.
* @param type			:: The type of the variable.
* @param p_packOffSet	:: The offset of the variable from the start of the CBuffer. If the
*							variable wasn't found then this will be set to -1.
*
* @return True if the variable was found.
*/
bool DBE::Shader::FindShaderVar( int cbufferSlot, const char* p_name, ShaderVarType type, int* p_packOffset) const {
	return( this->FindShaderVar( mp_CBufferDescs[cbufferSlot].mp_constants, p_name, type, p_packOffset));
}

/**
* Finds a texture within a CBuffer.
*
* @param p_name			:: The name of the texture variable to be found.
* @param p_packOffSet	:: The offset of the variable from the start of the CBuffer. If the
*							variable wasn't found then this will be set to -1.
*
* @return True if the variable was found.
*/
bool DBE::Shader::FindTexture( const char* p_name, int* p_packOffset) const {
	return( this->FindShaderVar( mp_textures, p_name, SVT_TEXTURE2D, p_packOffset));
}

/**
* Finds a sampler within a CBuffer.
*
* @param p_name			:: The name of the sampler to be found.
* @param p_packOffSet	:: The offset of the sampler from the start of the CBuffer. If the
*							sampler wasn't found then this will be set to -1.
*
* @return True if the sampler was found.
*/
bool DBE::Shader::FindSampler( const char* p_name, int* p_packOffset) const {
	return( this->FindShaderVar( mp_samplerStates, p_name, SVT_SAMPLER_STATE, p_packOffset));
}

/**
* Creates a buffer based on a CBuffer description. This is a default method; the variables required
* are public so it is possible to change the type of buffer, if required.
*
* @param cbufferSlot :: The array position of the CBuffer description in the 'mp_CBufferDescs'
*						array to make a buffer for.
*
* @return True if the buffer was successfully created.
*/
bool DBE::Shader::CreateCBuffer( s32 cbufferSlot) {
	mp_CBuffers[cbufferSlot] = CreateBuffer( GET_APP()->mp_D3DDevice, mp_CBufferDescs[cbufferSlot].m_sizeBytes, D3D11_USAGE_DYNAMIC, D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, nullptr);

	if( mp_CBuffers[cbufferSlot] == nullptr)
		return false;

	return true;
}

/**
* Creates a shader based on the parameters. Only use this function for 'PixelShader' and
* 'VertexShader'; for derived classes they provide the function 'LoadShader' to make it easier.
*
* @param isVertexShader	:: True if the shader is a vertex shader, otherwise it'll create a pixel
*							shader.
* @param p_params		:: Holds all the information required to create a shader.
*/
void DBE::Shader::CompileShader( const ShaderParams* p_params, ID3D11VertexShader** p_VS, ID3D11InputLayout** p_IL, ID3D11PixelShader** p_PS) {
	DBE_Assert( p_params->fileName != nullptr &&
			p_params->entryPoint != nullptr &&
			p_params->profile != nullptr &&
			(( p_VS != nullptr && p_IL != nullptr) || p_PS != nullptr));

	ID3D11Device*	p_device( GET_APP()->mp_D3DDevice);
	ID3D10Blob*		p_shader( nullptr);
	ID3D10Blob*		p_errors( nullptr);
	DWORD			flags( 0);

	// Define so I can convert 'const char*' to 'LPCWSTR'.
	USES_CONVERSION;

	flags |= D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
	flags |= D3DCOMPILE_DEBUG;
#endif	// #ifdef _DEBUG
	flags |= D3DCOMPILE_SKIP_OPTIMIZATION;

	DebugTrace( "Loading shader: %s (%s)\n", p_params->fileName, p_params->entryPoint);

	char buffer[256];
	GetModuleFileName( NULL, buffer, 256);

	HRESULT hr;
	hr = D3DCompileFromFile( A2W( p_params->fileName), p_params->p_macros, nullptr, p_params->entryPoint, p_params->profile, flags, 0, &p_shader, &p_errors);
	if( FAILED( hr)) {
		char buffer[256];
		char* error = nullptr;
		if( p_errors != nullptr)
			error = (char*)p_errors->GetBufferPointer();
		else
			error = "Unidentified";

		sprintf_s( buffer, 256, "Shader failed to be created.\n\nError code: %X\nMessage: %s\n", hr, error);

		MessageBox( nullptr, buffer, "Shader Compilation Error", MB_OK | MB_ICONERROR);
	}
#ifdef _DEBUG
	if( p_errors != nullptr) {
		char* error = (char*)p_errors->GetBufferPointer();
		DebugTrace( "Shader Error: %s\n", error);
	}
#endif	// #ifdef _DEBUG
	ReleaseCOM( p_errors);

	if( p_VS != nullptr) {
		HR( p_device->CreateVertexShader( p_shader->GetBufferPointer(), p_shader->GetBufferSize(), nullptr, *&p_VS));
		HR( p_device->CreateInputLayout( p_params->p_vertDesc, p_params->vertDescSize, p_shader->GetBufferPointer(), p_shader->GetBufferSize(), *&p_IL));
	}
	else {
		HR( p_device->CreatePixelShader( p_shader->GetBufferPointer(), p_shader->GetBufferSize(), nullptr, *&p_PS));
	}

	ID3D11ShaderReflection* p_RShader;
	HR( D3DReflect( p_shader->GetBufferPointer(), p_shader->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&p_RShader));

	D3D11_SHADER_DESC shaderDesc;
	p_RShader->GetDesc( &shaderDesc);

	// If there are no constant buffers to read in for this shader, then finish.
	if( shaderDesc.ConstantBuffers <= 0)
		goto done;

	// Add variables from the constant buffers.
	mp_CBufferDescs = new CBufferDescription[NUM_CBUFFER_SLOTS];

	for( UINT bufferIndex( 0); bufferIndex < shaderDesc.ConstantBuffers; ++bufferIndex) {
		ID3D11ShaderReflectionConstantBuffer* p_RBuffer( p_RShader->GetConstantBufferByIndex( bufferIndex));

		D3D11_SHADER_BUFFER_DESC bufferDesc;
		if( FAILED( p_RBuffer->GetDesc( &bufferDesc)))
			continue;

		D3D11_SHADER_INPUT_BIND_DESC bufferBindDesc;
		if( FAILED( p_RShader->GetResourceBindingDescByName( bufferDesc.Name, &bufferBindDesc)))
			continue;	// Bit mystifying... apparently.

		if( bufferBindDesc.BindPoint >= NUM_CBUFFER_SLOTS)
			continue;	// D3D supplied an invalid cbuffer slot - shouldn't happen!... apparently.

		CBufferDescription* p_CBuffer = &mp_CBufferDescs[bufferBindDesc.BindPoint];
		if( p_CBuffer->mp_name)
			continue;	// Two cbuffers bound to the same slot - shouldn't happen!... apparently.

		p_CBuffer->mp_name = _strdup( bufferDesc.Name);
		p_CBuffer->m_sizeBytes = bufferDesc.Size;

		VarDescription** pp_nextVar = &p_CBuffer->mp_constants;
		for( UINT varIndex( 0); varIndex < bufferDesc.Variables; ++varIndex) {
			ID3D11ShaderReflectionVariable* p_RVar( p_RBuffer->GetVariableByIndex( varIndex));
			if( !p_RVar)
				continue;

			D3D11_SHADER_VARIABLE_DESC varDesc;
			if( FAILED( p_RVar->GetDesc( &varDesc)))
				continue;

			ID3D11ShaderReflectionType* p_RVarType( p_RVar->GetType());
			if( !p_RVarType)
				continue;

			D3D11_SHADER_TYPE_DESC varTypeDesc;
			if( FAILED( p_RVarType->GetDesc( &varTypeDesc)))
				continue;

			ShaderVarType type( SVT_NONE);
			if( varTypeDesc.Type == D3D_SVT_FLOAT && varTypeDesc.Rows == 1 && varTypeDesc.Columns == 1)
				type = SVT_FLOAT;
			else if( varTypeDesc.Type == D3D_SVT_FLOAT && varTypeDesc.Rows == 1 && varTypeDesc.Columns == 2)
				type = SVT_FLOAT2;
			else if( varTypeDesc.Type == D3D_SVT_FLOAT && varTypeDesc.Rows == 1 && varTypeDesc.Columns == 3)
				type = SVT_FLOAT3;
			else if( varTypeDesc.Type == D3D_SVT_FLOAT && varTypeDesc.Rows == 1 && varTypeDesc.Columns == 4)
				type = SVT_FLOAT4;
			else if( varTypeDesc.Type == D3D_SVT_FLOAT && varTypeDesc.Rows == 4 && varTypeDesc.Columns == 4)
				type = SVT_FLOAT4x4;
			else if( varTypeDesc.Type == D3D_SVT_INT && varTypeDesc.Rows == 1 && varTypeDesc.Columns == 1)
				type = SVT_INT;

			// Store the variable if the type was confirmed as valid.
			if( type != SVT_NONE) {
				VarDescription* p_newVar = new VarDescription;

				p_newVar->mp_name = _strdup( varDesc.Name);
				p_newVar->m_type = type;
				p_newVar->m_slot = varDesc.StartOffset;

				*pp_nextVar = p_newVar;
				pp_nextVar = &p_newVar->mp_next;
			}
		}
	}

done:
	// Add shader inputs.
	mp_textures = this->GetResourceList( p_RShader, D3D_SIT_TEXTURE, SVT_TEXTURE2D);
	mp_samplerStates = this->GetResourceList( p_RShader, D3D_SIT_SAMPLER, SVT_SAMPLER_STATE);

	ReleaseCOM( p_shader);
}

/**
* Gets a list of all of the textures or sampler states in a shader.
*
* @param p_RShader			:: A reflection of the shader.
* @param D3DShaderInputType	:: Indicates whether it's finding all of the textures or samplers. Use
*								'D3D_SIT_TEXTURE' or 'D3D_SIT_SAMPLER' for this parameter.
* @param varType			:: The type to assign the variable when it's turned into a
*								'VarDescription'.
*
* @return A pointer to the first 'VarDescription' that was found.
*/
DBE::VarDescription* DBE::Shader::GetResourceList( ID3D11ShaderReflection* p_RShader, int D3DShaderInputType, ShaderVarType varType) {
	DBE_Assert( p_RShader != nullptr);

	D3D11_SHADER_DESC shaderDesc;
	if( FAILED( p_RShader->GetDesc( &shaderDesc)))
		return nullptr;

	VarDescription* p_firstVar( nullptr);
	VarDescription** pp_nextVar = &p_firstVar;

	for( UINT resIndex( 0); resIndex < shaderDesc.BoundResources; ++resIndex) {
		D3D11_SHADER_INPUT_BIND_DESC inputBindDesc;
		if( FAILED( p_RShader->GetResourceBindingDesc( resIndex, &inputBindDesc)))
			continue;

		if( inputBindDesc.Type == D3DShaderInputType) {
			VarDescription* p_newVar = new VarDescription();

			p_newVar->mp_name	= _strdup( inputBindDesc.Name);
			p_newVar->m_type	= varType;
			p_newVar->m_slot	= inputBindDesc.BindPoint;

			*pp_nextVar = p_newVar;
			pp_nextVar = &p_newVar->mp_next;
		}
	}

	return p_firstVar;
}

/**
* Deletes a VarDescription.
*
* @param desc :: The 'VarDescription' to be deleted.
*/
void DBE::Shader::DeleteVarDesc( DBE::VarDescription* &desc) {
	while( desc != nullptr) {
		VarDescription* p_next( desc->mp_next);

		free( desc->mp_name);
		SafeDelete( desc);

		desc = p_next;
	}
}