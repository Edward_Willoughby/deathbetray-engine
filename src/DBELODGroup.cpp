/********************************************************************
*	Function definitions for the LODGroup class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBELODGroup.h"

#include <DBEApp.h>
#include <DBEThreadPool.h>
#include <DBEUtilities.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

/**
*	STRUTURE	:: LODGroupThreadParams
*	DESCRIPTION	:: Holds vital information to load and store a texture created in a thread.
*/
struct LODGroupThreadParams {
	char*			mp_texturePath;
	DBE::Texture**	mpp_texture;
	LODState*		mp_state;
};

static ID3D11SamplerState* gs_sampler = nullptr;


/**
* Constructor.
*/
DBE::LODGroup::LODGroup( const char* texturePaths[LOD_Count], LevelOfDetail lod)
	: m_lod( lod)
{
	// Save the texture paths (as the textures won't all be loaded in right now) and initialise the
	// other member data.
	for( s32 i( 0); i < LOD_Count; ++i) {
		if( texturePaths[i] == nullptr) {
			sprintf_s( m_texturePaths[i], 256, "");
			m_textureStates[i] = LODS_Unavailable;
		}
		else {
			sprintf_s( m_texturePaths[i], 256, texturePaths[i]);
			m_textureStates[i] = LODS_NotLoaded;
		}

		mp_textures[i] = nullptr;
	}

	// Load in the primary quality.
	lod = this->GetAvailableTexture();
	gs_sampler = GET_APP()->GetSamplerState( true, true, true);
	mp_textures[lod] = MGR_TEXTURE().LoadTexture( m_texturePaths[lod], gs_sampler);

	// If the primary quality couldn't be loaded in then attempt to load in another, starting at
	// the lowest quality.
	if( mp_textures[lod] == nullptr) {
		m_textureStates[lod] = LODS_Unavailable;

		for( s32 i( 0); i < LOD_Count; ++i) {
			// Don't try to load in a texture that can't be.
			if( m_textureStates[lod] == LODS_Unavailable)
				continue;

			mp_textures[lod] = MGR_TEXTURE().LoadTexture( m_texturePaths[lod], gs_sampler);

			// Exit the loop if the texture was successfully loaded in.
			if( mp_textures[lod] != nullptr)
				break;

			// If the texture wasn't loaded in, then set this level to be unavailable as well.
			m_textureStates[lod] = LODS_Unavailable;
		}
	}
	else {
		m_textureStates[lod] = LODS_Loaded;
	}
}

/**
* Destructor.
*/
DBE::LODGroup::~LODGroup() {
	for( s32 i( 0); i < LOD_Count; ++i)
		MGR_TEXTURE().DeleteTexture( mp_textures[i]);
}

/**
* Sets the level of detail that the texture should use.
*
* @param lod :: The texture quality that this LOD Group should use.
*/
void DBE::LODGroup::SetLevelOfDetail( LevelOfDetail lod) {
	m_lod = lod;
}

/**
* Get the level of detail that the texture should use.
*
* @return The current texture quality that this L.O.D. group is set to.
*/
LevelOfDetail DBE::LODGroup::GetLevelOfDetail() const {
	return m_lod;
}

/**
* Gets the correct texture based on the desired quality level.
*
* @param usable :: Default: True. If false, the function will return the texture at the specified
*					L.O.D., even if that means it's a nullptr.
*
* @return The texture at the specified quality level.
*/
DBE::Texture* DBE::LODGroup::GetTexture( bool usable /*= true*/) {
	LevelOfDetail lod( m_lod);

	if( usable)
		lod = this->GetLoadedTexture();

	return mp_textures[lod];
}

/**
* Checks to make sure the LODGroup has at least one texture that is usable.
*
* @return True if the LODGroup has at least one texture loaded in that can be used for rendering.
*/
bool DBE::LODGroup::HasOneUsableTexture() {
	for( s32 i( 0); i < LOD_Count; ++i)
		if( m_textureStates[i] == LODS_Loaded)
			return true;

	return false;
}

/**
* Gets the texture that is closest to 'm_lod' and loaded in. Note that if a quality level is
* unavailable (i.e. not provided or the file doesn't exist) or has not loaded yet then the L.O.D.
* group will prioritise lower textures before looking at higher quality textures.
*
* @return The texture closest to 'm_lod' that is loaded and ready to be used.
*/
LevelOfDetail DBE::LODGroup::GetLoadedTexture() {
	LevelOfDetail lod( m_lod);

	// If the desired level is available, then use it.
	if( m_textureStates[lod] == LODS_Loaded) {
		return lod;
	}
	// If not, and it's not currently being loaded in, then start a thread to load it in.
	else if( m_textureStates[lod] != LODS_Loading) {
		LODGroupThreadParams* arguments = new LODGroupThreadParams();
		arguments->mp_texturePath = m_texturePaths[lod];
		arguments->mpp_texture = &mp_textures[lod];
		arguments->mp_state = &m_textureStates[lod];

		// Create and start the thread.
		MGR_THREADPOOL().RunThread( MGR_THREADPOOL().CreateThread( Thread_LoadTexture, arguments));
	}

	// Find another texture that's been loaded in.
	lod = LOD_Count;
	for( s32 i( m_lod); i >= 0; --i) {
		// Ignore the texture if it isn't loaded in.
		if( m_textureStates[i] != LODS_Loaded)
			continue;

		lod = LevelOfDetail( i);
	}

	// If a texture has been found then use that one.
	if( lod != LOD_Count)
		return lod;

	// If not, then search for a higher quality texture.
	for( s32 i( m_lod); i < LOD_Count; ++i) {
		// Ignore the texture if it isn't loaded in.
		if( m_textureStates[i] != LODS_Loaded)
			continue;

		lod = LevelOfDetail( i);
	}

	// If a texture HASN'T been found by this point then it means there are no textures loaded in,
	// which shouldn't have happened.
	DBE_AssertWithMsg( lod != LOD_Count, "Error: A L.O.D. Group has no useable textures loaded in.\n", "");

	return lod;
}

/**
* Gets the texture that is closest to 'm_lod'. Note that if a quality level is unavailable (i.e.
* not provided or the file doesn't exist) then the L.O.D. group will prioritise lower textures
* before looking at higher quality textures.
* Note: Unlike 'LODGroup::GetLoadedTexture', this function doesn't care whether or not the texture
* has been loaded in. Therefore, the texture that this function points to may not be immediately
* usable, but SHOULD be once loaded in (if the file doesn't exist then it won't know until it tries
* to load in the texture).
*
* @return The texture closest to 'm_lod' that has potential to be usable.
*/
LevelOfDetail DBE::LODGroup::GetAvailableTexture() {
	LevelOfDetail lod( m_lod);

	if( m_textureStates[lod] != LODS_Unavailable)
		return lod;

	// Find another texture that's been loaded in.
	lod = LOD_Count;
	for( s32 i( m_lod); i >= 0; --i) {
		// Ignore the texture if it isn't loaded in.
		if( m_textureStates[i] == LODS_Unavailable)
			continue;

		lod = LevelOfDetail( i);
	}

	// If a texture has been found then use that one.
	if( lod != LOD_Count)
		return lod;

	// If not, then search for a higher quality texture.
	for( s32 i( m_lod); i < LOD_Count; ++i) {
		// Ignore the texture if it isn't loaded in.
		if( m_textureStates[i] == LODS_Unavailable)
			continue;

		lod = LevelOfDetail( i);
	}

	// If a texture HASN'T been found by this point then it means there are no textures loaded in,
	// which shouldn't have happened.
	DBE_AssertWithMsg( lod != LOD_Count, "Error: A L.O.D. Group has no useable textures loaded in.\n", "");

	return lod;
}

/**
* Loads in a texture in a separate thread.
*
* @param p_arguments :: Contains information to create and store the resultant texture.
*/
void DBE::LODGroup::Thread_LoadTexture( void* p_arguments) {
	LODGroupThreadParams* param( static_cast<LODGroupThreadParams*>( p_arguments));

	// Load the texture.
	DBE::Texture* texture = MGR_TEXTURE().LoadTexture( param->mp_texturePath, gs_sampler);

	// If the texture wasn't loaded correctly then change the texture's state in unavailable.
	if( texture == nullptr) {
		*param->mp_state = LODS_Unavailable;
		return;
	}

	// If the texture loaded correctly, store the texture in the L.O.D. group.
	*param->mpp_texture = texture;

	// Change the texture's state in the L.O.D. group to indicate that it's been loaded in.
	*param->mp_state = LODS_Loaded;

	// Delete the parameters.
	SafeDelete( param);
}