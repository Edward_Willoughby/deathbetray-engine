/********************************************************************
*	Function definitions for the ThreadPool class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEThreadPool.h"

#include <process.h>

#include "DBERandom.h"
#include "DBEUtilities.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
/**
* Enum for describing the current state of a thread.
*/
enum ThreadState {
	TS_Inactive = 0,	// Available to hold a new thread.
	TS_Suspended,		// Currently holds a thread, but not executing it.
	TS_Active,			// Currently holds a thread AND executing it.
	TS_Finished,		// Holds a thread and has finished executing it (next 'OnUpdate()' will release it).
};

/// The state of each thread.
static ThreadState gs_threadStates[MAX_THREADS];
/// The function that each thread should called (specified in 'ThreadPool::CreateThread()').
static ThreadFunction gs_threadFunctions[MAX_THREADS];
/// The parameters to be sent to each thread.
static void* gs_threadArgs[MAX_THREADS];


/**
* Constructor.
*/
DBE::ThreadPool::ThreadPool() {}

/**
* Destructor.
*/
DBE::ThreadPool::~ThreadPool() {}

/**
* Initialises the thread pool.
*
* @return True if everything was initialised correctly.
*/
bool DBE::ThreadPool::Init() {
	for( s32 i( 0); i < MAX_THREADS; ++i) {
		m_threads[i] = INVALID_HANDLE_VALUE;
		gs_threadStates[i] = TS_Inactive;
		gs_threadArgs[i] = nullptr;
	}

	m_activeThreadCount = 0;

	return true;
}

/**
* Updates the threads and frees up HANDLEs that have finished.
*
* @param deltaTime :: The time taken to render the previous frame.
*
* @return True if everything was updated correctly.
*/
bool DBE::ThreadPool::OnUpdate( float deltaTime) {
	for( s32 i( 0); i < MAX_THREADS; ++i) {
		if( gs_threadStates[i] != TS_Finished)
			continue;

		DebugTrace( "ThreadPool: Closing thread %d.\n", i);
		this->CloseThread( i);
	}

	return true;
}

/**
* Waits for any remaining threads and cleans up. If the program 'hangs' when closing, this may be a
* good place to check in case there's a thread that's not ending correctly. To prevent hanging,
* change 'INFINITE' to however long you want to wait for.
*/
void DBE::ThreadPool::Shutdown() {
	HANDLE* threads = new HANDLE[MAX_THREADS];
	s32 noOfThreads( 0);

	for( s32 i( 0); i < MAX_THREADS; ++i) {
		threads[i] = 0;

		// Just close threads that have either finished or never got started.
		if( gs_threadStates[i] == TS_Finished || gs_threadStates[i] == TS_Suspended)
			this->CloseThread( i);

		// If the thread hasn't finished then add it to the array to wait for unfinished threads.
		if( gs_threadStates[i] == TS_Active) {
			threads[i] = m_threads[i];
			++noOfThreads;
		}
	}

	// The only handles in the array should be of those that have not yet finished.
	WaitForMultipleObjects( noOfThreads, m_threads, true, INFINITE);
	
	SafeDeleteArray( threads);
}

/**
* Creates a new thread in suspended mode (call 'RunThread()' to start it).
*
* @param func			:: The function that will be called by the thread.
* @param p_arguments	:: The arguments to be sent to the function.
*
* @return The position of the thread's HANDLE in the 'm_threads' array.
*/
s32 DBE::ThreadPool::CreateThread( ThreadFunction func, void* p_arguments) {
	s32 index( -1);

	for( s32 i( 0); i < MAX_THREADS; ++i) {
		if( gs_threadStates[i] != TS_Inactive)
			continue;

		// Create the thread in a suspended state (so it doesn't start immediately).
		index = i;
		m_threads[index] = (HANDLE)_beginthreadex( nullptr, 0, &ThreadFunc, &gs_threadStates[index], CREATE_SUSPENDED, nullptr);

		gs_threadStates[index] = TS_Suspended;
		gs_threadFunctions[index] = func;
		gs_threadArgs[index] = p_arguments;

		++m_activeThreadCount;

		break;
	}

	DBE_AssertWithMsg( index != -1, "ThreadPool Error: Too many threads.\n", 0);

	return index;
}

/**
* Starts a thread executing.
*
* @param index :: The position of the thread in the 'm_threads' array.
*/
void DBE::ThreadPool::RunThread( s32 index) {
	DBE_AssertWithMsg( index >= 0 && index < MAX_THREADS, "ThreadPool Error: Invalid thread index %d.\n", index);

	if( gs_threadStates[index] == TS_Suspended) {
		DebugTrace( "ThreadPool: Starting thread number %d.\n", index);

		ResumeThread( m_threads[index]);
		gs_threadStates[index] = TS_Active;
	}
}

/**
* Pauses a thread (debugging purposes ONLY). As stated on the MSDN website, this function should
* ONLY be used for debugging purposes as it may cause deadlock when used for thread synchronisation.
* https://msdn.microsoft.com/en-us/library/windows/desktop/ms686345(v=vs.85).aspx
*
* @param index :: The position of the thread in the 'm_threads' array.
*/
void DBE::ThreadPool::PauseThread( s32 index) {
	DBE_AssertWithMsg( index >= 0 && index < MAX_THREADS, "ThreadPool Error: Invalid thread index %d.\n", index);

	if( gs_threadStates[index] == TS_Suspended) {
		DebugTrace( "ThreadPool: Pausing thread number %d.\n", index);

		SuspendThread( m_threads[index]);
		gs_threadStates[index] = TS_Suspended;
	}
}

/**
* Checks whether a thread has finished executing or not.
*
* @param index :: The position of the thread's HANDLE in the 'm_threads' array.
*
* @return True if the thread has finished executing.
*/
bool DBE::ThreadPool::IsThreadFinished( s32 index) const {
	DBE_AssertWithMsg( index >= 0 && index < MAX_THREADS, "ThreadPool Error: Invalid thread index %d.\n", index);

	if( gs_threadStates[index] != TS_Active)
		return true;

	return false;
}

/**
* Gets the number of threads that currently exist (active OR suspended).
*
* @return The number of threads that currently exist.
*/
s32 DBE::ThreadPool::GetThreadCount() const {
	return m_activeThreadCount;
}

/**
* Finalises the deletion of a thread and frees up the HANDLE.
*
* @param index :: The position of the thread's HANDLE in the 'm_threads' array.
*/
void DBE::ThreadPool::CloseThread( s32 index) {
	CloseHandle( m_threads[index]);
	m_threads[index] = INVALID_HANDLE_VALUE;
	gs_threadStates[index] = TS_Inactive;
	gs_threadArgs[index] = nullptr;

	--m_activeThreadCount;
}

/**
* Called by all threads before calling their 'custom' function. From this function, the index
* position of the thread's data is calculated and then the 'intended function' (i.e. the one passed
* to 'ThreadPool::CreateThread()') is called, along with the parameters.
* Note: The reason a memory address from the 'gs_threadStates' array is used instead of just
* passing the integer value is because it would require the index position to be created on the
* heap so it doesn't go out of scope. This way, it's gauranteed that the address will still contain
* valid data.
*
* @param p_arguments :: Pointer to the thread's state in the 'gs_threadStates' array. From this, it
*						can determine the index number of the thread and obtain the other data
*						required for the thread from 'gs_threadFunctions' and 'gs_threadArgs'.
*
* @return Irrelevant: the thread is ended with '_endthreadex( 0)'.
*/
unsigned __stdcall DBE::ThreadPool::ThreadFunc( void* p_arguments) {
	// Calculate the index position of the thread so the data in the arrays can be accessed.
	ThreadState* tsIndex = (ThreadState*)p_arguments;
	s32 index( ((s32)tsIndex - (s32)&gs_threadStates) / sizeof( ThreadState));

	// Call the intended function.
	gs_threadFunctions[index]( gs_threadArgs[index]);

	// Signal that the thread has finished executing so it can be cleaned up.
	gs_threadStates[index] = TS_Finished;
	_endthreadex( 0);

	return 0;	// To get rid of the compiler error.
}