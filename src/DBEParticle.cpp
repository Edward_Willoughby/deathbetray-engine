/********************************************************************
*	Function definitions for the Particle class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEParticle.h"

#include "DBEApp.h"
#include "DBEColour.h"
#include "DBEUtilities.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
DBE::Particle::Particle()
	: m_age( 0.0f)
	, m_billBoardRotation( 0.0f, 0.0f, 0.0f)
	, m_colour( WHITE)
	, m_active( false)
{}

/**
* Destructor.
*/
DBE::Particle::~Particle() {}

/**
* Gets the colour of the particle.
*
* @return The colour of the particle.
*/
u32 DBE::Particle::GetColour() const {
	return m_colour;
}

/**
* Sets the colour of the particle.
*
* @param colour :: The colour of the particle.
*/
void DBE::Particle::SetColour( u32 colour) {
	m_colour = colour;
}

/**
* Gets whether the particle exists or not.
*
* @return True if the particle exists.
*/
bool DBE::Particle::IsActive() const {
	return m_active;
}

/**
* Sets whether the particle exists or not.
*
* @param active :: True if the particle exists.
*/
void DBE::Particle::SetActive( bool active) {
	m_active = active;
}