/********************************************************************
*	Function definitions for the AudioMgr class.
*********************************************************************/

/********************************************************************
*	Include the header file.
*********************************************************************/
#include "DBEAudioMgr.h"

#include <sstream>

#include "DBEMath.h"
#include "DBEUtilities.h"
/********************************************************************
*	Defines, constants and local variables.
*********************************************************************/


/********************************************************************
*	ReverpProps
********************************************************************/
/**
* Constructor.
*/
DBE::ReverbProps::ReverbProps( ReverbPreset r /*= ReverbPreset::RP_None*/) {
	this->UsePreset( r);
}

/**
* Set the member variables based on a preset from the 'ReverbPreset' enum.
*
* @param r :: Refer to the 'ReverbPreset' enum.
*/
void DBE::ReverbProps::UsePreset( ReverbPreset r) {
	switch( r) {
		case RP_None:		this->SetVars( -1,  1.00f, -10000, -10000, 0,   1.00f,  1.00f, 1.0f,  -2602, 0.007f,   200, 0.011f, 0.25f, 0.000f, 5000.0f, 250.0f,   0.0f,   0.0f);	break;
		case RP_Auditorium:	this->SetVars( 6,  1.00f, -1000,  -476,   0,   4.32f,  0.59f, 1.0f,   -789, 0.020f,  -289, 0.030f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f);		break;
		case RP_Cave:		this->SetVars( 8,  1.00f, -1000,  0,      0,   2.91f,  1.30f, 1.0f,   -602, 0.015f,  -302, 0.022f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f);		break;
		case RP_Alley:		this->SetVars( 14, 0.30f, -1000,  -270,   0,   1.49f,  0.86f, 1.0f,  -1204, 0.007f,    -4, 0.011f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f);		break;
		case RP_Forest:		this->SetVars( 15, 0.30f, -1000,  -3300,  0,   1.49f,  0.54f, 1.0f,  -2560, 0.162f,  -229, 0.088f, 0.25f, 0.000f, 5000.0f, 250.0f,  79.0f, 100.0f);		break;
		case RP_Quarry:		this->SetVars( 18, 1.00f, -1000,  -1000,  0,   1.49f,  0.83f, 1.0f, -10000, 0.061f,   500, 0.025f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f);		break;
		case RP_Plain:		this->SetVars( 19, 0.21f, -1000,  -2000,  0,   1.49f,  0.50f, 1.0f,  -2466, 0.179f, -1926, 0.100f, 0.25f, 0.000f, 5000.0f, 250.0f,  21.0f, 100.0f);		break;
		case RP_SewerPipe:	this->SetVars( 21, 0.80f, -1000,  -1000,  0,   2.81f,  0.14f, 1.0f,    429, 0.014f,  1023, 0.021f, 0.25f, 0.000f, 5000.0f, 250.0f,  80.0f,  60.0f);		break;
		case RP_Underwater:	this->SetVars( 22, 1.00f, -1000,  -4000,  0,   1.49f,  0.10f, 1.0f,   -449, 0.007f,  1700, 0.011f, 1.18f, 0.348f, 5000.0f, 250.0f, 100.0f, 100.0f);		break;
		default:			DebugTrace( "AudioMgr: Invalid ReverbProps preset specified.\n");
	}
}

/**
* Set all of the member variables. Refer to the member variable declarations in the structure to
* find out what each of the variables refer to.
*/
void DBE::ReverbProps::SetVars( s32 env, float envDiff, s32 room, s32 roomHF, s32 roomLF, float decay, float decayHF, float decayLF, s32 reflec, float reflecDelay,
			 s32 rev, float revDelay, float modTime, float modDepth, float refHF, float refLF, float diff, float density)
{
	m_environment = env;
	m_envDiffusion = envDiff;
	m_room = room;
	m_roomHF = roomHF;
	m_roomLF = roomLF;
	m_decayTime = decay;
	m_decayHFRatio = decayHF;
	m_decayLFRatio = decayLF;
	m_reflections = reflec;
	m_reflectionsDelay = reflecDelay;
	m_reverb = rev;
	m_reverbDelay = revDelay;
	m_modulationTime = modTime;
	m_modulationDepth = modDepth;
	m_HFRef = refHF;
	m_LFRef = refLF;
	m_diffusion = diff;
	m_density = density;
}

/**
* Copies the variables into a structure for FMOD.
*
* @param r :: The FMOD structure to copy the variables into.
*/
void DBE::ReverbProps::ConvertForFMOD( FMOD_REVERB_PROPERTIES& r) {
	r.Instance			= 0;
	r.Environment		= m_environment;
	r.EnvDiffusion		= m_envDiffusion;
	r.Room				= m_room;
	r.RoomHF			= m_roomHF;
	r.RoomLF			= m_roomLF;
	r.DecayTime			= m_decayTime;
	r.DecayHFRatio		= m_decayHFRatio;
	r.DecayLFRatio		= m_decayLFRatio;
	r.Reflections		= m_reflections;
	r.ReflectionsDelay	= m_reflectionsDelay;
	r.Reverb			= m_reverb;
	r.ReverbDelay		= m_reverbDelay;
	r.ModulationTime	= m_modulationTime;
	r.ModulationDepth	= m_modulationDepth;
	r.HFReference		= m_HFRef;
	r.LFReference		= m_LFRef;
	r.Diffusion			= m_diffusion;
	r.Density			= m_density;
	r.Flags				= 0x3f;
}


/********************************************************************
*	AudioMgr
********************************************************************/
/**
* Constructor for the AudioMgr class.
*/
DBE::AudioMgr::AudioMgr()
	: m_volumeSoundChanged( false)
	, m_volumeMusicChanged( false)
	, m_volumeMaster( 1.0f)
	, m_volumeSound( 1.0f)
	, m_volumeMusic( 1.0f)
	, m_lastError( nullptr)
{}

/**
* Initialises the audio manager.
*/
bool DBE::AudioMgr::Init() {
	unsigned int version;
	int drivers;

	// Create the FMOD interface object.
	if( !this->FMODSuccess( FMOD::System_Create( &mp_system))) {
		this->Shutdown();
		return false;
	}

	// Get the version number of FMOD.
	if( !this->FMODSuccess( mp_system->getVersion( &version))) {
		this->Shutdown();
		return false;
	}

	// Ensure that the version number of FMOD is correct.
	if( version < FMOD_VERSION) {
		std::ostringstream out;
		out << "Using old version of FMOD: " << version << " instead of " << FMOD_VERSION << ".";
		m_lastError = const_cast<char*>( out.str().c_str());

		this->Shutdown();
		return false;
	}

	// Check the number of sound cards and match FMOD to the first one.
	if( !this->FMODSuccess( mp_system->getNumDrivers( &drivers))) {
		this->Shutdown();
		return false;
	}

	if( drivers == 0) {
		if( !this->FMODSuccess( mp_system->setOutput( FMOD_OUTPUTTYPE_NOSOUND))) {
			this->Shutdown();
			return false;
		}
	}
	else {
		FMOD_CAPS caps;
		FMOD_SPEAKERMODE speakerMode;
		char name[256];

		// Get the capabilities of the default (0) sound card.
		if( !this->FMODSuccess( mp_system->getDriverCaps( 0, &caps, 0, &speakerMode))) {
			this->Shutdown();
			return false;
		}

		// Set the speaker mode to match that in control panel.
		if( !this->FMODSuccess( mp_system->setSpeakerMode( speakerMode))) {
			this->Shutdown();
			return false;
		}

		// Increase the buffer size if the user has Acceleration slider set to off.
		if( caps & FMOD_CAPS_HARDWARE_EMULATED) {
			if( !this->FMODSuccess( mp_system->setDSPBufferSize( 1024, 10))) {
				this->Shutdown();
				return false;
			}
		}

		// Get the name of the driver.
		if( !this->FMODSuccess( mp_system->getDriverInfo( 0, name, 256, 0))) {
			this->Shutdown();
			return false;
		}

		// SigmaTel sound devices crackle for some reason if the format is P.C.M. 16-bit. P.C.M.
		// floating point output seems to solve it.
		if( strstr( name, "SigmaTel")) {
			if( !this->FMODSuccess( mp_system->setSoftwareFormat( 48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR))) {
				this->Shutdown();
				return false;
			}
		}
	}

	// Initialise FMOD.
	FMOD_RESULT result;
	result = mp_system->init( MAX_AUDIO_CHANNELS, FMOD_INIT_NORMAL, 0);
	if( result == FMOD_ERR_OUTPUT_CREATEBUFFER) {
		if( !this->FMODSuccess( mp_system->setSpeakerMode( FMOD_SPEAKERMODE_STEREO))) {
			this->Shutdown();
			return false;
		}

		if( !this->FMODSuccess( mp_system->init( MAX_AUDIO_CHANNELS, FMOD_INIT_NORMAL, 0))) {
			this->Shutdown();
			return false;
		}
	}

	return true;
}

/**
* Destructor for the D3DAudioMgr class. Although this is a singleton class, I've created the
* instance of the Audio class by using 'static Audio' instead of dynamically creating a pointer
* when it's first used and thus doesn't need deleting.
*/
DBE::AudioMgr::~AudioMgr() {
	this->Shutdown();
}

/**
* Updates the FMOD system and clears out old handles in the array.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void DBE::AudioMgr::OnUpdate( float deltaTime) {
	DBE_Assert( mp_system != nullptr);

	// Run through and delete audio files that have finished playing.
	for( s32 i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		if( m_sounds[i].m_fileName == nullptr)
			continue;

		bool isPaused;
		FMOD_RESULT res = m_sounds[i].mp_channel->getPaused( &isPaused);

		// If the handle is invalid then the file has finished playing and can be deleted.
		if( res == FMOD_ERR_INVALID_HANDLE || res == FMOD_ERR_CHANNEL_STOLEN)
			m_sounds[i].Reset();
	}

	// Set the volume for all of the audio files.
	for( s32 i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		// Skip this sound, if it doesn't exist.
		if( m_sounds[i].m_fileName == nullptr)
			continue;

		if( m_sounds[i].m_isMusic && m_volumeMusicChanged)
			m_sounds[i].m_volume = m_sounds[i].m_volumeBase * m_volumeMusic;
		else if( !m_sounds[i].m_isMusic && m_volumeSoundChanged)
			m_sounds[i].m_volume = m_sounds[i].m_volumeBase * m_volumeSound;

		m_sounds[i].mp_channel->setVolume( m_sounds[i].m_volume * m_volumeMaster);
	}

	m_volumeMusicChanged = false;
	m_volumeSoundChanged = false;

	mp_system->update();
}

/**
* Loads an audio file into memory so it's not as processor intensive when playing (this should only
* be used for small audio files, such as sound effects).
*
* @param fileName :: The path of the audio file to cache.
*/
void DBE::AudioMgr::LoadAudio( const char* fileName) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindEmptyChannel());

	this->CreateSound( index, fileName, true);
}

/**
* Checks whether a specified audio file is playing or not. If the audio file can't be found (i.e.
* it hasn't been loaded at any point) then the function will return false.
*
* @param fileName :: The name of the audio file.
*
* @return True if the file is currently playing AND isn't paused.
*/
bool DBE::AudioMgr::IsPlaying( const char* fileName) const {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));
	bool playing( false);

	if( index >= 0) {
		m_sounds[index].mp_channel->isPlaying( &playing);

		if( playing) {
			m_sounds[index].mp_channel->getPaused( &playing);
			playing = !playing;
		}
	}

	return playing;
}

/**
* Plays an audio file once. This should NEVER be called on something that shouldn't be played
* multiple times simultaneously. E.g. if a music file has 'PlayAudioOneShot()' called on it twice
* then it will end up playing two instances at the same time; it's designed for use by sound
* effects (such as explosions). Of course, the first time a music file is played, it should be
* played using this function, but use 'PauseAudio()' to stop and start it from then on.
*
* @param fileName	:: The name of the audio file.
* @param volume		:: Default: 1.0f. The volume to play the audio file at.
*/
void DBE::AudioMgr::PlayAudioOneShot( const char* fileName, float volume /*= 1.0f*/) {
	DBE_Assert( mp_system != nullptr);

	volume = Clamp<float>( volume, VOLUME_MIN, VOLUME_MAX);

	s32 index( this->FindEmptyChannel());

	// Create the sound.
	this->CreateSound( index, fileName, false);

	// Set the volume (it'll be updated in the 'OnUpdate' function).
	m_sounds[index].m_volumeBase = volume;
	m_sounds[index].m_volume = volume * m_volumeSound;

	// Begin playing the sound.
	this->FMODSuccess( m_sounds[index].mp_channel->setPaused( false));
}

/**
* Plays an audio channel. This should be used to start off an audio file or to reset one. It
* shouldn't be used to play multiple instances of the same file as it will just end up resetting it
* over and over; use 'PlayAudioOneShot()' for this.
*
* @param fileName	:: The name of the audio file.
* @param volume		:: Default: 1.0f. The volume to play the audio file at.
*/
void DBE::AudioMgr::PlayAudio( const char* fileName, float volume /*= 1.0f*/) {
	DBE_Assert( mp_system != nullptr);

	volume = Clamp<float>( volume, VOLUME_MIN, VOLUME_MAX);

	s32 index( this->FindChannel( fileName));

	// If the file name wasn't found anywhere in the existing files, then create it.
	if( index < 0) {
		index = this->FindEmptyChannel();

		// Create the sound.
		this->CreateSound( index, fileName, false);
	}

	// Set the volume (it'll be updated in the 'OnUpdate' function).
	m_sounds[index].m_volumeBase = volume;
	if( m_sounds[index].m_isMusic)
		m_sounds[index].m_volume = volume * m_volumeMusic;
	else
		m_sounds[index].m_volume = volume * m_volumeSound;

	// Set the channel to the start of the audio file and begin playing the sound.
	this->FMODSuccess( m_sounds[index].mp_channel->setPosition( 0, FMOD_TIMEUNIT_PCM));
	this->FMODSuccess( m_sounds[index].mp_channel->setPaused( false));
}

/**
* Toggles an audio file's play status. If it's playing then it gets paused, but if it's already
* paused then it begins playing again.
*
* @param fileName :: The name of the audio file.
*/
void DBE::AudioMgr::PauseAudio( const char* fileName) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));

	// If the file was found, then pause/unpause it.
	if( index >= 0) {
		bool isPaused;

		this->FMODSuccess( m_sounds[index].mp_channel->getPaused( &isPaused));
		this->FMODSuccess( m_sounds[index].mp_channel->setPaused( !isPaused));
	}
}

/**
* Stops the audio file from playing. If the file doesn't already exist then it does nothing.
*
* @param fileName :: The name of the audio file to be stopped.
*/
void DBE::AudioMgr::StopAudio( const char* fileName) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));

	if( index >= 0)
		this->FMODSuccess( m_sounds[index].mp_channel->stop());
}

/**
* Gets the master volume.
*
* @return The master volume.
*/
float DBE::AudioMgr::GetMasterVolume() {
	return m_volumeMaster;
}

/**
* Gets the sound volume.
*
* @return The sound volume.
*/
float DBE::AudioMgr::GetSoundVolume() {
	return m_volumeSound;
}

/**
* Gets the music volume.
*
* @return The music volume.
*/
float DBE::AudioMgr::GetMusicVolume() {
	return m_volumeMusic;
}

/**
* Sets the master volume.
*
* @param volume :: The value to set the master volume to.
*/
void DBE::AudioMgr::SetMasterVolume( float volume /*= 1.0f*/) {
	m_volumeMaster = Clamp<float>( volume, VOLUME_MIN, VOLUME_MAX);
}

/**
* Sets the sound volume.
*
* @param volume :: The value to set the sound volume to.
*/
void DBE::AudioMgr::SetSoundVolume( float volume /*= 1.0f*/) {
	m_volumeSound = Clamp<float>( volume, VOLUME_MIN, VOLUME_MAX);
	m_volumeSoundChanged = true;
}

/**
* Sets the music volume.
*
* @param volume :: The value to set the music volume to.
*/
void DBE::AudioMgr::SetMusicVolume( float volume /*= 1.0f*/) {
	m_volumeMusic = Clamp<float>( volume, VOLUME_MIN, VOLUME_MAX);
	m_volumeMusicChanged = true;
}

/**
* Gets the volums of a specified audio clip.
*
* @param fileName	:: The name of the audio file.
*
* @return The volume of the audio clip (between 0.0f and 1.0f).
*/
float DBE::AudioMgr::GetVolume( const char* fileName) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));

	if( index >= 0)
		return m_sounds[index].m_volumeBase;
	else
		return 0.0f;
}

/**
* Sets the volume of a specified audio clip.
*
* @param fileName	:: The name of the audio file.
* @param volume		:: Default: 1.0f. The volume to play the audio file at.
*/
void DBE::AudioMgr::SetVolume( const char* fileName, float volume /*= 1.0f*/) {
	DBE_Assert( mp_system != nullptr);

	volume = Clamp<float>( volume, VOLUME_MIN, VOLUME_MAX);

	s32 index( this->FindChannel( fileName));

	if( index >= 0) {
		m_sounds[index].m_volumeBase = volume;

		if( m_sounds[index].m_isMusic)
			m_sounds[index].m_volume = volume * m_volumeMusic;
		else
			m_sounds[index].m_volume = volume * m_volumeSound;
	}
}

/**
* Gets the length of the audio clip, in milliseconds.
*
* @param fileName :: The name of the audio file.
*
* @return The length of the audio clip, in milliseconds. If '0' then the file doesn't exist.
*/
u32 DBE::AudioMgr::GetDuration( const char* fileName) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));
	u32 length( 0);

	if( index >= 0)
		this->FMODSuccess( m_sounds[index].mp_sound->getLength( &length, FMOD_TIMEUNIT_MS));

	return length;
}

/**
* Sets the playback position of an audio clip. If the position is set to -1 then it'll set the
* audio clip to the end. The position should be in milliseconds; i.e. if you want to skip to five
* seconds into the audio clip, then send through 5000 as the second parameter.
*
* @param fileName	:: The name of the audio file.
* @param pos		:: Default: -1. The position in the audio file.
*/
void DBE::AudioMgr::SetPlayPosition( const char* fileName, s32 pos /*= -1*/) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));
	u32 length( 0);
	m_sounds[index].mp_sound->getLength( &length, FMOD_TIMEUNIT_MS);

	if( index >= 0) {
		if( pos < 0 || (u32)pos > length)
			pos = length;
		
		this->FMODSuccess( m_sounds[index].mp_channel->setPosition( pos, FMOD_TIMEUNIT_MS));
	}
}

/**
* Gets the frequency of an audio clip.
*
* @param fileName	:: The name of the audio file.
*
* @return The current frequency of the audio file.
*/
float DBE::AudioMgr::GetFrequency( const char* fileName) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));
	float freq( 0.0f);

	if( index >= 0)
		this->FMODSuccess( m_sounds[index].mp_channel->getFrequency( &freq));

	return freq;
}

/**
* Sets the frequency of an audio clip.
*
* @param fileName	:: The name of the audio file.
* @param freq		:: The new frequency of the audio file.
*/
void DBE::AudioMgr::SetFrequency( const char* fileName, float freq) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));

	if( index >= 0)
		this->FMODSuccess( m_sounds[index].mp_channel->setFrequency( freq));
}

/**
* Set the reverberation of a specific audio file.
*
* @param fileName	:: The file name of the audio file.
* @param r			:: The reverberation properties to apply to the audio file.
*/
void DBE::AudioMgr::SetReverb( const char* fileName, ReverbProps r) {
	DBE_Assert( mp_system != nullptr);

	s32 index( this->FindChannel( fileName));

	if( index >= 0) {
		FMOD_REVERB_PROPERTIES props;
		r.ConvertForFMOD( props);

		this->FMODSuccess( mp_system->setReverbProperties( &props));

		FMOD_REVERB_CHANNELPROPERTIES p = { 0 };
		m_sounds[index].mp_channel->setReverbProperties( &p);
	}
}

/**
* Get a textual representation of a reverberation preset.
*
* @param r		:: A reverb preset listed in the 'ReverbPreset' enum.
* @param name	:: A char buffer of size 256 to hold the string.
*/
void DBE::AudioMgr::GetReverbPresetName( ReverbPreset r, char* name) const {
	switch( r) {
			case RP_None:				strcpy_s( name, 256, "None");				break;
			case RP_Auditorium:			strcpy_s( name, 256, "Auditorium");			break;
			case RP_Cave:				strcpy_s( name, 256, "Cave");				break;
			case RP_Alley:				strcpy_s( name, 256, "Alley");				break;
			case RP_Forest:				strcpy_s( name, 256, "Forest");				break;
			case RP_Quarry:				strcpy_s( name, 256, "Quarry");				break;
			case RP_Plain:				strcpy_s( name, 256, "Plain");				break;
			case RP_SewerPipe:			strcpy_s( name, 256, "Sewer Pipe");			break;
			case RP_Underwater:			strcpy_s( name, 256, "Underwater");			break;
			default:					DBE_AssertAlways();
		}
}

/**
* Stops all the audio files from playing.
*/
void DBE::AudioMgr::StopAllAudio() {
	DBE_Assert( mp_system != nullptr);

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		if( m_sounds[i].m_fileName == nullptr)
			continue;

		this->FMODSuccess( m_sounds[i].mp_channel->setPosition( 0, FMOD_TIMEUNIT_PCM));
		this->FMODSuccess( m_sounds[i].mp_channel->setPaused( true));
	}
}

/**
* Deletes all the currently stored audio information (save for the FMOD::System, so it can be used
* by the application as well as the Audio Shutdown() function).
*/
void DBE::AudioMgr::DeleteAllAudio() {
	DBE_Assert( mp_system != nullptr);

	// Just to be sure nothing goes wrong, stop all audio from playing before deleting it.
	this->StopAllAudio();

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i)
		m_sounds[i].Reset();
}

/**
* Locates the index number of an empty slot in the sounds array.
*
* @return The position of the empty channel in the Audio array. The value is negative if no empty
*			slot was found.
*/
int DBE::AudioMgr::FindEmptyChannel() const {
	DBE_Assert( mp_system != nullptr);

	s32 index( -1);

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		if( m_sounds[i].m_fileName == nullptr) {
			index = i;
			break;
		}
	}

	// If no empty slot was found then bitch about it now instead of somewhere down the line.
	if( index < 0)
		DBE_AssertAlways();

	return index;
}

/**
* Locates the index number of the audio file.
*
* @param fileName :: The name of the audio file.
*
* @return The position of the channel in the Audio array. If the value is negative then it means
*			the file wasn't found.
*/
int DBE::AudioMgr::FindChannel( const char* fileName) const {
	DBE_Assert( mp_system != nullptr);

	s32 index( -1);

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		// Check that the sound exists before trying to access an invalid instance.
		if( m_sounds[i].m_fileName == nullptr) {
			continue;
		}
		// If the instance exists, then check the file name.
		else if( strcmp( m_sounds[i].m_fileName, fileName) == 0) {
			index = i;
			break;
		}
	}

	return index;
}

/**
* Loads an audio file in and prepares it for play by storing it in the array at the given index.
* This function is used through-out the Audio class and thus files are created and paused because
* the calling function may not always want the file to be played straight away.
*
* @param index		:: The position in the array that the sound should be inserted.
* @param fileName	:: The name of the audio file.
* @param cache		:: Whether the sound should be loaded into memory or streamed from disk.
*
* @return 
*/
void DBE::AudioMgr::CreateSound( s32 index, const char* fileName, bool cache) {
	DBE_Assert( mp_system != nullptr);

	FMOD_RESULT (__stdcall FMOD::System::*funcPointer)( const char*, FMOD_MODE, FMOD_CREATESOUNDEXINFO*, FMOD::Sound**);

	// If the audio file should be cached then load it into memory with FMOD::System::createSound.
	if( cache)
		funcPointer = &FMOD::System::createSound;
	else
		funcPointer = &FMOD::System::createStream;

	// Create the sound and channel.
	this->FMODSuccess( (mp_system->*funcPointer)( fileName, FMOD_DEFAULT, 0, &m_sounds[index].mp_sound));
	this->FMODSuccess( mp_system->playSound( FMOD_CHANNEL_FREE, m_sounds[index].mp_sound, true, &m_sounds[index].mp_channel));

	m_sounds[index].m_fileName = fileName;
	m_sounds[index].m_isMusic = cache;
}

/**
* Deletes all the audio variables and shuts down FMOD.
*/
void DBE::AudioMgr::Shutdown() {
	this->DeleteAllAudio();

	mp_system->release();
}

/**
* Checks that the FMOD_RESULT means that the operation succeeded. If it didn't, then it records the
* error message and return false.
*
* @param result :: The variable returned by most FMOD functions.
*
* @return True if the FMOD_RESULT indicates nothing went wrong.
*/
bool DBE::AudioMgr::FMODSuccess( FMOD_RESULT result) {
	if( result != FMOD_OK)
		m_lastError = const_cast<char*>( FMOD_ErrorString( result));

	return( result == FMOD_OK);
}