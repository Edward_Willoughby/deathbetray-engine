/********************************************************************
*	Function definitions for the BasicVertexShader class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEBasicVertexShader.h"

#include "DBEApp.h"
#include "DBEGraphicsHelpers.h"
#include "DBEMath.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Constructor.
*/
BasicVertexShader::BasicVertexShader() {}

/**
* Destructor.
*/
BasicVertexShader::~BasicVertexShader() {}

/**
* 
*
* @param :: 
*
* @return 
*/
bool DBE::BasicVertexShader::Init() {
	char shaderPath[256];
	DebuggerDependentPath( shaderPath, sizeof( shaderPath), "DBEngine_ROOT", "", "shaders\\BasicShader.hlsl");

	this->LoadShader( shaderPath, g_vertPos3fColour4ubNormal3fMacros, g_vertPos3fColour4ubNormal3fDesc, g_vertPos3fColour4ubNormal3fSize);

	this->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
	
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_WVP",					SVT_FLOAT4x4, &m_slotWVP);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_colour",				SVT_FLOAT4, &m_slotColour);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_W",					SVT_FLOAT4x4, &m_slotLights.m_slotWorld);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_InvXposeW",			SVT_FLOAT4x4, &m_slotLights.m_slotInverse);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightDirections",		SVT_FLOAT4, &m_slotLights.m_slotLightDir);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightPositions",		SVT_FLOAT4, &m_slotLights.m_slotLightPos);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightColours",			SVT_FLOAT3, &m_slotLights.m_slotLightColour);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightAttenuations",	SVT_FLOAT4, &m_slotLights.m_slotLightAtten);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightSpots",			SVT_FLOAT4, &m_slotLights.m_slotLightSpots);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_numLights",			SVT_INT, &m_slotLights.m_slotNumLights);

	this->CreateCBuffer( m_slotCBufferGlobalVars);

	return true;
}

/**
* 
*
* @param :: 
*
* @return 
*/
void DBE::BasicVertexShader::SetVars( DBE::Matrix4* world, DBE::Vec4* colour) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	if( mp_CBuffers[m_slotCBufferGlobalVars]) {
		D3D11_MAPPED_SUBRESOURCE vsMap;
		if( !mp_CBuffers[m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_CBuffers[m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &vsMap)))
			vsMap.pData = nullptr;

		Matrix4 wvp = GET_APP()->GetWVP();

		// Send the WVP matrix to the shader.
		SetCBufferMatrix( vsMap, m_slotWVP, wvp);

		SetCBufferVar( vsMap, m_slotColour, *colour);

		GET_APP()->PassLightsToShader( this, vsMap, &m_slotLights, *world);

		// Do... something to the shader maps.
		if( vsMap.pData)
			p_context->Unmap( mp_CBuffers[m_slotCBufferGlobalVars], 0);
	}
}