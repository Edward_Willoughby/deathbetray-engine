/********************************************************************
*	Function definitions for the math functionality.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBEMath.h"

/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/********************************************************************
*	DBE::Vec2
********************************************************************/
/**
* Constructor.
*/
DBE::Vec2::Vec2()
	: m_vector( 0.0f, 0.0f)
{}

/**
* Constructor.
*/
DBE::Vec2::Vec2( float s)
	: m_vector( s, s)
{}

/**
* Constructor.
*
* @param x :: The x value of the vector.
* @param y :: The y value of the vector.
*/
DBE::Vec2::Vec2( float x, float y)
	: m_vector( x, y)
{}

/**
* Constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec2::Vec2( const DirectX::XMFLOAT2& vec)
	: m_vector( vec)
{}

/**
* Constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec2::Vec2( const DirectX::XMVECTOR& vec) {
	XMStoreFloat2( &m_vector, vec);
}

/**
* Copy constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec2::Vec2( const DBE::Vec2& vec) {
	m_vector = vec.m_vector;
}

/**
* Get the x value.
*
* @return The x value of the vector.
*/
float DBE::Vec2::GetX() const {
	return m_vector.x;
}

/**
* Get the y value.
*
* @return The y value of the vector.
*/
float DBE::Vec2::GetY() const {
	return m_vector.y;
}

/**
* Get the vector.
*
* @return A copy of the vector.
*/
DirectX::XMFLOAT2 DBE::Vec2::GetVector() const {
	return m_vector;
}

/**
* Get the vector in 32-bit aligned format.
*
* @return A reference to the vector.
*/
DirectX::XMVECTOR DBE::Vec2::GetXMVector() {
	return XMLoadFloat2( &m_vector);
}

/**
* Get the vector in 32-bit aligned format.
*
* @return A copy of the vector.
*/
DirectX::XMVECTOR DBE::Vec2::GetXMVectorConst() const {
	return XMLoadFloat2( &m_vector);
}

/**
* Sets the x value.
*
* @param x :: The new x value of the vector.
*/
void DBE::Vec2::SetX( float x) {
	m_vector.x = x;
}

/**
* Sets the y value.
*
* @param y :: The new y value of the vector.
*/
void DBE::Vec2::SetY( float y) {
	m_vector.y = y;
}

/**
* Get the length of the vector.
*
* @return The length of the vector.
*/
float DBE::Vec2::GetLength() const {
	return XMVectorGetByIndex( XMVector2Length( XMLoadFloat2( &m_vector)), 0);
}

/**
* Normalises the vector.
*/
void DBE::Vec2::Normalise() {
	XMStoreFloat2( &m_vector, XMVector2Normalize( XMLoadFloat2( &m_vector)));
}

/**
* Gets a normalised version of the vector.
*
* @return Normalised version of the vector.
*/
Vec2 DBE::Vec2::GetNormalised() const {
	return Vec2( XMVector2Normalize( XMLoadFloat2( &m_vector)));
}

/**
* Sets all values to positive.
*/
void DBE::Vec2::Abs() {
	XMStoreFloat2( &m_vector, XMVectorAbs( XMLoadFloat2( &m_vector)));
}

/**
* Gets a version of the vector that has all values set to positive.
*
* @return An absolute version of the vector.
*/
DBE::Vec2 DBE::Vec2::GetAbs() const {
	return Vec2( XMVectorAbs( XMLoadFloat2( &m_vector)));
}

/**
* Converts the vector to be in degrees.
*/
void DBE::Vec2::ToDegrees() {
	m_vector.x = DBE_ToDegrees( m_vector.x);
	m_vector.y = DBE_ToDegrees( m_vector.y);
}

/**
* Gets a converted vector that's in degrees.
*
* @return A copy of the vector that's converted to degrees.
*/
DBE::Vec2 DBE::Vec2::GetDegrees() const {
	Vec2 v( m_vector);
	v.ToDegrees();

	return v;
}

/**
* Converts the vector to be in radians.
*/
void DBE::Vec2::ToRadians() {
	m_vector.x = DBE_ToRadians( m_vector.x);
	m_vector.y = DBE_ToRadians( m_vector.y);
}

/**
* Gets a converted vector that's in radians.
*/
DBE::Vec2 DBE::Vec2::GetRadians() const {
	Vec2 v( m_vector);
	v.ToRadians();

	return v;
}

/**
* Check if the vector's values are 0.0f.
*
* @return True if all components are 0.0f.
*/
bool DBE::Vec2::IsZero() {
	if( this->GetX() == 0.0f &&
		this->GetY() == 0.0f)
		return true;

	return false;
}

/**
* Overloaded operator.
*/
DBE::Vec2 DBE::Vec2::operator +( const DBE::Vec2& r) const {
	return Vec2( XMVectorAdd( XMLoadFloat2( &m_vector), XMLoadFloat2( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec2 DBE::Vec2::operator -( const DBE::Vec2& r) const {
	return Vec2( XMVectorSubtract( XMLoadFloat2( &m_vector), XMLoadFloat2( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec2 DBE::Vec2::operator *( const DBE::Vec2& r) const {
	return Vec2( XMVectorMultiply( XMLoadFloat2( &m_vector), XMLoadFloat2( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec2 DBE::Vec2::operator *( const float r) const {
	return Vec2( m_vector.x * r, m_vector.y * r);
}

/**
* Overloaded operator.
*/
void DBE::Vec2::operator +=( const DBE::Vec2& r) {
	XMStoreFloat2( &m_vector, XMVectorAdd( XMLoadFloat2( &m_vector), XMLoadFloat2( &r.m_vector)));
}

/**
* Overloaded operator.
*/
void DBE::Vec2::operator -=( const DBE::Vec2& r) {
	XMStoreFloat2( &m_vector, XMVectorSubtract( XMLoadFloat2( &m_vector), XMLoadFloat2( &r.m_vector)));
}

/**
* Overloaded operator.
*/
void DBE::Vec2::operator *=( const float r) {
	XMStoreFloat2( &m_vector, XMVectorScale( XMLoadFloat2( &m_vector), r));
}

/**
* Overloaded operator.
*/
void DBE::Vec2::operator /=( const float r) {
	DBE_Assert( r != 0.0f);

	m_vector.x /= r;
	m_vector.y /= r;
}

/**
* Overloaded operator.
*/
DBE::Vec2 DBE::Vec2::operator-() const {
	Vec2 v( -m_vector.x, -m_vector.y);
	return v;
}

/**
* Overloaded operator.
*/
void DBE::Vec2::operator=( const DBE::Vec2& r) {
	m_vector = r.m_vector;
}

/**
* Overloaded operator.
*/
void DBE::Vec2::operator=( const DirectX::XMFLOAT2& r) {
	m_vector = r;
}

/**
* Overloaded operator.
*/
void DBE::Vec2::operator=( const DirectX::XMVECTOR& r) {
	XMStoreFloat2( &m_vector, r);
}

/**
* Overloaded operator.
*/
bool DBE::Vec2::operator ==( const DBE::Vec2& r) const {
	XMVECTOR a( XMLoadFloat2( &m_vector));
	XMVECTOR b( XMLoadFloat2( &r.m_vector));

	if( XMVector2Equal( a, b))
		return true;
	
	return false;
}

/**
* Overloaded operator.
*/
bool DBE::Vec2::operator !=( const DBE::Vec2& r) const {
	if( *this == r)
		return false;
	
	return true;
}

/**
* Implicit conversion operator.
*/
DBE::Vec2::operator DirectX::XMVECTOR() const
{
	return DirectX::XMVectorSet( m_vector.x, m_vector.y, 0.0f, 0.0f);
}


/********************************************************************
*	DBE::Vec3
********************************************************************/
/**
* Constructor.
*/
DBE::Vec3::Vec3()
	: m_vector( 0.0f, 0.0f, 0.0f)
{}

/**
* Constructor.
*/
DBE::Vec3::Vec3( float s)
	: m_vector( s, s, s)
{}

/**
* Constructor.
*
* @param x :: The x value of the vector.
* @param y :: The y value of the vector.
* @param z :: The z value of the vector.
*/
DBE::Vec3::Vec3( float x, float y, float z)
	: m_vector( x, y, z)
{}

/**
* Constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec3::Vec3( const DirectX::XMFLOAT3& vec)
	: m_vector( vec)
{}

/**
* Constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec3::Vec3( const DirectX::XMVECTOR& vec) {
	XMStoreFloat3( &m_vector, vec);
}

/**
* Copy constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec3::Vec3( const DBE::Vec3& vec) {
	m_vector = vec.m_vector;
}

/**
* Get the x value.
*
* @return The x value of the vector.
*/
float DBE::Vec3::GetX() const {
	return m_vector.x;
}

/**
* Get the y value.
*
* @return The y value of the vector.
*/
float DBE::Vec3::GetY() const {
	return m_vector.y;
}

/**
* Get the z value.
*
* @return The z value of the vector.
*/
float DBE::Vec3::GetZ() const {
	return m_vector.z;
}

/**
* Gets the x and y values of the vector.
*
* @return The x and y values of the vector.
*/
DBE::Vec2 DBE::Vec3::GetXY() const {
	return Vec2( m_vector.x, m_vector.y);
}

/**
* Get the vector.
*
* @return A copy of the vector.
*/
DirectX::XMFLOAT3 DBE::Vec3::GetVector() const {
	return m_vector;
}

/**
* Get the vector in 32-bit aligned format.
*
* @return A reference to the vector.
*/
DirectX::XMVECTOR DBE::Vec3::GetXMVector() {
	return XMLoadFloat3( &m_vector);
}

/**
* Get the vector in 32-bit aligned format.
*
* @return A reference to the vector.
*/
DirectX::XMVECTOR DBE::Vec3::GetXMVectorConst() const {
	return XMLoadFloat3( &m_vector);
}

/**
* Sets the x value.
*
* @param x :: The new x value of the vector.
*/
void DBE::Vec3::SetX( float x) {
	m_vector.x = x;
}

/**
* Sets the y value.
*
* @param y :: The new y value of the vector.
*/
void DBE::Vec3::SetY( float y) {
	m_vector.y = y;
}

/**
* Sets the z value.
*
* @param z :: The new z value of the vector.
*/
void DBE::Vec3::SetZ( float z) {
	m_vector.z = z;
}

/**
* Get the length of the vector.
*
* @return The length of the vector.
*/
float DBE::Vec3::GetLength() const {
	return XMVectorGetByIndex( XMVector3Length( XMLoadFloat3( &m_vector)), 0);
}

/**
* Normalises the vector.
*/
void DBE::Vec3::Normalise() {
	XMStoreFloat3( &m_vector, XMVector3Normalize( XMLoadFloat3( &m_vector)));
}

/**
* Gets a normalised version of the vector.
*
* @return Normalised version of the vector.
*/
Vec3 DBE::Vec3::GetNormalised() const {
	return Vec3( XMVector3Normalize( XMLoadFloat3( &m_vector)));
}

/**
* Sets all values to positive.
*/
void DBE::Vec3::Abs() {
	XMStoreFloat3( &m_vector, XMVectorAbs( XMLoadFloat3( &m_vector)));
}

/**
* Gets a version of the vector that has all values set to positive.
*
* @return An absolute version of the vector.
*/
DBE::Vec3 DBE::Vec3::GetAbs() const {
	return Vec3( XMVectorAbs( XMLoadFloat3( &m_vector)));
}

/**
* Converts the vector to be in degrees.
*/
void DBE::Vec3::ToDegrees() {
	m_vector.x = DBE_ToDegrees( m_vector.x);
	m_vector.y = DBE_ToDegrees( m_vector.y);
	m_vector.z = DBE_ToDegrees( m_vector.z);
}

/**
* Gets a converted vector that's in degrees.
*
* @return A copy of the vector that's converted to degrees.
*/
DBE::Vec3 DBE::Vec3::GetDegrees() const {
	Vec3 v( m_vector);
	v.ToDegrees();

	return v;
}

/**
* Converts the vector to be in radians.
*/
void DBE::Vec3::ToRadians() {
	m_vector.x = DBE_ToRadians( m_vector.x);
	m_vector.y = DBE_ToRadians( m_vector.y);
	m_vector.z = DBE_ToRadians( m_vector.z);
}

/**
* Gets a converted vector that's in radians.
*/
DBE::Vec3 DBE::Vec3::GetRadians() const {
	Vec3 v( m_vector);
	v.ToRadians();

	return v;
}

/**
* Check if the vector's values are 0.0f.
*
* @return True if all components are 0.0f.
*/
bool DBE::Vec3::IsZero() {
	if( this->GetX() == 0.0f &&
		this->GetY() == 0.0f &&
		this->GetZ() == 0.0f)
		return true;

	return false;
}

/**
* Overloaded operator.
*/
DBE::Vec3 DBE::Vec3::operator +( const DBE::Vec3& r) const {
	return Vec3( XMVectorAdd( XMLoadFloat3( &m_vector), XMLoadFloat3( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec3 DBE::Vec3::operator -( const DBE::Vec3& r) const {
	return Vec3( XMVectorSubtract( XMLoadFloat3( &m_vector), XMLoadFloat3( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec3 DBE::Vec3::operator *( const DBE::Vec3& r) const {
	return Vec3( XMVectorMultiply( XMLoadFloat3( &m_vector), XMLoadFloat3( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec3 DBE::Vec3::operator *( const float r) const {
	return Vec3( m_vector.x * r, m_vector.y * r, m_vector.z * r);
}

/**
* Overloaded operator.
*/
void DBE::Vec3::operator +=( const DBE::Vec3& r) {
	XMStoreFloat3( &m_vector, XMVectorAdd( XMLoadFloat3( &m_vector), XMLoadFloat3( &r.m_vector)));
}

/**
* Overloaded operator.
*/
void DBE::Vec3::operator -=( const DBE::Vec3& r) {
	XMStoreFloat3( &m_vector, XMVectorSubtract( XMLoadFloat3( &m_vector), XMLoadFloat3( &r.m_vector)));
}

/**
* Overloaded operator.
*/
void DBE::Vec3::operator *=( const float r) {
	XMStoreFloat3( &m_vector, XMVectorScale( XMLoadFloat3( &m_vector), r));
}

/**
* Overloaded operator.
*/
void DBE::Vec3::operator /=( const float r) {
	DBE_Assert( r != 0.0f);

	m_vector.x /= r;
	m_vector.y /= r;
	m_vector.z /= r;
}

/**
* Overloaded operator.
*/
DBE::Vec3 DBE::Vec3::operator-() const {
	Vec3 v( -m_vector.x, -m_vector.y, -m_vector.z);
	return v;
}

/**
* Overloaded operator.
*/
void DBE::Vec3::operator=( const DBE::Vec3& r) {
	m_vector = r.m_vector;
}

/**
* Overloaded operator.
*/
void DBE::Vec3::operator=( const DirectX::XMFLOAT3& r) {
	m_vector = r;
}

/**
* Overloaded operator.
*/
void DBE::Vec3::operator=( const DirectX::XMVECTOR& r) {
	XMStoreFloat3( &m_vector, r);
}

/**
* Overloaded operator.
*/
bool DBE::Vec3::operator ==( const DBE::Vec3& r) const {
	XMVECTOR a( XMLoadFloat3( &m_vector));
	XMVECTOR b( XMLoadFloat3( &r.m_vector));

	if( XMVector3Equal( a, b))
		return true;
	
	return false;
}

/**
* Overloaded operator.
*/
bool DBE::Vec3::operator !=( const DBE::Vec3& r) const {
	if( *this == r)
		return false;
	
	return true;
}

/**
* Implicit conversion operator.
*/
DBE::Vec3::operator DirectX::XMVECTOR() const
{
	return DirectX::XMVectorSet( m_vector.x, m_vector.y, m_vector.z, 0.0f);
}


/********************************************************************
*	DBE::Vec4
********************************************************************/
/**
* Constructor.
*/
DBE::Vec4::Vec4()
	: m_vector( 0.0f, 0.0f, 0.0f, 0.0f)
{}

/**
* Constructor.
*/
DBE::Vec4::Vec4( float s)
	: m_vector( s, s, s, s)
{}

/**
* Constructor.
*
* @param x :: The x value of the vector.
* @param y :: The y value of the vector.
* @param z :: The z value of the vector.
* @param w :: The w value of the vector.
*/
DBE::Vec4::Vec4( float x, float y, float z, float w)
	: m_vector( x, y, z, w)
{}

/**
* Constructor.
*
* @param v :: The x, y, and z values of the vector.
* @param w :: The w value of the vector.
*/
DBE::Vec4::Vec4( const DBE::Vec3& v, float w)
	: m_vector( v.GetX(), v.GetY(), v.GetZ(), w)
{}

/**
* Constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec4::Vec4( const DirectX::XMFLOAT4& vec) 
	: m_vector( vec)
{}

/**
* Constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec4::Vec4( const DirectX::XMVECTOR& vec) {
	XMStoreFloat4( &m_vector, vec);
}

/**
* Copy constructor.
*
* @param vec :: The vector to copy.
*/
DBE::Vec4::Vec4( const DBE::Vec4& vec)
	: m_vector( vec.m_vector)
{}

/**
* Get the x value.
*
* @return The x value of the vector.
*/
float DBE::Vec4::GetX() const {
	return m_vector.x;
}

/**
* Get the y value.
*
* @return The y value of the vector.
*/
float DBE::Vec4::GetY() const {
	return m_vector.y;
}

/**
* Get the z value.
*
* @return The z value of the vector.
*/
float DBE::Vec4::GetZ() const {
	return m_vector.z;
}

/**
* Get the w value.
*
* @return The w value of the vector.
*/
float DBE::Vec4::GetW() const {
	return m_vector.w;
}

/**
* Gets the x and y values of the vector.
*
* @return The x and y values of the vector.
*/
DBE::Vec2 DBE::Vec4::GetXY() const {
	return Vec2( m_vector.x, m_vector.y);
}

/**
* Gets the x, y, and z values of the vector.
*
* @return The x, y, and z values of the vector.
*/
DBE::Vec3 DBE::Vec4::GetXYZ() const {
	return Vec3( m_vector.x, m_vector.y, m_vector.z);
}

/**
* Get the vector.
*
* @return A copy of the vector.
*/
DirectX::XMFLOAT4 DBE::Vec4::GetVector() const {
	return m_vector;
}

/**
* Get the vector in 32-bit aligned format.
*
* @return A reference to the vector.
*/
DirectX::XMVECTOR DBE::Vec4::GetXMVector() {
	return XMLoadFloat4( &m_vector);
}

/**
* Get the vector in 32-bit aligned format.
*
* @return A reference to the vector.
*/
DirectX::XMVECTOR DBE::Vec4::GetXMVectorConst() const {
	return XMLoadFloat4( &m_vector);
}

/**
* Sets the x value.
*
* @param x :: The new x value of the vector.
*/
void DBE::Vec4::SetX( float x) {
	m_vector.x = x;
}

/**
* Sets the y value.
*
* @param y :: The new y value of the vector.
*/
void DBE::Vec4::SetY( float y) {
	m_vector.y = y;
}

/**
* Sets the z value.
*
* @param z :: The new z value of the vector.
*/
void DBE::Vec4::SetZ( float z) {
	m_vector.z = z;
}

/**
* Sets the w value.
*
* @param w :: The new w value of the vector.
*/
void DBE::Vec4::SetW( float w) {
	m_vector.w = w;
}

/**
* Get the length of the vector.
*
* @return The length of the vector.
*/
float DBE::Vec4::GetLength() const {
	return XMVectorGetByIndex( XMVector4Length( XMLoadFloat4( &m_vector)), 0);
}

/**
* Normalises the vector.
*/
void DBE::Vec4::Normalise() {
	XMStoreFloat4( &m_vector, XMVector4Normalize( XMLoadFloat4( &m_vector)));
}

/**
* Gets a normalised version of the vector.
*
* @return Normalised version of the vector.
*/
Vec4 DBE::Vec4::GetNormalised() const {
	return Vec4( XMVector4Normalize( XMLoadFloat4( &m_vector)));
}

/**
* Sets all values to positive.
*/
void DBE::Vec4::Abs() {
	XMStoreFloat4( &m_vector, XMVectorAbs( XMLoadFloat4( &m_vector)));
}

/**
* Gets a version of the vector that has all values set to positive.
*
* @return An absolute version of the vector.
*/
DBE::Vec4 DBE::Vec4::GetAbs() const {
	return Vec4( XMVectorAbs( XMLoadFloat4( &m_vector)));
}

/**
* Converts the vector to be in degrees.
*/
void DBE::Vec4::ToDegrees() {
	m_vector.x = DBE_ToDegrees( m_vector.x);
	m_vector.y = DBE_ToDegrees( m_vector.y);
	m_vector.z = DBE_ToDegrees( m_vector.z);
	m_vector.w = DBE_ToDegrees( m_vector.w);
}

/**
* Gets a converted vector that's in degrees.
*
* @return A copy of the vector that's converted to degrees.
*/
DBE::Vec4 DBE::Vec4::GetDegrees() const {
	Vec4 v( m_vector);
	v.ToDegrees();

	return v;
}

/**
* Converts the vector to be in radians.
*/
void DBE::Vec4::ToRadians() {
	m_vector.x = DBE_ToRadians( m_vector.x);
	m_vector.y = DBE_ToRadians( m_vector.y);
	m_vector.z = DBE_ToRadians( m_vector.z);
	m_vector.w = DBE_ToRadians( m_vector.w);
}

/**
* Gets a converted vector that's in radians.
*/
DBE::Vec4 DBE::Vec4::GetRadians() const {
	Vec4 v( m_vector);
	v.ToRadians();

	return v;
}

/**
* Check if the vector's values are 0.0f.
*
* @return True if all components are 0.0f.
*/
bool DBE::Vec4::IsZero() {
	if( this->GetX() == 0.0f &&
		this->GetY() == 0.0f &&
		this->GetZ() == 0.0f &&
		this->GetW() == 0.0f)
		return true;

	return false;
}

/**
* Overloaded operator.
*/
DBE::Vec4 DBE::Vec4::operator +( const DBE::Vec4& r) const {
	return Vec4( XMVectorAdd( XMLoadFloat4( &m_vector), XMLoadFloat4( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec4 DBE::Vec4::operator -( const DBE::Vec4& r) const {
	return Vec4( XMVectorSubtract( XMLoadFloat4( &m_vector), XMLoadFloat4( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec4 DBE::Vec4::operator *( const DBE::Vec4& r) const {
	return Vec4( XMVectorMultiply( XMLoadFloat4( &m_vector), XMLoadFloat4( &r.m_vector)));
}

/**
* Overloaded operator.
*/
DBE::Vec4 DBE::Vec4::operator *( const float r) const {
	return Vec4( m_vector.x * r, m_vector.y * r, m_vector.z * r, m_vector.w * r);
}

/**
* Overloaded operator.
*/
void DBE::Vec4::operator +=( const DBE::Vec4& r) {
	XMStoreFloat4( &m_vector, XMVectorAdd( XMLoadFloat4( &m_vector), XMLoadFloat4( &r.m_vector)));
}

/**
* Overloaded operator.
*/
void DBE::Vec4::operator -=( const DBE::Vec4& r) {
	XMStoreFloat4( &m_vector, XMVectorSubtract( XMLoadFloat4( &m_vector), XMLoadFloat4( &r.m_vector)));
}

/**
* Overloaded operator.
*/
void DBE::Vec4::operator *=( const float r) {
	XMStoreFloat4( &m_vector, XMVectorScale( XMLoadFloat4( &m_vector), r));
}

/**
* Overloaded operator.
*/
void DBE::Vec4::operator /=( const float r) {
	DBE_Assert( r != 0.0f);

	m_vector.x /= r;
	m_vector.y /= r;
	m_vector.z /= r;
	m_vector.w /= r;
}

/**
* Overloaded operator.
*/
DBE::Vec4 DBE::Vec4::operator-() const {
	Vec4 v( -m_vector.x, -m_vector.y, -m_vector.z, -m_vector.w);
	return v;
}

/**
* Overloaded operator.
*/
void DBE::Vec4::operator=( const DBE::Vec4& r) {
	m_vector = r.m_vector;
}

/**
* Overloaded operator.
*/
void DBE::Vec4::operator=( const DirectX::XMFLOAT4& r) {
	m_vector = r;
}

/**
* Overloaded operator.
*/
void DBE::Vec4::operator=( const DirectX::XMVECTOR& r) {
	XMStoreFloat4( &m_vector, r);
}

/**
* Overloaded operator.
*/
bool DBE::Vec4::operator ==( const DBE::Vec4& r) const {
	XMVECTOR a( XMLoadFloat4( &m_vector));
	XMVECTOR b( XMLoadFloat4( &r.m_vector));

	if( XMVector4Equal( a, b))
		return true;
	
	return false;
}

/**
* Overloaded operator.
*/
bool DBE::Vec4::operator !=( const DBE::Vec4& r) const {
	if( *this == r)
		return false;
	
	return true;
}

/**
* Implicit conversion operator.
*/
DBE::Vec4::operator DirectX::XMVECTOR() const
{
	return DirectX::XMVectorSet( m_vector.x, m_vector.y, m_vector.z, m_vector.z);
}


/********************************************************************
*	DBE::Matrix4
********************************************************************/
/**
* Constructor.
*/
DBE::Matrix4::Matrix4() {
	this->SetIdentity();
}

/**
* Constructor.
*/
DBE::Matrix4::Matrix4( const DBE::Matrix4& r) {
	m_mat = r.m_mat;
}

/**
* Constructor.
*/
DBE::Matrix4::Matrix4( const DirectX::XMFLOAT4X4& r) {
	m_mat = r;
}

/**
* Constructor.
*/
DBE::Matrix4::Matrix4( const DirectX::XMMATRIX& r) {
	XMStoreFloat4x4( &m_mat, r);
}

/**
* Set the matrix to the identity.
*/
void DBE::Matrix4::SetIdentity() {
	XMStoreFloat4x4( &m_mat, XMMatrixIdentity());
}

/**
* Get the vector.
*/
DirectX::XMFLOAT4X4 DBE::Matrix4::GetMatrix() const {
	return m_mat;
}

/**
* Get the vector in 32-bit aligned format.
*/
DirectX::XMMATRIX DBE::Matrix4::GetXMMatrix() const {
	return XMLoadFloat4x4( &m_mat);
}

/**
* Assignment operator.
*/
void DBE::Matrix4::operator=( const DBE::Matrix4& r) {
	m_mat = r.m_mat;
}

/**
* Assignment operator.
*/
void DBE::Matrix4::operator=( const DirectX::XMFLOAT4X4& r) {
	m_mat = r;
}

/**
* Assignment operator.
*/
void DBE::Matrix4::operator=( const DirectX::XMMATRIX& r) {
	XMStoreFloat4x4( &m_mat, r);
}

/**
* Implicit conversion operator.
*/
DBE::Matrix4::operator DirectX::XMFLOAT4X4() const {
	return m_mat;
}

/**
* Implicit conversion operator.
*/
DBE::Matrix4::operator DirectX::XMMATRIX() const {
	return XMLoadFloat4x4( &m_mat);
}


/********************************************************************
*	Free Functions.
********************************************************************/
/**
* Calculates the Dot product of two 'Vec2's.
*
* @param a :: The first vector.
* @param b :: The second vector.
*
* @return The dot product of the two parameters.
*/
float DBE::Dot( const DBE::Vec2& a, const DBE::Vec2& b)
{
	return XMVectorGetByIndex( XMVector2Dot( a.GetXMVectorConst(), b.GetXMVectorConst()), 0);
}

/**
* Calculates the Dot product of two 'Vec3's.
*
* @param a :: The first vector.
* @param b :: The second vector.
*
* @return The dot product of the two parameters.
*/
float DBE::Dot( const DBE::Vec3& a, const DBE::Vec3& b)
{
	return XMVectorGetByIndex( XMVector3Dot( a.GetXMVectorConst(), b.GetXMVectorConst()), 0);
}

/**
* Calculates the Dot product of two 'Vec4's.
*
* @param a :: The first vector.
* @param b :: The second vector.
*
* @return The dot product of the two parameters.
*/
float DBE::Dot( const DBE::Vec4& a, const DBE::Vec4& b)
{
	return XMVectorGetByIndex( XMVector4Dot( a.GetXMVectorConst(), b.GetXMVectorConst()), 0);
}

/**
* Calculates the Cross product of two 'Vec3's.
*
* @param a :: The first vector.
* @param b :: The second vector.
*
* @return The cross product of the two parameters.
*/
DBE::Vec3 DBE::Cross( const DBE::Vec3& a, const DBE::Vec3& b)
{
	return XMVector3Cross( a.GetXMVectorConst(), b.GetXMVectorConst());
}