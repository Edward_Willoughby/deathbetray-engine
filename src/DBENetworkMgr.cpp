/********************************************************************
*	Function definitions for the NetworkMgr class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBENetworkMgr.h"

#include <sstream>
#include <WinInet.h>

#include "DBEUtilities.h"

/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
/// The required version that WinSock neesd to load.
const u8 REQ_WINSOCK_VERSION( 2);


/**
* Constructor.
*/
DBE::NetworkMgr::NetworkMgr()
	: m_checkConn( false)
{}

/**
* Destructor.
*/
DBE::NetworkMgr::~NetworkMgr() {
	WSACleanup();
}

/**
* Initialises the winsock library.
*
* @return True if everything initialised correctly.
*/
bool DBE::NetworkMgr::Init() {
	WSADATA wsaData;

	if( WSAStartup( MAKEWORD( REQ_WINSOCK_VERSION, 0), &wsaData) != 0)
		return false;

	// Ensure the major version number is correct.
	if( LOBYTE( wsaData.wVersion) < REQ_WINSOCK_VERSION)
		return false;

	return true;
}

/**
* Forces the NetworkMgr to perform internet connection checks. This means that whenever the
* 'NetworkMgr' uses a function in which data is sent or received, it will first check that the
* computer is connected to the internet.
*/
void DBE::NetworkMgr::SetConnectionChecking( bool check) {
	m_checkConn = check;
}

/**
* Checks that the computer is currently connected to the internet.
*
* @return True if the computer is connected to the internet.
*/
bool DBE::NetworkMgr::IsConnectedToInternet() const {
	DWORD flag;
	return InternetGetConnectedState( &flag, 0) == TRUE;
}

/**
* Fills a sockaddr with all the information required to make a SOCKET.
*
* @param pSockAddr	:: Pointer to the 'sockaddr' that will receive the information.
* @param serverName	:: The I.P. of the server.
* @param portNumber	:: The port that'll be used to communicate the data.
*/
void DBE::NetworkMgr::FillSockAddr( DBE::NetworkSocket& socket, const char* serverName, u16 portNumber) {
	socket.m_sockAddr.sin_family = AF_INET;
	socket.m_sockAddr.sin_port = this->NetworkAlignPortNumber( portNumber);
	socket.m_sockAddr.sin_addr.S_un.S_addr = (serverName == nullptr ? INADDR_ANY : inet_addr( serverName));
}

/**
* Sets up a socket to conenct to a server.
*
* @param s		:: The socket to be created.
* @param server	:: The I.P. of the server.
* @param port	:: The port number that'll transmit the data.
*
* @return True if the socket was created correctly.
*/
bool DBE::NetworkMgr::CreateSocket( DBE::NetworkSocket& s, const char* server, u16 port) {
	s.Reset();

	// Fill the socket's sockaddr structure.
	this->FillSockAddr( s, server, port);

	// Create the socket.
	if(( s.m_socket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
		return false;

	// Set the socket to be non-blocking.
	u_long mode( 1);
	if( ioctlsocket( s.m_socket, FIONBIO, &mode) != 0)
		return false;

	return true;
}

/**
* Checks that a socket has successfully connected to a server.
*
* @param s :: The socket that should be connected to a server.
*
* @return 'SC_Connected' if connected. Look at the enum definition for more information.
*/
ServerConnection DBE::NetworkMgr::CheckSocketConnection( const DBE::NetworkSocket& s) const {
	s32 ret = connect( s.m_socket, reinterpret_cast<const sockaddr*>( &s.m_sockAddr), sizeof( s.m_sockAddr));

	if( ret != 0) {
		s32 error( WSAGetLastError());

		if( error == WSAEWOULDBLOCK)
			return ServerConnection::SC_NotConnected;
		else if( error != WSAEALREADY)
			return ServerConnection::SC_Error;
	}

	// Connection accepted.
	return ServerConnection::SC_Connected;
}

/**
* Sets up a socket to listen to a port for incoming connections (used for servers).
*
* @param s		:: The socket to be created.
* @param port	:: The port number that'll transmit the data.
*
* @return True if the socket was created correctly.
*/
bool DBE::NetworkMgr::CreateListeningSocket( DBE::NetworkSocket& s, u16 port) {
	s.Reset();

	// Create the socket.
	if(( s.m_socket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
		return false;

	// Set the socket to be non-blocking.
	u_long mode( 1);
	if( ioctlsocket( s.m_socket, FIONBIO, &mode) != 0)
		return false;

	// Bind the socket.
	this->FillSockAddr( s, nullptr, port);
	if( bind( s.m_socket, reinterpret_cast<sockaddr*>( &s.m_sockAddr), sizeof( s.m_sockAddr)) != 0)
		return false;

	// Put the socket into listening mode.
	if( listen( s.m_socket, SOMAXCONN) != 0)
		return false;

	return true;
}

/**
* Checks for an incoming connection and creates a 'NetworkSocket' for it.
*
* @param server		:: The server's listening socket.
* @param newConn	:: The details of the new connection, if one was made.
*
* @return True if a new connection was made.
*/
bool DBE::NetworkMgr::CheckNewConnection( const DBE::NetworkSocket& server, DBE::NetworkSocket& newConn) {
	if( m_checkConn && !this->IsConnectedToInternet())
		return false;

	if( newConn.Exists())
		newConn.Reset();

	s32 socketSize( sizeof( newConn.m_sockAddr));

	// Check for a connection.
	newConn.m_socket = accept( server.m_socket, reinterpret_cast<sockaddr*>( &newConn.m_sockAddr), &socketSize);

	// Check if a connection was made.
	if( newConn.m_socket == INVALID_SOCKET)
		return false;
	else
		return true;
}

/**
* Sends a message through a SOCKET.
*
* @param hSocket	:: The SOCKET to use the message through.
* @param msg		:: The null-terminated string to send.
*/
void DBE::NetworkMgr::MessageSend( const DBE::NetworkSocket& socket, const char* msg) {
	if( m_checkConn && !this->IsConnectedToInternet())
		return;

	s32 len( strlen( msg));
	s32 ret( 0);

	// Keep looping until the entire message is sent.
	while( len > 0) {
		ret = send( socket.m_socket, msg, len, 0);

		if( ret == SOCKET_ERROR) {
			// The send request failed.
			DebugTrace( "NetworkMgr Error: Failed to send message.\n");
			break;
		}
		else {
			// Move along the message array, and decrement the amount of bytes to send.
			len -= ret;
			msg += ret;
		}
	}
}

/**
* Returns a message sent through a SOCKET.
*
* @param hSocket	:: The SOCKET that the message was sent through.
* @param msg		:: The null-terminated string that was received.
*
* @return True if the message was received correctly. False can mean that there was either an error
*			or no message was sent.
*/
bool DBE::NetworkMgr::MessageGet( const DBE::NetworkSocket& socket, std::string& msg) {
	if( m_checkConn && !this->IsConnectedToInternet())
		return false;

	std::ostringstream out;
	char buffer[256];
	s32 errorMsg( 0);
	s32 ret = recv( socket.m_socket, buffer, sizeof( buffer)-1, 0);

	// The connection is closed and thus no data.
	if( ret == 0) {
		return false;
	}
	else if( ret == SOCKET_ERROR) {
		errorMsg = WSAGetLastError();

		if( errorMsg != WSAEWOULDBLOCK && errorMsg != WSAENOTCONN)
			DebugTrace( "NetworkMgr Error: Couldn't read the incoming message.\n");

		return false;
	}

	// Store the first part of the message.
	buffer[ret] = 0;
	out << buffer;

	// If the ret value is less than the buffer size, then that was the whole message...
	if( ret < sizeof( buffer)-1) {
		msg = out.str();
		return true;
	}

	// ... otherwise, get the rest of the message as well.
	while( true) {
		ret = recv( socket.m_socket, buffer, sizeof( buffer)-1, 0);

		if( ret == 0) {
			// Connection has been closed.
			break;
		}
		else if( ret == SOCKET_ERROR) {
			errorMsg = WSAGetLastError();

			if( errorMsg == WSAEWOULDBLOCK) {
				continue;
			}
			else if( errorMsg == WSAENOTCONN) {
				break;
			}
			else {
				DebugTrace( "NetworkMgr Error: Couldn't read *end of* the incoming message.\n");
				break;
			}
		}
		else {
			// ret is the number of bytes read.
			// Terminate buffer with zero and add to the string stream.
			buffer[ret] = 0;
			out << buffer;
		}
	}

	msg = out.str();
	return true;
}

/**
* Check a message to see if it begins with a keyword.
*
* @param msg		:: The message to check.
* @param keyword	:: The keyword to look for.
*
* @return True if the message begins with the keyword.
*/
bool DBE::NetworkMgr::MsgStartsWithKeyword( const char* msg, const char* keyword) {
	u32 keywordLen( strlen( keyword));

	// If the message is shorter than the keyword, then it can't match.
	if( strlen( msg) < keywordLen)
		return false;

	// Run through and check the characters are the same.
	bool match( true);
	for( u32 i( 0); i < keywordLen; ++i) {
		if( msg[i] != keyword[i]) {
			match = false;
			break;
		}
	}

	return match;
}

/**
* Returns the I.P. address held in a 'sockaddr'.
*
* @param sockAddr :: The 'sockaddr' whose I.P. you want to know.
*
* @return A null-terminated string of the I.P.
*/
char* DBE::NetworkMgr::GetSockAddrIP( const DBE::NetworkSocket& socket) {
	return inet_ntoa( socket.m_sockAddr.sin_addr);
}

/**
* Returns the port held in a 'sockaddr'.
*
* @param sockAddr :: The 'sockaddr' whose port you want to know.
*
* @return The port number.
*/
u16 DBE::NetworkMgr::GetSockAddrPort( const DBE::NetworkSocket& socket) {
	return ntohs( socket.m_sockAddr.sin_port);
}

/**
* Aligns an integer to correctly work in a 'sockaddr' structure.
*
* @param portNumber :: The port number.
*
* @return The 'sockaddr' aligned port number.
*/
u16 DBE::NetworkMgr::NetworkAlignPortNumber( u16 portNumber) {
	return htons( portNumber);
}

/**
* Gets the I.P. of a hostname (e.g. "www.google.com").
*
* @param serverName :: The name of the server/host.
*
* @return The I.P. number, in network byte order, of the server/host.
*/
u32 DBE::NetworkMgr::FindHostIP( const char* serverName) {
	if( m_checkConn && !this->IsConnectedToInternet())
		return 0;

	HOSTENT* pHostent( nullptr);
	u32 ret( 0);

	// Get the hostent structure for the hostname.
	if( pHostent == gethostbyname( serverName)) {
		// Extract the primary IP address from hostent structure.
		if( pHostent->h_addr_list && pHostent->h_addr_list[0])
			ret = *reinterpret_cast<u32*>( pHostent->h_addr_list[0]);
	}

	return ret;
}