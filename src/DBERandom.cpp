/********************************************************************
*	Function definitions for the Random class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DBERandom.h"

#include "DBETime.h"
#include "DBEUtilities.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

/// Static pointer for the random functionality.
static DBE::Random* gsp_rand( nullptr);
/// Static seed value.
static s32 gs_seedValue( 1);
/// Static boolean to indicate whether the 'random' numbers have a pattern or not.
static bool gs_trulyRandom( true);


/**
* Deletes the Random class (this should only be called by D3DApp).
*/
void DBE::Random::Shutdown() {
	if( gsp_rand != nullptr)
		SafeDelete( gsp_rand);
}

/**
* Generates an integer between 0 and RAND_MAX inclusively.
*
* @return An integer between 0 and RAND_MAX inclusively.
*/
s32 DBE::Random::Int() {
	EnsureInstantiated();

	return gsp_rand->Rand();
}

/**
* Generates an integer between 0 and max inclusively.
*
* @params max :: Integral value that defines the highest number this function can return.
*
* @return Signed integer between 0 and nMax inclusively.
*/
s32 DBE::Random::Int( s32 max) {
	EnsureInstantiated();

	if( max <= 0)
		return 0;

	return( Random::Int() % max);
}

/**
* Generates an integer between min and max inclusively.
*
* @params min :: Integral value that defines the lowest number this function can return.
* @params max :: Integral value that defines the highest number this function can return.
*
* @return Signed integer between min and max inclusively.
*/
s32 DBE::Random::Int( s32 min, s32 max) {
	Random::EnsureInstantiated();

	return( min + Random::Int(( max - min) + 1));
}

/**
* Generates a float between 0.0f and 1.0f inclusively.
*
* @return Float, to two decimal places, between 0.0f and 1.0f.
*/
float DBE::Random::Float() {
	Random::EnsureInstantiated();

	return Random::Float( 0.0f, 1.0f);
}

/**
* Generates a float between 0.0f and max inclusively.
*
* @params max :: Float that defines the highest value this function can return.
*
* @return Float, to two decimal places, between 0.0f and max inclusively.
*/
float DBE::Random::Float( float max) {
	Random::EnsureInstantiated();

	// Get everything before the decimal point (the integer).
	s32 integral( Random::Int( static_cast<s32>( max - 1)));
	// Get everything after the decimal point.
	float fractional( static_cast<float>( Random::Int( 1, 100)) / 100);
	// Add the two together.
	return( integral + fractional);
}

/**
* Generates a float between min and max inclusively.
*
* @params min :: Float that defines the lowest value this function can return.
* @params max :: Float that defines the highest value this function can return.
*
* @return Float, to two decimal places, between min and max inclusively.
*/
float DBE::Random::Float( float min, float max) {
	Random::EnsureInstantiated();

	return( min + Random::Float( max - min));
}

/**
* Generates a random boolean value.
*
* @return A random boolean value.
*/
bool DBE::Random::Bool() {
	Random::EnsureInstantiated();

	// Generates a value and take the first bit to determine the bool's value.
	return( gsp_rand->Rand() & 0x1);
}

/**
* Generates a random colour (always opaque).
*
* @return Unsigned integer which contains red, green, blue, and alpha values.
*/
u32 DBE::Random::Colour() {
	Random::EnsureInstantiated();

	u8 r( Random::Int( 255)),
	   g( Random::Int( 255)),
	   b( Random::Int( 255));

	return IntsToColour( r, g, b, 255);
}

/**
* Sets whether the values that are generated have any kind of pattern.
*
* @param trulyRandom :: Default: True. Set to true if you don't want subsequent values to be
*						calculated based on the previous value.
*/
void DBE::Random::SetTrulyRandom( bool trulyRandom /*= true*/) {
	gs_trulyRandom = trulyRandom;
}

/**
* Sets the initial value that is used to create a random number. Only use this function if you want
* the random numbers to be predictable... weird thing to say about a RANDOM class, right?
*
* @param seedValue :: The value to seed the R.N.G. to.
*/
void DBE::Random::SetSeedValue( s32 seedValue) {
	gs_seedValue = seedValue;
	gs_trulyRandom = false;
}

/**
* Checks that the class exists, if it doesn't then it creates it. This function SHOULD be at the
* start of every static function to make sure that the 'Random' class exists before trying to use
* it to generate a value. It performs a check and creates the class if it doesn't already exist.
*/
void DBE::Random::EnsureInstantiated() {
	if( gsp_rand != nullptr)
		return;

	gsp_rand = new Random();
	gsp_rand->Seed();
}

/**
* Seeds the random number generator. This seeds the class using the system time and will only be
* used when the class is first initialised; hence why it's private.
*/
void DBE::Random::Seed() {
	gs_seedValue = 1;

	if( gs_trulyRandom)
		gs_seedValue = Time::GetTime().m_seconds;

	srand( 0);
}

/**
* Returns a number between 0 and RAND_MAX inclusively. If the class has been set to be truly random
* then this function will refresh the seed value using the system time everytime it's called,
* otherwise it'll manipulate to previously used seed value and use that.
*/
s32 DBE::Random::Rand() {
	if( gs_trulyRandom)
		gs_seedValue += Time::GetTime().m_seconds;

	gs_seedValue *= 214013L + 2531011L;
	gs_seedValue = (( gs_seedValue >> 16) & 0x7FFF);

	return gs_seedValue + rand();
}