/********************************************************************
*	Function definitions for the BasicMesh class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "DBEBasicMesh.h"

#include <d3d11.h>

#include "DBEApp.h"
#include "DBEBasicPixelShader.h"
#include "DBEBasicVertexShader.h"
#include "DBEColour.h"
#include "DBEGraphicsHelpers.h"
#include "DBEShader.h"
#include "DBEVertexTypes.h"
/********************************************************************
*	Defines, constants, namespaces and local variables.
********************************************************************/
using namespace DirectX;


/**
* Constructor.
*/
DBE::BasicMesh::BasicMesh( MeshType type, float param1 /*= 1.0f*/, float param2 /*= 1.0f*/, float param3 /*= 1.0f*/, u32 colour /*= 0*/)
	: mp_verts( nullptr)
	, mp_indicies( nullptr)
	, mp_VS( nullptr)
	, mp_PS( nullptr)
{
	if( colour == 0)
		colour = DEFAULT_MESH_COLOUR;
	this->SetColour( colour);

	// This is now used to set the colour of the verticies.
	colour = WHITE;

	switch( type) {
		case MT_Plane:
			this->CreatePlane( param1, param2, param3, colour);
			break;
		case MT_Cube:
			this->CreateCube( param1, param2, param3, colour);
			break;
		case MT_Sphere:
			this->CreateSphere( param1, (u32)param2, (u32)param3, colour);
			break;
		case MT_Cylinder:
			this->CreateCone( param1, param1, param2, colour);
			break;
		case MT_Cone:
			this->CreateCone( param1, param2, param3, colour);
			break;
		case MT_Cross:
			this->CreateCross( param1, param2, param3, colour);
			break;
		default:
			DBE_AssertAlways();
	}

	this->CreateBaseBoundingSphere<VertPos3fColour4ubNormal3f>( mp_verts);

	if( !MGR_SHADER().GetShader<BasicVertexShader>( gsc_basicVertexShaderName, mp_VS)) {
		mp_VS = new BasicVertexShader();
		mp_VS->Init();
		MGR_SHADER().AddShader<BasicVertexShader>( gsc_basicVertexShaderName, mp_VS);
	}
	
	if( !MGR_SHADER().GetShader<BasicPixelShader>( gsc_basicPixelShaderName, mp_PS)) {
		mp_PS = new BasicPixelShader();
		mp_PS->Init();
		MGR_SHADER().AddShader<BasicPixelShader>( gsc_basicPixelShaderName, mp_PS);
	}
}

/**
* Destructor.
*/
DBE::BasicMesh::~BasicMesh() {
	SafeDeleteArray( mp_verts);
	SafeDeleteArray( mp_indicies);

	ReleaseCOM( mp_vertBuffer);
	ReleaseCOM( mp_indexBuffer);

	MGR_SHADER().RemoveShader<BasicVertexShader>( mp_VS);
	MGR_SHADER().RemoveShader<BasicPixelShader>( mp_PS);
}

/**
* Renders the object (this shouldn't be called outside the 'Render' function).
*
* @param deltaTime :: The time taken to render the last frame.
*/
void DBE::BasicMesh::OnRender( float deltaTime) {
	// Don't try to render if the vertex or index buffers are invalid, the shader isn't loaded, or
	// if there are too few polygons.
	if( mp_vertBuffer == nullptr || mp_indexBuffer == nullptr || mp_VS == nullptr || mp_PS == nullptr || m_indexCount < 3)
		return;

	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	float c[4];
	ColourToFloats( c[0], c[1], c[2], c[3], this->GetColour());
	mp_VS->SetVars( &this->GetWorldMatrix( false), &Vec4( c[0], c[1], c[2], c[3]));
	
	if( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_VS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
	
	p_context->VSSetShader( mp_VS->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_PS->mp_PS, nullptr, 0);

	p_context->IASetInputLayout( mp_VS->mp_IL);
	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { mp_vertBuffer };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubNormal3f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( mp_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	p_context->DrawIndexed( m_indexCount, 0, 0);
}

/**
* Creates the mesh data for a plane.
*/
void DBE::BasicMesh::CreatePlane( float width /*= 1.0f*/, float height /*= 1.0f*/, float depth /*= 1.0f*/, u32 colour /*= 0*/) {
	width /= 2;
	height /= 2;

	mp_verts = new VertPos3fColour4ubNormal3f[4];
	mp_verts[0] = VertPos3fColour4ubNormal3f( Vec3( -width, -height, 0.0f), colour, Vec3YAxis.GetVector());
	mp_verts[1] = VertPos3fColour4ubNormal3f( Vec3( -width,  height, 0.0f), colour, Vec3YAxis.GetVector());
	mp_verts[2] = VertPos3fColour4ubNormal3f( Vec3(  width,  height, 0.0f), colour, Vec3YAxis.GetVector());
	mp_verts[3] = VertPos3fColour4ubNormal3f( Vec3(  width, -height, 0.0f), colour, Vec3YAxis.GetVector());
	mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3f)*4, mp_verts);

	m_indexCount = 6;
	mp_indicies = new UINT[m_indexCount];
	mp_indicies[0] = 0;
	mp_indicies[1] = 1;
	mp_indicies[2] = 2;
	mp_indicies[3] = 0;
	mp_indicies[4] = 2;
	mp_indicies[5] = 3;
	mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT)*m_indexCount, mp_indicies);
}

/**
* Creates the mesh data for a cube.
* SOURCE: DirectXTK.
*
* @param width	:: The width of the cube.
* @param height	:: The height of the cube.
* @param depth	:: The depth of the cube.
* @param colour	:: The colour of the cube.
*/
void DBE::BasicMesh::CreateCube( float width /*= 1.0f*/, float height /*= 1.0f*/, float depth /*= 1.0f*/, u32 colour /*= 0*/) {
	// Halve the dimensions as the cube is going to be expanded in both directions by those values.
	Vec3 size( width / 2, height / 2, depth / 2);

	static const Vec3 faceNormals[6] = {
		Vec3(  0.0f,  0.0f,  1.0f ),
		Vec3(  0.0f,  0.0f, -1.0f ),
		Vec3(  1.0f,  0.0f,  0.0f ),
		Vec3( -1.0f,  0.0f,  0.0f ),
		Vec3(  0.0f,  1.0f,  0.0f ),
		Vec3(  0.0f, -1.0f,  0.0f ),
	};

	mp_verts = new VertPos3fColour4ubNormal3f[6 * 4];
	mp_indicies = new UINT[6 * 6];

	// Start the counters at -1 because they get incremented before being used.
	s32 vertCount( -1);
	s32 indexCount( -1);

	for( s32 i( 0); i < 6; ++i) {
		Vec3 normal( faceNormals[i]);

		// Get two vectors perpendicular both to the face normal and to each other.
		Vec3 basis( i >= 4 ? g_XMIdentityR2 : g_XMIdentityR1);

		Vec3 side1( Cross( normal, basis));
		Vec3 side2( Cross( normal, side1));

		// Six indicies (two triangles) per face.
		s32 base( vertCount + 1);
		mp_indicies[++indexCount] = base + 0;
		mp_indicies[++indexCount] = base + 1;
		mp_indicies[++indexCount] = base + 2;

		mp_indicies[++indexCount] = base + 0;
		mp_indicies[++indexCount] = base + 2;
		mp_indicies[++indexCount] = base + 3;

		// Four verticies per face.
		Vec3 vert0 = (normal - side1 - side2) * size;
		Vec3 vert1 = (normal - side1 + side2) * size;
		Vec3 vert2 = (normal + side1 + side2) * size;
		Vec3 vert3 = (normal + side1 - side2) * size;

		mp_verts[++vertCount] = VertPos3fColour4ubNormal3f( vert0.GetVector(), colour, normal.GetVector());
		mp_verts[++vertCount] = VertPos3fColour4ubNormal3f( vert1.GetVector(), colour, normal.GetVector());
		mp_verts[++vertCount] = VertPos3fColour4ubNormal3f( vert2.GetVector(), colour, normal.GetVector());
		mp_verts[++vertCount] = VertPos3fColour4ubNormal3f( vert3.GetVector(), colour, normal.GetVector());
	}

	m_vertexCount = vertCount;
	m_indexCount = indexCount;

	// Create the buffers.
	mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3f) * vertCount, mp_verts);
	mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT) * m_indexCount, mp_indicies);
}

/**
* Creates the mesh data for a sphere.
* SOURCE: DirectXTK.
*
* @param radius		:: The radius of the sphere.
* @param vSegments	:: The vertical tessalation of the sphere.
* @param hSegments	:: The horizontal tessalation of the sphere.
* @param colour		:: The colour of the sphere.
*/
void DBE::BasicMesh::CreateSphere( float radius /*= 1.0f*/, u32 vSegments /*= 1.0f*/, u32 hSegments /*= 1.0f*/, u32 colour /*= 0*/) {
	// There needs to be at least three vertical segments.
	if( vSegments < 3)
		vSegments = 3;

	// There needs to be at least six horizontal segments.
	if( hSegments < 6)
		hSegments = 6;

	mp_verts = new VertPos3fColour4ubNormal3f[(vSegments+1) * (hSegments+1)];
	mp_indicies = new UINT[(vSegments * hSegments) * 6];

	// Start the counters at -1 because they get incremented before being used.
	s32 vertCount( -1);
	s32 indexCount( -1);

	// Create rings of vertices at progressively higher latitudes.
	for( u32 i( 0); i <= vSegments; ++i) {
		float v( 1 - (float)i / vSegments);

		float latitude(( i * XM_PI / vSegments) - XM_PIDIV2);
		float dy( 0.0f), dxz( 0.0f);

		XMScalarSinCos( &dy, &dxz, latitude);

		for( u32 j( 0); j <= hSegments; ++j) {
			float u( (float)j / hSegments);

			float longitude( j * XM_2PI / hSegments);
			float dx( 0.0f), dz( 0.0f);

			XMScalarSinCos( &dx, &dz, longitude);

			dx *= dxz;
			dz *= dxz;

			Vec3 normal( dx, dy, dz);
			Vec2 textCoord( u, v);

			Vec3 pos( normal.GetXMVector() * radius);

			mp_verts[++vertCount] = VertPos3fColour4ubNormal3f( pos, colour, normal.GetVector());
		}
	}
	m_vertexCount = vertCount;

	// Fill the index buffer with triangles joining each pair of latitude rings.
	u32 stride( hSegments + 1);

	for( u32 i( 0); i < vSegments; ++i) {
		for( u32 j( 0); j < hSegments; ++j) {
			u32 nextI( i + 1);
			u32 nextJ(( j + 1) % stride);

			mp_indicies[++indexCount] = i * stride + j;
			mp_indicies[++indexCount] = nextI * stride + j;
			mp_indicies[++indexCount] = i * stride + nextJ;

			mp_indicies[++indexCount] = i * stride + nextJ;
			mp_indicies[++indexCount] = nextI * stride + j;
			mp_indicies[++indexCount] = nextI * stride + nextJ;
		}
	}
	m_indexCount = indexCount;

	// Create the buffers.
	mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3f) * vertCount, mp_verts);
	mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT) * m_indexCount, mp_indicies);
}

/**
* Creates the mesh data for a cone.
* SOURCE: DirectXTK.
*
* @param baseRadius	:: The radius of the base of the cone.
* @param topRadius	:: The radius of the top of the cone.
* @param height		:: The height of the cone.
* @param colour		:: The colour of the cone.
*/
void DBE::BasicMesh::CreateCone( float baseRadius /*= 1.0f*/, float topRadius /*= 1.0f*/, float height /*= 1.0f*/, u32 colour /*= 0*/) {
	// The tessellation isn't being passed through as a parameter, so I'll have create an equation
	// that'll suffice for any sized cone.
	s32 tessellation( (s32)(( baseRadius + topRadius) * 10) + 10);

	s32 vertCount(( tessellation + 1) * 2);
	s32 indexCount(( tessellation + 1) * 6);

	// Additional verticies and indicies for the bottom cap.
	if( baseRadius != 0.0f) {
		vertCount += tessellation;
		indexCount += (tessellation-2) * 3;
	}

	// Additional verticies and indicies for the top cap.
	if( topRadius != 0.0f) {
		vertCount += tessellation;
		indexCount += (tessellation-2) * 3;
	}

	mp_verts = new VertPos3fColour4ubNormal3f[vertCount];
	mp_indicies = new UINT[indexCount];
	
	// Start the counters at -1 because they get incremented before being used.
	vertCount = -1;
	indexCount = -1;

	height /= 2;

	Vec3 heightOffset = g_XMIdentityR1 * height;

	s32 stride( tessellation + 1);

	// Create a ring of triangles around the outside of the cone.
	//DebugTrace( "Cone verticies:\n");
	for( s32 i( 0); i <= tessellation; ++i) {
		Vec3 circleVec = this->GetCircleVector( i, tessellation);

		Vec3 baseOffset = circleVec.GetXMVector() * baseRadius;
		Vec3 topOffset = circleVec.GetXMVector() * topRadius;

		float u( (float)i / tessellation);
		Vec2 textureCoord( u);

		Vec3 base, top;
		base = baseOffset - heightOffset;
		top = topOffset + heightOffset;

		Vec3 normal = XMVector3Cross( GetCircleTangent( i, tessellation), topOffset - base);
		normal = XMVector3Normalize( normal.GetXMVector());

		mp_verts[++vertCount] = VertPos3fColour4ubNormal3f( top, colour, normal.GetVector());
		mp_verts[++vertCount] = VertPos3fColour4ubNormal3f( base, colour, normal.GetVector());

		mp_indicies[++indexCount] = i * 2;
		mp_indicies[++indexCount] = (i * 2 + 2) % (stride * 2);
		mp_indicies[++indexCount] = i * 2 + 1;

		mp_indicies[++indexCount] = i * 2 + 1;
		mp_indicies[++indexCount] = (i * 2 + 2) % (stride * 2);
		mp_indicies[++indexCount] = (i * 2 + 3) % (stride * 2);

		//DebugTrace( "Vert %i. X: %f, Y: %f, Z: %f\n", vertCount, top.x, top.y, top.z);
		//DebugTrace( "Vert %i. X: %f, Y: %f, Z: %f\n", vertCount, base.x, base.y, base.z);
	}
	
	if( baseRadius != 0.0f)
		this->CreateCylinderCap( &mp_verts[0], vertCount, &mp_indicies[0], indexCount, tessellation, height, baseRadius, colour, false);
	if( topRadius != 0.0f)
		this->CreateCylinderCap( &mp_verts[0], vertCount, &mp_indicies[0], indexCount, tessellation, height, topRadius, colour, true);

	m_vertexCount = vertCount;
	m_indexCount = indexCount;

	// Create the buffers.
	mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3f) * vertCount, mp_verts);
	mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT) * m_indexCount, mp_indicies);
}

/**
* Creates the mesh data for a cross.
*/
void DBE::BasicMesh::CreateCross( float width /*= 1.0f*/, float height /*= 1.0f*/, float depth /*= 1.0f*/, u32 colour /*= 0*/) {
	width /= 2;
	height /= 2;
	depth /= 2;

	float aWidth = width - (width / 10);
	float aHeight = height - (height / 10);
	depth /= 2;

	Vec3 nFront( 0.0f, 0.0f, 1.0f);
	Vec3 nBack( 0.0f, 0.0f, -1.0f);

	mp_verts = new VertPos3fColour4ubNormal3f[16];
	mp_verts[0] = VertPos3fColour4ubNormal3f( Vec3( -aWidth,   height,  depth), colour, nFront);
	mp_verts[1] = VertPos3fColour4ubNormal3f( Vec3(   width, -aHeight,  depth), colour, nFront);
	mp_verts[2] = VertPos3fColour4ubNormal3f( Vec3(  aWidth,  -height,  depth), colour, nFront);
	mp_verts[3] = VertPos3fColour4ubNormal3f( Vec3(  -width,  aHeight,  depth), colour, nFront);
		
	mp_verts[4] = VertPos3fColour4ubNormal3f( Vec3(  aWidth,   height,  depth), colour, nFront);
	mp_verts[5] = VertPos3fColour4ubNormal3f( Vec3(   width,  aHeight,  depth), colour, nFront);
	mp_verts[6] = VertPos3fColour4ubNormal3f( Vec3( -aWidth,  -height,  depth), colour, nFront);
	mp_verts[7] = VertPos3fColour4ubNormal3f( Vec3(  -width, -aHeight,  depth), colour, nFront);

	mp_verts[8] = VertPos3fColour4ubNormal3f( Vec3( -aWidth,   height, -depth), colour, nBack);
	mp_verts[9] = VertPos3fColour4ubNormal3f( Vec3(   width, -aHeight, -depth), colour, nBack);
	mp_verts[10] = VertPos3fColour4ubNormal3f( Vec3(  aWidth,  -height, -depth), colour, nBack);
	mp_verts[11] = VertPos3fColour4ubNormal3f( Vec3(  -width,  aHeight, -depth), colour, nBack);

	mp_verts[12] = VertPos3fColour4ubNormal3f( Vec3(  aWidth,   height, -depth), colour, nBack);
	mp_verts[13] = VertPos3fColour4ubNormal3f( Vec3(   width,  aHeight, -depth), colour, nBack);
	mp_verts[14] = VertPos3fColour4ubNormal3f( Vec3( -aWidth,  -height, -depth), colour, nBack);
	mp_verts[15] = VertPos3fColour4ubNormal3f( Vec3(  -width, -aHeight, -depth), colour, nBack);

	mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3f)*16, mp_verts);
	
	m_vertexCount = 15;
	m_indexCount = 24;
	mp_indicies = new UINT[24];

	// Front face '\'
	mp_indicies[0] = 0;
	mp_indicies[1] = 1;
	mp_indicies[2] = 2;
	mp_indicies[3] = 0;
	mp_indicies[4] = 2;
	mp_indicies[5] = 3;
	
	// Front face '/'
	mp_indicies[6] = 4;
	mp_indicies[7] = 5;
	mp_indicies[8] = 6;
	mp_indicies[9] = 4;
	mp_indicies[10] = 6;
	mp_indicies[11] = 7;

	// Back face '\'
	mp_indicies[12] = 8;
	mp_indicies[13] = 9;
	mp_indicies[14] = 10;
	mp_indicies[15] = 8;
	mp_indicies[16] = 10;
	mp_indicies[17] = 11;

	// Back face '/'
	mp_indicies[18] = 12;
	mp_indicies[19] = 13;
	mp_indicies[20] = 14;
	mp_indicies[21] = 12;
	mp_indicies[22] = 14;
	mp_indicies[23] = 15;

	mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT)*m_indexCount, mp_indicies);
}

/**
* Helper function to compute a point on a unit circle, aligned to the x/z plane.
* SOURCE: DirectXTK
*
* @param i		:: The loop number.
* @param tess	:: The amount of tessellation in the object.
*
* @return A unit vector pointing outwards from the origin.
*/
DBE::Vec3 DBE::BasicMesh::GetCircleVector( s32 i, s32 tess) {
	float angle( i * XM_2PI / tess);
	float dx( 0.0f), dz( 0.0f);

	XMScalarSinCos( &dx, &dz, angle);

	Vec3 v( dx, 0.0f, dz);
	return v;
}

/**
* Helper function to compute a tangent to a circle... *shrug*.
* SOURCE: DirectXTK.
*
* @param i		:: The loop number.
* @param tess	:: The amount of tessellation in the object.
*
* @return A unit vector of... something...
*/
DBE::Vec3 DBE::BasicMesh::GetCircleTangent( s32 i, s32 tess) {
	float angle(( i * XM_2PI / tess) + XM_PIDIV2);
	float dx( 0.0f), dz( 0.0f);

	XMScalarSinCos( &dx, &dz, angle);

	Vec3 v( dx, 0.0f, dz);
	return v;
}

/**
* Helper function to generate a flat triangle fan cap for cones and cylinders.
* SOURCE: DirectXTK.
*
* @param verts		:: The array of verticies.
* @param vertCount	:: The current position in the 'verts' array.
* @param indicies	:: The array of indicies.
* @param indexCount	:: The current position in the 'indicies' array.
* @param tess		:: The tessellation of the mesh.
* @param height		:: The height of the cone/cylinder.
* @param radius		:: The radius of the cap.
* @param isTop		:: Used for determining the normal and texture values of the fan.
*/
void DBE::BasicMesh::CreateCylinderCap( VertPos3fColour4ubNormal3f* verts, s32& vertCount, UINT* indicies, s32& indexCount, s32 tess, float height, float radius, u32 colour, bool isTop) {
	// Create cap indicies.
	for( s32 i( 0); i < tess - 2; ++i) {
		s32 i1(( i + 1) % tess);
		s32 i2(( i + 2) % tess);

		if( isTop)
			std::swap( i1, i2);

		s32 base( vertCount + 1);
		indicies[++indexCount] = base;
		indicies[++indexCount] = base + i1;
		indicies[++indexCount] = base + i2;
	}

	Vec3 normal = g_XMIdentityR1;
	Vec4 textureScale = g_XMNegativeOneHalf;

	if( !isTop) {
		normal *= -1;
		textureScale = textureScale * g_XMNegateX;
	}

	// Create cap verticies.
	//DebugTrace( "Cylinder cap: %s\n", isTop ? "top" : "bottom");
	for( s32 i( 0); i < tess; ++i) {
		Vec3 circleVec = GetCircleVector( i, tess);

		Vec3 position = (circleVec.GetXMVector() * radius) + (normal.GetXMVector() * height);
		Vec3 pos( position);

		Vec2 textureCoord = XMVectorMultiplyAdd( XMVectorSwizzle<0, 2, 3, 3>( circleVec), textureScale, g_XMOneHalf);

		verts[++vertCount] = VertPos3fColour4ubNormal3f( pos, colour, normal.GetVector());

		//DebugTrace( "Vert %i. X: %f, Y: %f, Z: %f\n", vertCount, pos.x, pos.y, pos.z);
	}
}