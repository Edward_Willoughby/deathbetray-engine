/********************************************************************
*
*	CLASS		:: TaskBarButtonSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's 'Win7TaskBarButton'
*					functionality.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 12 / 22
*
********************************************************************/

#ifndef TaskBarButtonSampleH
#define TaskBarButtonSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>

/********************************************************************
*	Defines and constants.
********************************************************************/
/// Enumration for the different states that the task bar button can be in.
enum TBB_State {
	TBB_None = 0,
	TBB_Play,
	TBB_Pause,
	TBB_Error,
	TBB_Unknown,
};


/*******************************************************************/
class TaskBarButtonSample : public DBE::App {
	public:
		/// Constructor.
		TaskBarButtonSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		/// Gets a string that represents the current task bar state.
		char* GetStateAsString();

		const char* m_windowTitle;

		TBB_State m_TBBState;
		u32 m_completed;
		u32 m_total;

		bool m_flashOnce;
		bool m_flashed;


		/// Private copy constructor to prevent multiple instances.
		TaskBarButtonSample( const TaskBarButtonSample&);
		/// Private assignment operator to prevent multiple instances.
		TaskBarButtonSample& operator=( const TaskBarButtonSample&);

};

APP_MAIN( TaskBarButtonSample, BLUE);

/*******************************************************************/
#endif	// #ifndef TaskBarButtonSampleH