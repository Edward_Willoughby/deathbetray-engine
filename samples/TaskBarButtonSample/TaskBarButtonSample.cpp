/********************************************************************
*	Function definitions for the TaskBarButtonSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "TaskBarButtonSample.h"

#include <DBEFont.h>
#include <DBEMath.h>
#include <DBEWin7TaskBarButtonMgr.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool TaskBarButtonSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - TaskBar Button";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_Normal);

	m_TBBState = TBB_State::TBB_None;
	m_completed = 0;
	m_total = 500;

	m_flashOnce = true;
	m_flashed = false;

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool TaskBarButtonSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();
	
	// Begin progress by pressing 'Y'.
	if( MGR_INPUT().KeyPressed( 'Y'))
		m_TBBState = TBB_Play;

	// Set the progress to be complete by pressing 'U'.
	if( MGR_INPUT().KeyPressed( 'U')) {
		m_TBBState = TBB_None;
		m_completed = 0;
		MGR_TBBUTTON().ProgressDone();
	}

	// Pause the progress by pressing 'I'.
	if( MGR_INPUT().KeyPressed( 'I')) {
		m_TBBState = TBB_Pause;
		MGR_TBBUTTON().ProgressPause();
	}

	// Signal an error has occurred by pressing 'O'.
	if( MGR_INPUT().KeyPressed( 'O')) {
		m_TBBState = TBB_Error;
		MGR_TBBUTTON().ProgressError();
	}
	
	// Signal that the progress is unknown by pressing 'P'.
	if( MGR_INPUT().KeyPressed( 'P')) {
		m_TBBState = TBB_Unknown;
		MGR_TBBUTTON().ProgressIndeterminant();
	}

	// Flash the task bar button once when focus is next lost by pressing 'H'.
	if( MGR_INPUT().KeyPressed( 'H'))
		m_flashOnce = true;

	// Flash the task bar button five times when focus is next lost by pressing 'J'.
	if( MGR_INPUT().KeyPressed( 'J'))
		m_flashOnce = false;

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool TaskBarButtonSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	// 'TBB_Play' is the only one that needs to be updated; the others can just be left alone.
	if( m_TBBState == TBB_Play) {
		// If it's completed, then set the progress to be done...
		if( m_completed > m_total) {
			m_TBBState = TBB_None;
			MGR_TBBUTTON().ProgressDone();
			m_completed = 0;
		}
		// ... Otherwise, keep going.
		else {
			MGR_TBBUTTON().ProgressStart( m_completed, m_total);
			++m_completed;
		}
	}

	// If the program is not in focus then flash the specified number of times.
	if( !this->IsInFocus()) {
		if( !m_flashed) {
			if( m_flashOnce)
				MGR_TBBUTTON().Flash();
			else
				MGR_TBBUTTON().FlashCount( 5);
		}
		
		m_flashed = true;
	}
	else {
		m_flashed = false;
	}

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool TaskBarButtonSample::HandleRender( float deltaTime) {
	m_matProj = DirectX::XMMatrixOrthographicLH( 1.0f, 1.0f, 1.0f, 250.0f);
	m_matView = DirectX::XMMatrixTranslation( 0.0f, 0.0f, 2.0f);

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.300f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Begin Process   ('Y')\n");
	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "End Process     ('U')");
	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.250f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Pause Process   ('I')");
	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.225f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Error Process   ('O')");
	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.200f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Unknown Process ('P')");
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.175f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Current State:    %s", GetStateAsString());
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.150f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Current Progress: %i / %i", m_completed, m_total);

	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.100f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Flash Once: ('H')%s", m_flashOnce ? " - Active" : "");
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.075f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Flash Five: ('J')%s", m_flashOnce ? "" : " - Active");

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool TaskBarButtonSample::HandleShutdown() {

	return true;
}

/**
* Gets a string that represents the current task bar state.
*
* @return A string that represents the current task bar state.
*/
char* TaskBarButtonSample::GetStateAsString() {
	switch( m_TBBState) {
		case TBB_None:		return "None";
		case TBB_Play:		return "Playing";
		case TBB_Pause:		return "Paused";
		case TBB_Error:		return "Error";
		case TBB_Unknown:	return "Unknown";
		default:			return "BLEUGH!";
	}
}