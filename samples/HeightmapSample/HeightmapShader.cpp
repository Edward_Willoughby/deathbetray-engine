/********************************************************************
*	Function definitions for the HeightmapShader class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "HeightmapShader.h"

#include <DBEApp.h>
#include <DBEVertexTypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/


/********************************************************************
*	HeightmapVertexShader
********************************************************************/
/**
* Constructor.
*/
HeightmapVertexShader::HeightmapVertexShader() {}

/**
* Destructor.
*/
HeightmapVertexShader::~HeightmapVertexShader() {}

/**
* 
*
* @param :: 
*
* @return 
*/
bool HeightmapVertexShader::Init() {
	this->LoadShader( "shaders\\HeightmapShader.hlsl", g_vertPos3fColour4ubNormal3fTex2fMacros, g_vertPos3fColour4ubNormal3fTex2fDesc, g_vertPos3fColour4ubNormal3fTex2fSize);

	this->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_WVP",					SVT_FLOAT4x4,	&m_slotWVP);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_W",					SVT_FLOAT4x4,	&m_slotLights.m_slotWorld);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_InvXposeW",			SVT_FLOAT4x4,	&m_slotLights.m_slotInverse);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightDirections",		SVT_FLOAT4,		&m_slotLights.m_slotLightDir);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightPositions",		SVT_FLOAT4,		&m_slotLights.m_slotLightPos);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightColours",			SVT_FLOAT3,		&m_slotLights.m_slotLightColour);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightAttenuations",	SVT_FLOAT4,		&m_slotLights.m_slotLightAtten);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightSpots",			SVT_FLOAT4,		&m_slotLights.m_slotLightSpots);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_numLights",			SVT_INT,		&m_slotLights.m_slotNumLights);

	this->FindTexture( "g_materialMap", &m_slotMaterialMap);

	if( !this->CreateCBuffer( m_slotCBufferGlobalVars))
		return false;

	return true;
}


/********************************************************************
*	HeightmapPixelShader
********************************************************************/
/**
* Constructor.
*/
HeightmapPixelShader::HeightmapPixelShader() {}

/**
* Destructor.
*/
HeightmapPixelShader::~HeightmapPixelShader() {}

/**
* 
*
* @param :: 
*
* @return 
*/
bool HeightmapPixelShader::Init() {
	this->LoadShader( "shaders\\HeightmapShader.hlsl", g_vertPos3fColour4ubNormal3fTex2fMacros);

	// GlobalVars CBuffer.
	this->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_W",					SVT_FLOAT4x4,	&m_slotLights.m_slotWorld);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_InvXposeW",			SVT_FLOAT4x4,	&m_slotLights.m_slotInverse);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightDirections",		SVT_FLOAT4,		&m_slotLights.m_slotLightDir);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightPositions",		SVT_FLOAT4,		&m_slotLights.m_slotLightPos);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightColours",			SVT_FLOAT3,		&m_slotLights.m_slotLightColour);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightAttenuations",	SVT_FLOAT4,		&m_slotLights.m_slotLightAtten);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightSpots",			SVT_FLOAT4,		&m_slotLights.m_slotLightSpots);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_numLights",			SVT_INT,		&m_slotLights.m_slotNumLights);

	this->FindTexture( "g_texture0", &m_slotTexture0);
	this->FindTexture( "g_texture1", &m_slotTexture1);
	this->FindTexture( "g_texture2", &m_slotTexture2);
	this->FindSampler( "g_sampler", &m_slotSampler);

	// ShadowVars CBuffer.
	this->FindCBuffer( "ShadowVars", &m_slotCBufferShadowVars);
	if( m_slotCBufferShadowVars >= 0) {
		this->FindShaderVar( m_slotCBufferShadowVars, "g_shadowMat",	SVT_FLOAT4x4,	&m_slotShadowMat);
		this->FindShaderVar( m_slotCBufferShadowVars, "g_shadowColour",	SVT_FLOAT4,		&m_slotShadowColour);

		this->FindTexture( "g_textureShadow", &m_slotTextureShadow);
		this->FindSampler( "g_samplerShadow", &m_slotSamplerShadow);

		if( !this->CreateCBuffer( m_slotCBufferShadowVars))
			return false;
	}

	if( !this->CreateCBuffer( m_slotCBufferGlobalVars))
		return false;

	return true;
}