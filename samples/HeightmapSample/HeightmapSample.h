/********************************************************************
*
*	CLASS		:: HeightmapSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's 'Heightmap'.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 09 / 28
*
********************************************************************/

#ifndef HeightmapSampleH
#define HeightmapSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>

class Heightmap;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class HeightmapSample : public DBE::App {
	public:
		/// Constructor.
		HeightmapSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		Heightmap*	mp_heightmap;

		float	m_camRadius;
		float	m_camPhi;
		float	m_camTheta;
		bool	m_rotate;

		bool m_wireframe;

		/// Private copy constructor to prevent multiple instances.
		HeightmapSample( const HeightmapSample&);
		/// Private assignment operator to prevent multiple instances.
		HeightmapSample& operator=( const HeightmapSample&);

};

APP_MAIN( HeightmapSample, BLUE);

/*******************************************************************/
#endif	// #ifndef HeightmapSampleH