/********************************************************************
*	Function definitions for the Heightmap class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Heightmap.h"

#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEGraphicsHelpers.h>
#include <DBETextureMgr.h>
#include <DBEVertexTypes.h>

#include "HeightmapShader.h"
/********************************************************************
*	Defines, constants, namespaces and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

/// Constant for the raw colour of the heightmap's verticies.
static const VertColour gc_mapColour = HEIGHTMAP;


/**
* Constructor for the D3DHeightmap class.
*
* @param p_fileName	:: The image that the heightmap will be created from.
* @param gridSize	:: The scale of the heightmap.
*/
Heightmap::Heightmap( char* p_fileName, float gridSize) {
	/**************************/
	/********** NOTE **********/
	// For now, I'm going to force the grid size to be 2.0f because it's the only size that works
	// for some reason.
	gridSize = 2.0f;
	/********** NOTE **********/
	/**************************/

	// Initialise the array of textures.
	for( size_t i( 0); i < NUM_TEXTURE_FILES; ++i)
		mp_textures[i] = nullptr;

	mp_heightmapBuffer = nullptr;

	// Loads in the heightmap data.
	this->LoadHeightmap( p_fileName, gridSize);

	// Calculate how many verticies will be in the heightmap.
	m_vertexCount = m_length * m_width * 6;
	mp_mapVerticies = new VertPos3fColour4ubNormal3fTex2f[m_vertexCount];

	s32 vertIndex( 0), mapIndex( 0);
	Vec3 v0, v1, v2, v3;
	float tX0, tY0, tX1, tY1, tX2, tY2, tX3, tY3;

	// Create the heightmap's verticies (this is the unstripped method).
	for( s32 l( 0); l < m_length; ++l) {
		for( s32 w( 0); w < m_width; ++w) {
			if( w < m_width-1 && l < m_length-1) {
				v0 = mp_heightmap[mapIndex];
				v1 = mp_heightmap[mapIndex + m_width];
				v2 = mp_heightmap[mapIndex + 1];
				v3 = mp_heightmap[mapIndex + m_width + 1];

				Vec3 vA( v0 - v1);
				Vec3 vB( v1 - v2);
				Vec3 vC( v3 - v1);

				Vec3 vN1, vN2;
				vN1 = Cross( vA, vB);
				vN1.Normalise();

				vN2 = Cross( vB, vC);
				vN2.Normalise();

				// Spread textures evenly across landscape.
				tX0 = (v0.GetX() + 512) / 32.0f;
				tY0 = (v0.GetZ() + 512) / 32.0f;
				tX1 = (v1.GetX() + 512) / 32.0f;
				tY1 = (v1.GetZ() + 512) / 32.0f;
				tX2 = (v2.GetX() + 512) / 32.0f;
				tY2 = (v2.GetZ() + 512) / 32.0f;
				tX3 = (v3.GetX() + 512) / 32.0f;
				tY3 = (v3.GetZ() + 512) / 32.0f;
				
				mp_mapVerticies[vertIndex+0] = VertPos3fColour4ubNormal3fTex2f( v0, gc_mapColour, vN1, Vec2( tX0, tY0));
				mp_mapVerticies[vertIndex+1] = VertPos3fColour4ubNormal3fTex2f( v1, gc_mapColour, vN1, Vec2( tX1, tY1));
				mp_mapVerticies[vertIndex+2] = VertPos3fColour4ubNormal3fTex2f( v2, gc_mapColour, vN1, Vec2( tX2, tY2));
				mp_mapVerticies[vertIndex+3] = VertPos3fColour4ubNormal3fTex2f( v2, gc_mapColour, vN2, Vec2( tX2, tY2));
				mp_mapVerticies[vertIndex+4] = VertPos3fColour4ubNormal3fTex2f( v1, gc_mapColour, vN2, Vec2( tX1, tY1));
				mp_mapVerticies[vertIndex+5] = VertPos3fColour4ubNormal3fTex2f( v3, gc_mapColour, vN2, Vec2( tX3, tY3));

				vertIndex += 6;
			}

			++mapIndex;
		}
	}

	// Create the buffer.
	mp_heightmapBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3fTex2f)*m_vertexCount, mp_mapVerticies);

	// Load in the textures.
	ID3D11SamplerState* p_sampler( GET_APP()->GetSamplerState( true, true, true));
	for( size_t i( 0); i < NUM_TEXTURE_FILES; ++i) {
		char buffer[256];
		buffer[0] = '\0';

		strcat_s( buffer, sizeof( buffer), g_texturePath);
		strcat_s( buffer, sizeof( buffer), g_textureFileNames[i]);

		mp_textures[i] = MGR_TEXTURE().LoadTexture( buffer, p_sampler);
	}

	// Load the heightmap's shader.
	this->LoadShader();
}

/**
* Destructor for the D3DHeightmap.
*/
Heightmap::~Heightmap() {
	ReleaseCOM( mp_heightmapBuffer);
	SafeDeleteArray( mp_mapVerticies);

	for( u8 i( 0); i < NUM_TEXTURE_FILES; ++i)
		MGR_TEXTURE().DeleteTexture( mp_textures[i]);

	MGR_SHADER().RemoveShader<HeightmapVertexShader>( mp_VS);
	MGR_SHADER().RemoveShader<HeightmapPixelShader>( mp_PS);
}

/**
* Renders the heightmap.
*/
void Heightmap::OnRender( float deltaTime) {
	DBE_Assert( mp_heightmapBuffer != nullptr && mp_VS != nullptr && mp_PS != nullptr);

	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	D3D11_MAPPED_SUBRESOURCE vsMap, psMap;
	if( !mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &vsMap)))
		vsMap.pData = nullptr;
	if( !mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &psMap)))
		psMap.pData = nullptr;

	// Send the WVP matrix to the vertex shader.
	Matrix4 wvp = GET_APP()->GetWVP();
	mp_VS->SetCBufferMatrix( vsMap, mp_VS->m_slotWVP, wvp);

	GET_APP()->PassLightsToShader( mp_VS, vsMap, &mp_VS->m_slotLights, GET_APP()->m_matWorld);
	GET_APP()->PassLightsToShader( mp_PS, psMap, &mp_PS->m_slotLights, GET_APP()->m_matWorld);

	// Do... something to the shader maps.
	if( vsMap.pData)
		p_context->Unmap( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars], 0);
	if( psMap.pData)
		p_context->Unmap( mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars], 0);

	// Set the constant buffers for the vertex and pixel shaders.
	if( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_VS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
	if( mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars] };
		p_context->PSSetConstantBuffers( mp_PS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}

	if( mp_VS->m_slotMaterialMap >= 0)
		p_context->VSSetShaderResources( mp_VS->m_slotMaterialMap, 1, &mp_textures[3]->mp_textureView);

	if( mp_PS->m_slotTexture0 >= 0)
		p_context->PSSetShaderResources( mp_PS->m_slotTexture0, 1, &mp_textures[0]->mp_textureView);

	if( mp_PS->m_slotTexture1 >= 0)
		p_context->PSSetShaderResources( mp_PS->m_slotTexture1, 1, &mp_textures[1]->mp_textureView);

	if( mp_PS->m_slotTexture2 >= 0)
		p_context->PSSetShaderResources( mp_PS->m_slotTexture2, 1, &mp_textures[2]->mp_textureView);

	p_context->VSSetShader( mp_VS->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_PS->mp_PS, nullptr, 0);

	ID3D11SamplerState* apTextureSampler[] = { mp_textures[0]->mp_samplerState };
	p_context->VSSetSamplers( mp_PS->m_slotSampler, 1, apTextureSampler);
	p_context->PSSetSamplers( mp_PS->m_slotSampler, 1, apTextureSampler);

	p_context->IASetInputLayout( mp_VS->mp_IL);
	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { mp_heightmapBuffer };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubNormal3fTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->Draw( m_vertexCount, 0);
}

/**
* Loads the heightmap data from the image.
* SOURCE :: rastertek.com
*
* @param p_fileName	:: The image that the heightmap will be created from.
* @param gridSize	:: The scale of the heightmap.
*
* @return True if the data was loaded successfully.
*/
bool Heightmap::LoadHeightmap( char* p_fileName, float gridSize) {
	DBE_Assert( p_fileName != nullptr);

	FILE* p_file;
	int error;
	unsigned int count;
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	int imageSize, i, j, k, index;
	unsigned char* bitmapImage;
	unsigned char height;

	// open the heightmap file in binary
	error = fopen_s( &p_file, p_fileName, "rb");
	if( error != 0)
		return false;

	// read in the file header
	count = fread( &bitmapFileHeader, sizeof( BITMAPFILEHEADER), 1, p_file);
	if( count != 1)
		return false;

	// read in the bitmap info header
	count = fread( &bitmapInfoHeader, sizeof( BITMAPINFOHEADER), 1, p_file);
	if( count != 1)
		return false;

	// save the dimensions of the terrain
	m_width = bitmapInfoHeader.biWidth;
	m_length = bitmapInfoHeader.biHeight;

	// calculate the size of the bitmap image data
	imageSize = m_width * m_length * 3;

	// allocate memory for the bitmap image data
	bitmapImage = new unsigned char[imageSize];
	if( !bitmapImage)
		return false;

	// move to the beginning of the bitmap data
	fseek( p_file, bitmapFileHeader.bfOffBits, SEEK_SET);

	// read in the bitmap image data
	count = fread( bitmapImage, 1, imageSize, p_file);
	if( count != imageSize)
		return false;

	// close the file
	error = fclose( p_file);
	if( error != 0)
		return false;

	// create the structure to hold the height map data
	Vec3* p_unsmoothedMap = new Vec3[m_width * m_length];
	mp_heightmap = new Vec3[m_width * m_length];

	if( !mp_heightmap)
		return false;

	// initialise the position in the image data buffer
	k = 0;

	// read the image data into the height map
	for( j = 0; j < m_length; ++j) {
		for( i = 0; i < m_width; ++i) {
			height = bitmapImage[k];

			index = (m_width*j) + i;

			mp_heightmap[index].x = (float)(i - (m_width/2)) * gridSize;
			mp_heightmap[index].y = (float)height / 6 * gridSize;
			mp_heightmap[index].z = (float)(j - (m_length/2)) * gridSize;

			p_unsmoothedMap[index].y = (float)height / 6 * gridSize;

			k += 3;
		}
	}

	// smoothing the landscape makes a big difference to the look of the shading
	for( int s( 0); s < 2; ++s) {
		for( j = 0; j < m_length; ++j) {
			index = (m_width * j) + i;

			if(( j > 0) && (j < m_length-1) && (i > 0) && (i < m_width-1)) {
				mp_heightmap[index].y = 0.0f;
				mp_heightmap[index].y += p_unsmoothedMap[index-m_width-1].y + p_unsmoothedMap[index-m_width].y + p_unsmoothedMap[index-m_width+1].y;
				mp_heightmap[index].y += p_unsmoothedMap[index-1].y + p_unsmoothedMap[index].y + p_unsmoothedMap[index+1].y;
				mp_heightmap[index].y += p_unsmoothedMap[index+m_width-1].y + p_unsmoothedMap[index+m_width].y + p_unsmoothedMap[index-m_width+1].y;
				mp_heightmap[index].y /= 9;
			}
		}

		for( j = 0; j < m_length; ++j) {
			for( i = 0; i < m_width; ++i) {
				index = (m_width * j) + i;
				p_unsmoothedMap[index].y = mp_heightmap[index].y;
			}
		}
	}

	// release the bitmap image data
	SafeDeleteArray( bitmapImage);
	SafeDeleteArray( p_unsmoothedMap);
	bitmapImage = 0;

	return true;
}

/**
* Loads the heightmap's shader.
*
* @return True if the shader was loaded correctly.
*/
bool Heightmap::LoadShader() {
	if( mp_VS != nullptr || mp_PS != nullptr)
		return false;

	if( !MGR_SHADER().GetShader<HeightmapVertexShader>( gsc_heightmapVertexShaderName, mp_VS)) {
		mp_VS = new HeightmapVertexShader();
		mp_VS->Init();
		MGR_SHADER().AddShader<HeightmapVertexShader>( gsc_heightmapVertexShaderName, mp_VS);
	}
	
	if( !MGR_SHADER().GetShader<HeightmapPixelShader>( gsc_heightmapPixelShaderName, mp_PS)) {
		mp_PS = new HeightmapPixelShader();
		mp_PS->Init();
		MGR_SHADER().AddShader<HeightmapPixelShader>( gsc_heightmapPixelShaderName, mp_PS);
	}

	return true;
}

////// FUNCTION :- D3DHeightmap::GetFaceNormal
//DBE::Vec3 D3DHeightmap::GetFaceNormal( int faceIndex, int offset) {
//	assert( true);
//
//	// last row of verticies will map off the end of the face list
//	if( faceIndex >= m_faceCount)
//		return Vec3( 0.0f, 1.0f, 0.0f);
//
//	assert( faceIndex >= 0 && faceIndex < m_faceCount);
//
//	int newIndex( faceIndex + offset),
//		oldRow( faceIndex / m_facesPerRow),
//		newRow( newIndex / m_facesPerRow);
//
//	if( newIndex < 0 || oldRow != newRow || newIndex > m_faceCount)
//		return mp_faceNormals[faceIndex];
//
//	return mp_faceNormals[newIndex];
//}

////// FUNCTION :- D3DHeightmap::GetAveragedVertexNormal
//DDBE::Vec3 D3DHeightmap::GetAveragedVertexNormal( int index, int row) {
//	assert( index >= 0 && index < m_vertexCount);
//
//	int faceIndex(( index-row)*2);
//
//	Vec3 average =	this->GetFaceNormal( faceIndex,  0) +
//					this->GetFaceNormal( faceIndex, -1) +
//					this->GetFaceNormal( faceIndex, -2) +
//					this->GetFaceNormal( faceIndex, -m_facesPerRow-1) +
//					this->GetFaceNormal( faceIndex, -m_facesPerRow) +
//					this->GetFaceNormal( faceIndex, -m_facesPerRow+1);
//
//	average /= 6;
//
//	average.Normalise();
//
//	return average;
//}