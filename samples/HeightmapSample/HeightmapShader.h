/********************************************************************
*
*	CLASS		:: HeightmapShader
*	DESCRIPTION	:: The vertex and pixel shader for the heightmap.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 21
*
********************************************************************/

#ifndef HeightmapShaderH
#define HeightmapShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEPixelShader.h>
#include <DBEVertexShader.h>

/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the heightmap's vertex shader so it can be found in the shader manager.
static const char* gsc_heightmapVertexShaderName = "HeightmapVertexShader";
/// The unique name for the heightmap's pixel shader so it can be found in the shader manager.
static const char* gsc_heightmapPixelShaderName = "HeightmapPixelShader";


/*******************************************************************/
class HeightmapVertexShader : public DBE::VertexShader {
	public:
		/// Constructor.
		HeightmapVertexShader();
		/// Destructor.
		~HeightmapVertexShader();

		bool Init();
		
		s32 m_slotCBufferGlobalVars;

		s32 m_slotWVP;
		ShaderLightingSlots m_slotLights;
		
		s32 m_slotMaterialMap;

	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		HeightmapVertexShader( const HeightmapVertexShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		HeightmapVertexShader& operator=( const HeightmapVertexShader& other);
		
};

/*******************************************************************/
class HeightmapPixelShader : public DBE::PixelShader {
	public:
		/// Constructor.
		HeightmapPixelShader();
		/// Destructor.
		~HeightmapPixelShader();
		
		bool Init();

		s32 m_slotCBufferGlobalVars;
		ShaderLightingSlots m_slotLights;
		
		s32 m_slotTexture0;
		s32 m_slotTexture1;
		s32 m_slotTexture2;
		s32 m_slotSampler;

		// Shadow slots.
		s32 m_slotCBufferShadowVars;
		s32 m_slotShadowMat;
		s32 m_slotShadowColour;

		s32 m_slotTextureShadow;
		s32 m_slotSamplerShadow;
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		HeightmapPixelShader( const HeightmapPixelShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		HeightmapPixelShader& operator=( const HeightmapPixelShader& other);
		
};

/*******************************************************************/
#endif	// #ifndef HeightmapShaderH
