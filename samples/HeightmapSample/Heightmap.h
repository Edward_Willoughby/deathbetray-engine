/********************************************************************
*
*	CLASS		:: Heightmap
*	DESCRIPTION	:: Creates a 3D model from an image.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2013 / 08 / 18
*
********************************************************************/

#ifndef HeightmapH
#define HeightmapH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEMath.h>
#include <DBERenderable.h>
#include <DBEShader.h>
#include <DBEUtilities.h>

namespace DBE {
	struct Texture;
}
class HeightmapPixelShader;
class HeightmapVertexShader;
struct ID3D11Buffer;
struct VertPos3fColour4ubNormal3fTex2f;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// Variable for holding the path to the texture files.
static char g_texturePath[256] = "res\\";
/// Array for holding the file names of the textures for the heightmap.
static char* const g_textureFileNames[] = {
	"moss.dds",
	"grass.dds",
	"asphalt.dds",
	"materialmap.dds",
};

/// Constant for how many textures are used by the heightmap.
static const size_t NUM_TEXTURE_FILES = sizeof( g_textureFileNames) / sizeof( g_textureFileNames[0]);


/*******************************************************************/
class Heightmap : public DBE::Renderable {
	public:
		/// Constructor.
		Heightmap( char* p_fileName, float gridSize);
		/// Destructor.
		~Heightmap();

		/// Renders the heightmap.
		void OnRender( float deltaTime);

		void SetShadowVars( ID3D11ShaderResourceView* p_texture, ID3D11SamplerState* p_sampler, DBE::Matrix4* p_shadowMat, DBE::Vec4* p_shadowColour);

	private:
		/// Loads in the heightmap data from the given file.
		bool LoadHeightmap( char* p_fileName, float gridSize);
		/// Loads the shader.
		bool LoadShader();

		/// Calculates a face's normal.
		//DBE::Vec3 GetFaceNormal( int faceIndex, int offset);
		/// Calculates the average normal of a group of verticies.
		//DBE::Vec3 GetAveragedVertexNormal( int index, int row);

		ID3D11Buffer* mp_heightmapBuffer;

		s32			m_width;
		s32			m_length;
		s32			m_vertexCount;
		s32			m_faceCount;
		s32			m_facesPerRow;
		DBE::Vec3*	mp_heightmap;
		DBE::Vec3*	mp_faceNormals;
		DBE::Vec3*	mp_normalMap;

		VertPos3fColour4ubNormal3fTex2f* mp_mapVerticies;

		HeightmapVertexShader*	mp_VS;
		HeightmapPixelShader*	mp_PS;

		DBE::Texture* mp_textures[NUM_TEXTURE_FILES];
		
		/// Private copy constructor to prevent multiple instances.
		Heightmap( const Heightmap&);
		/// Private assignment operator to prevent multiple instances.
		Heightmap& operator=( const Heightmap&);

};

/*******************************************************************/
#endif	// #ifndef HeightmapH