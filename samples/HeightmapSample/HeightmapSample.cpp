/********************************************************************
*	Function definitions for the HeightmapSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "HeightmapSample.h"

#include <DBEMath.h>

#include "Heightmap.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool HeightmapSample::HandleInit() {
	m_camRadius	= 1000.0f;
	m_camTheta	= DBE_Pi * 1.5f;
	m_camPhi	= DBE_Pi * 0.25f;
	m_rotate	= true;

	mp_heightmap = nullptr;
	mp_heightmap = new Heightmap( "res/heightmap.bmp", 2.0f);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	m_wireframe = false;

	m_windowTitle = "DeathbetrayEngine Sample - Heightmap";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool HeightmapSample::HandleInput() {
	// Toggle wireframe mode.
	if( MGR_INPUT().KeyPressed( 'W'))
		m_wireframe = !m_wireframe;

	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	// Toggle the orbital rotation of the camera.
	if( MGR_INPUT().KeyPressed( 'R'))
		m_rotate = !m_rotate;

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool HeightmapSample::HandleUpdate( float deltaTime) {
	// Rotate the camera.
	if( m_rotate)
		m_camTheta += deltaTime / 2;

	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool HeightmapSample::HandleRender( float deltaTime) {
	float x( m_camRadius * sin( m_camPhi) * cos( m_camTheta)),
		  y( m_camRadius * cos( m_camPhi)),
		  z( m_camRadius * sin( m_camPhi) * sin( m_camTheta));

	Vec3 pos(		x, y, z),
		 target(	0.0f, 0.0f, 0.0f),
		 up(		0.0f, 1.0f, 0.0f);

	float fov( 0.25f * (float)DBE_Pi);
	float aspectRatio( this->GetAspectRatio());
	m_matProj = DirectX::XMMatrixPerspectiveFovLH( fov, aspectRatio, 1.5f, 5000.0f);
	m_matView = DirectX::XMMatrixLookAtLH( pos.GetXMVector(), target.GetXMVector(), up.GetXMVector());

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, m_wireframe);

	m_matWorld = mp_heightmap->GetWorldMatrix();
	mp_heightmap->Render( deltaTime);

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool HeightmapSample::HandleShutdown() {
	SafeDelete( mp_heightmap);

	return true;
}