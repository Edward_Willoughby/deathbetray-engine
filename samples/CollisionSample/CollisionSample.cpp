/********************************************************************
*	Function definitions for the CollisionSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "CollisionSample.h"

#include <DBECollision.h>
#include <DBEFont.h>
#include <DBEMath.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool CollisionSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - Collision";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_Normal);

	mp_sphereStatic = new BasicMesh( MeshType::MT_Sphere, 1.0f, 10.0f, 10.0f, WHITE);
	mp_sphereStatic->MoveTo( 0.0f, 0.0f, 0.0f);

	mp_sphereMobile = new BasicMesh( MeshType::MT_Sphere, 1.0f, 10.0f, 10.0f, WHITE);
	mp_sphereMobile->MoveTo( 0.0f, 0.0f, 5.0f);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool CollisionSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	static float mov = 0.1f;
	if( MGR_INPUT().KeyHeld( 'A'))
		mp_sphereMobile->MoveBy( 0.0f, 0.0f, -mov);
	if( MGR_INPUT().KeyHeld( 'D'))
		mp_sphereMobile->MoveBy( 0.0f, 0.0f, mov);

	if( MGR_INPUT().KeyHeld( 'W'))
		mp_sphereMobile->ScaleBy( Vec3( mov));
	if( MGR_INPUT().KeyHeld( 'S'))
		mp_sphereMobile->ScaleBy( Vec3( -mov));
	
	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool CollisionSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	mp_sphereStatic->Update( deltaTime);
	mp_sphereMobile->Update( deltaTime);

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool CollisionSample::HandleRender( float deltaTime) {
	// Perspective stuff.
	m_camera.ApplyPerspectiveMatrixLH( 1.0f, 5000.0f);
	GET_APP()->m_matView = m_camera.ApplyViewMatrixLH();

	mp_sphereStatic->Render( deltaTime);
	mp_sphereMobile->Render( deltaTime);

	// Orthographic stuff.
	m_camera.ApplyOrthoMatrixLH( 1.0f, 250.0f);

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	//DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.300f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Begin Thread ('T')");
	float dist( 0.0f);
	bool colliding( Collision::IntersectSphereSphere( mp_sphereStatic->GetBoundingSphere(), mp_sphereMobile->GetBoundingSphere(), &dist));
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Colliding: %s", colliding ? "True" : "False");
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.200f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Distance: %f", dist);

	//DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.150f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Create 'One of Many' Threads ('Y')");
	//DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.125f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Number of Threads: %d", MGR_THREADPOOL().GetThreadCount());

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool CollisionSample::HandleShutdown() {
	SafeDelete( mp_sphereStatic);
	SafeDelete( mp_sphereMobile);

	return true;
}