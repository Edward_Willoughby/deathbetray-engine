/********************************************************************
*
*	CLASS		:: CollisionSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's collision detection.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 18
*
********************************************************************/

#ifndef CollisionSampleH
#define CollisionSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEBasicMesh.h>
#include <DBECamera.h>
#include <DBEColour.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class CollisionSample : public DBE::App {
	public:
		/// Constructor.
		CollisionSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		DBE::Camera m_camera;

		DBE::BasicMesh* mp_sphereStatic;
		DBE::BasicMesh* mp_sphereMobile;


		/// Private copy constructor to prevent multiple instances.
		CollisionSample( const CollisionSample&);
		/// Private assignment operator to prevent multiple instances.
		CollisionSample& operator=( const CollisionSample&);

};

APP_MAIN( CollisionSample, BLUE);

/*******************************************************************/
#endif	// #ifndef CollisionSampleH