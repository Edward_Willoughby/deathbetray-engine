/********************************************************************
*	Function definitions for the MovableSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "MovableSample.h"

#include <DBEBasicMesh.h>
#include <DBECamera.h>
#include <DBEFont.h>
#include <DBEMath.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool MovableSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - Movable";
	this->SetWindowTitle( m_windowTitle);

	mp_cam = new Camera();
	mp_cam->m_position = Vec3( 10.0f, 10.0f, -10.0f);

	mp_cubeLateral = new BasicMesh( MeshType::MT_Cube);
	mp_cubeCircle = new BasicMesh( MeshType::MT_Cube);

	mp_cubeLateral->SetXVelocity( 0.1f);
	m_circleTheta = 0.0f;

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool MovableSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool MovableSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	// Update the cubes' positions.
	mp_cubeLateral->MoveBy( mp_cubeLateral->GetVelocity());
	mp_cubeLateral->Update( deltaTime);
	mp_cubeCircle->Update( deltaTime);

	const static float cs_lateralMax = 5.0f;
	const static float cs_lateralMin = -5.0f;

	// Moving in the +X.
	if( mp_cubeLateral->GetVelocity().GetX() > 0.0f) {
		if( mp_cubeLateral->GetPosition().GetX() >= cs_lateralMax)
			mp_cubeLateral->SetXVelocity( mp_cubeLateral->GetVelocity().GetX() * -1.0f);
	}
	// Moving in the -X.
	else {
		if( mp_cubeLateral->GetPosition().GetX() <= cs_lateralMin)
			mp_cubeLateral->SetXVelocity( mp_cubeLateral->GetVelocity().GetX() * -1.0f);
	}

	float posZ = DBE_Cos( m_circleTheta) * 2.5f;
	float posY = DBE_Sin( m_circleTheta) * 2.5f + 2.5f;
	mp_cubeCircle->MoveTo( 0.0f, posY, posZ);

	m_circleTheta += 0.1f;

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool MovableSample::HandleRender( float deltaTime) {
	// Perspective.
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 100.0f);
	mp_cam->ApplyViewMatrixLH();

	mp_cubeLateral->Render( deltaTime);
	mp_cubeCircle->Render( deltaTime);

	// Orthographic.
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 10.0f);

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool MovableSample::HandleShutdown() {
	SafeDelete( mp_cam);

	SafeDelete( mp_cubeLateral);
	SafeDelete( mp_cubeCircle);

	return true;
}