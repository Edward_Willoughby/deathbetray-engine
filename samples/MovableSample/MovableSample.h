/********************************************************************
*
*	CLASS		:: MovableSample
*	DESCRIPTION	:: Sample application to demonstate the DeathbetrayEngine's 'Movable' objects.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 01
*
********************************************************************/

#ifndef MovableSampleH
#define MovableSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>

namespace DBE {
	class BasicMesh;
	class Camera;
}
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class MovableSample : public DBE::App {
	public:
		/// Constructor.
		MovableSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		DBE::Camera* mp_cam;

		DBE::BasicMesh* mp_floor;
		DBE::BasicMesh*	mp_cubeLateral;
		DBE::BasicMesh* mp_cubeCircle;

		float m_circleTheta;


		/// Private copy constructor to prevent multiple instances.
		MovableSample( const MovableSample&);
		/// Private assignment operator to prevent multiple instances.
		MovableSample& operator=( const MovableSample&);

};

APP_MAIN( MovableSample, BLUE);

/*******************************************************************/
#endif	// #ifndef MovableSampleH