/********************************************************************
*	Function definitions for the ParticleSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "ParticleSample.h"

#include <DBEBasicMesh.h>
#include <DBECamera.h>
#include <DBEFont.h>
#include <DBEMath.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool ParticleSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - Particles";
	this->SetWindowTitle( m_windowTitle);

	mp_cam = new Camera();

	mp_stars = new Emitter<StarEmitter>;
	if( !mp_stars->Init( 200, 10.0f))
		return false;
	mp_cam->AttachToObject( mp_stars);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool ParticleSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	if( MGR_INPUT().KeyPressed( 'S'))
		mp_stars->AddParticle( 10);

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool ParticleSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	mp_stars->Update( deltaTime);
	mp_stars->BillboardParticles( mp_cam->m_position);

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool ParticleSample::HandleRender( float deltaTime) {
	// Perspective.
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 100.0f);
	mp_cam->ApplyViewMatrixLH();
	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	mp_stars->Render( deltaTime);

	// Orthographic.
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 10.0f);
	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	DEFAULT_FONT()->DrawStringf2D( Vec3( -0.48f, 0.300f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Particle Count: %i", mp_stars->GetNumberOfParticles());
	DEFAULT_FONT()->DrawString2D( Vec3( -0.48f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Particle Surge (S)");

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool ParticleSample::HandleShutdown() {
	SafeDelete( mp_cam);
	SafeDelete( mp_stars);

	return true;
}