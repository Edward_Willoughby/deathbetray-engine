/********************************************************************
*	Function definitions for the StarEmitter class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "StarEmitter.h"

#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEGraphicsHelpers.h>
#include <DBEPixelShader.h>
#include <DBERandom.h>
#include <DBEShader.h>
#include <DBEUITexture.h>
#include <DBEVertexShader.h>
#include <DBEVertexTypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

StarParticleMesh* StarEmitter::mp_mesh = nullptr;

/// File name of the star texture.
static const char* gsc_starTextureName( "res/star.dds");

/// How long particles live for before dying.
static float gs_timeToDie( 3.0f);


/********************************************************************
*	StarParticle
********************************************************************/
/**
* Constructor.
*/
StarParticleMesh::StarParticleMesh() {
	this->SetTexture( gsc_starTextureName, GET_APP()->GetSamplerState( true));

	if( !MGR_SHADER().GetShader<StarParticleVertexShader>( gsc_starParticleVertexShaderName, mp_starVS)) {
		mp_starVS = new StarParticleVertexShader();
		mp_starVS->Init();

		MGR_SHADER().AddShader<StarParticleVertexShader>( gsc_starParticleVertexShaderName, mp_starVS);
	}
	if( !MGR_SHADER().GetShader<StarParticlePixelShader>( gsc_starParticlePixelShaderName, mp_starPS)) {
		mp_starPS = new StarParticlePixelShader();
		mp_starPS->Init();

		MGR_SHADER().AddShader<StarParticlePixelShader>( gsc_starParticlePixelShaderName, mp_starPS);
	}
}

/**
* Destructor.
*/
StarParticleMesh::~StarParticleMesh() {
	MGR_SHADER().RemoveShader<StarParticleVertexShader>( mp_starVS);
	MGR_SHADER().RemoveShader<StarParticlePixelShader>( mp_starPS);
}

/**
* Handles some of the rendering that all subsequent particles will need.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void StarParticleMesh::PreRender( float deltaTime) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	mp_starPS->PassTextureAndSampler( mp_texture->mp_textureView, mp_texture->mp_samplerState);

	// Set up the vertex and pixel shaders.
	p_context->VSSetShader( mp_starVS->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_starPS->mp_PS, nullptr, 0);

	p_context->IASetInputLayout( mp_starVS->mp_IL);
	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { this->GetVertexBuffer() };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( this->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);
}

/**
* Renders the particle (call 'Render').
*
* @param deltaTime :: The time taken to render the last frame.
*/
void StarParticleMesh::OnRender( float deltaTime) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	mp_starVS->PassVarsToCBuffer( &GET_APP()->GetWVP(), mp_texture->m_textureHeight, mp_texture->m_textureWidth);
	mp_starPS->PassVarsToCBuffer( this->GetColour());

	// Set the constant buffers for the vertex and pixel shaders.
	if( mp_starVS->mp_CBuffers[mp_starVS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_starVS->mp_CBuffers[mp_starVS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_starVS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
	if( mp_starPS->mp_CBuffers[mp_starPS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_starPS->mp_CBuffers[mp_starPS->m_slotCBufferGlobalVars] };
		p_context->PSSetConstantBuffers( mp_starPS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}

	p_context->DrawIndexed( this->GetIndexCount(), 0, 0);
}

/********************************************************************
*	StarEmitter
********************************************************************/
/**
* Initialise the particles.
*
* @return True if the particles were initialised correctly.
*/
bool StarEmitter::ParticlesInit() {
	if( mp_mesh == nullptr)
		mp_mesh = new StarParticleMesh();

	if( mp_mesh == nullptr)
		return false;

	return true;
}

/**
* Deletes the particles.
*/
void StarEmitter::ParticlesShutdown() {
	SafeDelete( mp_mesh);
}

/**
* Creates the particles. This is useful for when you want additional properties per particle; you
* can create an array of objects that derive from 'Particle'.
*
* @param particles		:: The pointer to hold the array of particles.
* @param maxParticles	:: The maximum number of particles that the emitter will hold.
*/
void StarEmitter::ParticlesCreateArray( DBE::Particle** particles, s32 maxParticles) {
	for( s32 i( 0); i < maxParticles; ++i)
		particles[i] = new Particle();
}

/**
* Event handler for when a particle spawns. This is called immediately after a particle has been
* created.
*
* @param particle :: The particle that just spawned.
*/
void StarEmitter::ParticleSpawn( DBE::Particle* particle) {
	particle->MoveTo( 0.0f, 0.0f, 0.0f);
	particle->RotateTo( 0.0f, 0.0f, 0.0f);
	particle->ScaleTo( Random::Float( 0.5f, 2.5f));

	particle->SetVelocity( StarEmitter::RandomVelocity(), StarEmitter::RandomVelocity(), StarEmitter::RandomVelocity());

	particle->SetColour( WHITE);
}

/**
* Update a particle.
*
* @param deltaTime	:: The time taken to render the last frame.
* @param particle	:: The particle to update.
*/
void StarEmitter::ParticleUpdate( float deltaTime, DBE::Particle* particle) {
	particle->m_age += deltaTime;
	
	Vec3 vel( particle->GetVelocity());
	particle->MoveBy( vel.GetX() * (deltaTime*10), vel.GetY() * (deltaTime*10), vel.GetZ() * (deltaTime*10));

	// If it's past middle-aged then begin fading away.
	float timeToFade( gs_timeToDie / 3);
	if( particle->m_age >= (gs_timeToDie - timeToFade)) {
		u32 colour( particle->GetColour());
		u8 c[4];
		ColourToInts( c[0], c[1], c[2], c[3], colour);

		float energy( particle->m_age - (gs_timeToDie - timeToFade));
		c[3] = (u8)(Lerp( 1.0f, 0.0f, energy / timeToFade) * 255.0f);

		particle->SetColour( NumbersToColour( c[0], c[1], c[2], c[3]));
	}

	particle->Update( deltaTime);
}

/**
* Pre-rendering that all particles will require. This isn't necessary, but it will speed up
* rendering times.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void StarEmitter::ParticlePreRender( float deltaTime) {
	GET_APP()->SetBlendState( true);
	GET_APP()->SetDepthStencilState( true, false);
	GET_APP()->SetRasteriserState( true, false);

	mp_mesh->PreRender( deltaTime);
}

/**
* Render a particle.
*
* @param deltaTime		:: The time taken to render the last frame.
* @param particle		:: The particle to render.
* @param parentWorld	:: The particle's parent matrix (i.e. the emitter's world matrix).
*/
void StarEmitter::ParticleRender( float deltaTime, DBE::Particle* particle, const DirectX::XMMATRIX& parentWorld) {
	mp_mesh->MoveTo( particle->GetPosition());
	mp_mesh->RotateTo( particle->m_billBoardRotation + particle->GetRotation());
	mp_mesh->ScaleTo( particle->GetScale());
	mp_mesh->SetVelocity( particle->GetVelocity());

	mp_mesh->SetColour( particle->GetColour());

	mp_mesh->Render( deltaTime, parentWorld);
}

/**
* Test if a particle has expired.
*
* @param particle :: The particle to test.
*
* @return True if the particle should be deleted.
*/
bool StarEmitter::ParticleIsDead( DBE::Particle* particle) {
	if( particle->m_age >= gs_timeToDie)
		return true;

	return false;
}

/**
* Event handler for when a particle dies.
*
* @param particle :: The particle to kill.
*/
void StarEmitter::ParticleDeath( DBE::Particle* particle) {
	particle->m_age = 0;
}

/**
* Create a random velocity value.
*
* @return A random velocity value.
*/
float StarEmitter::RandomVelocity() {
	float v( 1.0f);

	float res( Random::Float( 0.0f, v) - (v / 2));

	return res / 5.0f;
}