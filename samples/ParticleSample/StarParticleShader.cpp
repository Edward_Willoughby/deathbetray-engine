/********************************************************************
*	Function definitions for the StarParticleVertexShader and StarParticlePixelShader classes.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "StarParticleShader.h"

#include <DBEApp.h>
#include <DBEMath.h>
#include <DBEUtilities.h>
#include <DBEVertexTypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

/// The file name where the shaders are.
static const char* gsc_shaderPath = "res/StarParticleShader.hlsl";


/********************************************************************
*	HeightmapVertexShader
********************************************************************/
/**
* Constructor.
*/
StarParticleVertexShader::StarParticleVertexShader() {}

/**
* Destructor.
*/
StarParticleVertexShader::~StarParticleVertexShader() {}

/**
* 
*
* @param :: 
*
* @return 
*/
bool StarParticleVertexShader::Init() {
	this->LoadShader( gsc_shaderPath, g_vertPos3fColour4ubTex2fMacros, g_vertPos3fColour4ubTex2fDesc, g_vertPos3fColour4ubTex2fSize);

	this->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_WVP",			SVT_FLOAT4x4,	&m_slotWVP);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_imageHeight",	SVT_FLOAT,		&m_slotImageHeight);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_imageWidth",	SVT_FLOAT,		&m_slotImageWidth);

	if( !this->CreateCBuffer( m_slotCBufferGlobalVars))
		return false;

	return true;
}

bool StarParticleVertexShader::PassVarsToCBuffer( DBE::Matrix4* wvp, s32 imageHeight, s32 imageWidth) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);
	D3D11_MAPPED_SUBRESOURCE map;

	if( mp_CBuffers[m_slotCBufferGlobalVars]) {
		if( FAILED( p_context->Map( mp_CBuffers[m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &map)))
			map.pData = nullptr;

		this->SetCBufferMatrix( map, m_slotWVP, *wvp);

		this->SetCBufferVar( map, m_slotImageHeight, imageHeight);
		this->SetCBufferVar( map, m_slotImageWidth, imageWidth);

		if( map.pData)
			p_context->Unmap( mp_CBuffers[m_slotCBufferGlobalVars], 0);
	}

	return true;
}


/********************************************************************
*	HeightmapPixelShader
********************************************************************/
/**
* Constructor.
*/
StarParticlePixelShader::StarParticlePixelShader() {}

/**
* Destructor.
*/
StarParticlePixelShader::~StarParticlePixelShader() {}

/**
* 
*
* @param :: 
*
* @return 
*/
bool StarParticlePixelShader::Init() {
	this->LoadShader( gsc_shaderPath, g_vertPos3fColour4ubTex2fMacros);

	this->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_colour",		SVT_FLOAT4,		&m_slotColour);

	this->FindTexture( "g_texture", &m_slotTexture);
	this->FindSampler( "g_sampler", &m_slotSampler);

	if( !this->CreateCBuffer( m_slotCBufferGlobalVars))
		return false;

	return true;
}

bool StarParticlePixelShader::PassVarsToCBuffer( u32 colour) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);
	D3D11_MAPPED_SUBRESOURCE map;

	if( mp_CBuffers[m_slotCBufferGlobalVars]) {
		if( FAILED( p_context->Map( mp_CBuffers[m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &map)))
			map.pData = nullptr;

		float c[4];
		ColourToFloats( c[0], c[1], c[2], c[3], colour);
		Vec4 vColour( c[0], c[1], c[2], c[3]);
		this->SetCBufferVar( map, m_slotColour, vColour);

		if( map.pData)
			p_context->Unmap( mp_CBuffers[m_slotCBufferGlobalVars], 0);
	}

	return true;
}

bool StarParticlePixelShader::PassTextureAndSampler( ID3D11ShaderResourceView* p_texture, ID3D11SamplerState* p_sampler) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	// Set the texture.
	if( m_slotTexture >= 0)
		p_context->PSSetShaderResources( m_slotTexture, 1, &p_texture);

	// Set the sampler.
	if( m_slotSampler >= 0)
		p_context->PSSetSamplers( m_slotSampler, 1, &p_sampler);

	return true;
}