/********************************************************************
*
*	CLASS		:: ParticleSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's particle effects.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 26
*
********************************************************************/

#ifndef ParticleSampleH
#define ParticleSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEEmitter.h>

#include "StarEmitter.h"

namespace DBE {
	class Camera;
}
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class ParticleSample : public DBE::App {
	public:
		/// Constructor.
		ParticleSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		DBE::Camera* mp_cam;

		DBE::Emitter<StarEmitter>* mp_stars;


		/// Private copy constructor to prevent multiple instances.
		ParticleSample( const ParticleSample&);
		/// Private assignment operator to prevent multiple instances.
		ParticleSample& operator=( const ParticleSample&);

};

APP_MAIN( ParticleSample, BLUE);

/*******************************************************************/
#endif	// #ifndef ParticleSampleH