/********************************************************************
*
*	CLASS		:: StarParticleShader
*	DESCRIPTION	:: The vertex and pixel shader for the 'StarEmitter'.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 26
*
********************************************************************/

#ifndef StarParticleShaderH
#define StarParticleShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEPixelShader.h>
#include <DBETypes.h>
#include <DBEVertexShader.h>

namespace DBE {
	class Matrix4;
}
struct ID3D11SamplerState;
struct ID3D11ShaderResourceView;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the sparkle particle vertex shader so it can be found in the shader manager.
static const char* gsc_starParticleVertexShaderName = "StarParticleVertexShader";
/// The unique name for the sparkle particle pixel shader so it can be found in the shader manager.
static const char* gsc_starParticlePixelShaderName = "StarParticlePixelShader";


/*******************************************************************/
class StarParticleVertexShader : public DBE::VertexShader {
	public:
		/// Constructor.
		StarParticleVertexShader();
		/// Destructor.
		~StarParticleVertexShader();

		bool Init();
		bool PassVarsToCBuffer( DBE::Matrix4* wvp, s32 imageHeight, s32 imageWidth);
		
		s32 m_slotCBufferGlobalVars;
		s32 m_slotWVP;
		s32 m_slotImageHeight;
		s32 m_slotImageWidth;
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		StarParticleVertexShader( const StarParticleVertexShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		StarParticleVertexShader& operator=( const StarParticleVertexShader& other);
		
};

/*******************************************************************/
class StarParticlePixelShader : public DBE::PixelShader {
	public:
		/// Constructor.
		StarParticlePixelShader();
		/// Destructor.
		~StarParticlePixelShader();
		
		bool Init();
		bool PassVarsToCBuffer( u32 colour);
		bool PassTextureAndSampler( ID3D11ShaderResourceView* p_texture, ID3D11SamplerState* p_sampler);

		s32 m_slotCBufferGlobalVars;
		s32 m_slotColour;
		
		s32 m_slotTexture;
		s32 m_slotSampler;
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		StarParticlePixelShader( const StarParticlePixelShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		StarParticlePixelShader& operator=( const StarParticlePixelShader& other);
		
};

/*******************************************************************/
#endif	// #ifndef StarParticleShaderH
