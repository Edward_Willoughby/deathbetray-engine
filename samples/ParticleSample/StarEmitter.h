/********************************************************************
*
*	CLASS		:: StarEmitter
*	DESCRIPTION	:: I still hate particle emitters...
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 26
*
********************************************************************/

#ifndef StarEmitterH
#define StarEmitterH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEParticle.h>
#include <DBEUITexture.h>

#include "StarParticleShader.h"
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
//class StarParticle : public DBE::Particle {
//	public:
//		u64 m_type;
//
//	private:
//
//
//};

/*******************************************************************/
class StarParticleMesh : public DBE::UITexture {
	public:
		/// Constructor.
		StarParticleMesh();
		/// Destructor.
		~StarParticleMesh();

		/// Handles some of the rendering that all subsequent particles will need.
		void PreRender( float deltaTime);
		/// Renders the particle (call 'Render').
		void OnRender( float deltaTime);

	private:
		StarParticleVertexShader* mp_starVS;
		StarParticlePixelShader* mp_starPS;

		/// Private copy constructor to prevent accidental multiple instances.
		StarParticleMesh( const StarParticleMesh&);
		/// Private assignment operator to prevent accidental multiple instances.
		StarParticleMesh& operator=( const StarParticleMesh&);

};

/*******************************************************************/
class StarEmitter {
	public:
		/// Initialise the particles.
		static bool ParticlesInit();
		/// Deletes the particles.
		static void ParticlesShutdown();

		/// Creates the particles.
		static void ParticlesCreateArray( DBE::Particle** particles, s32 maxParticles);

		/// Event handler for when a particle spawns.
		static void ParticleSpawn( DBE::Particle* particle);
		/// Update a particle.
		static void ParticleUpdate( float deltaTime, DBE::Particle* particle);
		/// Pre-rendering that all particles will require.
		static void ParticlePreRender( float deltaTime);
		/// Render a particle.
		static void ParticleRender( float deltaTime, DBE::Particle* particle, const DirectX::XMMATRIX& parentWorld);

		/// Test if a particle has expired.
		static bool ParticleIsDead( DBE::Particle* particle);
		/// Event handler for when a particle dies.
		static void ParticleDeath( DBE::Particle* particle);
		
	private:
		/// Create a random velocity value.
		static float RandomVelocity();

		static StarParticleMesh* mp_mesh;
		
};

/*******************************************************************/
#endif	// #ifndef EmitterEffectH
