/********************************************************************
*
*	CLASS		:: AudioSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's 'AudioMgr'
*					functionality.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 12 / 21
*
********************************************************************/

#ifndef AudioSampleH
#define AudioSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class AudioSample : public DBE::App {
	public:
		/// Constructor.
		AudioSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		float m_volume;

		float m_frequency;
		s32 m_reverb;
		s32 m_reverbPrev;

		/// Private copy constructor to prevent multiple instances.
		AudioSample( const AudioSample&);
		/// Private assignment operator to prevent multiple instances.
		AudioSample& operator=( const AudioSample&);

};

APP_MAIN( AudioSample, BLUE);

/*******************************************************************/
#endif	// #ifndef AudioSampleH