/********************************************************************
*	Function definitions for the AudioSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "AudioSample.h"

#include <DBEAudioMgr.h>
#include <DBEFont.h>
#include <DBEMath.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;

/// Path for the music file.
const char* gs_musicWarriors	= "res/Imagine Dragons - Warriors.mp3";
/// Path for the 'Ding' S.F.X. file.
const char* gs_sfxDing			= "res/Ding.mp3";
/// Path for the 'Whip' S.F.X. file.
const char* gs_sfxWhip			= "res/Whip.wav";


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool AudioSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - Audio";
	this->SetWindowTitle( m_windowTitle);

	// Only the music needs to be loaded; S.F.X. are loaded when played.
	MGR_AUDIO().LoadAudio( gs_musicWarriors);
	m_volume = 1.0f;

	m_frequency = MGR_AUDIO().GetFrequency( gs_musicWarriors);
	m_reverb = ReverbPreset::RP_None;

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool AudioSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	// Start/Stop the music (stopping resets it) if 'M' is pressed.
	if( MGR_INPUT().KeyPressed( 'M')) {
		if( MGR_AUDIO().IsPlaying( gs_musicWarriors))
			MGR_AUDIO().StopAudio( gs_musicWarriors);
		else
			MGR_AUDIO().PlayAudio( gs_musicWarriors);
	}

	// Increase/Decrease the music volume when '-' or '=' is pressed.
	if( MGR_INPUT().KeyPressed( VK_OEM_MINUS))
		m_volume -= 0.1f;
	else if( MGR_INPUT().KeyPressed( VK_OEM_PLUS))
		m_volume += 0.1f;

	// Pause/Unpause the music if 'P' is pressed.
	if( MGR_INPUT().KeyPressed( 'P'))
		MGR_AUDIO().PauseAudio( gs_musicWarriors);

	static s32 hrz = 1000;
	if( MGR_INPUT().KeyPressed( VK_RIGHT))
		m_frequency += hrz;
	else if( MGR_INPUT().KeyPressed( VK_LEFT))
		m_frequency -= hrz;

	if( MGR_INPUT().KeyPressed( VK_UP))
		++m_reverb;
	else if( MGR_INPUT().KeyPressed( VK_DOWN))
		--m_reverb;
	m_reverb = Clamp<s32>( m_reverb, ReverbPreset::RP_None, ReverbPreset::RP_Count - 1);

	// Play S.F.X.
	if( MGR_INPUT().KeyPressed( 'A'))
		MGR_AUDIO().PlayAudioOneShot( gs_sfxDing);
	if( MGR_INPUT().KeyPressed( 'S'))
		MGR_AUDIO().PlayAudioOneShot( gs_sfxWhip);


	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool AudioSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	// Clamp and set the music volume.
	m_volume = DBE::Clamp<float>( m_volume, VOLUME_MIN, VOLUME_MAX);
	MGR_AUDIO().SetMusicVolume( m_volume);

	MGR_AUDIO().SetFrequency( gs_musicWarriors, m_frequency);

	if( m_reverb != m_reverbPrev) {
		MGR_AUDIO().SetReverb( gs_musicWarriors, ReverbPreset( m_reverb));
		m_reverbPrev = m_reverb;
	}

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool AudioSample::HandleRender( float deltaTime) {
	m_matProj = DirectX::XMMatrixOrthographicLH( 1.0f, 1.0f, 1.0f, 250.0f);
	m_matView = DirectX::XMMatrixTranslation( 0.0f, 0.0f, 2.0f);

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.300f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Toggle Music ('M')");
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Music Volume ('-' / '='): %f", m_volume);
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.250f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Music Paused ('P'): %s", MGR_AUDIO().IsPlaying( gs_musicWarriors) ? "False" : "True");

	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.150f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "S.F.X. Ding ('A')");
	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.125f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "S.F.X. Whip ('S')");

	char name[256];
	MGR_AUDIO().GetReverbPresetName( ReverbPreset( m_reverb), name);
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.0f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Reverb ('^' & 'v'): %s", name);

	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, -0.025f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Frequency ('<' & '>'): %f", m_frequency);

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool AudioSample::HandleShutdown() {

	return true;
}