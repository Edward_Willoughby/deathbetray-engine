/********************************************************************
*	Function definitions for the ThreadingSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "ThreadingSample.h"

#include <DBEFont.h>
#include <DBEMath.h>
#include <DBERandom.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool ThreadingSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - Threading";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_Normal);

	m_thread = -1;

	mp_mesh = new BasicMesh( MeshType::MT_Cube, 1.0f, 1.0f, 1.0f, WHITE);
	mp_mesh->MoveTo( 0.0f, 0.0f, 0.0f);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool ThreadingSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	// If the thread doesn't exist, allow 'T' to create it.
	if( m_thread == -1) {
		if( MGR_INPUT().KeyPressed( 'T')) {
			m_thread = MGR_THREADPOOL().CreateThread( &DoSomething, nullptr);
			MGR_THREADPOOL().RunThread( m_thread);
		}
	}
	// If the thread does exist, reset 'm_thread' if it's finished.
	else {
		if( MGR_THREADPOOL().IsThreadFinished( m_thread))
			m_thread = -1;
	}

	// Create a thread and leave it alone.
	if( MGR_INPUT().KeyHeld( 'Y')) {
		s32 t = MGR_THREADPOOL().CreateThread( &DoSomething, (void*)true);
		MGR_THREADPOOL().RunThread( t);
	}
	
	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool ThreadingSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	mp_mesh->RotateBy( 0.0f, DBE_ToRadians( 1.0f), 0.0f);
	mp_mesh->Update( deltaTime);

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool ThreadingSample::HandleRender( float deltaTime) {
	m_camera.ApplyPerspectiveMatrixLH( 1.0f, 5000.0f);
	GET_APP()->m_matView = m_camera.ApplyViewMatrixLH();

	mp_mesh->Render( deltaTime);


	m_camera.ApplyOrthoMatrixLH( 1.0f, 250.0f);

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.300f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Begin Thread ('T')");
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Thread Running: %s", m_thread == -1 ? "False" : "True");

	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.150f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Create 'One of Many' Threads ('Y')");
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.125f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Number of Threads: %d", MGR_THREADPOOL().GetThreadCount());

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool ThreadingSample::HandleShutdown() {
	SafeDelete( mp_mesh);

	return true;
}

/**
* Function to be performed on a seperate thread.
*
* @param p_arguments :: The arguments sent to the thread.
*/
void ThreadingSample::DoSomething( void* p_arguments) {
	s32 var( Random::Int( 1, 40));
	s32 multiplier( 100);

	// If 'true' was passed through, then reduce the amount of time to pause by.
	if( p_arguments != nullptr) {
		bool arg( (bool)p_arguments);
		if( arg)
			multiplier /= 10;
	}

	// Hold the thread for a random amount of time.
	var *= multiplier;
	Sleep( var);

	// Output how much time it paused for.
	DebugTrace( "Thread paused for %dms.\n", var);
}