/********************************************************************
*
*	CLASS		:: ThreadingSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's threading functionality.
*					A cube is rendered and rotated to demonstrate that the threads don't
*					hang/disturb the main thread.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 12 / 22
*
********************************************************************/

#ifndef ThreadingSampleH
#define ThreadingSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEBasicMesh.h>
#include <DBECamera.h>
#include <DBEColour.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class ThreadingSample : public DBE::App {
	public:
		/// Constructor.
		ThreadingSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		/// Function to be performed on a seperate thread.
		static void DoSomething( void* p_arguments);

		const char* m_windowTitle;

		s32 m_thread;

		DBE::Camera m_camera;
		DBE::BasicMesh* mp_mesh;

		/// Private copy constructor to prevent multiple instances.
		ThreadingSample( const ThreadingSample&);
		/// Private assignment operator to prevent multiple instances.
		ThreadingSample& operator=( const ThreadingSample&);

};

APP_MAIN( ThreadingSample, BLUE);

/*******************************************************************/
#endif	// #ifndef ThreadingSampleH