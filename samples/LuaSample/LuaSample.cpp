/********************************************************************
*	Function definitions for the LuaSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "LuaSample.h"

#include <DBEBasicMesh.h>
#include <DBECamera.h>
#include <DBEFont.h>
#include <DBEMath.h>
#include <DBERandom.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool LuaSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - Lua";
	this->SetWindowTitle( m_windowTitle);

	mp_cam = new Camera();

	mp_cube = new BasicMesh( MeshType::MT_Cube);
	m_rotSpeed = DBE_ToRadians( 1.0f);

	m_output[0] = '\0';

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	// Set the name of the Lua script to be read. This sample only uses one script so it can be set
	// at the start and not changed.
	MGR_LUA().SetScriptToRead( "res/LuaScript.lua");

	// Add the function to Lua's list of valid functions.
	MGR_LUA().RegisterFunction( "SetRotSpeed", &LuaFunction_SetRotSpeed);
	MGR_LUA().RegisterFunction( "GetText", &LuaFunction_GetText);
	MGR_LUA().RegisterFunction( "SetText", &LuaFunction_SetText);
	
	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool LuaSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	// Reload the Lua script.
	if( MGR_INPUT().KeyPressed( 'R'))
		MGR_LUA().ReadScript();


	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool LuaSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	mp_cube->RotateBy( 0.0f, m_rotSpeed * (deltaTime*60), 0.0f);

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool LuaSample::HandleRender( float deltaTime) {
	// Perspective.
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 100.0f);
	mp_cam->ApplyViewMatrixLH();

	mp_cube->Render( deltaTime);

	// Orthographic.
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 10.0f);

	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.300f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Reload Script ('R')");
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Rotation Speed: %f", m_rotSpeed);

	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.200f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, m_output);

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool LuaSample::HandleShutdown() {
	SafeDelete( mp_cam);
	SafeDelete( mp_cube);

	return true;
}

/**
* Sets the rotation speed of the cube.
*
* @param s :: The new rotation speed of the cube, in degrees.
*/
void LuaSample::SetSpeed( float s) {
	m_rotSpeed = DBE_ToRadians( s);
}

/**
* Sets the text to be displayed.
*
* @param t :: The text to be used.
*/
void LuaSample::SetText( const char* t) {
	strcpy_s( m_output, 256, t);
}

/**
* Function exposed to Lua that sets the rotation speed of the cube.
*
* @return Number of values that have been pushed onto the stack (i.e. returned).
*/
s32 LuaSample::LuaFunction_SetRotSpeed() {
	s32 noOfParams( 0);
	float speed( 0.0f);

	// Get the number of parameters. If the number of parameters is fewer than 1 (which is what is
	// required, then the function will return false and we can exit early...
	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	// ... Otherwise, get the first parameter.
	MGR_LUA().GetParameter<float>( 1, LuaReturnType::LRT_Float, speed);

	LuaSample* p_app( dynamic_cast<LuaSample*>( GET_APP()));
	p_app->SetSpeed( speed);

	return 0;
}

/**
* Function exposed to Lua that gets a string containing a random integer.
*
* @return Number of values that have been pushed onto the stack (i.e. returned).
*/
s32 LuaSample::LuaFunction_GetText() {
	s32 valuesPushed( 0);
	char buffer[256];

	sprintf_s( buffer, 256, "Text: %i\n", Random::Int());

	if( MGR_LUA().ReturnValueString( buffer))
		++valuesPushed;

	return valuesPushed;
}

/**
* Function exposed to Lua that sets the value of 'm_output'.
*
* @return Number of values that have been pushed onto the stack (i.e. returned).
*/
s32 LuaSample::LuaFunction_SetText() {
	s32 noOfParams( 0);
	const char* text;

	// Get the number of parameters. If the number of parameters is fewer than 1 (which is what is
	// required, then the function will return false and we can exit early...
	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	// ... Otherwise, get the first parameter.
	MGR_LUA().GetParameterString<const char*>( 1, text);

	LuaSample* p_app( dynamic_cast<LuaSample*>( GET_APP()));
	p_app->SetText( text);

	return 0;
}