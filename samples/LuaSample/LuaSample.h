/********************************************************************
*
*	CLASS		:: LuaSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's 'LuaMgr'.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 09
*
********************************************************************/

#ifndef LuaSampleH
#define LuaSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>

namespace DBE {
	class BasicMesh;
	class Camera;
}
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class LuaSample : public DBE::App {
	public:
		/// Constructor.
		LuaSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

		/// Sets the rotation speed of the cube.
		void SetSpeed( float s);
		/// Sets the text to be displayed.
		void SetText( const char* t);

	private:
		/// Function exposed to Lua that sets the rotation speed of the cube.
		static s32 LuaFunction_SetRotSpeed();
		/// Function exposed to Lua that gets a string containing a random integer.
		static s32 LuaFunction_GetText();
		/// Function exposed to Lua that sets the value of 'm_output'.
		static s32 LuaFunction_SetText();

		const char* m_windowTitle;

		DBE::Camera* mp_cam;

		DBE::BasicMesh*	mp_cube;
		float			m_rotSpeed;

		char m_output[256];

		/// Private copy constructor to prevent multiple instances.
		LuaSample( const LuaSample&);
		/// Private assignment operator to prevent multiple instances.
		LuaSample& operator=( const LuaSample&);

};

APP_MAIN( LuaSample, BLUE);

/*******************************************************************/
#endif	// #ifndef LuaSampleH