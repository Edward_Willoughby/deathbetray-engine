-- Set the rotation speed of the cube.
SetRotSpeed( 10)

-- Get some text from the C++ program.
t = GetText()

-- Perform some modifications to the text.
-- This removes the 'new line' character and adds " bottles." to the end of the string.
t = string.sub( t, 0, string.len( t)-1)
t = t .. " bottles."

-- Then pass the text to the C++ program.
SetText( t)