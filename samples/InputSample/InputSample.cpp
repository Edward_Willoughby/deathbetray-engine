/********************************************************************
*	Function definitions for the InputSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "InputSample.h"

#include <DBEBasicMesh.h>
#include <DBECamera.h>
#include <DBEFont.h>
#include <DBEMath.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool InputSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - Input";
	this->SetWindowTitle( m_windowTitle);

	mp_cam = new Camera();
	mp_cam->m_position = Vec3( -1.0f, 10.0f, 0.0f);

	mp_cube = new BasicMesh( MeshType::MT_Cube);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool InputSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	const static float cs_movValue = 0.1f;
	if( MGR_INPUT().KeyHeld( 'W'))
		mp_cube->MoveBy( cs_movValue, 0.0f, 0.0f);
	if( MGR_INPUT().KeyHeld( 'S'))
		mp_cube->MoveBy( -cs_movValue, 0.0f, 0.0f);
	if( MGR_INPUT().KeyHeld( 'A'))
		mp_cube->MoveBy( 0.0f, 0.0f, cs_movValue);
	if( MGR_INPUT().KeyHeld( 'D'))
		mp_cube->MoveBy( 0.0f, 0.0f, -cs_movValue);

	if( MGR_INPUT().KeyPressed( 'R'))
		mp_cube->MoveTo( 0.0f, 0.0f, 0.0f);

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool InputSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	mp_cube->Update( deltaTime);

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool InputSample::HandleRender( float deltaTime) {
	// Perspective.
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 100.0f);
	mp_cam->ApplyViewMatrixLH();

	mp_cube->Render( deltaTime);

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool InputSample::HandleShutdown() {
	SafeDelete( mp_cam);

	SafeDelete( mp_cube);

	return true;
}