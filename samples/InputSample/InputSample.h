/********************************************************************
*
*	CLASS		:: InputSample
*	DESCRIPTION	:: Sample application to demonstate the DeathbetrayEngine's 'InputMgr'.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 01
*
********************************************************************/

#ifndef InputSampleH
#define InputSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>

namespace DBE {
	class BasicMesh;
	class Camera;
}
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class InputSample : public DBE::App {
	public:
		/// Constructor.
		InputSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		DBE::Camera* mp_cam;

		DBE::BasicMesh*	mp_cube;


		/// Private copy constructor to prevent multiple instances.
		InputSample( const InputSample&);
		/// Private assignment operator to prevent multiple instances.
		InputSample& operator=( const InputSample&);

};

APP_MAIN( InputSample, BLUE);

/*******************************************************************/
#endif	// #ifndef InputSampleH