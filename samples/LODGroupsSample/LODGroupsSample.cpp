/********************************************************************
*	Function definitions for the LODGroupsSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "LODGroupsSample.h"

#include <DBEFont.h>
#include <DBEMath.h>

#include "DemoRenderable.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool LODGroupsSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - L.O.D. Groups";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_Normal);

	mp_mesh = new DemoRenderable();
	mp_mesh->MoveTo( 0.0f, 0.0f, 0.0f);
	mp_mesh->ScaleTo( 10.0f, 10.0f, 10.0f);
	mp_mesh->RotateTo( DBE_ToRadians( 90.0f), 0.0f, DBE_ToRadians( 90.0f));

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool LODGroupsSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	// Set the L.O.D. level.
	if( MGR_INPUT().KeyPressed( '1'))
		mp_mesh->SetLOD( LevelOfDetail::LOD_Low);
	if( MGR_INPUT().KeyPressed( '2'))
		mp_mesh->SetLOD( LevelOfDetail::LOD_Medium);
	if( MGR_INPUT().KeyPressed( '3'))
		mp_mesh->SetLOD( LevelOfDetail::LOD_High);
	if( MGR_INPUT().KeyPressed( '4'))
		mp_mesh->SetLOD( LevelOfDetail::LOD_Extreme);


	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool LODGroupsSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	//mp_mesh->RotateBy( DBE_ToRadians( 1.0f), 0.0f, 0.0f);
	mp_mesh->Update( deltaTime);

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool LODGroupsSample::HandleRender( float deltaTime) {
	// Perspective stuff.
	m_camera.ApplyPerspectiveMatrixLH( 1.0f, 5000.0f);
	GET_APP()->m_matView = m_camera.ApplyViewMatrixLH();

	mp_mesh->Render( deltaTime);

	// Orthographic stuff.
	m_camera.ApplyOrthoMatrixLH( 1.0f, 250.0f);

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	DEFAULT_FONT()->DrawString2D(	Vec3( -0.48f, 0.300f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Use keys 1 to 4 to change the quality.");
	DEFAULT_FONT()->DrawStringf2D(	Vec3( -0.48f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "L.O.D. Level: %i", mp_mesh->GetLOD() + 1);

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool LODGroupsSample::HandleShutdown() {
	SafeDelete( mp_mesh);

	return true;
}