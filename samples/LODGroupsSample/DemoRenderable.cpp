/********************************************************************
*	Function definitions for the DemoRenderable class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DemoRenderable.h"

#include <DBEApp.h>
#include <DBEGraphicsHelpers.h>
#include <DBEMath.h>
#include <DBEPixelShader.h>
#include <DBEUITexture.h>
#include <DBEVertexShader.h>
#include <DBEVertexTypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Constructor.
*/
DemoRenderable::DemoRenderable() {
	float width( 0.5f);
	float height( 0.5f);
	u32 colour( WHITE);

	mp_verts = new VertPos3fColour4ubNormal3fTex2f[4];
	mp_verts[0] = VertPos3fColour4ubNormal3fTex2f( Vec3( -width, -height, 0.0f), colour, Vec3YAxis.GetVector(), Vec2( 0.0f, 1.0f));
	mp_verts[1] = VertPos3fColour4ubNormal3fTex2f( Vec3( -width,  height, 0.0f), colour, Vec3YAxis.GetVector(), Vec2( 0.0f, 0.0f));
	mp_verts[2] = VertPos3fColour4ubNormal3fTex2f( Vec3(  width,  height, 0.0f), colour, Vec3YAxis.GetVector(), Vec2( 1.0f, 0.0f));
	mp_verts[3] = VertPos3fColour4ubNormal3fTex2f( Vec3(  width, -height, 0.0f), colour, Vec3YAxis.GetVector(), Vec2( 1.0f, 1.0f));
	mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3fTex2f)*4, mp_verts);

	m_indexCount = 6;
	mp_indicies = new UINT[m_indexCount];
	mp_indicies[0] = 0;
	mp_indicies[1] = 1;
	mp_indicies[2] = 2;
	mp_indicies[3] = 0;
	mp_indicies[4] = 2;
	mp_indicies[5] = 3;
	mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT)*m_indexCount, mp_indicies);

	char shaderPath[256];
	DebuggerDependentPath( shaderPath, sizeof( shaderPath), "DBEngine_ROOT", "", "shaders\\BasicUITextureShader.hlsl");

	// Create the vertex shader.
	if( !MGR_SHADER().GetShader<VertexShader>( gsc_UITextureVertexShaderName, mp_VS)) {
		mp_VS = new VertexShader();

		mp_VS->LoadShader( shaderPath, nullptr, g_vertPos3fColour4ubTex2fDesc, g_vertPos3fColour4ubTex2fSize);

		mp_VS->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
		mp_VS->FindShaderVar( m_slotCBufferGlobalVars, "g_WVP",			SVT_FLOAT4x4, &m_slotWVP);
		mp_VS->FindShaderVar( m_slotCBufferGlobalVars, "g_imageHeight",	SVT_FLOAT, &m_slotTextureHeight);
		mp_VS->FindShaderVar( m_slotCBufferGlobalVars, "g_imageWidth",		SVT_FLOAT, &m_slotTextureWidth);

		mp_VS->CreateCBuffer( m_slotCBufferGlobalVars);

		MGR_SHADER().AddShader<VertexShader>( gsc_UITextureVertexShaderName, mp_VS);
	}
	
	// Create the pixel shader.
	if( !MGR_SHADER().GetShader<PixelShader>( gsc_UITexturePixelShaderName, mp_PS)) {
		mp_PS = new PixelShader();

		mp_PS->LoadShader( shaderPath, nullptr);

		mp_PS->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
		mp_PS->FindShaderVar( m_slotCBufferGlobalVars, "g_colour", SVT_FLOAT4, &m_slotColour);

		mp_PS->CreateCBuffer( m_slotCBufferGlobalVars);

		mp_PS->FindTexture( "g_texture", &m_slotTexture);
		mp_PS->FindSampler( "g_sampler", &m_slotSampler);

		MGR_SHADER().AddShader<PixelShader>( gsc_UITexturePixelShaderName, mp_PS);
	}

	// Create the L.O.D. group for the object.
	const char* paths[] = {
		"res/texture_low.dds",
		"res/texture_med.dds",
		"res/texture_high.dds",
		"res/texture_ext.dds",
	};
	mp_LODGroup = new LODGroup( paths, LevelOfDetail::LOD_Low);
}

/**
* Destructor.
*/
DemoRenderable::~DemoRenderable() {
	SafeDelete( mp_verts);
	SafeDelete( mp_indicies);

	SafeDelete( mp_LODGroup);

	MGR_SHADER().RemoveShader<VertexShader>( mp_VS);
	MGR_SHADER().RemoveShader<PixelShader>( mp_PS);
}

/**
* Renders the object.
*
* @param deltaTime :: The time taken to render the previous frame.
*/
void DemoRenderable::OnRender( float deltaTime) {
	// Don't try to render if the vertex or index buffers are invalid, the shader isn't loaded, or
	// if there are too few polygons.
	if( mp_vertBuffer == nullptr || mp_indexBuffer == nullptr || mp_VS == nullptr || mp_PS == nullptr || m_indexCount < 3)
		return;

	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);
	Texture* texture( mp_LODGroup->GetTexture());

	if( mp_VS->mp_CBuffers[m_slotCBufferGlobalVars]) {
		D3D11_MAPPED_SUBRESOURCE vsMap, psMap;
		if( !mp_VS->mp_CBuffers[m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_VS->mp_CBuffers[m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &vsMap)))
			vsMap.pData = nullptr;
		if( !mp_PS->mp_CBuffers[m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_PS->mp_CBuffers[m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &psMap)))
			psMap.pData = nullptr;

		Matrix4 wvp = GET_APP()->GetWVP();

		// Send the WVP matrix to the shader.
		mp_VS->SetCBufferMatrix( vsMap, m_slotWVP, wvp);

		// Send the height and width of the texture to the shader.
		mp_VS->SetCBufferVar( vsMap, m_slotTextureHeight, texture->m_textureHeight);
		mp_VS->SetCBufferVar( vsMap, m_slotTextureWidth, texture->m_textureWidth);

		// Send the colour of the texture.
		float c[4];
		ColourToFloats( c[0], c[1], c[2], c[3], this->GetColour());
		Vec4 colour( c[0], c[1], c[2], c[3]);
		mp_PS->SetCBufferVar( psMap, m_slotColour, colour);

		// Do... something to the shader maps.
		if( vsMap.pData)
			p_context->Unmap( mp_VS->mp_CBuffers[m_slotCBufferGlobalVars], 0);
		if( psMap.pData)
			p_context->Unmap( mp_PS->mp_CBuffers[m_slotCBufferGlobalVars], 0);

		// Set the constant buffers for the vertex shader.
		ID3D11Buffer* p_vsConstBuffers[1] = { mp_VS->mp_CBuffers[m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( m_slotCBufferGlobalVars, 1, p_vsConstBuffers);

		// Set the constant buffers for the pixel shader.
		ID3D11Buffer* p_psConstBuffers[1] = { mp_PS->mp_CBuffers[m_slotCBufferGlobalVars] };
		p_context->PSSetConstantBuffers( m_slotCBufferGlobalVars, 1, p_psConstBuffers);
	}

	// Set the texture.
	if( m_slotTexture >= 0)
		p_context->PSSetShaderResources( m_slotTexture, 1, &texture->mp_textureView);

	// Set the sampler.
	if( m_slotSampler >= 0)
		p_context->PSSetSamplers( m_slotSampler, 1, &texture->mp_samplerState);

	// Set up the vertex and pixel shaders.
	p_context->VSSetShader( mp_VS->mp_VS, nullptr, 0);
	p_context->IASetInputLayout( mp_VS->mp_IL);

	p_context->PSSetShader( mp_PS->mp_PS, nullptr, 0);

	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { mp_vertBuffer };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubNormal3fTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( mp_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	p_context->DrawIndexed( m_indexCount, 0, 0);
}

LevelOfDetail DemoRenderable::GetLOD() const {
	return mp_LODGroup->GetLevelOfDetail();
}

void DemoRenderable::SetLOD( LevelOfDetail lod) {
	mp_LODGroup->SetLevelOfDetail( lod);
}