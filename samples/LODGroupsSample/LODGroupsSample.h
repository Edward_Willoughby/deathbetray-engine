/********************************************************************
*
*	CLASS		:: LODGroupsSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's L.O.D. groups
					functionality.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 20
*
********************************************************************/

#ifndef LODGroupsSampleH
#define LODGroupsSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBECamera.h>
#include <DBEColour.h>

class DemoRenderable;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class LODGroupsSample : public DBE::App {
	public:
		/// Constructor.
		LODGroupsSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		DBE::Camera m_camera;

		DemoRenderable* mp_mesh;

		/// Private copy constructor to prevent multiple instances.
		LODGroupsSample( const LODGroupsSample&);
		/// Private assignment operator to prevent multiple instances.
		LODGroupsSample& operator=( const LODGroupsSample&);

};

APP_MAIN( LODGroupsSample, BLUE);

/*******************************************************************/
#endif	// #ifndef LODGroupsSampleH