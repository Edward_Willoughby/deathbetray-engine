/********************************************************************
*
*	CLASS		:: DemoRenderable
*	DESCRIPTION	:: A renderable object that uses a L.O.D. group.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 21
*
********************************************************************/

#ifndef DemoRenderableH
#define DemoRenderableH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBELODGroup.h>
#include <DBERenderable.h>

namespace DBE {
	class LODGroup;
	class PixelShader;
	class VertexShader;
}
struct VertPos3fColour4ubNormal3fTex2f;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class DemoRenderable : public DBE::Renderable {
	public:
		/// Constructor.
		DemoRenderable();
		/// Destructor.
		~DemoRenderable();

		/// Renders the object.
		void OnRender( float deltaTime);
		
		LevelOfDetail GetLOD() const;
		void SetLOD( LevelOfDetail lod);
		
	private:
		DBE::LODGroup* mp_LODGroup;

		VertPos3fColour4ubNormal3fTex2f*	mp_verts;
		UINT*								mp_indicies;

		DBE::VertexShader*	mp_VS;
		DBE::PixelShader*	mp_PS;

		s32 m_slotCBufferGlobalVars;
		s32 m_slotWVP;
		s32 m_slotTextureHeight;
		s32 m_slotTextureWidth;
		s32 m_slotColour;
		s32 m_slotTexture;
		s32 m_slotSampler;
		
		/// Private copy constructor to prevent accidental multiple instances.
		DemoRenderable( const DemoRenderable& other);
		/// Private assignment operator to prevent accidental multiple instances.
		DemoRenderable& operator=( const DemoRenderable& other);
		
};

/*******************************************************************/
#endif	// #ifndef DemoRenderableH
