/********************************************************************
*
*	CLASS		:: FontSample
*	DESCRIPTION	:: Sample application to demonstrate the DeathbetrayEngine's 'FontMgr'.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 26
*
********************************************************************/

#ifndef FontSampleH
#define FontSampleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>

namespace DBE {
	class BasicMesh;
	class Camera;
	class Font;
}
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class FontSample : public DBE::App {
	public:
		/// Constructor.
		FontSample() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		DBE::Camera* mp_cam;

		DBE::BasicMesh* mp_mesh;
		float			m_meshRot;

		DBE::Font* mp_fontDarkCrystal;

		/// Private copy constructor to prevent multiple instances.
		FontSample( const FontSample&);
		/// Private assignment operator to prevent multiple instances.
		FontSample& operator=( const FontSample&);

};

APP_MAIN( FontSample, BLUE);

/*******************************************************************/
#endif	// #ifndef FontSampleH