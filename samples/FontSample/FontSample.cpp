/********************************************************************
*	Function definitions for the FontSample class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "FontSample.h"

#include <DBEBasicMesh.h>
#include <DBECamera.h>
#include <DBEFont.h>
#include <DBEMath.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool FontSample::HandleInit() {
	m_windowTitle = "DeathbetrayEngine Sample - Font";
	this->SetWindowTitle( m_windowTitle);

	mp_cam = new Camera();

	mp_mesh = new BasicMesh( MeshType::MT_Cube);
	m_meshRot = 0.0f;

	// Create a new font. 'InstallFont' is required to use a font sheet that isn't in the Windows
	// folder; the engine will automatically remove the font during shutdown.
	Font::InstallFont( "res/dc_o.ttf");
	mp_fontDarkCrystal = Font::CreateByName( "FontDarkCrystal", "Dark Crystal Outline", 15, 0);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, false);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool FontSample::HandleInput() {
	// Quit the program.
	if( MGR_INPUT().KeyPressed( 'Q'))
		this->Terminate();

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool FontSample::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);
	
	m_meshRot += deltaTime * 50;

	mp_mesh->RotateTo( 0.0f, DBE_ToRadians( m_meshRot), 0.0f);
	mp_mesh->Update( deltaTime);

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool FontSample::HandleRender( float deltaTime) {
	// Perspective.
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 1000.0f);
	mp_cam->ApplyViewMatrixLH();

	mp_mesh->Render( deltaTime);

	// Output text in 3D space.
	// NOTE: The text's origin is the bottom left and thus is NOT justified. The scale also
	// currently skews the positioning.
	DEFAULT_FONT()->DrawString3D( Vec3( -16.0f, 3.0f, 0.0f), Vec3( 0.0f, DBE_ToRadians( m_meshRot), 0.0f), 0.1f, nullptr, "Cube");


	// Orthographic.
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 10.0f);

	// Use the font created in this sample.
	mp_fontDarkCrystal->DrawStringf2D( Vec3( -0.45f, 0.3f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "This is Dark Crystal Outline.");

	// Use the default font created automatically by the engine.
	DEFAULT_FONT()->DrawString2D( Vec3( -0.45f, 0.2f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "This is the engine's default font.");

	// Output 'deltaTime'.
	DEFAULT_FONT()->DrawStringf2D( Vec3( -0.45f, 0.1f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Delta Time: %f", deltaTime);

	// Output two lines in one call.
	DEFAULT_FONT()->DrawString2D( Vec3( -0.45f, 0.0f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "This is on the first line\nand this is on the second.");

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool FontSample::HandleShutdown() {
	SafeDelete( mp_cam);
	SafeDelete( mp_mesh);
	SafeDelete( mp_fontDarkCrystal);

	return true;
}